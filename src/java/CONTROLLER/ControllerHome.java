/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLER;

import BEANS.SearchDTO;
import EJB.SBNewsletter;
import EJB.SBannonce;
import EJB.SBcompte;
import EJB.SBgallerie;
import EJB.SBlaboratoire;
import EJB.SBmembre;
import EJB.SBpresentation;
import EJB.SBpublication;
import EJB.SBtheme;
import EJB.SBtypepublication;
import ENTITY.Image;
import ENTITY.Membre;
import ENTITY.Newsletter;
import ENTITY.Publication;
import ENTITY.Thematique;
import ENTITY.TypePublication;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author HP
 */
public class ControllerHome extends HttpServlet {

    @EJB
    private SBtheme sBtheme;

    @EJB
    private SBcompte sBcompte;

    @EJB
    private SBmembre sBmembre;
    @EJB
    private SBgallerie sBgallerie;
    @EJB
    private SBlaboratoire sBlaboratoire;
    @EJB
    private SBtypepublication sBtypepublication;

    @EJB
    private SBpresentation sBpresentation;

    @EJB
    private SBpublication sBpublication;
    @EJB
    private SBannonce sBannonce;

    @EJB
    private SBNewsletter sBNewsletter;
    private static String URL = "";
    private final String Adresse_serveur = "http://192.168.0.16/";      // http://127.0.0.1/www/  "http://192.168.0.16/";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userPath = request.getServletPath();
        String active = "active";
        request.setAttribute("DIR_AVATAR", Adresse_serveur + "iuc/prie/admin/images/avatar");
        request.setAttribute("DIR_ANNONCE", Adresse_serveur + "iuc/prie/admin/document/annonce");
        request.setAttribute("DIR_ANNONCE_IMG", Adresse_serveur + "iuc/prie/admin/images/annonce");
        request.setAttribute("DIR_ARCHIVE", Adresse_serveur + "iuc/prie/admin/document/archive");
        request.setAttribute("DIR_NEWSLETTER", Adresse_serveur + "iuc/prie/admin/document/newsletter");
        request.setAttribute("DIR_PRESENTATION", Adresse_serveur + "iuc/prie/admin/images/presentation");
        request.setAttribute("DIR_PUBLICATION", Adresse_serveur + "iuc/prie/admin/document/publications");
        request.setAttribute("DIR_SLIDER", Adresse_serveur + "iuc/prie/admin/images/slider");
        request.setAttribute("DIR_LABO", Adresse_serveur + "iuc/prie/admin/images/laboratoire");
        if (userPath.equals("/accueil")) {
            request.setAttribute("page", "Accueil");
            //request.setAttribute("labo", sBlaboratoire.getAllLaboratoire2() );
            request.setAttribute("activeHome", active);

            List<Image> GallerieSlider = sBgallerie.getAllImageSliderPublie();
            request.setAttribute("allslider", GallerieSlider);

            List<Publication> liste = sBannonce.getAll5Annonces();
            request.setAttribute("allannonce", liste);

            List<Newsletter> allnews = sBNewsletter.getAllNewsletter();
            request.setAttribute("allnewsletter", allnews);
            request.setAttribute("first", sBNewsletter.getFirstNews());

            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            request.setAttribute("facebook", sBpresentation.getPresentation(2));

        } else if (userPath.equals("/error404")) {

        } else if (userPath.equals("/about")) {
            request.setAttribute("page", "A Propos");
            request.setAttribute("activeAbout", active);
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            request.setAttribute("about", sBpresentation.getPresentation(1));
            request.setAttribute("responsable", sBmembre.getAllResponsable());
            request.setAttribute("google", sBpresentation.getPresentation(3));

        } else if (userPath.equals("/annonce")) {
            request.setAttribute("page", "Annonces");
            request.setAttribute("activeAnnonce", active);
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            List<Publication> liste = sBannonce.getAllAnnonces();
            request.setAttribute("allannonce", liste);

        } else if (userPath.equals("/iframe-annonce")) {
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            try {
                int annonce = Integer.parseInt(request.getQueryString());
                request.setAttribute("annonce", sBpublication.getPublication(annonce));
            } catch (Exception e) {
                response.sendRedirect("error404");
            }

        } else if (userPath.equals("/details")) {
            request.setAttribute("page", "Details");
            request.setAttribute("activeAnnonce", active);
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            try {
                int annonce = Integer.parseInt(request.getQueryString());
                request.setAttribute("annonce", sBpublication.getPublication(annonce));
            } catch (Exception e) {
                response.sendRedirect("error404");
            }

        } else if (userPath.equals("/laboratoire")) {
            request.setAttribute("page", "Laboratoire");
            request.setAttribute("activeLabo", active);
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            try {
                int idLabo = Integer.parseInt(request.getQueryString());
                if (sBmembre.getResponsableLABO(idLabo) == null) {
                    request.setAttribute("labo", sBlaboratoire.getLaboratoire(idLabo));
                } else {
                    request.setAttribute("labo", sBlaboratoire.getLaboratoire(idLabo));
                    request.setAttribute("responsable", sBmembre.getResponsableLABO(idLabo));
                }
            } catch (Exception e) {
                response.sendRedirect("error404");
            }
        } else if (userPath.equals("/themes")) {
            request.setAttribute("page", "Laboratoire");
            request.setAttribute("activeLabo", active);
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            try {
                int idLabo = Integer.parseInt(request.getQueryString());
                List<Thematique> theme = sBtheme.getAllThematiqueLabo(idLabo);
                request.setAttribute("alltheme", theme);
                request.setAttribute("labo", sBlaboratoire.getLaboratoire(idLabo));
            } catch (Exception e) {
                response.sendRedirect("error404");
            }

        } else if (userPath.equals("/membres")) {
            request.setAttribute("page", "Membres");
            request.setAttribute("activeLabo", active);
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            String val = request.getQueryString();
            try {
                if (val != null) {

                    int idlabo = Integer.parseInt(val);
                    List<Membre> allmembrelabo = sBmembre.getAllMembreLabo(idlabo);
                    if (sBlaboratoire.getLaboratoire(idlabo) != null) {
                        request.setAttribute("labo", sBlaboratoire.getLaboratoire(idlabo));
                        if (allmembrelabo != null) {
                            request.setAttribute("membres", allmembrelabo);
                        }
                    }
                }

            } catch (Exception e) {
                response.sendRedirect("error404");
            }

        } else if (userPath.equals("/perso")) {
            request.setAttribute("page", "Membre");
            request.setAttribute("activeLabo", active);
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            String val = request.getParameter("t");
            String id = request.getParameter("i");
            try {
                if (val != null) {
                    request.setAttribute("activation", "active");
                    request.setAttribute("URL", URL);
                    int idtype = Integer.parseInt(val);
                    int idM = sBcompte.getCompteM(Integer.parseInt(id)).getIdCompte();
                    request.setAttribute("membre", sBmembre.getMembre(Integer.parseInt(id)));
                    if (sBtypepublication.getTypePublication(idtype) != null) {
                        request.setAttribute("module", sBtypepublication.getTypePublication(idtype));
                    }
                    if (sBpublication.getAllPublicationCompteByType(idtype, idM) != null) {
                        List<Publication> allpub = sBpublication.getAllPublicationCompteByType(idtype, idM);
                        request.setAttribute("allpubtype", allpub);
                    }
                } else {
                    String urlperso = request.getRequestURL().toString();
                    URL = urlperso + "?" + request.getQueryString();
                    request.setAttribute("URL", URL);
                    request.setAttribute("activation2", "active");
                    request.setAttribute("color", "white");
                    int membre = sBmembre.getMembre(request.getQueryString()).getIdMembre();
                    int compte = sBcompte.getCompteM(membre).getIdCompte();
                    request.setAttribute("membre", sBmembre.getMembre(request.getQueryString()));

                    if (sBtypepublication.getTypePublication(2) != null) {
                        request.setAttribute("module", sBtypepublication.getTypePublication(2));
                    }

                    if (sBpublication.getAllPublicationCompteByType(2, compte) != null) {
                        List<Publication> allpub = sBpublication.getAllPublicationCompteByType(2, compte);
                        request.setAttribute("allpubtype", allpub);
                    }
                }
                List<TypePublication> liste = sBtypepublication.getAllTypeAnyAnnonce();
                request.setAttribute("allmodule", liste);

            } catch (Exception e) {
                response.sendRedirect("error404");
            }

        } else if (userPath.equals("/publication")) {
            request.setAttribute("page", "Publication");
            request.setAttribute("activePub", active);
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            String val = request.getParameter("t");

            try {
                if (val != null) {
                    request.setAttribute("activation", "active");
                    int idtype = Integer.parseInt(val);
                    if (sBtypepublication.getTypePublication(idtype) != null) {
                        request.setAttribute("module", sBtypepublication.getTypePublication(idtype));
                    }
                    if (sBpublication.getAllPublicationByType(idtype) != null) {
                        List<Publication> allpub = sBpublication.getAllPublicationByType(idtype);
                        request.setAttribute("allpub", allpub);
                    }
                } else {
                    request.setAttribute("activation2", "active");
                    request.setAttribute("color", "white");
                    if (sBtypepublication.getTypePublication(2) != null) {
                        request.setAttribute("module", sBtypepublication.getTypePublication(2));
                    }
                    if (sBpublication.getAllPublicationByType(2) != null) {
                        List<Publication> allpub = sBpublication.getAllPublicationByType(2);
                        request.setAttribute("allpub", allpub);
                    }
                }
                List<TypePublication> liste = sBtypepublication.getAllTypeAnyAnnonce();
                request.setAttribute("allmodule", liste);

            } catch (Exception e) {
                response.sendRedirect("error404");
            }

        } else if (userPath.equals("/iframe-pub")) {
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            try {
                int annonce = Integer.parseInt(request.getQueryString());
                request.setAttribute("annonce", sBpublication.getPublication(annonce));
            } catch (Exception e) {
                response.sendRedirect("error404");
            }
        } else if (userPath.equals("/iframe-news")) {
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            try {
                int news = Integer.parseInt(request.getQueryString());
                request.setAttribute("news", sBNewsletter.getNewsletter(news));
            } catch (Exception e) {
                response.sendRedirect("error404");
            }
        } else if (userPath.equals("/recherche")) {
            request.setAttribute("page", "Recherche");
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
        }

        String url = "/WEB-INF/frontend" + userPath + ".jsp";
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userPath = request.getServletPath();
        if (userPath.equals("/recherche")) {
             request.setAttribute("page", "Recherche");
            request.setAttribute("menu", sBlaboratoire.getAllLaboratoire2());
            try {
                String element = request.getParameter("search");
                if (element != null) {
                    SearchDTO s = sBmembre.resultatSearch(element);
                    if (s.getAllmembre().size() > 0) {
                        request.setAttribute("TestMemb", 0);
                        request.setAttribute("SearchMembre", s.getAllmembre());
                    }
                    if (s.getAllthematique().size() > 0) {
                        request.setAttribute("TestThem", 0);
                        request.setAttribute("SearchThematique", s.getAllthematique());
                    }
                    if (s.getAlllaboratoire().size() > 0) {
                        request.setAttribute("TestLabo", 0);
                        request.setAttribute("SearchLabo", s.getAlllaboratoire());
                    }
                    if (s.getAllpublication().size() > 0) {
                        request.setAttribute("TestPub", 0);
                        request.setAttribute("SearchPub", s.getAllpublication());
                    }
                    request.setAttribute("element", element);
                }
            } catch (Exception e) {
                response.sendRedirect("recherche");
            }
        }
        String url = "/WEB-INF/frontend" + userPath + ".jsp";
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
