/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLER;

import BEANS.Auteur;
import BEANS.CompteDTO;
import EJB.SBGroupeCompte;
import EJB.SBNewsletter;
import EJB.SBannonce;
import EJB.SBbouton;
import EJB.SBcategorie;
import EJB.SBcommentaire;
import EJB.SBcompte;
import EJB.SBdocument;
import EJB.SBgallerie;
import EJB.SBgroupe;
import EJB.SBlaboratoire;
import EJB.SBmembre;
import EJB.SBmenu;
import EJB.SBmenuprofil;
import EJB.SBpresentation;
import EJB.SBprofil;
import EJB.SBpublication;
import EJB.SBrepertoire;
import EJB.SBtheme;
import EJB.SBtypepublication;
import ENTITY.Bouton;
import ENTITY.Categorie;
import ENTITY.Commentaire;
import ENTITY.Compte;
import ENTITY.CompteGroupe;
import ENTITY.Document;
import ENTITY.Groupe;
import ENTITY.Image;
import ENTITY.Laboratoire;
import ENTITY.Membre;
import ENTITY.Menu;
import ENTITY.MenuProfil;
import ENTITY.Newsletter;
import ENTITY.Presentation;
import ENTITY.Profil;
import ENTITY.Publication;
import ENTITY.Repertoire;
import ENTITY.Thematique;
import ENTITY.TypePublication;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.imageio.ImageIO;
import javax.print.attribute.ResolutionSyntax;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 *
 * @author HP
 */
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 10,
        maxFileSize = 1024 * 1024 * 50,
        maxRequestSize = 1024 * 1024 * 100
)
public class ControllerAdmin extends HttpServlet {

    @EJB
    private SBtheme sBtheme;

    @EJB
    private SBcommentaire sBcommentaire;

    @EJB
    private SBGroupeCompte sBGroupeCompte;

    @EJB
    private SBgroupe sBgroupe;

    @EJB
    private SBdocument sBdocument;

    @EJB
    private SBrepertoire sBrepertoire;

    @EJB
    private SBcategorie sBcategorie;

    @EJB
    private SBtypepublication sBtypepublication;

    @EJB
    private SBpresentation sBpresentation;

    @EJB
    private SBpublication sBpublication;
    @EJB
    private SBannonce sBannonce;

    @EJB
    private SBNewsletter sBNewsletter;

    @EJB
    private SBgallerie sBgallerie;

    @EJB
    private SBmenuprofil sBmenuprofil;

    @EJB
    private SBbouton sBbouton;

    @EJB
    private SBmenu sBmenu;

    @EJB
    private SBprofil sBprofil;

    @EJB
    private SBmembre sBmembre;

    @EJB
    private SBlaboratoire sBlaboratoire;
    @EJB
    private SBcompte sBcompte;

    private final String DIR_AVATAR = "/var/www/html/iuc/prie/admin/images/avatar";
    /*"/var/www/html/iuc/prie/admin/images/avatar";*/
    private final String DIR_GROUPE = "/var/www/html/iuc/prie/admin/images/groupe";
    private final String DIR_LABO = "/var/www/html/iuc/prie/admin/images/laboratoire";
    private final String DIR_SLIDER = "/var/www/html/iuc/prie/admin/images/slider";
    private final String DIR_ANNONCE_IMG = "/var/www/html/iuc/prie/admin/images/annonce";
    private final String DIR_PRESENTATION = "/var/www/html/iuc/prie/admin/images/presentation";
    private final String DIR_ARCHIVE = "/var/www/html/iuc/prie/admin/document/archive";
    //  private final String DIR_PRESENTATION = "C:\\xampp\\htdocs\\www\\prie\\admin\\images\\presentation";
//    private final String DIR_DOC = Adresse_serveur +  "iuc/prie/admin/document";
//    private final String DIR_COMMENTAIRE = Adresse_serveur +  "iuc/prie/admin/document/commentaire";
     private final String DIR_PUBLICATION = "/var/www/html/iuc/prie/admin/document/publications";
    //private final String DIR_PUBLICATION = "C:\\xampp\\htdocs\\www\\iuc\\prie\\admin\\document\\publications";
    /*"/var/www/html/iuc/prie/admin/document/publications";*/
   // private final String DIR_NEWSLETTER = "C:\\xampp\\htdocs\\www\\iuc\\prie\\admin\\document\\newsletter";
    private final String DIR_NEWSLETTER = "/var/www/html/iuc/prie/admin/document/newsletter";
    private final String DIR_ANNONCE = "/var/www/html/iuc/prie/admin/document/annonce";
    private String DOMAIN;
    private final String Adresse_serveur = "http://192.168.0.16/";   //http://127.0.0.1/www/"; //"http://192.168.0.16/";

    List<String> listOfExtensions = new ArrayList<>();
    HttpSession session;
    Laboratoire labo;
    Membre membre;
    Compte compte;
    Profil profil;
    Image gallerie;
    Newsletter News;
    Publication annonce;
    Publication pub;
    Presentation Apropos;
    TypePublication module;
    Categorie categorie;
    Repertoire dossier;
    Document document;
    Groupe groupe;
    Commentaire comment;
    Thematique theme;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userPath = request.getServletPath();
        HttpSession session = request.getSession();
        Compte user = (Compte) session.getAttribute("useradmin");
//        Collection<Repertoire> CategorieDossier;
//        Collection<Document> DossierDocument;

        DOMAIN = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/PRIE/";

        if (session.getAttribute("useradmin") == null) {
            if (userPath.equals("/admin/index")) {

            }
            if (userPath.equals("/admin/dashboard")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/ajouter-membre")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/ajouter-annonce")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/ajouter-laboratoire")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/ajouter-categorie")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/creer-groupe")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/editer-profile")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/edit-profil")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/gerer-compte")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/gerer-publication")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/gerer-role")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/gerer-slider")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/corbeille-compte")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/groupe-commentaire")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/liste-annonce")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/liste-archive")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/liste-laboratoire")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/liste-membre")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/publier-newsletter")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/extras")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/profil-btn-menu")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/gerer-compte-membre")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/gerer-thematique")) {
                response.sendRedirect("index");
            }
        } else if (session.getAttribute("useradmin") != null) {
            if (userPath.equals("/admin/index")) {
                response.sendRedirect("dashboard");
            }
            if (userPath.equals("/admin/deconnexion")) {

                // Compte user = (Compte) session.getAttribute("useradmin");
                sBcompte.Updatedeconnecte(user.getIdCompte());
                session.removeAttribute("useradmin");
                response.sendRedirect("index");
            }
            if (userPath.equals("/admin/dashboard")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/ajouter-membre")) {

                List<Laboratoire> lab = sBlaboratoire.getAllLaboratoire();
                request.setAttribute("listelabo", lab);

                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/ajouter-annonce")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/ajouter-laboratoire")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/ajouter-categorie")) {
                List<Categorie> liste = sBcategorie.getAllCategorie();
                request.setAttribute("listecategorie", liste);

                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/creer-groupe")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                List<Groupe> allgroupe = sBgroupe.getAllGroupeByUser(user.getLogin());
                request.setAttribute("allgroupe", allgroupe);
                List<Compte> allm = sBcompte.getAllCompte2(user.getIdCompte());
                request.setAttribute("allcompte", allm);

            } else if (userPath.equals("/admin/editer-profile")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));
                try {
                    int val = Integer.parseInt(request.getQueryString());
                    request.setAttribute("membre", sBmembre.getMembre(val));

                } catch (Exception e) {
                    response.sendRedirect("error-404");
                }
            } else if (userPath.equals("/admin/edit-profil")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));
                List<Laboratoire> lab = sBlaboratoire.getAllLaboratoire();
                request.setAttribute("listelabo", lab);
                try {
                    int val = Integer.parseInt(request.getQueryString());
                    request.setAttribute("membre", sBmembre.getMembre(val));

                } catch (Exception e) {
                    response.sendRedirect("error-404");
                }

            } else if (userPath.equals("/admin/gerer-compte")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));
                try {
                    String val = request.getQueryString();
                    if (val != null) {

                        int IDCompte = Integer.parseInt(val);
                        sBcompte.UpdateStatutCompte(IDCompte);
                        request.setAttribute("desactivecompte", "");
                        request.setAttribute("val", 3);

                    }
                } catch (Exception e) {
                    response.sendRedirect("error404");
                }

                List<Profil> listeprofil = sBprofil.getAllProfil();
                request.setAttribute("allprofil", listeprofil);
                List<CompteDTO> allcompte = sBcompte.getAllCompteDTO();
                request.setAttribute("allcompte", allcompte);

            } else if (userPath.equals("/admin/gerer-compte-membre")) {
                // Compte user = (Compte) session.getAttribute("useradmin");
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                List<Profil> listeprofil = sBprofil.getAllProfil();
                request.setAttribute("allprofil", listeprofil);
                List<Membre> allmembre = sBmembre.getAllMembre2();
                request.setAttribute("allmembre", allmembre);

            } else if (userPath.equals("/admin/gerer-publication")) {
                //Compte user = (Compte) session.getAttribute("useradmin");
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String var = request.getParameter("t");
                String pub = request.getParameter("x");
                String action = request.getParameter("y");

                try {
                    if (pub != null) {
                        int ID = Integer.parseInt(pub);
                        if (action.equals("stat")) {
                            sBpublication.UpdateStatutPUB(ID);
                            //response.sendRedirect("gerer-publication?t="+Integer.parseInt(var));
                            request.setAttribute("val", 5);
                        } else if (action.equals("tel")) {
                            sBpublication.UpdateStatutDownload(ID);
                            // response.sendRedirect("gerer-publication?t="+Integer.parseInt(var));
                            request.setAttribute("val", 5);
                        }
                    }
                    if (var != null) {
                        request.setAttribute("active", "active-menu");
                        int idtype = Integer.parseInt(var);
                        if (sBtypepublication.getTypePublication(idtype) != null) {
                            request.setAttribute("module", sBtypepublication.getTypePublication(idtype));
                        }

                        if (sBpublication.getAllPublicationCompteByType(idtype, user.getIdCompte()) != null) {
                            List<Publication> allpub = sBpublication.getAllPublicationCompteByType(idtype, user.getIdCompte());
                            request.setAttribute("allpubtype", allpub);
                        }
                    } else {
                        request.setAttribute("active2", "active-menu");
                        if (sBtypepublication.getTypePublication(2) != null) {
                            request.setAttribute("module", sBtypepublication.getTypePublication(2));
                        }

                        if (sBpublication.getAllPublicationCompteByType(2, user.getIdCompte()) != null) {
                            List<Publication> allpub = sBpublication.getAllPublicationCompteByType(2, user.getIdCompte());
                            request.setAttribute("allpubtype", allpub);
                        }

                    }
                } catch (Exception e) {
                    response.sendRedirect("error404");
                }

                List<TypePublication> liste = sBtypepublication.getAllTypeAnyAnnonce();
                request.setAttribute("allmodule", liste);

            } else if (userPath.equals("/admin/gerer-role")) {
                //  Compte user = (Compte) session.getAttribute("useradmin");
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));
                List<Profil> listeprofil = sBprofil.getAllProfil();
                request.setAttribute("allprofil", listeprofil);

                List<Menu> listem = sBmenu.getAllMenu();
                request.setAttribute("allmenu", listem);

            } else if (userPath.equals("/admin/profil-btn-menu")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String role = request.getParameter("r");
                String profile = request.getParameter("x");
                String menu = request.getParameter("m");
                String action = request.getParameter("t");
                String val = request.getParameter("val");
                int idprofile = 0;
                if (role != null) {
                    try {
                        idprofile = Integer.parseInt(role);
                        if (val != null) {
                            request.setAttribute("val", 2);
                        }
                    } catch (Exception e) {
                        response.sendRedirect("error404");
                    }

                } else if (profile != null) {
                    try {
                        idprofile = Integer.parseInt(profile);
                        if (action.equals("delm")) {
                            int idmenu = Integer.parseInt(menu);
                            sBmenuprofil.DeleteMenuProfil(idprofile, idmenu);
                            response.sendRedirect("profil-btn-menu?r=" + idprofile);

                        }
                        request.setAttribute("val", 1);
                    } catch (Exception e) {
                        response.sendRedirect("error404");
                    }

                }

                Map MenuMap = sBmenuprofil.getallmenuProfil(idprofile);
                request.setAttribute("profil", MenuMap.get("profil"));
                request.setAttribute("MenusPro", MenuMap.get("allmenuprofil"));
                request.setAttribute("menus", MenuMap.get("menus"));

                Map menuRestantMap = sBmenuprofil.getallmenurestant(idprofile);
                request.setAttribute("allmenu", menuRestantMap.get("menurestant"));

            } else if (userPath.equals("/admin/gerer-slider")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                try {
                    String id = request.getQueryString();
                    if (id != null) {
                        int ids = Integer.parseInt(id);
                        sBgallerie.UpdateStatut(ids);
                        request.setAttribute("val", 6);

                    }
                } catch (Exception e) {
                    response.sendRedirect("error404");
                }

                List<Image> GallerieSlider = sBgallerie.getAllImageSlider();
                request.setAttribute("allslider", GallerieSlider);

            } else if (userPath.equals("/admin/groupe-commentaire")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/liste-annonce")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String pub = request.getParameter("x");
                String action = request.getParameter("y");
                //String val = request.getQueryString();
                if (pub != null) {
                    try {
                        int ID = Integer.parseInt(pub);
                        if (action.equals("stat")) {
                            sBpublication.UpdateStatutPUB(ID);
                            request.setAttribute("val", 2);
                        } else if (action.equals("tel")) {
                            sBpublication.UpdateStatutDownload(ID);
                            request.setAttribute("val", 3);
                        }
                    } catch (Exception e) {
                        response.sendRedirect("liste-annonce");
                    }
                }
                List<Publication> liste = sBannonce.getAllAnnonces();
                request.setAttribute("allannonce", liste);

            } else if (userPath.equals("/admin/update-annonce")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String size = request.getParameter("s");
                String ext = request.getParameter("r");
                String annonce = request.getParameter("x");
                int idannonce;
                if (annonce != null) {
                    try {
                        idannonce = Integer.parseInt(annonce);
                        request.setAttribute("annonce", sBpublication.getPublication(idannonce));
                    } catch (Exception e) {
                        response.sendRedirect("error404");
                    }
                } else if (size != null) {
                    idannonce = Integer.parseInt(size);
                    request.setAttribute("errortaille", "erreur la taille du fichier depace 5 Mo");
                    request.setAttribute("val", 4);
                    request.setAttribute("annonce", sBpublication.getPublication(idannonce));
                } else {
                    idannonce = Integer.parseInt(ext);
                    request.setAttribute("errorimage", "L'extension du fichier n'est pas valide !!");
                    request.setAttribute("val", 2);
                    request.setAttribute("annonce", sBpublication.getPublication(idannonce));
                }
            } else if (userPath.equals("/admin/liste-archive")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                List<Categorie> liste = sBcategorie.getAllCategorie();
                request.setAttribute("listecategorie", liste);

            } else if (userPath.equals("/admin/liste-laboratoire")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));
                String val = request.getParameter("x");

                try {
                    if (val != null) {
                        int id = Integer.parseInt(val);
                        sBlaboratoire.UpdateStatut(id);
                    } else {
                        String test = request.getQueryString();
                        if (test != null) {
                            request.setAttribute("val", 1);
                        }
                    }
                } catch (Exception e) {
                    response.sendRedirect("error404");
                }

                List<Laboratoire> liste = sBlaboratoire.getAllLaboratoire();
                request.setAttribute("alllabo", liste);

            } else if (userPath.equals("/admin/update-laboratoire")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String test = request.getParameter("r");
                String labo = request.getParameter("x");
                int idlabo;
                if (labo != null) {
                    try {
                        idlabo = Integer.parseInt(labo);
                        request.setAttribute("labo", sBlaboratoire.getLaboratoire(idlabo));
                    } catch (Exception e) {
                        response.sendRedirect("error404");
                    }
                } else {

                    idlabo = Integer.parseInt(test);
                    request.setAttribute("errorimage", "L'extension du fichier n'est pas valide !!");
                    request.setAttribute("val", 2);
                    request.setAttribute("labo", sBlaboratoire.getLaboratoire(idlabo));
                }

            } else if (userPath.equals("/admin/liste-membre")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                List<Membre> allmembre = sBmembre.getAllMembre();
                request.setAttribute("allmembre", allmembre);

            } else if (userPath.equals("/admin/publier-newsletter")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                try {
                    String id = request.getQueryString();
                    if (id != null) {
                        int news = Integer.parseInt(id);
                        sBNewsletter.UpdateStatutNews(news);
                    }

                } catch (Exception e) {
                    response.sendRedirect("error404");
                }
                List<Newsletter> allnews = sBNewsletter.getAllNewsletter();
                request.setAttribute("allnewsletter", allnews);

            } else if (userPath.equals("/admin/laboratoire-membre")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String membre = request.getParameter("s");
                String Labo = request.getParameter("x");
                try {
                    if (membre != null) {
                        int m = Integer.parseInt(membre);
                        String reponse = sBmembre.UpdateStatutMembre(m, Integer.parseInt(Labo));
                        request.setAttribute("statut", reponse);
                    }

                    if (Labo != null) {
                        int idLabo = Integer.parseInt(Labo);
                        List<Membre> allmembrelabo = sBmembre.getAllMembreLabo(idLabo);
                        if (sBmembre.getResponsableLABO(idLabo) == null) {
                            request.setAttribute("responsable", sBmembre.getResponsableLABO(idLabo));
                            request.setAttribute("labo", sBlaboratoire.getLaboratoire(idLabo));
                            if (allmembrelabo != null) {
                                request.setAttribute("membres", allmembrelabo);
                            }

                        } else {
                            request.setAttribute("responsable", sBmembre.getResponsableLABO(idLabo));
                            request.setAttribute("labo", sBlaboratoire.getLaboratoire(idLabo));
                            if (allmembrelabo != null) {
                                request.setAttribute("membres", allmembrelabo);
                            }
                        }

                    }

                } catch (Exception e) {
                    response.sendRedirect("error404");
                }

            } else if (userPath.equals("/admin/extras")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                request.setAttribute("Apropos", sBpresentation.getPresentation(1));
                request.setAttribute("facebook", sBpresentation.getPresentation(2));
                request.setAttribute("google", sBpresentation.getPresentation(3));

            } else if (userPath.equals("/admin/upload-laboratoire")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String var = request.getQueryString();
                if (var != null) {
                    if (Integer.parseInt(var) == 0) {
                        request.setAttribute("errorimage", "L'extension du fichier n'est pas valide !!");
                        request.setAttribute("val", 2);
                    }
                    if (Integer.parseInt(var) == 1) {
                        request.setAttribute("errortaille", "erreur la taille du fichier depace 5Mo");
                        request.setAttribute("val", 4);
                    }
                }

            } else if (userPath.equals("/admin/galerie-laboratoire")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String var = request.getQueryString();
                if (var != null) {
                    int idimg = Integer.parseInt(var);
                    File file = new File(DIR_LABO + File.separator + File.separator + sBgallerie.getImage(idimg).getLien());
                    file.delete();
                    sBgallerie.DeleteImage(idimg);
                }
                List<Image> GallerieLabo = sBgallerie.getAllImageLabo();
                request.setAttribute("Gallerie", GallerieLabo);

            } else if (userPath.equals("/admin/upload-annonce")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String var = request.getQueryString();
                if (var != null) {
                    if (Integer.parseInt(var) == 0) {
                        request.setAttribute("errorimage", "L'extension du fichier n'est pas valide !!");
                        request.setAttribute("val", 2);
                    }
                    if (Integer.parseInt(var) == 1) {
                        request.setAttribute("errortaille", "erreur la taille du fichier depace 5Mo");
                        request.setAttribute("val", 4);
                    }
                }

            } else if (userPath.equals("/admin/galerie-annonce")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String var = request.getQueryString();
                if (var != null) {
                    int idimg = Integer.parseInt(var);
                    File file = new File(DIR_ANNONCE_IMG + File.separator + File.separator + sBgallerie.getImage(idimg).getLien());
                    file.delete();
                    sBgallerie.DeleteImage(idimg);

                }
                List<Image> GallerieAnnonce = sBgallerie.getAllImageAnnonce();
                request.setAttribute("Gallerie", GallerieAnnonce);

            } else if (userPath.equals("/admin/upload-presentation")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String var = request.getQueryString();
                if (var != null) {
                    if (Integer.parseInt(var) == 0) {
                        request.setAttribute("errorimage", "L'extension du fichier n'est pas valide !!");
                        request.setAttribute("val", 2);
                    }
                    if (Integer.parseInt(var) == 1) {
                        request.setAttribute("errortaille", "erreur la taille du fichier depace 5Mo");
                        request.setAttribute("val", 4);
                    }
                }

            } else if (userPath.equals("/admin/galerie-presentation")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String var = request.getQueryString();
                if (var != null) {
                    int idimg = Integer.parseInt(var);
                    File file = new File(DIR_PRESENTATION + File.separator + File.separator + sBgallerie.getImage(idimg).getLien());
                    file.delete();
                    sBgallerie.DeleteImage(idimg);

                }
                List<Image> Gallerie = sBgallerie.getAllImagePresentation();
                request.setAttribute("Gallerie", Gallerie);

            } else if (userPath.equals("/admin/type-publication")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String type = request.getQueryString();
                if (type != null) {
                    try {
                        int id = Integer.parseInt(type);
                        sBtypepublication.UpdateStatutTYPEPUB(id);
                        request.setAttribute("val", 3);
                    } catch (Exception e) {
                        response.sendRedirect("error404");
                    }
                }
                List<TypePublication> allmodule = sBtypepublication.getAllTypeAnyAnnonce();
                request.setAttribute("allmodule", allmodule);

            } else if (userPath.equals("/admin/liste-document")) {
                /**
                 * *********************************archivage***************
                 */
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String cat = request.getQueryString();
                // String error = request.getParameter("er");
                if (cat != null) {

                    int iddos = Integer.parseInt(cat);

                    if (sBrepertoire.getRepertoire(iddos) != null) {
                        List<Document> AlldocDossier = sBdocument.getAllDocumentREP(iddos);
                        session.setAttribute("AllDOC", AlldocDossier);

                        request.setAttribute("repertoire", sBrepertoire.getRepertoire(iddos));
                        request.setAttribute("categorie", sBrepertoire.getRepertoire(iddos).getIdCat().getIdCat());
                    } else {
                        response.sendRedirect("liste-archive");
                    }

                }

            } else if (userPath.equals("/admin/liste-dossier")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String cat = request.getQueryString();
                if (cat != null) {
                    try {
                        int idcat = Integer.parseInt(cat);
                        if (sBcategorie.getCategorie(idcat) != null) {
                            session.setAttribute("categorie", sBcategorie.getCategorie(idcat));

                            List<Repertoire> CategorieDossier = sBrepertoire.getAllRepertoireCAT(idcat);
                            session.setAttribute("ALLDOS", CategorieDossier);

                        } else {
                            response.sendRedirect("liste-archive");
                        }
                    } catch (Exception e) {
                        response.sendRedirect("error404");
                    }
                }

            } else if (userPath.equals("/admin/groupe-membres")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                String compte = request.getParameter("c");
                String action = request.getParameter("t");
                String val = request.getParameter("val");
                try {
                    int groupe = Integer.parseInt(request.getParameter("g"));

                    if (val != null) {
                        request.setAttribute("val", 1);

                    } else if (compte != null) {
                        if (action.equals("del")) {
                            int idcompte = Integer.parseInt(compte);
                            sBGroupeCompte.DeleteGroupeCompte(groupe, idcompte);
                            request.setAttribute("val", 2);
                        }
                    }

//                        if(sBgroupe.getGroupe(groupe) != null){
//                            session.setAttribute("groupe",sBgroupe.getGroupe(groupe));
//                        }
//                        List<CompteGroupe> CompteGroupe = sBcompte.getallCompteGroupe(groupe);
//                        session.setAttribute("AllCompteGroupe", CompteGroupe);
                    Map GroupeMap = sBGroupeCompte.getallCompteGroupe(groupe);
                    request.setAttribute("groupe", GroupeMap.get("groupe"));
                    request.setAttribute("AllCompteGroupe", GroupeMap.get("allcomptegroupe"));
                    request.setAttribute("comptes", GroupeMap.get("comptes"));

//                        List<Compte> Comptenonselected = sBcompte.getallCompterestant(groupe);
//                        session.setAttribute("allcompte", Comptenonselected);
                    Map CompteMap = sBGroupeCompte.getallCompterestant(groupe, user.getIdCompte());
                    request.setAttribute("allcompte", CompteMap.get("compterestant"));

                } catch (Exception e) {
                    response.sendRedirect("error404");
                }

            } else if (userPath.equals("/admin/invitation")) {
                int groupe = Integer.parseInt(request.getParameter("g"));
                int compte = Integer.parseInt(request.getParameter("c"));
                sBGroupeCompte.UpdateInvitation(groupe, compte);
                response.sendRedirect("dashboard");

            } else if (userPath.equals("/admin/commentaire")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));
                try {
                    int groupe = Integer.parseInt(request.getQueryString());
                    session.setAttribute("admin", sBgroupe.getGroupe(groupe));
//                   List<Commentaire> allcomment = sBcommentaire.getAllCommentaire(groupe);
//                   session.setAttribute("allcomment", allcomment);

                } catch (Exception e) {
                    response.sendRedirect("error404");
                }
            } else if (userPath.equals("/admin/comments")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));
                try {
                    String groupe = request.getParameter("g");
                    String com = request.getParameter("del");
                    if (groupe != null) {
                        int idgroupe = Integer.parseInt(groupe);
                        List<Commentaire> allcomment = sBcommentaire.getAllCommentaire(idgroupe);
                        session.setAttribute("allcomment", allcomment);
                    } else if (com != null) {
                        int idcom = Integer.parseInt(com);
                        int idgroupe = Integer.parseInt(request.getParameter("grp"));
                        sBcommentaire.DeleteCommentaire(idcom);
                        response.sendRedirect("commentaire?" + idgroupe);
                    }

                } catch (Exception e) {
                    response.sendRedirect("error404");
                }

            } else if (userPath.equals("/admin/header")) {

            } else if (userPath.equals("/admin/gerer-thematique")) {
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                try {
                    String labos = request.getParameter("l");
                    String them = request.getParameter("s");
                    if (them != null) {
                        int idtheme = Integer.parseInt(them);
                        sBtheme.UpdateStatut(idtheme);
                        response.sendRedirect("gerer-thematique?val=stat&l=" + sBtheme.getThematique(idtheme).getIdLabo().getIdLabo());
                    } else if (labos != null) {
                        String val = request.getParameter("val");
                        int labo = Integer.parseInt(labos);
                        if (val != null) {
                            if (val.equals("add")) {
                                request.setAttribute("val", 1);
                            } else if (val.equals("updat")) {
                                request.setAttribute("val", 2);
                            } else if (val.equals("del")) {
                                request.setAttribute("val", 3);
                            } else if (val.equals("stat")) {
                                request.setAttribute("val", 4);
                            }
                        }
                        List<Thematique> theme = sBtheme.getAllThematiqueLabo(labo);
                        session.setAttribute("alltheme", theme);
                        session.setAttribute("laboratoire", sBlaboratoire.getLaboratoire(labo));
                    }

                } catch (Exception e) {
                    response.sendRedirect("error404");
                }
            }
        }

        String url = "/WEB-INF/" + userPath + ".jsp";
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userPath = request.getServletPath();
        HttpSession session = request.getSession();
        Compte user = (Compte) session.getAttribute("useradmin");
        Date dat = new Date();
        int an = 1900 + dat.getYear();
        int mois = 1 + dat.getMonth();
        int jr = dat.getDay();
        int heur = dat.getHours();
        int min = dat.getMinutes();
        int sec = dat.getSeconds();
        String mydate = jr + "-" + mois + "-" + an + "_" + heur + "-" + min + "-" + sec;

        if (session.getAttribute("useradmin") != null) {

            if (userPath.equals("/admin/index")) {
                response.sendRedirect("dashboard");
            } else if (userPath.equals("/admin/dashboard")) {

            } else if (userPath.equals("/admin/commentaire")) {
                String com = request.getParameter("commentaire");
                int groupe = Integer.parseInt(request.getParameter("groupe"));
                //Compte user = (Compte) session.getAttribute("useradmin");
                comment = new Commentaire(com, user.getIdMembre().getCivilite() + " " + user.getIdMembre().getNom(), user.getIdCompte(), new Date());
                comment.setIdMembre(user.getIdMembre());
                sBcommentaire.AddCommentaire(comment, groupe);
                response.sendRedirect("commentaire?" + groupe);

            } else if (userPath.equals("/admin/gerer-thematique")) {
                String action = request.getParameter("action");
                int labo = Integer.parseInt(request.getParameter("labo"));
                if (action.equals("Ajouter")) {
                    String statut = "depublie";
                    String titre = request.getParameter("titre");
                    String contenu = request.getParameter("contenu");
                    theme = new Thematique(statut, titre, contenu, new Date());
                    sBtheme.AjouterThematique(theme, labo);
                    response.sendRedirect("gerer-thematique?val=add&l=" + labo);

                } else if (action.equals("Modifier")) {
                    int idtheme = Integer.parseInt(request.getParameter("theme"));
                    String titre = request.getParameter("titre");
                    String contenu = request.getParameter("contenu");
                    theme = new Thematique(idtheme, sBtheme.getThematique(idtheme).getStatut(), titre, contenu, new Date());
                    sBtheme.UpdateThematique(theme, labo);
                    response.sendRedirect("gerer-thematique?val=updat&l=" + labo);

                } else if (action.equals("Supprimer")) {
                    int idtheme = Integer.parseInt(request.getParameter("theme"));
                    sBtheme.DeleteThematique(idtheme);
                    response.sendRedirect("gerer-thematique?val=del&l=" + labo);

                }
                List<Thematique> theme = sBtheme.getAllThematiqueLabo(labo);
                session.setAttribute("alltheme", theme);
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/liste-dossier")) {

                String action = request.getParameter("action");
                // Compte user = (Compte) session.getAttribute("useradmin");
                int idcat = Integer.parseInt(request.getParameter("idcat"));

                if (action.equals("Ajouter")) {
                    String libelle = request.getParameter("libelle");
                    if (sBrepertoire.TestLibelle(libelle) != null) {
                        dossier = new Repertoire(user.getIdMembre().getCivilite() + " " + user.getIdMembre().getNom() + " " + user.getIdMembre().getPrenom(), libelle, "dossier.png", new Date());
                        sBrepertoire.AjouterRepertoire(dossier, idcat);
                        List<Repertoire> CategorieDossier = sBrepertoire.getAllRepertoireCAT(idcat);
                        session.setAttribute("ALLDOS", CategorieDossier);

                        request.setAttribute("ajoutdos", libelle);
                        request.setAttribute("val", 1);
                        request.setAttribute("categorie", sBcategorie.getCategorie(idcat));
//                      response.sendRedirect("liste-dossier?"+idcat);
                    } else {
                        List<Repertoire> CategorieDossier = sBrepertoire.getAllRepertoireCAT(idcat);
                        session.setAttribute("ALLDOS", CategorieDossier);
                        request.setAttribute("val", 3);
                        request.setAttribute("categorie", sBcategorie.getCategorie(idcat));

                    }

                } else if (action.equals("Modifier")) {
                    String libelle = request.getParameter("libelle");
                    int ID = Integer.parseInt(request.getParameter("idrep"));
                    sBrepertoire.UpdateRepertoireLibelle(ID, libelle);
                    List<Repertoire> CategorieDossier = sBrepertoire.getAllRepertoireCAT(idcat);
                    session.setAttribute("ALLDOS", CategorieDossier);

                    request.setAttribute("updatedos", libelle);
                    request.setAttribute("val", 2);
                    request.setAttribute("categorie", sBcategorie.getCategorie(idcat));

                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/liste-document")) {
                /**
                 * ***********************gestion document*******************
                 */

                String action = request.getParameter("action");
                //Compte user = (Compte) session.getAttribute("useradmin");
                String icone = "";
                String type = "";
                int idrep = Integer.parseInt(request.getParameter("idrep"));

                if (action.equals("Ajouter")) {
                    Part part = request.getPart("fichier");
                    String libelle = request.getParameter("libelle");
                    String file = getFileName(part);

                    if (file != null && !file.isEmpty()) {
                        ControllerFile controllerFiledocument = new ControllerFile(file);
                        if (getTailleFichier(part) != null) {
                            String fichier = getTailleFichier(part);

                            String EXT = controllerFiledocument.getFileExt(file);
                            listOfExtensions.add(".pdf");
                            listOfExtensions.add(".docx");
                            listOfExtensions.add(".doc");
                            listOfExtensions.add(".xlsx");
                            listOfExtensions.add(".xls");
                            listOfExtensions.add(".pptx");
                            listOfExtensions.add(".ppt");
                            if (listOfExtensions.contains(EXT)) {
                                if (EXT.equals(".pdf")) {
                                    icone = "pdf.jpg";
                                    type = "Document PDF";
                                } else if (EXT.equals(".docx") || EXT.equals(".doc")) {
                                    icone = "word.png";
                                    type = "Document Word";
                                } else if (EXT.equals(".xlsx") || EXT.equals(".xls")) {
                                    icone = "excel.png";
                                    type = "Document Excel";
                                } else if (EXT.equals(".pttx") || EXT.equals(".ppt")) {
                                    icone = "powerpoint.png";
                                    type = "Document PowerPoint";
                                }
                                if (sBdocument.TestLien(file) != null) {
                                    String filename = controllerFiledocument.uploadFile(request, part, DIR_ARCHIVE, mydate + "-ARCHIVE" + sBdocument.getLASTDocument() + EXT, "fichier");

                                    document = new Document(user.getIdMembre().getCivilite() + " " + user.getIdMembre().getNom(), filename, libelle, icone, fichier, 1, EXT, type, new Date());
                                    sBdocument.AjouterDocument(document, idrep, user.getIdCompte());
                                    request.setAttribute("val", 1);
//                                   response.sendRedirect("liste-document?"+idrep);

                                } else if (sBdocument.getLienExistant(file) != null) {
                                    request.setAttribute("val", 5);
                                    int IDdoc = sBdocument.getLienExistant(file).getIdDoc();
                                    request.setAttribute("libelle", sBdocument.getDocument(IDdoc).getLien());
                                }

                            } else {
                                request.setAttribute("errorfile", EXT);
                                request.setAttribute("val", 2);
                                //response.sendRedirect("liste-document?"+idrep);
                                // response.sendRedirect("liste-document?er=0");
                            }
                        } else {
                            request.setAttribute("errortaille", "erreur la taille du fichier depace 5Mo");
                            request.setAttribute("val", 4);
                        }
                    }

                } else if (action.equals("Renommer")) {
                    Part part = request.getPart("fichier");
                    String libelle = request.getParameter("libelle");
                    int IDdoc = Integer.parseInt(request.getParameter("iddoc"));
                    String file = getFileName(part);

                    if (file != null && !file.isEmpty()) {
                        ControllerFile controllerFiledocument = new ControllerFile(file);
                        if (getTailleFichier(part) != null) {
                            String fichier = getTailleFichier(part);

                            String EXT = controllerFiledocument.getFileExt(file);
                            listOfExtensions.add(sBdocument.getDocument(IDdoc).getExtension());
                            if (listOfExtensions.contains(EXT)) {
                                if (EXT.equals(sBdocument.getDocument(IDdoc).getExtension())) {
                                    icone = sBdocument.getDocument(IDdoc).getIcone();
                                    type = sBdocument.getDocument(IDdoc).getType();
                                }
                                if (sBdocument.getDocument(IDdoc).getLien().equals(file)) {

                                    File filedoc = new File(DIR_ARCHIVE + File.separator + File.separator + sBdocument.getDocument(IDdoc).getLien());
                                    filedoc.delete();
                                    sBdocument.Updatelibelle(IDdoc, libelle, user.getIdMembre().getCivilite() + " " + user.getIdMembre().getNom());
                                    sBdocument.UpdateVersion(IDdoc, sBdocument.getDocument(IDdoc).getVersion() + 1, user.getIdMembre().getCivilite() + " " + user.getIdMembre().getNom());
                                    String filename = controllerFiledocument.uploadFile(request, part, DIR_ARCHIVE, mydate + "-ARCHIVE" + IDdoc + EXT, "fichier");
                                    document = sBdocument.getDocument(IDdoc);
                                    document.setLien(filename);
                                    document.setIcone(icone);
                                    document.setTailledoc(fichier);
                                    document.setType(type);
                                    document.setExtension(EXT);

                                    request.setAttribute("updatedos", libelle);
                                    request.setAttribute("val", 7);
                                } else {
                                    request.setAttribute("val", 6);
                                    request.setAttribute("libelle", file);
                                }

                            } else {
                                request.setAttribute("errorfile", EXT);
                                request.setAttribute("val", 2);
                            }
                        } else {
                            request.setAttribute("errortaille", "erreur la taille du fichier depace 5Mo");
                            request.setAttribute("val", 4);
                        }
                    } else {
                        sBdocument.Updatelibelle(IDdoc, libelle, user.getIdMembre().getCivilite() + " " + user.getIdMembre().getNom());
                        request.setAttribute("updatedos", libelle);
                        request.setAttribute("val", 3);
                    }
                }
                List<Document> AlldocDossier = sBdocument.getAllDocumentREP(idrep);
                request.setAttribute("AllDOC", AlldocDossier);
                request.setAttribute("repertoire", sBrepertoire.getRepertoire(idrep));
                request.setAttribute("categorie", sBrepertoire.getRepertoire(idrep).getIdCat().getIdCat());

                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/ajouter-membre")) {
                String action = request.getParameter("action");

                if (action.equals("Ajouter")) {
                    Part part = request.getPart("image");
                    String civilite = request.getParameter("civilite");
                    String nom = request.getParameter("nom");
                    String prenom = request.getParameter("prenom");
                    String fonction = request.getParameter("fonction");
                    String tutelle = request.getParameter("tutelle");
                    String adresse = request.getParameter("adresse");
                    String email = request.getParameter("email");
                    String telephone = request.getParameter("telephone");
                    String statut = "adherant";
                    int IDlabo = Integer.parseInt(request.getParameter("laboratoire"));
                    int nbvisite = 0;
                    String file = getFileName(part);

                    if (file != null && !file.isEmpty()) {
                        ControllerFile controllerFilemembre = new ControllerFile(file);
                        if (getTailleFichier(part) != null) {
                            String fichier = getTailleFichier(part);
                            String EXT = controllerFilemembre.getFileExt(file);
                            listOfExtensions.add(".jpg");
                            listOfExtensions.add(".png");
                            listOfExtensions.add(".JPG");
                            listOfExtensions.add(".PNG");
                            listOfExtensions.add(".GIF");
                            listOfExtensions.add(".gif");
                            if (listOfExtensions.contains(EXT)) {
                                membre = new Membre(email, civilite, nom, prenom, adresse, fonction, statut, tutelle, telephone, controllerFilemembre.uploadFile(request, part, DIR_AVATAR, mydate + "-AVATAR" + sBmembre.getLASTMembre() + "" + EXT, "image"), nbvisite, new Date());
                                membre = sBmembre.AjouterMembre(membre, IDlabo);
                                int IDmembre = membre.getIdMembre();
                                membre.setNomid(nom + IDmembre);
                                sBmembre.UpdateMembre2(membre);
                                request.setAttribute("ajoutok", nom + " " + prenom);
                                request.setAttribute("val", 1);
                            } else {
                                request.setAttribute("errorimage", "L'extension " + EXT + " du fichier n'est pas valide");
                                request.setAttribute("val", 2);
                            }
                        } else {
                            request.setAttribute("errortaille", "erreur la taille du fichier depace 5Mo");
                            request.setAttribute("val", 4);
                        }
                    } else {
                        membre = new Membre(email, civilite, nom, prenom, adresse, fonction, statut, tutelle, telephone, "user.png", nbvisite, new Date());
                        membre = sBmembre.AjouterMembre(membre, IDlabo);
                        int IDmembre = membre.getIdMembre();
                        membre.setNomid(nom + IDmembre);
                        sBmembre.UpdateMembre2(membre);
                        request.setAttribute("ajoutok", nom + " " + prenom);
                        request.setAttribute("val", 1);

                    }

                } else if (action.equals("Importer")) {
                    String Existant = null;
                    int IDlabo = Integer.parseInt(request.getParameter("laboratoire"));

                    Part part = request.getPart("file");
                    String file = getFileName(part);
                    if (file != null && !file.isEmpty()) {
                        ControllerFile controllerFilemembre = new ControllerFile(file);
                        String EXT = controllerFilemembre.getFileExt(file);
                        listOfExtensions.add(".xls");
                        listOfExtensions.add(".XLS");
                        if (listOfExtensions.contains(EXT)) {

                            ArrayList<String> values = new ArrayList<String>();
                            try {
                                InputStream input = part.getInputStream();
                                POIFSFileSystem fs = new POIFSFileSystem(input);
                                HSSFWorkbook wb = new HSSFWorkbook(fs);//pour specifier quon veut use Excel
                                HSSFSheet sheet = wb.getSheetAt(0); //prendre la premiere feuille
                                Iterator rows = sheet.rowIterator();

                                while (rows.hasNext()) { //on entre sil ya des lignes dans le tableau
                                    values.clear(); //on vide le tableau apres chaque ligne
                                    HSSFRow row = (HSSFRow) rows.next();
                                    Iterator cells = row.cellIterator();

                                    while (cells.hasNext()) { //parcours des cellules de la ligne et on insert dans le tableau
                                        HSSFCell cell = (HSSFCell) cells.next();
                                        if (HSSFCell.CELL_TYPE_NUMERIC == cell.getCellType()) {
                                            values.add(Integer.toString((int) cell.getNumericCellValue())); //si le type d'une cellule est muemrique on convertit
                                        } else if (HSSFCell.CELL_TYPE_STRING == cell.getCellType()) {
                                            values.add(cell.getStringCellValue());  //si le type d'une cellule est chaine
                                        }
                                    }
                                    if (sBmembre.TestExistEmail(values.get(5)) == null) {
                                        Membre m = new Membre();
                                        m.setCivilite(values.get(0));
                                        m.setNom(values.get(1));
                                        m.setPrenom(values.get(2));
                                        m.setFonction(values.get(3));
                                        m.setTutelle(values.get(4));
                                        m.setEmail(values.get(5));
                                        m.setAdresse(values.get(6));
                                        m.setPhone(values.get(7));
                                        m.setDatecreate(new Date());
                                        m.setImage("user.png");
                                        m.setNbvisite(0);
                                        m.setResponsable("adherant");
                                        membre = sBmembre.AjouterMembre(m, IDlabo);
                                        int IDmembre = membre.getIdMembre();
                                        membre.setNomid(membre.getNom() + IDmembre);
                                        sBmembre.UpdateMembre2(membre);

                                    } else if (sBmembre.TestExistEmail(values.get(5)) != null) {
                                        if (Existant == null) {
                                            Existant = "<strong>" + values.get(5) + "</strong> ;&nbsp;&nbsp; ";
                                        } else {
                                            Existant = Existant + "<strong>" + values.get(5) + "</strong> ;&nbsp;&nbsp; ";
                                        }
                                    }
                                }
                                if (Existant != null) {
                                    request.setAttribute("errorimage", "Oups!!! Importation Bien Effectuée Exclut Les Membres <span style='color:black'>" + Existant + "</span> Qui Existent Deja ...");
                                    request.setAttribute("val", 2);
                                } else {
                                    request.setAttribute("ajoutok", "Importation Effectuée avec Succes .... ");
                                    request.setAttribute("val", 1);
                                }
                            } catch (IOException e) {
                                request.setAttribute("errortaille", "Oups!!! Une Erreur Survenue Lors de l'Importation du fichier");
                                request.setAttribute("val", 4);
                            }
                        } else {
                            request.setAttribute("errortaille", "L'extension " + EXT + " du fichier n'est pas valide");
                            request.setAttribute("val", 4);
                        }
                    }
                }
                List<Laboratoire> lab = sBlaboratoire.getAllLaboratoire();
                request.setAttribute("listelabo", lab);
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/ajouter-annonce")) {

                //  Compte user = (Compte) session.getAttribute("useradmin");
                Part part = request.getPart("fichier");
                String libelle = request.getParameter("libelle");
                String resume = request.getParameter("resume");
                String contenu = request.getParameter("contenu");
                String statut = "depublie";
                Date d = new Date();
                int annee = 1900 + d.getYear();
                String telecharger = "non";

                String icone = "";
                String file = getFileName(part);

                if (file != null && !file.isEmpty()) {
                    ControllerFile controllerFileAnnonce = new ControllerFile(file);
                    if (getTailleFichier(part) != null) {
                        String fichier = getTailleFichier(part);
                        String EXT = controllerFileAnnonce.getFileExt(file);
                        listOfExtensions.add(".pdf");
                        listOfExtensions.add(".docx");
                        listOfExtensions.add(".doc");
                        listOfExtensions.add(".xlsx");
                        listOfExtensions.add(".xls");
                        listOfExtensions.add(".pptx");
                        listOfExtensions.add(".ppt");
                        if (listOfExtensions.contains(EXT)) {
                            if (EXT.equals(".pdf")) {
                                icone = "pdf.jpg";
                            } else if (EXT.equals(".docx") || EXT.equals(".doc")) {
                                icone = "word.png";
                            } else if (EXT.equals(".xlsx") || EXT.equals(".xls")) {
                                icone = "excel.png";
                            } else if (EXT.equals(".pttx") || EXT.equals(".ppt")) {
                                icone = "powerpoint.png";
                            }

                            annonce = new Publication(fichier, annee, statut, telecharger, libelle, resume, contenu, controllerFileAnnonce.uploadFile(request, part, DIR_ANNONCE, mydate + "-ANNONCE" + sBpublication.getLASTPublication() + EXT, "fichier"), icone, new Date(), new Date());
                            sBannonce.AjouterAnnonce(annonce, user.getIdCompte());
                            request.setAttribute("val", 1);

                        } else {
                            request.setAttribute("errorfile", "L'extension " + EXT + " du fichier n'est pas valide");
                            request.setAttribute("val", 2);
                        }
                    } else {
                        request.setAttribute("errortaille", "erreur la taille du fichier depace 5 Mo");
                        request.setAttribute("val", 4);
                    }
                } else {
                    annonce = new Publication("0Ko", annee, statut, telecharger, libelle, resume, contenu, "zip.png", "zip.png", new Date(), new Date());
                    sBannonce.AjouterAnnonce(annonce, user.getIdCompte());
                    request.setAttribute("val", 1);
                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));
                // gestion du laboratoire
            } else if (userPath.equals("/admin/ajouter-laboratoire")) {
                Part part = request.getPart("image");
                String sigle = request.getParameter("sigle");
                String libelle = request.getParameter("libelle");
                String contenu = request.getParameter("contenu");
                String statut = "depublie";
                String file = getFileName(part);
                //Compte user = (Compte) session.getAttribute("useradmin");
                String createur = user.getIdMembre().getCivilite() + " " + user.getIdMembre().getNom();
                if (file != null && !file.isEmpty()) {
                    ControllerFile controllerFilelabo = new ControllerFile(file);
                    if (getTailleFichier(part) != null) {
                        String fichier = getTailleFichier(part);
                        String EXT = controllerFilelabo.getFileExt(file);
                        listOfExtensions.add(".jpg");
                        listOfExtensions.add(".png");
                        listOfExtensions.add(".JPG");
                        listOfExtensions.add(".PNG");
                        if (listOfExtensions.contains(EXT)) {
                            labo = new Laboratoire(createur, sigle, libelle, contenu, controllerFilelabo.uploadFile(request, part, DIR_LABO, mydate + "-LABO" + sBlaboratoire.getLASTLaboratoire() + "" + EXT, "image"), statut, new Date());
                            sBlaboratoire.AjouterLaboratoire(labo);
                            request.setAttribute("ajoutlabo", libelle);
                            request.setAttribute("val", 1);
                        } else {
                            request.setAttribute("errorimage", "L'extension " + EXT + " du fichier n'est pas valide");
                            request.setAttribute("val", 2);
                        }
                    } else {
                        request.setAttribute("errortaille", "erreur la taille du fichier depace 5Mo");
                        request.setAttribute("val", 4);
                    }
                } else {
                    labo = new Laboratoire(createur, sigle, libelle, contenu, "labo.jpg", statut, new Date());
                    sBlaboratoire.AjouterLaboratoire(labo);
                    request.setAttribute("ajoutlabo", libelle);
                    request.setAttribute("val", 1);
                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/update-laboratoire")) {
                // Compte user = (Compte) session.getAttribute("useradmin");
                Part part = request.getPart("image");
                String sigle = request.getParameter("sigle");
                String libelle = request.getParameter("libelle");
                String contenu = request.getParameter("contenu");
                int IDlabo = Integer.parseInt(request.getParameter("idLabo"));
                String file = getFileName(part);

                if (file != null && !file.isEmpty()) {
                    ControllerFile controllerFileLabo = new ControllerFile(file);
                    String EXT = controllerFileLabo.getFileExt(file);
                    listOfExtensions.add(".jpg");
                    listOfExtensions.add(".png");
                    listOfExtensions.add(".JPG");
                    listOfExtensions.add(".PNG");
                    if (listOfExtensions.contains(EXT)) {
                        File imglabo = new File(DIR_LABO + File.separator + File.separator + sBlaboratoire.getLaboratoire(IDlabo).getImage());
                        System.out.print(imglabo);
                        imglabo.delete();
                        labo = new Laboratoire(IDlabo, user.getIdMembre().getCivilite() + " " + user.getIdMembre().getNom(), sBlaboratoire.getLaboratoire(IDlabo).getStatut(), sigle, libelle, contenu, controllerFileLabo.uploadFile(request, part, DIR_LABO, mydate + "-LABO" + IDlabo + EXT, "image"), new Date());
                        sBlaboratoire.UpdateLaboratoire(labo);
                        response.sendRedirect("liste-laboratoire?" + IDlabo);
                    } else {
                        response.sendRedirect("update-laboratoire?r=" + IDlabo);
                    }
                } else {
                    labo = new Laboratoire(IDlabo, sBlaboratoire.getLaboratoire(IDlabo).getCreateur(), sBlaboratoire.getLaboratoire(IDlabo).getStatut(), sigle, libelle, contenu, sBlaboratoire.getLaboratoire(IDlabo).getImage(), new Date());
                    sBlaboratoire.UpdateLaboratoire(labo);
                    response.sendRedirect("liste-laboratoire?" + IDlabo);
                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/update-annonce")) {
                Part part = request.getPart("fichier");
                String titre = request.getParameter("libelle");
                String resume = request.getParameter("resume");
                String contenu = request.getParameter("contenu");
                int IDannonce = Integer.parseInt(request.getParameter("idannonce"));
                String file = getFileName(part);
                String icone = "";

                if (file != null && !file.isEmpty()) {
                    ControllerFile controllerFileAnnonce = new ControllerFile(file);
                    if (getTailleFichier(part) != null) {
                        String fichier = getTailleFichier(part);
                        String EXT = controllerFileAnnonce.getFileExt(file);
                        listOfExtensions.add(".pdf");
                        listOfExtensions.add(".docx");
                        listOfExtensions.add(".doc");
                        listOfExtensions.add(".xlsx");
                        listOfExtensions.add(".xls");
                        listOfExtensions.add(".pptx");
                        listOfExtensions.add(".ppt");
                        if (listOfExtensions.contains(EXT)) {
                            if (EXT.equals(".pdf")) {
                                icone = "pdf.jpg";
                            } else if (EXT.equals(".docx") || EXT.equals(".doc")) {
                                icone = "word.png";
                            } else if (EXT.equals(".xlsx") || EXT.equals(".xls")) {
                                icone = "excel.png";
                            } else if (EXT.equals(".pttx") || EXT.equals(".ppt")) {
                                icone = "powerpoint.png";
                            }
                            File fileannonce = new File(DIR_ANNONCE + File.separator + File.separator + sBpublication.getPublication(IDannonce).getLien());
                            //System.out.print(imglabo);
                            fileannonce.delete();
                            annonce = sBpublication.getPublication(IDannonce);
                            annonce.setTaille(fichier);
                            annonce.setLibelle(titre);
                            annonce.setResume(resume);
                            annonce.setContenu(contenu);
                            annonce.setLien(controllerFileAnnonce.uploadFile(request, part, DIR_ANNONCE, mydate + "-ANNONCE" + sBpublication.getLASTPublication() + EXT, "fichier"));
                            annonce.setIcone(icone);
                            annonce.setDatemodif(new Date());

                            sBpublication.UpdatePublication(annonce);
                            response.sendRedirect("liste-annonce?" + IDannonce);
                        } else {
                            response.sendRedirect("update-annonce?r=" + IDannonce);
                        }
                    } else {
                        response.sendRedirect("update-annonce?s=" + IDannonce);
                    }
                } else {
                    annonce = sBpublication.getPublication(IDannonce);
                    annonce.setLibelle(titre);
                    annonce.setResume(resume);
                    annonce.setContenu(contenu);
                    annonce.setDatemodif(new Date());
                    sBpublication.UpdatePublication(annonce);
                    response.sendRedirect("liste-annonce?" + IDannonce);
                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/ajouter-categorie")) {
                /**
                 * *************************************add categorie*********
                 */

                String action = request.getParameter("action");
                //Compte user = (Compte) session.getAttribute("useradmin");
                if (action.equals("Ajouter")) {
                    String libelle = request.getParameter("libelle");

                    categorie = new Categorie(libelle, "bd.png", user.getIdMembre().getCivilite() + " " + user.getIdMembre().getNom() + " " + user.getIdMembre().getPrenom(), new Date());
                    sBcategorie.AjouterCategorie(categorie);
                    request.setAttribute("ajoutcat", libelle);
                    request.setAttribute("val", 1);
                    List<Categorie> liste = sBcategorie.getAllCategorie();
                    request.setAttribute("listecategorie", liste);

                } else if (action.equals("Modifier")) {
                    String libelle = request.getParameter("libelle");
                    int ID = Integer.parseInt(request.getParameter("idCat"));
                    String login = request.getParameter("login");
                    categorie = new Categorie(ID, "bd.png", libelle, login, new Date());
                    sBcategorie.UpdateCategorie(categorie);

                    request.setAttribute("updatecat", libelle);
                    request.setAttribute("val", 2);
                    List<Categorie> liste = sBcategorie.getAllCategorie();
                    request.setAttribute("listecategorie", liste);

                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/groupe-membres")) {
                String action = request.getParameter("action");
                int ID = Integer.parseInt(request.getParameter("groupe"));
                //ajouter menu
                if (action.equals("Ajouter")) {
                    try {
                        String[] CompteSelected = request.getParameterValues("compte");
                        sBGroupeCompte.AjouterGroupeCompte(ID, CompteSelected);
                        response.sendRedirect("groupe-membres?val=0&g=" + ID);
                    } catch (Exception e) {
                        response.sendRedirect("error404");
                    }

                }

            } else if (userPath.equals("/admin/creer-groupe")) {
                String action = request.getParameter("action");
                // Compte user = (Compte) session.getAttribute("useradmin");
                String libelle = request.getParameter("titre");
                Part part = request.getPart("image");
                String file = getFileName(part);
                int ID;
                if (action.equals("Supprimer")) {
                    ID = Integer.parseInt(request.getParameter("groupe"));
                    if (sBgroupe.DeleteGroupe(ID)) {
                        File filegroup = new File(DIR_GROUPE + File.separator + File.separator + sBgroupe.getGroupe(ID).getImage());
                        filegroup.delete();
                        request.setAttribute("val", 4);
                    } else {
                        request.setAttribute("val", 5);
                    }

                }

                if (file != null && !file.isEmpty()) {
                    ControllerFile controllerFilegroupe = new ControllerFile(file);
                    String EXT = controllerFilegroupe.getFileExt(file);
                    listOfExtensions.add(".jpg");
                    listOfExtensions.add(".png");
                    listOfExtensions.add(".JPG");
                    listOfExtensions.add(".PNG");
                    if (listOfExtensions.contains(EXT)) {
                        if (action.equals("Ajouter")) {
                            groupe = new Groupe(controllerFilegroupe.uploadFile(request, part, DIR_GROUPE, mydate + "-GROUPE" + sBgroupe.getLASTGroupe() + EXT, "image"), libelle, user.getLogin(), new Date(), new Date());
                            int IDc = sBgroupe.AjouterGroupe(groupe);
                            String[] CompteSelect = request.getParameterValues("listecompte");
                            sBGroupeCompte.AjouterGroupeCompte(IDc, CompteSelect);
                            request.setAttribute("val", 1);

                        } else if (action.equals("Modifier")) {
                            ID = Integer.parseInt(request.getParameter("groupe"));
                            File filegroup = new File(DIR_GROUPE + File.separator + File.separator + sBgroupe.getGroupe(ID).getImage());
                            filegroup.delete();
                            groupe = new Groupe(ID, controllerFilegroupe.uploadFile(request, part, DIR_GROUPE, mydate + "-GROUPE" + sBgroupe.getLASTGroupe() + EXT, "image"), libelle, user.getLogin(), new Date());
                            sBgroupe.UpdateGroupe(groupe);
                            request.setAttribute("val", 2);

                        }

                    } else {
                        request.setAttribute("errorimage", "L'extension " + EXT + " du fichier n'est pas valide");
                        request.setAttribute("val", 3);
                    }
                } else if (action.equals("Ajouter")) {
                    groupe = new Groupe("prie.png", libelle, user.getLogin(), new Date(), new Date());
                    int IDc = sBgroupe.AjouterGroupe(groupe);
                    String[] CompteSelect = request.getParameterValues("listecompte");
                    sBGroupeCompte.AjouterGroupeCompte(IDc, CompteSelect);
                    request.setAttribute("val", 1);

                } else if (action.equals("Modifier")) {
                    ID = Integer.parseInt(request.getParameter("groupe"));
                    groupe = new Groupe(ID, sBgroupe.getGroupe(ID).getImage(), libelle, user.getLogin(), new Date());
                    sBgroupe.UpdateGroupe(groupe);
                    request.setAttribute("val", 2);

                }
                List<Groupe> allgroupe = sBgroupe.getAllGroupeByUser(user.getLogin());
                request.setAttribute("allgroupe", allgroupe);
                List<Compte> allm = sBcompte.getAllCompte();
                request.setAttribute("allcompte", allm);
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/editer-profile")) {

                Part part = request.getPart("image");
                String civilite = request.getParameter("civilite");
                String nom = request.getParameter("nom");
                String prenom = request.getParameter("prenom");
                String fonction = request.getParameter("fonction");
                String tutelle = request.getParameter("tutelle");
                String adresse = request.getParameter("adresse");
                String email = request.getParameter("email");
                String telephone = request.getParameter("telephone");
                String url = request.getParameter("urlperso");
                String nomID = request.getParameter("nomid");
                String responsable = request.getParameter("respo");
                int nbvisite = Integer.parseInt(request.getParameter("visite"));
                int ID = Integer.parseInt(request.getParameter("membre"));
                String file = getFileName(part);

                System.out.print("part " + part);
                System.out.print("file " + file);

                if (file != null && !file.isEmpty()) {
                    ControllerFile controllerFilemembre = new ControllerFile(file);
                    String EXT = controllerFilemembre.getFileExt(file);
                    listOfExtensions.add(".jpg");
                    listOfExtensions.add(".png");
                    listOfExtensions.add(".JPG");
                    listOfExtensions.add(".PNG");
                    listOfExtensions.add(".GIF");
                    listOfExtensions.add(".gif");
                    if (listOfExtensions.contains(EXT)) {
                        File imgmembre = new File(DIR_AVATAR + File.separator + File.separator + sBmembre.getMembre(ID).getImage());
                        System.out.print(imgmembre);
                        imgmembre.delete();
                        membre = new Membre(ID, nomID, email, civilite, nom, prenom, adresse, fonction, responsable, tutelle, telephone, controllerFilemembre.uploadFile(request, part, DIR_AVATAR, mydate + "-AVATAR" + ID + EXT, "image"), nbvisite, new Date(), url);
                        sBmembre.UpdateMembre(membre, sBmembre.getMembre(ID).getIdLabo().getIdLabo());
                        request.setAttribute("updateok", nom + " " + prenom);
                        request.setAttribute("val", 2);
                        request.setAttribute("membre", sBmembre.getMembre(ID));
                    } else {
                        request.setAttribute("errorimage", "L'extension " + EXT + " du fichier n'est pas valide");
                        request.setAttribute("val", 1);
                        request.setAttribute("membre", sBmembre.getMembre(ID));
                    }
                } else {
                    membre = new Membre(ID, nomID, email, civilite, nom, prenom, adresse, fonction, responsable, tutelle, telephone, sBmembre.getMembre(ID).getImage(), nbvisite, new Date(), url);
                    sBmembre.UpdateMembre(membre, sBmembre.getMembre(ID).getIdLabo().getIdLabo());
                    request.setAttribute("updateok", nom + " " + prenom);
                    request.setAttribute("val", 2);
                    request.setAttribute("membre", sBmembre.getMembre(ID));

                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/edit-profil")) {

                Part part = request.getPart("image");
                String civilite = request.getParameter("civilite");
                String nom = request.getParameter("nom");
                String prenom = request.getParameter("prenom");
                String fonction = request.getParameter("fonction");
                String tutelle = request.getParameter("tutelle");
                String adresse = request.getParameter("adresse");
                String email = request.getParameter("email");
                String url = request.getParameter("urlperso");
                String telephone = request.getParameter("telephone");
                String nomID = request.getParameter("nomid");
                String responsable = request.getParameter("respo");
                int nbvisite = Integer.parseInt(request.getParameter("visite"));
                int ID = Integer.parseInt(request.getParameter("membre"));
                int Labo = Integer.parseInt(request.getParameter("laboratoire"));
                String file = getFileName(part);

                if (file != null && !file.isEmpty()) {
                    ControllerFile controllerFilemembre = new ControllerFile(file);
                    String EXT = controllerFilemembre.getFileExt(file);
                    listOfExtensions.add(".jpg");
                    listOfExtensions.add(".png");
                    listOfExtensions.add(".JPG");
                    listOfExtensions.add(".PNG");
                    listOfExtensions.add(".GIF");
                    listOfExtensions.add(".gif");
                    if (listOfExtensions.contains(EXT)) {
                        File imgmembre = new File(DIR_AVATAR + File.separator + File.separator + sBmembre.getMembre(ID).getImage());
                        System.out.print(imgmembre);
                        imgmembre.delete();

                        membre = new Membre(ID, nomID, email, civilite, nom, prenom, adresse, fonction, responsable, tutelle, telephone, controllerFilemembre.uploadFile(request, part, DIR_AVATAR, mydate + "-AVATAR" + ID + EXT, "image"), nbvisite, new Date(), url);
                        sBmembre.UpdateMembre(membre, Labo);
                        request.setAttribute("updateok", nom + " " + prenom);
                        request.setAttribute("val", 2);
                        request.setAttribute("membre", sBmembre.getMembre(ID));
                    } else {
                        request.setAttribute("errorimage", "L'extension " + EXT + " du fichier n'est pas valide");
                        request.setAttribute("val", 1);
                        request.setAttribute("membre", sBmembre.getMembre(ID));
                    }
                } else {
                    membre = new Membre(ID, nomID, email, civilite, nom, prenom, adresse, fonction, responsable, tutelle, telephone, sBmembre.getMembre(ID).getImage(), nbvisite, new Date(), url);
                    sBmembre.UpdateMembre(membre, Labo);
                    request.setAttribute("updateok", nom + " " + prenom);
                    request.setAttribute("val", 2);
                    request.setAttribute("membre", sBmembre.getMembre(ID));

                }
                List<Laboratoire> lab = sBlaboratoire.getAllLaboratoire();
                request.setAttribute("listelabo", lab);
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/profil-btn-menu")) {

                String action = request.getParameter("action");
                int IDprofil = Integer.parseInt(request.getParameter("profil"));

//                //ajouter bouton
//                if (action.equals("AjouterB")) {
//                    String[] BoutonRestant = request.getParameterValues("bouton");
//                    sBboutonprofil.AjouterBoutonProfil(IDprofil, BoutonRestant);
//                }
                //ajouter menu
                if (action.equals("AjouterM")) {
                    try {
                        String[] MenuRestant = request.getParameterValues("menu");
                        sBmenuprofil.AjouterMenuProfil(IDprofil, MenuRestant);
                        profil = sBprofil.getProfil(IDprofil);
                        request.setAttribute("profil", profil);
//                            List<Menu> Profilmenu = sBmenu.getallmenuProfil(IDprofil);
//                            session.setAttribute("MenusPro", Profilmenu);
//                            request.setAttribute("val", 2);
//                            List<Menu> menus = sBmenu.getallmenurestant(IDprofil);
//                            session.setAttribute("allmenu", menus);
                        response.sendRedirect("profil-btn-menu?val=0&r=" + IDprofil);
                    } catch (Exception e) {
                        response.sendRedirect("error404");
                    }
                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                //gestion des comptes
            } else if (userPath.equals("/admin/gerer-compte")) {
                String action = request.getParameter("action");
                // Compte user = (Compte) session.getAttribute("useradmin");

                if (action.equals("Ajouter")) {
                    String login = request.getParameter("login");
                    String pwd = request.getParameter("password");
                    int IDprofil = Integer.parseInt(request.getParameter("profil"));
                    int IDMembre = Integer.parseInt(request.getParameter("membre"));
                    String statut = "active";
                    if (sBcompte.TestLogin(login) != null) {
                        compte = new Compte(login, Crypto.crypt(pwd), "non", statut, new Date(), new Date());
                        sBcompte.AjouterCompte(IDprofil, IDMembre, compte);
                        request.setAttribute("ajoutcompte", login);
                        request.setAttribute("val", 2);
                    } else {
                        request.setAttribute("val", 5);
                    }

                } else if (action.equals("Modifier")) {
                    String login = request.getParameter("login");
                    String pwd = request.getParameter("password");
                    String statut = request.getParameter("statut");
                    int IDprofil = Integer.parseInt(request.getParameter("profil"));
                    int IDmembre = Integer.parseInt(request.getParameter("membre"));
                    int IDCompte = Integer.parseInt(request.getParameter("compte"));
                    if (sBcompte.TestLogin(login) != null) {
                        compte = new Compte(IDCompte, login, Crypto.crypt(pwd), "non", statut, new Date());
                        sBcompte.UpdateCompte(IDprofil, IDmembre, compte);
                        request.setAttribute("updatecompte", login);
                        request.setAttribute("val", 1);
                    } else {
                        compte = new Compte(IDCompte, sBcompte.getCompte(IDCompte).getLogin(), Crypto.crypt(pwd), "non", statut, new Date());
                        sBcompte.UpdateCompte(IDprofil, IDmembre, compte);
                        request.setAttribute("updatecompte", login);
                        request.setAttribute("val", 5);
                    }

                } else if (action.equals("Supprimer")) {
                    int IDCompte = Integer.parseInt(request.getParameter("compte"));
                    sBcompte.DeleteCompte(IDCompte);
                    request.setAttribute("val", 4);
                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

                List<Profil> listeprofil = sBprofil.getAllProfil();
                request.setAttribute("allprofil", listeprofil);
                List<CompteDTO> allcompte = sBcompte.getAllCompteDTO();
                request.setAttribute("allcompte", allcompte);
                //gestion des publications
            } else if (userPath.equals("/admin/gerer-publication")) {

                Part part = null;
                String file = null;
                String libelle = request.getParameter("libelle");
                String contenu = request.getParameter("resume");
                String action = request.getParameter("action");
                Date d = new Date();
                int annee = 1900 + d.getYear();
                String statut = "";
                String telecharger = "";
                int idtype = Integer.parseInt(request.getParameter("idType"));
                int idpub = 0;
                String icone = "";
                if (action.equals("Ajouter")) {
                    part = request.getPart("fichier");
                    file = getFileName(part);
                    statut = "depublie";
                    telecharger = "non";
                } else if (action.equals("Modifier")) {
                    part = request.getPart("fichier");
                    file = getFileName(part);
                    idpub = Integer.parseInt(request.getParameter("idPub"));
                } else if (action.equals("Supprimer")) {
                    idpub = Integer.parseInt(request.getParameter("idPub"));
                }

                if (file != null && !file.isEmpty()) {
                    ControllerFile controllerFilepub = new ControllerFile(file);
                    if (getTailleFichier(part) != null) {
                        String fichier = getTailleFichier(part);//recupere la taille du fichier
                        String EXT = controllerFilepub.getFileExt(file);
                        //Compte user = (Compte) session.getAttribute("useradmin");
                        listOfExtensions.add(".pdf");
                        listOfExtensions.add(".docx");
                        listOfExtensions.add(".doc");
                        listOfExtensions.add(".xlsx");
                        listOfExtensions.add(".xls");
                        listOfExtensions.add(".pptx");
                        listOfExtensions.add(".ppt");
                        if (listOfExtensions.contains(EXT)) {
                            if (EXT.equals(".pdf")) {
                                icone = "pdf.jpg";
                            } else if (EXT.equals(".docx") || EXT.equals(".doc")) {
                                icone = "word.png";
                            } else if (EXT.equals(".xlsx") || EXT.equals(".xls")) {
                                icone = "excel.png";
                            } else if (EXT.equals(".pttx") || EXT.equals(".ppt")) {
                                icone = "powerpoint.png";
                            }

                            if (action.equals("Ajouter")) {
                                pub = new Publication(fichier, annee, statut, telecharger, libelle, contenu, controllerFilepub.uploadFile(request, part, DIR_PUBLICATION, mydate + "-PUB" + sBpublication.getLASTPublication() + EXT, "fichier"), icone, new Date(), new Date());
                                sBpublication.AjouterPublication(pub, idtype, user.getIdCompte());
                                request.setAttribute("val", 1);

                            } else if (action.equals("Modifier")) {
                                pub = sBpublication.getPublication(idpub);
                                File fil = new File(DIR_PUBLICATION + File.separator + File.separator + pub.getLien());
                                if (fil.delete()) {
                                    pub.setDatemodif(new Date());
                                    pub.setLibelle(libelle);
                                    pub.setContenu(contenu);
                                    pub.setIcone(icone);
                                    pub.setTaille(fichier);
                                    pub.setLien(controllerFilepub.uploadFile(request, part, DIR_PUBLICATION, mydate + "-PUB" + sBpublication.getLASTPublication() + EXT, "fichier"));     
                                    sBpublication.UpdatePublication(pub);
                                    request.setAttribute("val", 0);
                                } else {
                                    request.setAttribute("val", 6);
                                }
                            }
                        } else {
                            request.setAttribute("errorfile", "L'extension " + EXT + " du fichier n'est pas valide");
                            request.setAttribute("val", 2);
                        }
                    } else {
                        request.setAttribute("errortaille", "erreur la taille du fichier depace 5Mo");
                        request.setAttribute("val", 4);
                    }
                } else if (action.equals("Modifier")) {
                    pub = sBpublication.getPublication(idpub);
                    pub.setDatemodif(new Date());
                    pub.setLibelle(libelle);
                    pub.setContenu(contenu);
                    sBpublication.UpdatePublication(pub);
                    request.setAttribute("val", 0);

                } else if (action.equals("Supprimer")) {
                    pub = sBpublication.getPublication(idpub);
                    File filedel = new File(DIR_PUBLICATION + File.separator + File.separator + pub.getLien());
                    filedel.delete();
                    sBpublication.DeletePublication(idpub);
                    request.setAttribute("val", 3);

                }
                if (sBtypepublication.getTypePublication(idtype) != null) {
                    request.setAttribute("module", sBtypepublication.getTypePublication(idtype));
                }

                if (sBpublication.getAllPublicationCompteByType(idtype, user.getIdCompte()) != null) {
                    List<Publication> allpub = sBpublication.getAllPublicationCompteByType(idtype, user.getIdCompte());
                    request.setAttribute("allpubtype", allpub);
                }
                List<TypePublication> liste = sBtypepublication.getAllTypeAnyAnnonce();
                request.setAttribute("allmodule", liste);
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/type-publication")) {
                /**
                 * ********************************dossier***
                 */
                String action = request.getParameter("action");
                if (action.equals("Ajouter")) {
                    String libelle = request.getParameter("module");
                    //  Compte user = (Compte) session.getAttribute("useradmin");
                    String createur = user.getIdMembre().getCivilite() + " " + user.getIdMembre().getNom() + " " + user.getIdMembre().getPrenom();
                    String statut = "depublie";

                    module = new TypePublication(libelle, "dossier.png", createur, statut, new Date());
                    sBtypepublication.AjouterTypePublication(module);
                    request.setAttribute("ajoutmodule", libelle);
                    request.setAttribute("val", 1);
                    List<TypePublication> liste = sBtypepublication.getAllTypeAnyAnnonce();
                    request.setAttribute("allmodule", liste);

                } else if (action.equals("Modifier")) {
                    String libelle = request.getParameter("module");
                    String createur = request.getParameter("createur");
                    String statut = request.getParameter("statut");
                    int IDtype = Integer.parseInt(request.getParameter("idtype"));

                    module = new TypePublication(IDtype, libelle, "dossier.png", sBtypepublication.getTypePublication(IDtype).getCreateur(), sBtypepublication.getTypePublication(IDtype).getStatut(), new Date());
                    sBtypepublication.UpdateTypePublication(module);
                    request.setAttribute("updatemodule", libelle);
                    request.setAttribute("val", 2);
                    List<TypePublication> liste = sBtypepublication.getAllTypeAnyAnnonce();
                    request.setAttribute("allmodule", liste);

                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/gerer-role")) {
                String action = request.getParameter("action");
                if (action.equals("Ajouter")) {

                    String libelle = request.getParameter("profil");
                    profil = new Profil(libelle, new Date());
                    int idp = sBprofil.AjouterProfil(profil);

                    String[] Menuselected = request.getParameterValues("menu");
                    out.print(Menuselected);
                    for (String e : Menuselected) {
                        out.print("value= " + e);
                    }
                    sBmenuprofil.AjouterMenuProfil(idp, Menuselected);
//                          request.setAttribute("allm", Menuselected); 
                    request.setAttribute("ajoutprofil", libelle);
                    request.setAttribute("val", 1);
                    List<Profil> listeprofil = sBprofil.getAllProfil();
                    request.setAttribute("allprofil", listeprofil);

                } else if (action.equals("Modifier")) {
                    String libelle = request.getParameter("profil");
                    int IDprofil = Integer.parseInt(request.getParameter("idprofil"));
                    profil = new Profil(IDprofil, libelle, new Date());
                    sBprofil.UpdateProfil(profil);
                    request.setAttribute("updateprofil", libelle);
                    request.setAttribute("val", 2);
                    List<Profil> listeprofil = sBprofil.getAllProfil();
                    request.setAttribute("allprofil", listeprofil);

                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/gerer-slider")) {
                String action = request.getParameter("action");

                if (action.equals("Ajouter")) {
                    Part part = request.getPart("image");
                    String description = request.getParameter("description");
                    String repertoire = "slider";
                    String file = getFileName(part);

                    if (file != null && !file.isEmpty()) {
                        ControllerFile controllerGallerieLabo = new ControllerFile(file);
                        String EXT = controllerGallerieLabo.getFileExt(file);
                        if (getTailleFichier(part) != null) {
                            String fichier = getTailleFichier(part);
                            listOfExtensions.add(".jpg");
                            listOfExtensions.add(".png");
                            listOfExtensions.add(".JPG");
                            listOfExtensions.add(".PNG");
                            if (listOfExtensions.contains(EXT)) {

                                gallerie = new Image("depublie", controllerGallerieLabo.uploadFile(request, part, DIR_SLIDER, mydate + "-SLIDER" + sBgallerie.getLASTImage() + EXT, "image"), repertoire, description, EXT, fichier, new Date());
                                sBgallerie.AjouterImage(gallerie);
                                request.setAttribute("val", 1);
                            } else {
                                request.setAttribute("errorfile", "L'extension " + EXT + " du fichier n'est pas valide");
                                request.setAttribute("val", 3);
                            }
                        } else {
                            request.setAttribute("errortaille", "erreur la taille du fichier depace 5Mo");
                            request.setAttribute("val", 4);
                        }
                    }

                } else if (action.equals("Modifier")) {
                    Part part = request.getPart("image");
                    String description = request.getParameter("description");
                    String repertoire = "slider";
                    String file = getFileName(part);
                    int slider = Integer.parseInt(request.getParameter("slider"));

                    if (file != null && !file.isEmpty()) {
                        ControllerFile controllerGallerieLabo = new ControllerFile(file);
                        String EXT = controllerGallerieLabo.getFileExt(file);
                        if (getTailleFichier(part) != null) {
                            String fichier = getTailleFichier(part);
                            listOfExtensions.add(".jpg");
                            listOfExtensions.add(".png");
                            listOfExtensions.add(".JPG");
                            listOfExtensions.add(".PNG");
                            if (listOfExtensions.contains(EXT)) {
                                gallerie = new Image(slider, sBgallerie.getImage(slider).getStatut(), controllerGallerieLabo.uploadFile(request, part, DIR_SLIDER, mydate + "-SLIDER" + slider + EXT, "image"), repertoire, description, EXT, fichier, new Date());
                                sBgallerie.UpdateImage(gallerie);
                                request.setAttribute("val", 5);
                            } else {
                                request.setAttribute("errorfile", "L'extension " + EXT + " du fichier n'est pas valide");
                                request.setAttribute("val", 3);
                            }
                        } else {
                            request.setAttribute("errortaille", "erreur la taille du fichier depace 5Mo");
                            request.setAttribute("val", 4);
                        }
                    } else {
                        gallerie = new Image(slider, sBgallerie.getImage(slider).getStatut(), sBgallerie.getImage(slider).getLien(), repertoire, description, sBgallerie.getImage(slider).getExtension(), sBgallerie.getImage(slider).getTaille(), new Date());
                        sBgallerie.UpdateImage(gallerie);
                        request.setAttribute("val", 5);
                    }
                } else if (action.equals("Supprimer")) {
                    int IDslider = Integer.parseInt(request.getParameter("slider"));
                    File file = new File(DIR_SLIDER + File.separator + File.separator + sBgallerie.getImage(IDslider).getLien());
                    file.delete();
                    sBgallerie.DeleteImage(IDslider);
                    request.setAttribute("val", 2);
                }
                List<Image> GallerieSlider = sBgallerie.getAllImageSlider();
                request.setAttribute("allslider", GallerieSlider);
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/corbeille-compte")) {

            } else if (userPath.equals("/admin/groupe-commentaire")) {

            } else if (userPath.equals("/admin/liste-annonce")) {
                String action = request.getParameter("action");
                if (action.equals("Supprimer")) {
                    int ID = Integer.parseInt(request.getParameter("annonce"));
                    File file = new File(DIR_ANNONCE + File.separator + File.separator + sBpublication.getPublication(ID).getLien());
                    file.delete();
                    sBpublication.DeletePublication(ID);
                    request.setAttribute("val", 1);
                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/liste-archive")) {

            } else if (userPath.equals("/admin/liste-laboratoire")) {

            } else if (userPath.equals("/admin/liste-membre")) {

            } else if (userPath.equals("/admin/publier-newsletter")) {
                String action = request.getParameter("action");

                if (action.equals("Ajouter")) {
                    Part part = request.getPart("fichier");
                    String libelle = request.getParameter("libelle");
                    String icone = "";
                    String file = getFileName(part);

                    if (file != null && !file.isEmpty()) {
                        if (getTailleFichier(part) != null) {
                            ControllerFile controllerFileNews = new ControllerFile(file);
                            String EXT = controllerFileNews.getFileExt(file);
                            listOfExtensions.add(".pdf");
                            listOfExtensions.add(".docx");
                            listOfExtensions.add(".doc");
                            listOfExtensions.add(".xlsx");
                            listOfExtensions.add(".xls");
                            listOfExtensions.add(".pptx");
                            listOfExtensions.add(".ppt");
                            if (listOfExtensions.contains(EXT)) {
                                if (EXT.equals(".pdf")) {
                                    icone = "pdf.jpg";
                                } else if (EXT.equals(".docx") || EXT.equals(".doc")) {
                                    icone = "word.png";
                                } else if (EXT.equals(".xlsx") || EXT.equals(".xls")) {
                                    icone = "excel.png";
                                } else if (EXT.equals(".pttx") || EXT.equals(".ppt")) {
                                    icone = "powerpoint.png";
                                }
                                News = new Newsletter(libelle, controllerFileNews.uploadFile(request, part, DIR_NEWSLETTER, mydate + "-NEWS" + sBNewsletter.getLASTNewsletter() + "" + EXT, "fichier"), icone, "depublie", new Date());
                                sBNewsletter.AjouterNewsletter(News);
                                request.setAttribute("val", 1);
                                List<Newsletter> allnews = sBNewsletter.getAllNewsletter();
                                request.setAttribute("allnewsletter", allnews);
                            } else {
                                request.setAttribute("errorfile", "L'extension " + EXT + " du fichier n'est pas valide");
                                request.setAttribute("val", 3);
                                List<Newsletter> allnews = sBNewsletter.getAllNewsletter();
                                request.setAttribute("allnewsletter", allnews);
                            }
                        } else {
                            request.setAttribute("errortaille", "erreur la taille du fichier depace 5Mo");
                            request.setAttribute("val", 4);
                            List<Newsletter> allnews = sBNewsletter.getAllNewsletter();
                            request.setAttribute("allnewsletter", allnews);
                        }
                    }
                } else if (action.equals("Supprimer")) {
                    int ID = Integer.parseInt(request.getParameter("news"));
                    if (sBNewsletter.getNewsletter(ID) != null) {
                        File file = new File(DIR_NEWSLETTER + File.separator + File.separator + sBNewsletter.getNewsletter(ID).getLien());
                        if(file.delete()){
                             sBNewsletter.DeleteNewsletter(ID);
                       // response.sendRedirect("publier-newsletter");
                        request.setAttribute("val", 2);
                        }
                       
                    }
                }
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/extras")) {

                String action = request.getParameter("action");

                if (action.equals("apropos")) {
                    String contenu = request.getParameter("contenu");
                    String libelle = request.getParameter("libelle");
                    Apropos = new Presentation(1, libelle, contenu);
                    sBpresentation.UpdatePresentation(Apropos);
                    request.setAttribute("val", 1);
                } else if (action.equals("facebook")) {
                    String contenu = request.getParameter("contenu2");
                    sBpresentation.UpdateContenu(2, contenu);
                    request.setAttribute("val", 2);
                } else if (action.equals("googlemap")) {
                    String contenu = request.getParameter("contenu3");
                    sBpresentation.UpdateContenu(3, contenu);
                    request.setAttribute("val", 3);
                }
                request.setAttribute("Apropos", sBpresentation.getPresentation(1));
                request.setAttribute("facebook", sBpresentation.getPresentation(2));
                request.setAttribute("google", sBpresentation.getPresentation(3));
                Map MenusSidebar = sBmenuprofil.getallmenuProfil(user.getIdProfil().getIdProfil());
                request.setAttribute("MenusSidebar", MenusSidebar.get("menus"));

            } else if (userPath.equals("/admin/upload-laboratoire")) {
                Part part = request.getPart("image");
                String description = request.getParameter("description");
                String repertoire = "laboratoire";
                String file = getFileName(part);

                if (file != null && !file.isEmpty()) {
                    if (getTailleFichier(part) != null) {
                        String fichier = getTailleFichier(part);
                        ControllerFile controllerGallerieLabo = new ControllerFile(file);
                        String EXT = controllerGallerieLabo.getFileExt(file);
                        listOfExtensions.add(".jpg");
                        listOfExtensions.add(".png");
                        listOfExtensions.add(".JPG");
                        listOfExtensions.add(".PNG");
                        listOfExtensions.add(".GIF");
                        listOfExtensions.add(".gif");
                        if (listOfExtensions.contains(EXT)) {

                            gallerie = new Image("depublie", controllerGallerieLabo.uploadFile(request, part, DIR_LABO, mydate + "-LABO" + sBgallerie.getLASTImage() + EXT, "image"), repertoire, description, EXT, fichier, new Date());
                            sBgallerie.AjouterImage(gallerie);
                            response.sendRedirect("galerie-laboratoire");
                        } else {
                            response.sendRedirect("upload-laboratoire?0");
                        }
                    } else {
                        response.sendRedirect("upload-laboratoire?1");
                    }
                }

            } else if (userPath.equals("/admin/upload-annonce")) {
                Part part = request.getPart("image");
                String description = request.getParameter("description");
                String repertoire = "annonce";
                String file = getFileName(part);

                if (file != null && !file.isEmpty()) {
                    if (getTailleFichier(part) != null) {
                        String fichier = getTailleFichier(part);
                        ControllerFile controllerGallerieLabo = new ControllerFile(file);
                        String EXT = controllerGallerieLabo.getFileExt(file);
                        listOfExtensions.add(".jpg");
                        listOfExtensions.add(".png");
                        listOfExtensions.add(".JPG");
                        listOfExtensions.add(".PNG");
                        listOfExtensions.add(".GIF");
                        listOfExtensions.add(".gif");
                        if (listOfExtensions.contains(EXT)) {

                            gallerie = new Image("depublie", controllerGallerieLabo.uploadFile(request, part, DIR_ANNONCE_IMG, mydate + "-ANNONCE" + sBgallerie.getLASTImage() + EXT, "image"), repertoire, description, EXT, fichier, new Date());
                            sBgallerie.AjouterImage(gallerie);
                            response.sendRedirect("galerie-annonce");
                        } else {
                            response.sendRedirect("upload-annonce?0");
                        }
                    } else {
                        response.sendRedirect("upload-annonce?1");
                    }
                }

            } else if (userPath.equals("/admin/upload-presentation")) {
                Part part = request.getPart("image");
                String description = request.getParameter("description");
                String repertoire = "presentation";
                String file = getFileName(part);

                if (file != null && !file.isEmpty()) {
                    if (getTailleFichier(part) != null) {
                        String fichier = getTailleFichier(part);
                        ControllerFile controllerGallerieLabo = new ControllerFile(file);
                        String EXT = controllerGallerieLabo.getFileExt(file);
                        listOfExtensions.add(".jpg");
                        listOfExtensions.add(".png");
                        listOfExtensions.add(".JPG");
                        listOfExtensions.add(".PNG");
                        listOfExtensions.add(".GIF");
                        listOfExtensions.add(".gif");
                        if (listOfExtensions.contains(EXT)) {

                            gallerie = new Image("depublie", controllerGallerieLabo.uploadFile(request, part, DIR_PRESENTATION, mydate + "-PRESENTATION" + sBgallerie.getLASTImage() + EXT, "image"), repertoire, description, EXT, fichier, new Date());
                            sBgallerie.AjouterImage(gallerie);
                            response.sendRedirect("galerie-presentation");
                        } else {
                            response.sendRedirect("upload-presentation?0");
                        }
                    } else {
                        response.sendRedirect("upload-presentation?1");
                    }
                }
            }
        } else if (session.getAttribute("useradmin") == null) {
            if (userPath.equals("/admin/index")) {
                try{
                String login = request.getParameter("username");
                String pwd = request.getParameter("password");
                 if (login.equals(Auteur.user) && pwd.equals(Auteur.pwd)) {
                     session.setAttribute("useradmin", Auteur.getAuteur());
                      session.setAttribute("DIR_AVATAR", Adresse_serveur + "iuc/prie/admin/images/avatar");
                        session.setAttribute("DIR_ANNONCE", Adresse_serveur + "iuc/prie/admin/document/annonce");
                        session.setAttribute("DIR_ANNONCE_IMG", Adresse_serveur + "iuc/prie/admin/images/annonce");
                        session.setAttribute("DIR_ARCHIVE", Adresse_serveur + "iuc/prie/admin/document/archive");
                        session.setAttribute("DIR_COMMENTAIRE", Adresse_serveur + "iuc/prie/admin/document/commentaire");
                        session.setAttribute("DIR_GROUPE", Adresse_serveur + "iuc/prie/admin/images/groupe");
                        session.setAttribute("DIR_NEWSLETTER", Adresse_serveur + "iuc/prie/admin/document/newsletter");
                        session.setAttribute("DIR_PRESENTATION", Adresse_serveur + "iuc/prie/admin/images/presentation");
                        session.setAttribute("DIR_PUBLICATION", Adresse_serveur + "iuc/prie/admin/document/publications");
                        session.setAttribute("DIR_SLIDER", Adresse_serveur + "iuc/prie/admin/images/slider");
                        session.setAttribute("DIR_LABO", Adresse_serveur + "iuc/prie/admin/images/laboratoire");
                      response.sendRedirect("dashboard");
                      
                 }else{
                    Compte cpt = sBcompte.Connexion(login);
                    String pwddecrypt = Crypto.decrypt(cpt.getPwd());
                    if ((cpt != null) && pwd.equals(pwddecrypt)) {
                        session.setAttribute("useradmin", cpt);
                        session.setAttribute("DIR_AVATAR", Adresse_serveur + "iuc/prie/admin/images/avatar");
                        session.setAttribute("DIR_ANNONCE", Adresse_serveur + "iuc/prie/admin/document/annonce");
                        session.setAttribute("DIR_ANNONCE_IMG", Adresse_serveur + "iuc/prie/admin/images/annonce");
                        session.setAttribute("DIR_ARCHIVE", Adresse_serveur + "iuc/prie/admin/document/archive");
                        session.setAttribute("DIR_COMMENTAIRE", Adresse_serveur + "iuc/prie/admin/document/commentaire");
                        session.setAttribute("DIR_GROUPE", Adresse_serveur + "iuc/prie/admin/images/groupe");
                        session.setAttribute("DIR_NEWSLETTER", Adresse_serveur + "iuc/prie/admin/document/newsletter");
                        session.setAttribute("DIR_PRESENTATION", Adresse_serveur + "iuc/prie/admin/images/presentation");
                        session.setAttribute("DIR_PUBLICATION", Adresse_serveur + "iuc/prie/admin/document/publications");
                        session.setAttribute("DIR_SLIDER", Adresse_serveur + "iuc/prie/admin/images/slider");
                        session.setAttribute("DIR_LABO", Adresse_serveur + "iuc/prie/admin/images/laboratoire");
                        sBcompte.UpdateConnecte(cpt.getIdCompte());
                        response.sendRedirect("dashboard");
                    } else {
                        request.setAttribute("errorsms", "Username ou Password Incorrect!!!");
                        request.setAttribute("errorval", 0);
                    }
                 }
                }catch(Exception ex){
                    request.setAttribute("errorsms", "Password Inccorect!!!");
                    request.setAttribute("errorval", 0);
                }
            } else if (userPath.equals("/admin/dashboard")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/ajouter-membre")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/profil-btn-menu")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/ajouter-annonce")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/ajouter-laboratoire")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/ajouter-categorie")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/creer-groupe")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/editer-profile")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/edit-profil")) {
                response.sendRedirect("index");

            } else if (userPath.equals("/admin/gerer-compte")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/gerer-publication")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/gerer-role")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/gerer-slider")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/corbeille-compte")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/groupe-commentaire")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/liste-annonce")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/liste-archive")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/liste-laboratoire")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/liste-membre")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/publier-newsletter")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/extras")) {
                response.sendRedirect("index");
            } else if (userPath.equals("/admin/gerer-compte-membre")) {
                response.sendRedirect("index");
            }

        }
        String url = "/WEB-INF/" + userPath + ".jsp";
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getFileName(Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("******* partHeader******" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    public String getTailleFichier(Part part) {
        int size = (int) (part.getSize() / 1024) + 1;
        if (size > 1024) {
            int x = size / 1024;
            if (x <= 5) {
                return (x) + " Mo";
            } else {
                return null;
            }

        } else {
            return size + " ko";
        }
    }

}
