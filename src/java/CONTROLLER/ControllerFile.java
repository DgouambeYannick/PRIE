/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLER;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.swing.JFileChooser;

/**
 *
 * @author HP
 */
public class ControllerFile {

    private File file = null;

    public ControllerFile(String filename) {
        this.file = new File(filename);
    }

    /**
     * Lire la date du fichier
     *
     * @return
     */
    public long getDate() {
        return this.file.lastModified();
    }

    /**
     * Afficher la date du fichier
     *
     * @return date au format dd/m/yyyy H:mm:ss
     */
    public String getFormatedDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy H:mm:ss");
        Date d = new Date(this.file.lastModified());
        return sdf.format(d);
    }

    /**
     * Taille du fichier
     *
     * @return unité byte
     */
    public Long getSize() {
        return this.file.length();
    }

    /**
     * Afficher la taille
     *
     * @return taille au format xx Ko ou xx Mo
     */
    public String getFormatedSize() {
        int size = (int) (this.file.length() / 1024) + 1;
        if (size > 1024) {
            return (size / 1024) + " Mo";
        } else {
            return size + " ko";
        }
    }

    /**
     * Type du fichier
     *
     * @return par exemple Image bitmap
     */
    public String getType() {
        JFileChooser chooser = new JFileChooser();
        return chooser.getTypeDescription(this.file);
    }

    /**
     * Extraire l'extension du fichier
     *
     * @param filename
     * @return format .xxx
     */
    public static String getFileExt(String filename) {
        int pos = filename.lastIndexOf(".");
        if (pos > -1) {
            return filename.substring(pos);
        } else {
            return filename;
        }
    }

    /* gestion des fichiers*/
    public String uploadFile(HttpServletRequest request, Part filePart, String CHEMIN, String ID, String test) {
        String filename = "";
        try {
            // Part filePart = request.getPart("imageP"); // filePart;
            if (test.equals("image")) {
                filename = "IMG_PRIE_" + ID;
            } else {
                filename = "FILE_PRIE_" + ID;
            }
//            String applicationPath = request.getServletContext().getRealPath("");
//             System.out.print("application path "+applicationPath);
//            String basePath = applicationPath + File.separator + CHEMIN + File.separator;
//            System.out.print("base path "+basePath);

            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                File outputFilePath = new File(CHEMIN + File.separator + filename);
                System.out.print("outputFilePath= "+outputFilePath);
                inputStream = filePart.getInputStream();
                outputStream = new FileOutputStream(outputFilePath);
                int read = 0;
                final byte[] bytes = new byte[1024];
                while ((read = inputStream.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, read);
                }
            } catch (Exception e) {
                e.printStackTrace();
                filename = "";
            } finally {
                if (outputStream != null) {
                    outputStream.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            }
        } catch (Exception e) {
            filename = "";
            e.printStackTrace();
        }
        return filename;
    }

}
