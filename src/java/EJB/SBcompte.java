/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;


import BEANS.CompteDTO;
import CONTROLLER.Crypto;
import ENTITY.Compte;
import ENTITY.Groupe;
import ENTITY.Membre;
import ENTITY.Profil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SBcompte {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;
    @Resource
    private SessionContext context;
    
    public List<Compte> getAllCompte(){
        List<Compte> req = em.createNamedQuery("Compte.findAll").getResultList();
        return req;
    }
    public List<CompteDTO> getAllCompteDTO(){
        List<CompteDTO> liste = new ArrayList<>();
        List<Compte> req = em.createNamedQuery("Compte.findAll").getResultList();
        for (Compte comp : req) {
            CompteDTO c = new CompteDTO();
            c.setPassword(Crypto.decrypt(comp.getPwd()));
            c.setLogin(comp.getLogin());
            c.setCivilite(comp.getIdMembre().getCivilite());
            c.setNom(comp.getIdMembre().getNom());
            c.setPrenom(comp.getIdMembre().getPrenom());
            c.setPhoto(comp.getIdMembre().getImage());
            c.setIdmemb(comp.getIdMembre().getIdMembre());
            c.setIdCompte(comp.getIdCompte());
            c.setDatemodif(comp.getDatemodif());
            c.setStatut(comp.getStatut());
            c.setRole(comp.getIdProfil().getLibelle());
            c.setIdrole(comp.getIdProfil().getIdProfil());
            liste.add(c);
        }
        return liste;
    }
    
    public List<Compte> getAllCompte2(int compte){
        Query req = em.createNamedQuery("Compte.findCompteAnyCreateur").setParameter("user", compte);
        return req.getResultList();
    }

    
    public Compte getCompteM(int m){
        try{
        Compte c = (Compte) em.createNamedQuery("Compte.findByCompteM").setParameter("id", m).getSingleResult();
        return c;
        }catch(Exception e){
            return null;
        }
    }
       public List<Compte> getAllCompteConnecte(int labo){
        Query req = em.createNamedQuery("Compte.findByOnline").setParameter("online", "oui").setParameter("labo", labo);
        return req.getResultList();
     }
     public List<Compte> getAllCompteDeconnecte(int labo){
        Query req = em.createNamedQuery("Compte.findByOnline").setParameter("online", "non").setParameter("labo", labo);
        return req.getResultList();
    }
       
    public void DeleteCompte(int IDC){
        Compte CPT = getCompte(IDC);
        em.remove(CPT);
    }
    
    public void UpdateCompte(int p, int m, Compte C){
        C.setIdProfil(em.find(Profil.class, p));
        C.setIdMembre(em.find(Membre.class, m));
        em.merge(C);
    }
    
    public void UpdateStatutCompte(int c){
        Compte C = getCompte(c);
        C.setDatemodif(new Date());
        String statut = C.getStatut();
        if(statut.equals("active")){
            statut = "desactive";
        }else{
            statut = "active";
        }
        C.setStatut(statut);
        em.merge(C);
    }
    
     public void UpdateConnecte(int c){
        Compte C = getCompte(c);
        C.setDatemodif(new Date());
        String online = C.getOnline();
            online = "oui";
        C.setOnline(online);
        em.merge(C);
    }
     
     public void Updatedeconnecte(int c){
        Compte C = getCompte(c);
        C.setDatemodif(new Date());
        String online = C.getOnline();
            online = "non";
        C.setOnline(online);
        em.merge(C);
    }
     
    public void AjouterCompte(int p, int m, Compte C){
        C.setIdProfil(em.find(Profil.class, p));
        C.setIdMembre(em.find(Membre.class, m));
        em.persist(C);
    }
    public Compte getCompte(int IDc){
        Compte c = em.find(Compte.class, IDc);
        if(c == null)
        {
         return null;
        }else{
        return c;
        }
    }
   
   public String TestLogin(String v){
        String x="ok";
        try{
            Compte r = (Compte) em.createNamedQuery("Compte.findByLogin").setParameter("login", v).getSingleResult();
            if(r != null){
               x= null;
            }
            return x;
        }catch(Exception e){
            return x;    
        }
    }
    public Compte Connexion(String user){
        try{
           return (Compte) em.createNamedQuery("Compte.login").setParameter("username", user).setParameter("statut", "active").getSingleResult();
            
        }catch(Exception e){
            return null;
        }
    }
    
    public void persist(Object object) {
        em.persist(object);
    }

    
}
