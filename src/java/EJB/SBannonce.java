/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Compte;
import ENTITY.Publication;
import ENTITY.TypePublication;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author HP
 */
@Stateless
public class SBannonce {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

      public Publication AjouterAnnonce(Publication p, int idcompte){
        p.setIdType(em.find(TypePublication.class, 1));
        p.setIdCompte(em.find(Compte.class, idcompte));
        em.persist(p);
        
        return p;
    }
      
    public List<Publication> getAllAnnonces(){
        try{
        List<Publication> Allannonce = em.createNamedQuery("Publication.findByALLannonce").setParameter("idannonce", 1).getResultList();      
        return Allannonce;
       }catch(Exception e){
           return null;
       }
    }
    
     public List<Publication> getAll5Annonces(){
        try{
        List<Publication> Firstannonce =  new ArrayList<>();
        List<Publication> Allannonce = getAllAnnonces();
        for (Publication p : Allannonce) {
             Firstannonce.add(p);
            if(Firstannonce.size() > 4 ){
                
               break;
            }
        }
        return Firstannonce;
        
       }catch(Exception e){
           return null;
       }
    }
}
