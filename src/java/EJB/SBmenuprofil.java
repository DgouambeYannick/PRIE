/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Menu;
import ENTITY.MenuProfil;
import ENTITY.MenuProfilPK;
import ENTITY.Profil;
import static com.sun.faces.facelets.util.Path.context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SBmenuprofil {

    @EJB
    private SBprofil sBprofil;

    @EJB
    private SBmenu sBmenu;
    
    
    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;
    
    @Resource
    private SessionContext context;
    
 
     public void AjouterMenuProfil(int p, String[] menu){
         try{
         
         for (String m : menu) {
             MenuProfilPK mpk = new MenuProfilPK();
             mpk.setIdMenu(Integer.parseInt(m));
             mpk.setIdProfil(p);
             MenuProfil mp = new MenuProfil(mpk,0);
             em.persist(mp);
         }
       } catch (Exception e) {
         
            e.printStackTrace();
        }
    }
     
        public Map getallmenurestant(int IDprofil){
        try{
         Map allm = new HashMap();
         List<Menu> allmenuP = new ArrayList<>();
         List<Menu> allmenu = sBmenu.getAllMenu();
        List<MenuProfil> allmenuprofil = em.createNamedQuery("MenuProfil.findByIdProfil").setParameter("idProfil", IDprofil).getResultList();
         for (MenuProfil liste : allmenuprofil) {
             Menu m = (Menu) sBmenu.getMenu(liste.getMenuProfilPK().getIdMenu());
             allmenuP.add(m);
         }
         
         for (Menu liste2 : allmenuP) {
             allmenu.remove(liste2);
         }
         allm.put("menurestant", allmenu);
         return allm;
        }catch(Exception e){
             
             return null;
        }
    }
    
  
    public Map getallmenuProfil(int IDprofil){
       try{
           Map allmp = new HashMap();
        Profil p = sBprofil.getProfil(IDprofil);
         List<Menu> allmenuP = new ArrayList<>();
        List<MenuProfil> allmenuprofil = em.createNamedQuery("MenuProfil.findByIdProfil").setParameter("idProfil", IDprofil).getResultList();
         for (MenuProfil liste : allmenuprofil) {
             Menu m = (Menu) sBmenu.getMenu(liste.getMenuProfilPK().getIdMenu());
             allmenuP.add(m);
         }
         
          allmp.put("profil", p);
           allmp.put("allmenuprofil", allmenuprofil);   
            allmp.put("menus", allmenuP);
         return allmp;
       }catch(Exception e){
           
             return null;
       }
    }
     
    public void DeleteMenuProfil(int p, int m){
        MenuProfil mp = (MenuProfil) em.createNamedQuery("MenuProfil.findByMenuProfil").setParameter("idMenu", m).setParameter("idProfil", p).getSingleResult();
        em.remove(mp);
    }
    
    public List<MenuProfil> getAllMenuProfil(int p){
        Query req = em.createNamedQuery("MenuProfil.findByIdProfil").setParameter("idProfil", p);
        return req.getResultList();
    }
    
    public void persist(Object object) {
        em.persist(object);
    }

   
}
