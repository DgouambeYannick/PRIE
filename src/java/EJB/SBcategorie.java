/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Categorie;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBcategorie {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    
    public List <Categorie> getAllCategorie(){
        Query req = em.createNamedQuery("Categorie.findAll");
        return req.getResultList();
    }
    
    
     public void DeleteCategorie(int ID){
        Categorie m = getCategorie(ID);
        em.remove(m);
    }
    
    public void UpdateCategorie(Categorie t){
        em.merge(t);
    }
    
    public void AjouterCategorie(Categorie t){
        
        em.persist(t);
    }
    
    public Categorie getCategorie(int ID){
        Categorie m = em.find(Categorie.class, ID);
        if(m == null)
        {
         return null;
        }else{
        return m;
        }
    }
    
}
