/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.TypePublication;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBtypepublication {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    public List<TypePublication> getAllTypePublication(){
        Query req = em.createNamedQuery("TypePublication.findAll");
        return req.getResultList();
    }
    
    public List<TypePublication> getAllTypeAnyAnnonce(){
        Query req = em.createNamedQuery("TypePublication.findByIdType").setParameter("idType", 1).setParameter("Type", 2);
        return req.getResultList();
    }
    
     public void DeleteTypePublication(int ID){
        TypePublication m = getTypePublication(ID);
        em.remove(m);
    }
    
    public void UpdateTypePublication(TypePublication t){
        em.merge(t);
    }
    
    public void UpdateStatutTYPEPUB(int id){
        TypePublication t = getTypePublication(id);
        t.setDatemodif(new Date());
        String statut = t.getStatut();
        if(statut.equals("publie")){
            statut = "depublie";
        }else{
            statut = "publie";
        }
        t.setStatut(statut);
        em.merge(t);
    }
    
    public void AjouterTypePublication(TypePublication t){
        em.persist(t);
    }
    
    public TypePublication getTypePublication(int ID){
        TypePublication m = em.find(TypePublication.class, ID);
        if(m == null)
        {
         return null;
        }else{
        return m;
        }
    }
    
}
