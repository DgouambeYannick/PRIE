/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Compte;
import ENTITY.Document;
import ENTITY.Repertoire;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBdocument {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

     
    public List <Document> getAllDocument(){
        Query req = em.createNamedQuery("Document.findAll");
        return req.getResultList();
    }
    public List <Document> getAllDocumentREP(int rep){
        Query req = em.createNamedQuery("Document.findAllDOCREP").setParameter("rep", rep);
        return req.getResultList();
    }
    
    public int getLASTDocument(){
        try{
        int lastDOC = (int) em.createNamedQuery("Document.findLASTID").getSingleResult();
        return lastDOC +1;
        }catch(Exception e){
            return 0;
        }
    }
    
      public void Updatelibelle(int s, String libelle, String user){
        Document d = getDocument(s);
        d.setLibelle(libelle);
        d.setDatemodif(new Date());
         d.setModificateur(user);
        em.merge(d);
    }
    public void UpdateVersion(int s, int version, String user){
        Document d = getDocument(s);
        d.setVersion(version);
        em.merge(d);
    }
    
     public String TestLien(String v){
        String x="ok";
        try{
            Document r = (Document) em.createNamedQuery("Document.findByLien").setParameter("lien", v).getSingleResult();
            if(r != null){
               x= null;
            }
            return x;
        }catch(Exception e){
            return x;    
        }
    }
     public Document getLienExistant(String v){
        
        try{
            Document r = (Document) em.createNamedQuery("Document.findByLien").setParameter("lien", v).getSingleResult();     
            return r;
        }catch(Exception e){
            return null;    
        }
    }
     
     public void DeleteDocument(int ID){
        Document r = getDocument(ID);
        em.remove(r);
    }
    
    public void UpdateDocument(Document r){
        em.merge(r);
    }
    
    public void AjouterDocument(Document r ,int dossier, int compte){
        r.setIdRep(em.find(Repertoire.class, dossier));
        r.setIdCompte(em.find(Compte.class, compte));
        em.persist(r);
    }
    public Document getDocument(int ID){
        Document m = em.find(Document.class, ID);
        if(m == null)
        {
         return null;
        }else{
        return m;
        }
    }
}
