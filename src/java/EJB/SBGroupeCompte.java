/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Compte;
import ENTITY.CompteGroupe;
import ENTITY.CompteGroupePK;
import ENTITY.Groupe;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author HP
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SBGroupeCompte {

    @EJB
    private SBgroupe sBgroupe;

    @EJB
    private SBcompte sBcompte;

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

        @Resource
    private SessionContext context;
    
     @TransactionAttribute(TransactionAttributeType.REQUIRED)
     public void AjouterGroupeCompte(int g, String[] compte){
         try{
         
         for (String c : compte) {
             CompteGroupePK cgk = new CompteGroupePK();
             cgk.setIdCompte(Integer.parseInt(c));
             cgk.setIdGroupe(g);
             CompteGroupe cg = new CompteGroupe(cgk,"En-cours");
             em.persist(cg);
         }
       } catch (Exception e) {
           context.getRollbackOnly();
            e.printStackTrace();
        }
    }
     
         public Map getallCompterestant(int groupe, int compte){
        try{
            Map allcomp = new HashMap();
            List<Compte> allCompteG = new ArrayList<>();
            List<Compte> allcompte = sBcompte.getAllCompte2(compte);
            //em.flush();
           List<CompteGroupe> allCompteGroupe = em.createNamedQuery("CompteGroupe.findByIdGroupe").setParameter("idGroupe", groupe).getResultList();
            for (CompteGroupe liste : allCompteGroupe) {
                Compte c =(Compte)sBcompte.getCompte(liste.getCompteGroupePK().getIdCompte());
                allCompteG.add(c);
            }

            for (Compte liste2 : allCompteG) {
                allcompte.remove(liste2);
            }
            
            allcomp.put("compterestant", allcompte);
            return allcomp;
         
        }catch(Exception e){
             return null;
        }
     }
   
    public Map getallCompteGroupe(int groupe){
       try{
        Map CompteMap = new HashMap();
        Groupe group = sBgroupe.getGroupe(groupe);

       List<CompteGroupe> allCompteGroupe = em.createNamedQuery("CompteGroupe.findByIdGroupe").setParameter("idGroupe", groupe).getResultList();
       List<Compte> allCompte = new ArrayList<>();
           
         for (CompteGroupe liste : allCompteGroupe) {
             Compte c =(Compte)sBcompte.getCompte(liste.getCompteGroupePK().getIdCompte());
             allCompte.add(c);
         }
        
         CompteMap.put("groupe", group);
         CompteMap.put("allcomptegroupe", allCompteGroupe);
         CompteMap.put("comptes", allCompte); //allCompte.iterator().next()
 
         return CompteMap;
         
       }catch(Exception e){
             return null;
       }
    
    }
    
    public void UpdateInvitation(int g, int c){
        CompteGroupe cg = (CompteGroupe) em.createNamedQuery("CompteGroupe.findCompteGroupe").setParameter("idCompte", c).setParameter("idGroupe", g).getSingleResult();
        cg.setStatutInvitation("Accepte");
        em.merge(cg);
    }
    
    public void DeleteGroupeCompte(int g, int c){
        CompteGroupe cg = (CompteGroupe) em.createNamedQuery("CompteGroupe.findCompteGroupe").setParameter("idCompte", c).setParameter("idGroupe", g).getSingleResult();
        em.remove(cg);
    }
     
    public void persist(Object object) {
        em.persist(object);
    }

    
}
