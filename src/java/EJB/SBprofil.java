/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;


import ENTITY.Menu;
import ENTITY.Profil;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBprofil {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

       public List<Profil> getAllProfil(){
        Query req = em.createNamedQuery("Profil.findAll");
        return req.getResultList();
    }
       public List<Menu> getAllProfilMenu(int profil){
        Query req = em.createNamedQuery("Profil.findAll");
        return req.getResultList();
    }
    
    public void DeleteProfil(int ID){
        Profil p = getProfil(ID);
        em.remove(p);
    }
    
    public void UpdateProfil(Profil p){
        em.merge(p);
    }
    
    public int AjouterProfil(Profil p){
        em.persist(p);
        em.flush();
        int lastp = (int) em.createNamedQuery("Profil.findLASTID").getSingleResult();
        return lastp;
    }
    public Profil getProfil(int ID){
        Profil p = em.find(Profil.class, ID);
        if(p == null)
        {
         return null;
        }else{
        return p;
        }
    }
    
    public void persist(Object object) {
        em.persist(object);
    }

    
}
