/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Compte;
import ENTITY.Publication;
import ENTITY.TypePublication;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBpublication {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    public List<Publication> getAllPublication(){
        Query req = em.createNamedQuery("Publication.findAll");
        return req.getResultList();
    }
    
    public List<Publication> getAllPublicationCompteByType(int type, int compte){
        try{
        Query req = em.createNamedQuery("Publication.findByPUBCOMPTE").setParameter("type", type).setParameter("compte",compte);
        return req.getResultList();
        }catch(Exception e){
            return null;
        }
    }
    
     public List<Publication> getAllPublicationByType(int type){
        try{
        Query req = em.createNamedQuery("Publication.findByTYPE").setParameter("type", type);
        return req.getResultList();
        }catch(Exception e){
            return null;
        }
    }

    public int getLASTPublication(){
        try{
        int lastm = (int) em.createNamedQuery("Publication.findLASTID").getSingleResult();
        return lastm +1;
        }catch(Exception e){
            return 0;
        }
    }

    public void DeletePublication(int ID){
        Publication pub = getPublication(ID);
        em.remove(pub);
    }
    
    public void UpdatePublication(Publication p){
        em.merge(p);
    }
    
    public void UpdateStatutPUB(int s){
        Publication p = getPublication(s);
        p.setDatemodif(new Date());
        String statut = p.getStatut();
        if(statut.equals("publie")){
            statut = "depublie";
        }else{
            statut = "publie";
        }
        p.setStatut(statut);
        em.merge(p);
    }
    
    public void UpdateStatutDownload(int s){
        Publication p = getPublication(s);
        p.setDatemodif(new Date());
        String download = p.getTelecharger();
        if(download.equals("non")){
            download = "oui";
        }else{
            download = "non";
        }
        p.setTelecharger(download);
        em.merge(p);
    }
    
    public Publication AjouterPublication(Publication p, int idtype, int idcompte){
        p.setIdType(em.find(TypePublication.class, idtype));
        p.setIdCompte(em.find(Compte.class, idcompte));
        em.persist(p);
        
        return p;
    }
    
    public Publication getPublication(int ID){
        Publication memb = em.find(Publication.class, ID);
        if(memb == null)
        {
         return null;
        }else{
        return memb;
        }
    }
    
}
