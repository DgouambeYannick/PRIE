/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Laboratoire;
import ENTITY.Thematique;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBtheme {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }
    
    public List<Thematique> getAllThematique(){
        Query req = em.createNamedQuery("Thematique.findAll");
        return req.getResultList();
    }
    
    public List<Thematique> getAllThematiqueLabo(int labo){
        Query req = em.createNamedQuery("Thematique.findAllThemeLabo").setParameter("labo", labo);
        return req.getResultList();
    }
    
    public void DeleteThematique(int ID){
        Thematique p = getThematique(ID);
        em.remove(p);
    }
    
     public void UpdateStatut(int s){
        Thematique n = getThematique(s);
        n.setDatecreate(new Date());
        String statut = n.getStatut();
        if(statut.equals("publie")){
            statut = "depublie";
        }else{
            statut = "publie";
        }
        n.setStatut(statut);
        em.merge(n);
    }
    public void UpdateThematique(Thematique p, int labo){
        p.setIdLabo(em.find(Laboratoire.class, labo));
        em.merge(p);
    }
    
    public void AjouterThematique(Thematique p, int labo){
        p.setIdLabo(em.find(Laboratoire.class, labo));
        em.persist(p);
    }
    public Thematique getThematique(int ID){
        Thematique t = em.find(Thematique.class, ID);
        if(t == null)
        {
         return null;
        }else{
        return t;
        }
    }
}
