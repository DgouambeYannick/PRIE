/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Newsletter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBNewsletter {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }
    public int getLASTNewsletter(){
        try{
        int lastNews =  (int) em.createNamedQuery("Newsletter.findLASTID").getSingleResult();
        return lastNews +1;
        }catch(Exception e){
            return 0;
        }
        
    }
    
     public void UpdateStatutNews(int s){
        Newsletter n = getNewsletter(s);
        n.setDatecreate(new Date());
        String statut = n.getStatut();
        if(statut.equals("publie")){
            statut = "depublie";
        }else{
            statut = "publie";
        }
        n.setStatut(statut);
        em.merge(n);
    }
     

    public List<Newsletter> getAllNewsletter(){
        Query req = em.createNamedQuery("Newsletter.findAll");
        return req.getResultList();
    }
    
    public Newsletter getFirstNews(){
        try{
       int x = (int) em.createNamedQuery("Newsletter.findFIRST").setParameter("statut", "publie").getSingleResult();
       return getNewsletter(x);
        }catch(Exception e){
            return null;
        }
    }
        
    public void DeleteNewsletter(int ID){
        Newsletter n = getNewsletter(ID);
        em.remove(n);
    }
     
    public void AjouterNewsletter(Newsletter n){
        em.persist(n);
    }
    
    public Newsletter getNewsletter(int ID){
        Newsletter i = em.find(Newsletter.class, ID);
        if(i == null)
        {
        return null;
        }else{
        return i;
        }
    }
}
