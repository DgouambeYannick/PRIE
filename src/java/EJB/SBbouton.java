/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Bouton;
import ENTITY.BoutonProfil;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBbouton {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public List<Bouton> getAllBouton(){
        Query req = em.createNamedQuery("Bouton.findAll");
        return req.getResultList();
    }
    
     public List<Bouton> getallBoutonrestant(int IDprofil){
         List<Bouton> allBoutonP = new ArrayList<>();
         List<Bouton> allBouton = getAllBouton();
        List<BoutonProfil> allBoutonprofil = em.createNamedQuery("BoutonProfil.findByIdProfil").setParameter("idProfil", IDprofil).getResultList();
         for (BoutonProfil liste : allBoutonprofil) {
             allBoutonP.add(liste.getBouton());
         }
         
         for (Bouton liste2 : allBoutonP) {
             allBouton.remove(liste2);
         }
 
         return allBouton;
    }
    
    public void AjouterCompte(Bouton C){
        em.persist(C);
    }
     
    public Bouton getBouton(int ID){
        Bouton b = em.find(Bouton.class, ID);
        if(b == null)
        {
         return null;
        }else{
        return b;
        }
    }
   
    public void persist(Object object) {
        em.persist(object);
    }

    
}
