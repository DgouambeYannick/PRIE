/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Presentation;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author HP
 */
@Stateless
public class SBpresentation {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    public void UpdatePresentation(Presentation p){
        em.merge(p);
    }
    
    public void UpdateContenu(int p, String contenu){
        Presentation pres = getPresentation(p);
        pres.setContenu(contenu);
        em.merge(pres);
    }
     public Presentation getPresentation(int ID){
        Presentation memb = em.find(Presentation.class, ID);
        if(memb == null)
        {
         return null;
        }else{
        return memb;
        }
    }
}
