/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Groupe;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBgroupe {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    public int AjouterGroupe(Groupe g) {
        em.persist(g);
        em.flush();
        int lastg = (int) em.createNamedQuery("Groupe.findLASTID").getSingleResult();
        return lastg;
    }

    public List<Groupe> getAllGroupeByUser(String user) {
        Query req = em.createNamedQuery("Groupe.findByCreateur").setParameter("createur", user);
        return req.getResultList();
    }

    public Groupe getGroupe(int ID) {
        Groupe g = em.find(Groupe.class, ID);
        if (g == null) {
            return null;
        } else {
            return g;
        }
    }

    public boolean DeleteGroupe(int ID) {
        try {
            Groupe g = getGroupe(ID);
            em.remove(g);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void UpdateGroupe(Groupe g) {
        em.merge(g);
    }

    public int getLASTGroupe() {
        try {
            int lastg = (int) em.createNamedQuery("Groupe.findLASTID").getSingleResult();
            return lastg + 1;
        } catch (Exception e) {
            return 0;
        }
    }
}
