/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Image;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBgallerie {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }
    
    public int getLASTImage(){
        try{
        int lastimg = (int) em.createNamedQuery("Image.findLASTID").getSingleResult();
        return lastimg +1;
        }catch(Exception e){
            return 0;
        }
    }
    
    public void UpdateStatut(int s){
        Image n = getImage(s);
        n.setDatecreate(new Date());
        String statut = n.getStatut();
        if(statut.equals("publie")){
            statut = "depublie";
        }else{
            statut = "publie";
        }
        n.setStatut(statut);
        em.merge(n);
    }
      
     public List<Image> getAllImage(){
        Query req = em.createNamedQuery("Image.findAll");
        return req.getResultList();
    }
     
     public List<Image> getAllImageLabo(){
        Query req = em.createNamedQuery("Image.findByNamerepertoire").setParameter("namerepertoire", "laboratoire");
        return req.getResultList();
    }
     
     public List<Image> getAllImageSlider(){
        Query req = em.createNamedQuery("Image.findByNamerepertoire").setParameter("namerepertoire", "slider");
        return req.getResultList();
    }
     public List<Image> getAllImageSliderPublie(){
        Query req = em.createNamedQuery("Image.findBySliderPost").setParameter("namerepertoire", "slider").setParameter("statut", "publie");
        return req.getResultList();
    }
     
     public List<Image> getAllImageAnnonce(){
        Query req = em.createNamedQuery("Image.findByNamerepertoire").setParameter("namerepertoire", "annonce");
        return req.getResultList();
    }
     public List<Image> getAllImagePresentation(){
        Query req = em.createNamedQuery("Image.findByNamerepertoire").setParameter("namerepertoire", "presentation");
        return req.getResultList();
    }
     
     public void DeleteImage(int ID){
        Image i = getImage(ID);
        em.remove(i);
    }
     
      public void AjouterImage(Image i){
        em.persist(i);
    }
      
    public void UpdateImage(Image i){
        em.merge(i);
    }
    
    public Image getImage(int ID){
        Image i = em.find(Image.class, ID);
        if(i == null)
        {
        return null;
        }else{
        return i;
        }
    }
}
