/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import BEANS.SearchDTO;
import ENTITY.Laboratoire;
import ENTITY.Membre;
import ENTITY.Publication;
import ENTITY.Thematique;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBmembre {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    ////***************search--------------------------BEGIN---------------------------------
    public List<Membre> searchNomMembre(String val) {
        List<Membre> req = em.createQuery("SELECT a FROM Membre a WHERE a.nom LIKE :val").setParameter("val", "%"+val+"%").getResultList();
        return req;
    }

    public List<Membre> searchPrenomMembre(String val) {
        List<Membre> req = em.createQuery("SELECT a FROM Membre a WHERE a.prenom LIKE :val").setParameter("val", "%" + val + "%").getResultList();
        return req;
    }

    public List<Thematique> searchTitreThematique(String val) {
        List<Thematique> req = em.createQuery("SELECT a FROM Thematique a WHERE a.statut =:stat AND a.titre LIKE :x").setParameter("x", "%" + val + "%").setParameter("stat", "publie").getResultList();
        return req;
    }

    public List<Laboratoire> searchTitreLaboratoire(String val) {
        List<Laboratoire> req = em.createQuery("SELECT a FROM Laboratoire a WHERE a.statut =:stat AND a.libelle LIKE :x").setParameter("x", "%" + val + "%").setParameter("stat", "publie").getResultList();
        return req;
    }

    public List<Publication> searchTitrePublication(String val) {
        List<Publication> req = em.createQuery("SELECT a FROM Publication a WHERE a.statut =:stat AND a.libelle LIKE :x").setParameter("x", "%" + val + "%").setParameter("stat", "publie").getResultList();
        return req;
    }

    public SearchDTO resultatSearch(String val) {
        SearchDTO recherch = new SearchDTO();
        List<Membre> allmem = new ArrayList<>();
        List<Membre> nom = searchNomMembre(val);
        List<Membre> pre = searchPrenomMembre(val);
        if (nom.size() > 0) {
            for (Membre membre : nom) {
                allmem.add(membre);
            }
            if (pre.size() > 0) {
                for (Membre memb : pre) {
                    if (!allmem.contains(memb)) {
                        allmem.add(memb);
                    }
                }
            } 
        }else if(pre.size() > 0){
            for (Membre membre : pre) {
                allmem.add(membre);
            }
        }
        recherch.setAllmembre(allmem);
        
        List<Thematique> them = searchTitreThematique(val);
        recherch.setAllthematique(them);
        
        List<Laboratoire> labo = searchTitreLaboratoire(val);
        recherch.setAlllaboratoire(labo);
        
        List<Publication> pub = searchTitrePublication(val);
        recherch.setAllpublication(pub);
        
        return recherch;
    }
    ////***************search----------------FIN-------------------------------------------

    public List<Membre> getAllMembre() {
        Query req = em.createNamedQuery("Membre.findAll");
        return req.getResultList();
    }

    public List<Membre> getAllMembre2() {
        Query req = em.createNamedQuery("Membre.findMEMBRESANSCOMPTE");
        return req.getResultList();
    }

    public List<Membre> getAllMembreLabo(int x) {
        try {
            Query req = em.createNamedQuery("Membre.findByMembreLabo").setParameter("labo", x);
            return req.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public Membre getResponsableLABO(int idlabo) {
        try {
            Membre responsable = (Membre) em.createNamedQuery("Membre.findByResponsable").setParameter("responsable", "responsable").setParameter("labo", idlabo).getSingleResult();
            return responsable;
        } catch (Exception e) {
            return null;
        }
    }

    public Membre getMembre(String nameID) {
        try {
            Membre m = (Membre) em.createNamedQuery("Membre.findByNomid").setParameter("nomid", nameID).getSingleResult();
            return m;
        } catch (Exception e) {
            return null;
        }
    }

    public String TestExistEmail(String email) {
        String x = "ok";
        try {
            Membre r = (Membre) em.createQuery("SELECT m FROM Membre m WHERE m.email = :email ").setParameter("email", email).getSingleResult();
            if (r == null) {
                x = null;
            }
            return x;
        } catch (Exception e) {
            return null;
        }
    }

    public List<Membre> getAllResponsable() {
        try {
            Query req = em.createNamedQuery("Membre.findALLResponsable").setParameter("val", "responsable");
            return req.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public String UpdateStatutMembre(int c, int labo) {
        Membre m = getMembre(c);

        m.setDatecreate(new Date());
        String statut = m.getResponsable();
        if (statut.equals("adherant")) {
            if (getResponsableLABO(labo) != null) {
                return "Desole un membre est dejà Responsable de ce Laboratoire";
            } else {
                statut = "responsable";
            }
        } else {
            statut = "adherant";
        }
        m.setResponsable(statut);
        em.merge(m);
        return "Operation OK";

    }

    public int getLASTMembre() {
        try {
            int lastm = (int) em.createNamedQuery("Membre.findAllLASTID").getSingleResult();
            return lastm;
        } catch (Exception e) {
            return 0;
        }
    }

    public void DeleteMembre(int IDMEMBRE) {
        Membre ent = getMembre(IDMEMBRE);
        em.remove(ent);
    }

    public void UpdateMembre(Membre M, int l) {
        M.setIdLabo(em.find(Laboratoire.class, l));
        em.merge(M);
    }

    public void UpdateMembre2(Membre M) {
        em.merge(M);
    }

    public Membre AjouterMembre(Membre m, int idlabo) {
        m.setIdLabo(em.find(Laboratoire.class, idlabo));
        em.persist(m);

        return m;
    }

    public Membre getMembre(int IDm) {
        Membre memb = em.find(Membre.class, IDm);
        if (memb == null) {
            return null;
        } else {
            return memb;
        }
    }

    public void persist(Object object) {
        em.persist(object);
    }

}
