/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Categorie;
import ENTITY.Repertoire;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBrepertoire {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    
    public List <Repertoire> getAllRepertoire(){
        Query req = em.createNamedQuery("Repertoire.findAll");
        return req.getResultList();
    }
    
    public String TestLibelle(String v){
        String x="ok";
        try{
            Repertoire r = (Repertoire) em.createNamedQuery("Repertoire.findByLibelle").setParameter("libelle", v).getSingleResult();
            if(r != null){
               x= null;
            }
            return x;
        }catch(Exception e){
            return x;    
        }
    }
    
    public List <Repertoire> getAllRepertoireCAT( int cat){
        Query req = em.createNamedQuery("Repertoire.findByREPCAT").setParameter("Cat", cat);
        return req.getResultList();
    }
    
     public void DeleteRepertoire(int ID){
        Repertoire r = getRepertoire(ID);
        em.remove(r);
    }
    
     public void UpdateRepertoireLibelle(int r, String libelle){
        Repertoire rep = getRepertoire(r);
        rep.setLibelle(libelle);
        rep.setDatemodif(new Date());
        em.merge(rep);
    }
     
    public void UpdateRepertoire(Repertoire r){
        em.merge(r);
    }
    
    public void AjouterRepertoire(Repertoire r, int cat){
        Categorie c = em.find(Categorie.class, cat) ;
        r.setIdCat(c);
        em.persist(r);
    }
    
    public Repertoire getRepertoire(int ID){
        Repertoire m = em.find(Repertoire.class, ID);
        if(m == null)
        {
         return null;
        }else{
        return m;
        }
    }
}
