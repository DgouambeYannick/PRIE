/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Laboratoire;
import ENTITY.Membre;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SBlaboratoire {

    @Resource
    private SessionContext context;

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public List<Laboratoire> getAllLaboratoire() {
        Query req = em.createNamedQuery("Laboratoire.findAll");
        return req.getResultList();
    }

    public List<Laboratoire> getAllLaboratoire2() {
        Query req = em.createNamedQuery("Laboratoire.findByStatut").setParameter("statut", "publie");
        return req.getResultList();
    }

    public int getLASTLaboratoire() {
        try {
            int lastLABO = (int) em.createNamedQuery("Laboratoire.findLASTID").getSingleResult();
            return lastLABO + 1;
        } catch (Exception e) {
            return 0;
        }
    }

//    public Membre getResponsableLabo(int idlabo){
//        Membre membre = null;
//        List<Membre> allmembreLABO = new ArrayList<>();
//        allmembreLABO = (List<Membre>) getLaboratoire(idlabo).getMembreCollection();
//        for (Membre m : allmembreLABO) {
//            if(m.getResponsable()== "oui"){
//                membre = m;
//                break;
//            }
//        }    
//        return membre;
//    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void UpdateStatut(int s) {
        try{
        Laboratoire n = getLaboratoire(s);
        n.setDatecreate(new Date());
        String statut = n.getStatut();
        if (statut.equals("publie")) {
            statut = "depublie";
        } else {
            statut = "publie";
        }
        n.setStatut(statut);
        em.merge(n);
        }catch(Exception e){
            context.getRollbackOnly();
        }
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void DeleteLaboratoire(int ID) {
        try{
        Laboratoire l = getLaboratoire(ID);
        em.remove(l);
        }catch(Exception e){
            context.getRollbackOnly();
        }
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void UpdateLaboratoire(Laboratoire l) {
        try{
        em.merge(l);
        }catch(Exception e){
            context.getRollbackOnly();
        }
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void AjouterLaboratoire(Laboratoire l) {
        try{
        em.persist(l);
        }catch(Exception e){
            context.getRollbackOnly();
        }
    }

    public Laboratoire getLaboratoire(int ID) {
        Laboratoire lab = em.find(Laboratoire.class, ID);
        if (lab == null) {
            return null;
        } else {
            return lab;
        }
    }

    public void persist(Object object) {
        em.persist(object);
    }

}
