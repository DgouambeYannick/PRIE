/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;


import ENTITY.Menu;
import ENTITY.MenuProfil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SBmenu {

    @EJB
    private SBprofil sBprofil;

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;
    
    @Resource
    private SessionContext context;
    
    public List<Menu> getAllMenu(){
        Query req = em.createNamedQuery("Menu.findAll");
        return req.getResultList();
    }   
          
    public void DeleteMenu(int ID){
        Menu m = getMenu(ID);
        em.remove(m);
    }
    
    public void UpdateMenu(Menu m){
        em.merge(m);
    }
    
    public void AjouterMenu(Menu m){
        em.persist(m);
    }
    public Menu getMenu(int ID){
        Menu m = em.find(Menu.class, ID);
        if(m == null)
        {
         return null;
        }else{
        return m;
        }
    }
    public void persist(Object object) {
        em.persist(object);
    }

    
}
