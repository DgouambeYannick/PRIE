/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import ENTITY.Commentaire;
import ENTITY.Groupe;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class SBcommentaire {

    @PersistenceContext(unitName = "PRIEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }
     public void AddCommentaire(Commentaire c, int groupe){
         c.setIdGroupe(em.find(Groupe.class, groupe));
        em.persist(c);
    }
    public List<Commentaire> getAllCommentaire(int groupe){
        Query req = em.createNamedQuery("Commentaire.findCommentGroupe").setParameter("groupe", groupe);
        return req.getResultList();
    }
    
     public Commentaire getComment(int ID){
        Commentaire g = em.find(Commentaire.class, ID);
        if(g == null)
        {
         return null;
        }else{
        return g;
        }
    }
     public void DeleteCommentaire(int ID){
        Commentaire g = getComment(ID);
        em.remove(g);
    }
    
    public void UpdateCommentaire(Commentaire g){
        em.merge(g);
    }
}
