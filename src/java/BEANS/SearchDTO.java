/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BEANS;

import ENTITY.Laboratoire;
import ENTITY.Membre;
import ENTITY.Publication;
import ENTITY.Thematique;
import java.util.List;

/**
 *
 * @author HP
 */
public class SearchDTO {
    private List<Membre> allmembre;
    private List<Thematique> allthematique;
    private List<Laboratoire> alllaboratoire;
    private List<Publication> allpublication;

    public List<Membre> getAllmembre() {
        return allmembre;
    }

    public void setAllmembre(List<Membre> allmembre) {
        this.allmembre = allmembre;
    }

    public List<Thematique> getAllthematique() {
        return allthematique;
    }

    public void setAllthematique(List<Thematique> allthematique) {
        this.allthematique = allthematique;
    }

    public List<Laboratoire> getAlllaboratoire() {
        return alllaboratoire;
    }

    public void setAlllaboratoire(List<Laboratoire> alllaboratoire) {
        this.alllaboratoire = alllaboratoire;
    }

    public List<Publication> getAllpublication() {
        return allpublication;
    }

    public void setAllpublication(List<Publication> allpublication) {
        this.allpublication = allpublication;
    }
    
    
}
