/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "compte_groupe")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompteGroupe.findCompteGroupe", query = "SELECT c FROM CompteGroupe c WHERE c.compteGroupePK.idCompte = :idCompte AND c.compteGroupePK.idGroupe = :idGroupe"),
    @NamedQuery(name = "CompteGroupe.findAll", query = "SELECT c FROM CompteGroupe c"),
    @NamedQuery(name = "CompteGroupe.findByIdCompte", query = "SELECT c FROM CompteGroupe c WHERE c.compteGroupePK.idCompte = :idCompte"),
    @NamedQuery(name = "CompteGroupe.findByIdGroupe", query = "SELECT c FROM CompteGroupe c WHERE c.compteGroupePK.idGroupe = :idGroupe"),
    @NamedQuery(name = "CompteGroupe.findByStatutInvitation", query = "SELECT c FROM CompteGroupe c WHERE c.statutInvitation = :statutInvitation")})
public class CompteGroupe implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CompteGroupePK compteGroupePK;
    @Size(max = 255)
    @Column(name = "STATUT_INVITATION")
    private String statutInvitation;
    @JoinColumn(name = "ID_GROUPE", referencedColumnName = "ID_GROUPE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Groupe groupe;
    @JoinColumn(name = "ID_COMPTE", referencedColumnName = "ID_COMPTE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Compte compte;

    public CompteGroupe() {
    }

    public CompteGroupe(CompteGroupePK compteGroupePK, String invitation) {
        this.compteGroupePK = compteGroupePK;
        this.statutInvitation = invitation;
    }

    public CompteGroupe(int idCompte, int idGroupe) {
        this.compteGroupePK = new CompteGroupePK(idCompte, idGroupe);
    }

    public CompteGroupePK getCompteGroupePK() {
        return compteGroupePK;
    }

    public void setCompteGroupePK(CompteGroupePK compteGroupePK) {
        this.compteGroupePK = compteGroupePK;
    }

    public String getStatutInvitation() {
        return statutInvitation;
    }

    public void setStatutInvitation(String statutInvitation) {
        this.statutInvitation = statutInvitation;
    }

    public Groupe getGroupe() {
        return groupe;
    }

    public void setGroupe(Groupe groupe) {
        this.groupe = groupe;
    }

    public Compte getCompte() {
        return compte;
    }

    public void setCompte(Compte compte) {
        this.compte = compte;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (compteGroupePK != null ? compteGroupePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompteGroupe)) {
            return false;
        }
        CompteGroupe other = (CompteGroupe) object;
        if ((this.compteGroupePK == null && other.compteGroupePK != null) || (this.compteGroupePK != null && !this.compteGroupePK.equals(other.compteGroupePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.CompteGroupe[ compteGroupePK=" + compteGroupePK + " ]";
    }
    
}
