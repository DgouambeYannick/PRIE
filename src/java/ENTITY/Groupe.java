/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "groupe")
@XmlRootElement
@NamedQueries({
     @NamedQuery(name = "Groupe.findLASTID", query = "SELECT MAX(g.idGroupe) FROM Groupe g"),
    @NamedQuery(name = "Groupe.findAll", query = "SELECT g FROM Groupe g"),
    @NamedQuery(name = "Groupe.findByIdGroupe", query = "SELECT g FROM Groupe g WHERE g.idGroupe = :idGroupe"),
    @NamedQuery(name = "Groupe.findByLibelle", query = "SELECT g FROM Groupe g WHERE g.libelle = :libelle"),
    @NamedQuery(name = "Groupe.findByImage", query = "SELECT g FROM Groupe g WHERE g.image = :image"),
    @NamedQuery(name = "Groupe.findByCreateur", query = "SELECT g FROM Groupe g WHERE g.createur = :createur ORDER BY g.idGroupe DESC"),
    @NamedQuery(name = "Groupe.findByDatecreate", query = "SELECT g FROM Groupe g WHERE g.datecreate = :datecreate"),
    @NamedQuery(name = "Groupe.findByDatemodif", query = "SELECT g FROM Groupe g WHERE g.datemodif = :datemodif")})
public class Groupe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_GROUPE")
    private Integer idGroupe;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @Size(max = 255)
    @Column(name = "IMAGE")
    private String image;
    @Size(max = 255)
    @Column(name = "CREATEUR")
    private String createur;
    @Basic(optional = false)
   
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @Column(name = "DATEMODIF")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datemodif;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupe")
    private Collection<CompteGroupe> compteGroupeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idGroupe")
    private Collection<Commentaire> commentaireCollection;

    public Groupe() {
    }

    public Groupe(String image, String libelle, String createur, Date datecreate, Date datemodif) {
        this.image = image;
        this.libelle = libelle;
        this.createur = createur;
        this.datecreate = datecreate;
        this.datemodif = datemodif;
    }

    public Groupe(Integer idgroupe, String image, String libelle, String createur,Date datemodif) {
        this.idGroupe = idgroupe;
        this.image = image;
        this.libelle = libelle;
        this.createur = createur;
        this.datemodif = datemodif;
    }

    public Integer getIdGroupe() {
        return idGroupe;
    }

    public void setIdGroupe(Integer idGroupe) {
        this.idGroupe = idGroupe;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreateur() {
        return createur;
    }

    public void setCreateur(String createur) {
        this.createur = createur;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    public Date getDatemodif() {
        return datemodif;
    }

    public void setDatemodif(Date datemodif) {
        this.datemodif = datemodif;
    }


    @XmlTransient
    public Collection<CompteGroupe> getCompteGroupeCollection() {
        return compteGroupeCollection;
    }

    public void setCompteGroupeCollection(Collection<CompteGroupe> compteGroupeCollection) {
        this.compteGroupeCollection = compteGroupeCollection;
    }

    @XmlTransient
    public Collection<Commentaire> getCommentaireCollection() {
        return commentaireCollection;
    }

    public void setCommentaireCollection(Collection<Commentaire> commentaireCollection) {
        this.commentaireCollection = commentaireCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGroupe != null ? idGroupe.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Groupe)) {
            return false;
        }
        Groupe other = (Groupe) object;
        if ((this.idGroupe == null && other.idGroupe != null) || (this.idGroupe != null && !this.idGroupe.equals(other.idGroupe))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Groupe[ idGroupe=" + idGroupe + " ]";
    }
    
}
