/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "document")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Document.findLASTID", query = "SELECT MAX(d.idDoc) FROM Document d"),
    @NamedQuery(name = "Document.findAll", query = "SELECT d FROM Document d ORDER BY d.idDoc DESC"),
    @NamedQuery(name = "Document.findAllDOCREP", query = "SELECT d FROM Document d WHERE d.idRep.idRep = :rep ORDER BY d.idDoc DESC"),
    @NamedQuery(name = "Document.findByIdDoc", query = "SELECT d FROM Document d WHERE d.idDoc = :idDoc"),
    @NamedQuery(name = "Document.findByLibelle", query = "SELECT d FROM Document d WHERE d.libelle = :libelle"),
    @NamedQuery(name = "Document.findByIcone", query = "SELECT d FROM Document d WHERE d.icone = :icone"),
    @NamedQuery(name = "Document.findByLien", query = "SELECT d FROM Document d WHERE d.lien = :lien"),
    @NamedQuery(name = "Document.findByTailledoc", query = "SELECT d FROM Document d WHERE d.tailledoc = :tailledoc"),
    @NamedQuery(name = "Document.findByVersion", query = "SELECT d FROM Document d WHERE d.version = :version"),
    @NamedQuery(name = "Document.findByExtension", query = "SELECT d FROM Document d WHERE d.extension = :extension"),
    @NamedQuery(name = "Document.findByNamedoc", query = "SELECT d FROM Document d WHERE d.namedoc = :namedoc"),
    @NamedQuery(name = "Document.findByType", query = "SELECT d FROM Document d WHERE d.type = :type"),
    @NamedQuery(name = "Document.findByDatecreate", query = "SELECT d FROM Document d WHERE d.datecreate = :datecreate"),
    @NamedQuery(name = "Document.findByDatemodif", query = "SELECT d FROM Document d WHERE d.datemodif = :datemodif"),
    @NamedQuery(name = "Document.findByDatesup", query = "SELECT d FROM Document d WHERE d.datesup = :datesup")})
public class Document implements Serializable {

    @Size(max = 255)
    @Column(name = "MODIFICATEUR")
    private String modificateur;

    @Size(max = 255)
    @Column(name = "LIEN")
    private String lien;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_DOC")
    private Integer idDoc;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @Size(max = 255)
    @Column(name = "ICONE")
    private String icone;
    @Size(max = 255)
    @Column(name = "TAILLEDOC")
    private String tailledoc;
    @Column(name = "VERSION")
    private Integer version;
    @Size(max = 255)
    @Column(name = "EXTENSION")
    private String extension;
    @Size(max = 255)
    @Column(name = "NAMEDOC")
    private String namedoc;
    @Size(max = 255)
    @Column(name = "TYPE")
    private String type;
    @Basic(optional = false)
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @Column(name = "DATEMODIF")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datemodif;
    @Column(name = "DATESUP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datesup;
    @JoinColumn(name = "ID_COMPTE", referencedColumnName = "ID_COMPTE")
    @ManyToOne(optional = false)
    private Compte idCompte;
    @JoinColumn(name = "ID_REP", referencedColumnName = "ID_REP")
    @ManyToOne(optional = false)
    private Repertoire idRep;

    public Document() {
    }

    public Document(Integer idDoc) {
        this.idDoc = idDoc;
    }

    public Document(String user, String lien, String libelle, String icone, String tailledoc, Integer version, String extension, String type, Date datemodif) {
        this.lien = lien;
        this.libelle = libelle;
        this.icone = icone;
        this.tailledoc = tailledoc;
        this.version = version;
        this.extension = extension;
        this.namedoc = namedoc;
        this.type = type;
        this.datemodif = datemodif;
        this.modificateur = user;
    }

    

    public Integer getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(Integer idDoc) {
        this.idDoc = idDoc;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    public String getTailledoc() {
        return tailledoc;
    }

    public void setTailledoc(String tailledoc) {
        this.tailledoc = tailledoc;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getNamedoc() {
        return namedoc;
    }

    public void setNamedoc(String namedoc) {
        this.namedoc = namedoc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    public Date getDatemodif() {
        return datemodif;
    }

    public void setDatemodif(Date datemodif) {
        this.datemodif = datemodif;
    }

    public Date getDatesup() {
        return datesup;
    }

    public void setDatesup(Date datesup) {
        this.datesup = datesup;
    }

    public Compte getIdCompte() {
        return idCompte;
    }

    public void setIdCompte(Compte idCompte) {
        this.idCompte = idCompte;
    }

    public Repertoire getIdRep() {
        return idRep;
    }

    public void setIdRep(Repertoire idRep) {
        this.idRep = idRep;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDoc != null ? idDoc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Document)) {
            return false;
        }
        Document other = (Document) object;
        if ((this.idDoc == null && other.idDoc != null) || (this.idDoc != null && !this.idDoc.equals(other.idDoc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Document[ idDoc=" + idDoc + " ]";
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public String getModificateur() {
        return modificateur;
    }

    public void setModificateur(String modificateur) {
        this.modificateur = modificateur;
    }
    
}
