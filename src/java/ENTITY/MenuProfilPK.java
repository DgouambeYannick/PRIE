/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author HP
 */
@Embeddable
public class MenuProfilPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_MENU")
    private int idMenu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PROFIL")
    private int idProfil;

    public MenuProfilPK() {
    }

    public MenuProfilPK(int idMenu, int idProfil) {
        this.idMenu = idMenu;
        this.idProfil = idProfil;
    }

    public int getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }

    public int getIdProfil() {
        return idProfil;
    }

    public void setIdProfil(int idProfil) {
        this.idProfil = idProfil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idMenu;
        hash += (int) idProfil;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MenuProfilPK)) {
            return false;
        }
        MenuProfilPK other = (MenuProfilPK) object;
        if (this.idMenu != other.idMenu) {
            return false;
        }
        if (this.idProfil != other.idProfil) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.MenuProfilPK[ idMenu=" + idMenu + ", idProfil=" + idProfil + " ]";
    }
    
}
