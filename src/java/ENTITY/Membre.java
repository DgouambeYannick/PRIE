/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "membre")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Membre.findAllLASTID", query = "SELECT MAX(m.idMembre) FROM Membre m"),
    @NamedQuery(name = "Membre.findAll", query = "SELECT m FROM Membre m ORDER BY m.idMembre DESC"),
    @NamedQuery(name = "Membre.findMEMBRESANSCOMPTE", query = "SELECT m FROM Membre m WHERE m.idMembre NOT IN (SELECT c.idMembre.idMembre FROM Compte c)"),
    @NamedQuery(name = "Membre.findByIdMembre", query = "SELECT m FROM Membre m WHERE m.idMembre = :idMembre"),
     @NamedQuery(name = "Membre.findByMembreLabo", query = "SELECT m FROM Membre m WHERE m.idLabo.idLabo = :labo ORDER BY m.idMembre DESC"),
    @NamedQuery(name = "Membre.findByCivilite", query = "SELECT m FROM Membre m WHERE m.civilite = :civilite"),
    @NamedQuery(name = "Membre.findByNom", query = "SELECT m FROM Membre m WHERE m.nom = :nom"),
    @NamedQuery(name = "Membre.findByPrenom", query = "SELECT m FROM Membre m WHERE m.prenom = :prenom"),
    @NamedQuery(name = "Membre.findByAdresse", query = "SELECT m FROM Membre m WHERE m.adresse = :adresse"),
    @NamedQuery(name = "Membre.findByFonction", query = "SELECT m FROM Membre m WHERE m.fonction = :fonction"),
    @NamedQuery(name = "Membre.findALLResponsable", query = "SELECT m FROM Membre m WHERE m.responsable = :val"),
    @NamedQuery(name = "Membre.findByResponsable", query = "SELECT m FROM Membre m WHERE m.responsable = :responsable AND m.idLabo.idLabo = :labo"),
    @NamedQuery(name = "Membre.findByNomid", query = "SELECT m FROM Membre m WHERE m.nomid = :nomid"),
    @NamedQuery(name = "Membre.findByTutelle", query = "SELECT m FROM Membre m WHERE m.tutelle = :tutelle"),
    @NamedQuery(name = "Membre.findByPhone", query = "SELECT m FROM Membre m WHERE m.phone = :phone"),
    @NamedQuery(name = "Membre.findByImage", query = "SELECT m FROM Membre m WHERE m.image = :image"),
    @NamedQuery(name = "Membre.findByNbvisite", query = "SELECT m FROM Membre m WHERE m.nbvisite = :nbvisite"),
    @NamedQuery(name = "Membre.findByMois", query = "SELECT m FROM Membre m WHERE m.mois = :mois"),
    @NamedQuery(name = "Membre.findByUrlperso", query = "SELECT m FROM Membre m WHERE m.urlperso = :urlperso"),
    @NamedQuery(name = "Membre.findByDatecreate", query = "SELECT m FROM Membre m WHERE m.datecreate = :datecreate")})
public class Membre implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMembre")
    private Collection<Commentaire> commentaireCollection;

    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "email")
    private String email;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_MEMBRE")
    private Integer idMembre;
    @Size(max = 5)
    @Column(name = "CIVILITE")
    private String civilite;
    @Size(max = 255)
    @Column(name = "NOM")
    private String nom;
    @Size(max = 255)
    @Column(name = "PRENOM")
    private String prenom;
    @Size(max = 255)
    @Column(name = "ADRESSE")
    private String adresse;
    @Size(max = 255)
    @Column(name = "FONCTION")
    private String fonction;
    @Size(max = 255)
    @Column(name = "RESPONSABLE")
    private String responsable;
    @Size(max = 255)
    @Column(name = "NOMID")
    private String nomid;
    @Size(max = 255)
    @Column(name = "TUTELLE")
    private String tutelle;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "PHONE")
    private String phone;
    @Size(max = 255)
    @Column(name = "IMAGE")
    private String image;
    @Column(name = "NBVISITE")
    private Integer nbvisite;
    @Size(max = 255)
    @Column(name = "MOIS")
    private String mois;
    @Size(max = 255)
    @Column(name = "URLPERSO")
    private String urlperso;
    @Basic(optional = false)  
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "idMembre")
    private Compte compte;
    @JoinColumn(name = "ID_LABO", referencedColumnName = "ID_LABO")
    @ManyToOne(optional = false)
    private Laboratoire idLabo;

    public Membre() {
    }

    public Membre(Integer idMembre) {
        this.idMembre = idMembre;
    }

    public Membre(String email, String civilite, String nom, String prenom, String adresse, String fonction, String responsable, String tutelle, String phone, String image, Integer nbvisite, Date datecreate) {
        this.email = email;
        this.civilite = civilite;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.fonction = fonction;
        this.responsable = responsable;
        this.tutelle = tutelle;
        this.phone = phone;
        this.image = image;
        this.nbvisite = nbvisite;
        this.datecreate= datecreate;
        
    }
    
      public Membre(int idMembre,String nomid, String email, String civilite, String nom, String prenom, String adresse, String fonction, String responsable, String tutelle, String phone, String image,Integer nbvisite, Date datecreate, String url) {
        this.email = email;
        this.idMembre = idMembre;
        this.civilite = civilite;
        this.nom = nom;
        this.nomid = nomid;
        this.prenom = prenom;
        this.adresse = adresse;
        this.fonction = fonction;
        this.responsable = responsable;
        this.tutelle = tutelle;
        this.phone = phone;
        this.image = image;
        this.nbvisite = nbvisite;
        this.datecreate= datecreate;
        this.urlperso = url;  
        
    }
    

    public Integer getIdMembre() {
        return idMembre;
    }

    public void setIdMembre(Integer idMembre) {
        this.idMembre = idMembre;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getNomid() {
        return nomid;
    }

    public void setNomid(String nomid) {
        this.nomid = nomid;
    }

    public String getTutelle() {
        return tutelle;
    }

    public void setTutelle(String tutelle) {
        this.tutelle = tutelle;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getNbvisite() {
        return nbvisite;
    }

    public void setNbvisite(Integer nbvisite) {
        this.nbvisite = nbvisite;
    }

    public String getMois() {
        return mois;
    }

    public void setMois(String mois) {
        this.mois = mois;
    }

    public String getUrlperso() {
        return urlperso;
    }

    public void setUrlperso(String urlperso) {
        this.urlperso = urlperso;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    public Compte getCompte() {
        return compte;
    }

    public void setCompte(Compte compte) {
        this.compte = compte;
    }

    public Laboratoire getIdLabo() {
        return idLabo;
    }

    public void setIdLabo(Laboratoire idLabo) {
        this.idLabo = idLabo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMembre != null ? idMembre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Membre)) {
            return false;
        }
        Membre other = (Membre) object;
        if ((this.idMembre == null && other.idMembre != null) || (this.idMembre != null && !this.idMembre.equals(other.idMembre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Membre[ idMembre=" + idMembre + " ]";
    }

    @XmlTransient
    public Collection<Commentaire> getCommentaireCollection() {
        return commentaireCollection;
    }

    public void setCommentaireCollection(Collection<Commentaire> commentaireCollection) {
        this.commentaireCollection = commentaireCollection;
    }

 
    
}
