/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author HP
 */
@Embeddable
public class CompteGroupePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_COMPTE")
    private int idCompte;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_GROUPE")
    private int idGroupe;

    public CompteGroupePK() {
    }

    public CompteGroupePK(int idCompte, int idGroupe) {
        this.idCompte = idCompte;
        this.idGroupe = idGroupe;
    }

    public int getIdCompte() {
        return idCompte;
    }

    public void setIdCompte(int idCompte) {
        this.idCompte = idCompte;
    }

    public int getIdGroupe() {
        return idGroupe;
    }

    public void setIdGroupe(int idGroupe) {
        this.idGroupe = idGroupe;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idCompte;
        hash += (int) idGroupe;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompteGroupePK)) {
            return false;
        }
        CompteGroupePK other = (CompteGroupePK) object;
        if (this.idCompte != other.idCompte) {
            return false;
        }
        if (this.idGroupe != other.idGroupe) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.CompteGroupePK[ idCompte=" + idCompte + ", idGroupe=" + idGroupe + " ]";
    }
    
}
