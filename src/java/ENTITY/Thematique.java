/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "thematique")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Thematique.findAll", query = "SELECT t FROM Thematique t ORDER BY t.idTheme DESC"),
    @NamedQuery(name = "Thematique.findAllThemeLabo", query = "SELECT t FROM Thematique t WHERE t.idLabo.idLabo= :labo ORDER BY t.idTheme DESC"),
    @NamedQuery(name = "Thematique.findByIdTheme", query = "SELECT t FROM Thematique t WHERE t.idTheme = :idTheme"),
    @NamedQuery(name = "Thematique.findByTitre", query = "SELECT t FROM Thematique t WHERE t.titre = :titre"),
    @NamedQuery(name = "Thematique.findByDatecreate", query = "SELECT t FROM Thematique t WHERE t.datecreate = :datecreate")})
public class Thematique implements Serializable {

    
    @Size(min = 1, max = 20)
    @Column(name = "statut")
    private String statut;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_THEME")
    private Integer idTheme;
    @Size(min = 1, max = 20)
    @Column(name = "titre")
    private String titre;
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "contenu")
    private String contenu;
    @Basic(optional = false)
    @Column(name = "datecreate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @JoinColumn(name = "ID_LABO", referencedColumnName = "ID_LABO")
    @ManyToOne(optional = false)
    private Laboratoire idLabo;

    public Thematique() {
    }

    public Thematique(Integer idTheme) {
        this.idTheme = idTheme;
    }

    public Thematique(Integer idTheme,String statut, String titre, String contenu, Date datecreate) {
        this.idTheme = idTheme;
        this.titre = titre;
        this.contenu = contenu;
        this.datecreate = datecreate;
        this.statut=statut;
    }
    public Thematique(String statut, String titre, String contenu, Date datecreate) {
        
        this.titre = titre;
        this.contenu = contenu;
        this.datecreate = datecreate;
        this.statut=statut;
    }

    public Integer getIdTheme() {
        return idTheme;
    }

    public void setIdTheme(Integer idTheme) {
        this.idTheme = idTheme;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    public Laboratoire getIdLabo() {
        return idLabo;
    }

    public void setIdLabo(Laboratoire idLabo) {
        this.idLabo = idLabo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTheme != null ? idTheme.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Thematique)) {
            return false;
        }
        Thematique other = (Thematique) object;
        if ((this.idTheme == null && other.idTheme != null) || (this.idTheme != null && !this.idTheme.equals(other.idTheme))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Thematique[ idTheme=" + idTheme + " ]";
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
    
}
