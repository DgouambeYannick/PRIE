/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "commentaire")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commentaire.findAll", query = "SELECT c FROM Commentaire c ORDER BY c.idCom DESC"),
     @NamedQuery(name = "Commentaire.findCommentGroupe", query = "SELECT c FROM Commentaire c WHERE c.idGroupe.idGroupe = :groupe"),
    @NamedQuery(name = "Commentaire.findByIdCom", query = "SELECT c FROM Commentaire c WHERE c.idCom = :idCom"),
    @NamedQuery(name = "Commentaire.findByLien", query = "SELECT c FROM Commentaire c WHERE c.lien = :lien"),
    @NamedQuery(name = "Commentaire.findByCreateur", query = "SELECT c FROM Commentaire c WHERE c.createur = :createur"),
    @NamedQuery(name = "Commentaire.findByDatecreate", query = "SELECT c FROM Commentaire c WHERE c.datecreate = :datecreate")})
public class Commentaire implements Serializable {

    @JoinColumn(name = "ID_MEMBRE", referencedColumnName = "ID_MEMBRE")
    @ManyToOne(optional = false)
    private Membre idMembre;

    @Column(name = "CODE")
    private Integer code;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_COM")
    private Integer idCom;
    @Lob
    @Size(max = 16777215)
    @Column(name = "COMMENTAIRE")
    private String commentaire;
    @Size(max = 255)
    @Column(name = "LIEN")
    private String lien;
    @Size(max = 255)
    @Column(name = "CREATEUR")
    private String createur;
    @Basic(optional = false)
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @JoinColumn(name = "ID_GROUPE", referencedColumnName = "ID_GROUPE")
    @ManyToOne(optional = false)
    private Groupe idGroupe;

    public Commentaire() {
    }

    public Commentaire(Integer idCom) {
        this.idCom = idCom;
    }

    public Commentaire(String comment, String user, int code, Date datecreate) {
        this.commentaire=comment;;
        this.createur = user;
        this.datecreate = datecreate;
        this.code= code;
    }

    public Integer getIdCom() {
        return idCom;
    }

    public void setIdCom(Integer idCom) {
        this.idCom = idCom;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public String getCreateur() {
        return createur;
    }

    public void setCreateur(String createur) {
        this.createur = createur;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    public Groupe getIdGroupe() {
        return idGroupe;
    }

    public void setIdGroupe(Groupe idGroupe) {
        this.idGroupe = idGroupe;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCom != null ? idCom.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commentaire)) {
            return false;
        }
        Commentaire other = (Commentaire) object;
        if ((this.idCom == null && other.idCom != null) || (this.idCom != null && !this.idCom.equals(other.idCom))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Commentaire[ idCom=" + idCom + " ]";
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Membre getIdMembre() {
        return idMembre;
    }

    public void setIdMembre(Membre idMembre) {
        this.idMembre = idMembre;
    }
    
}
