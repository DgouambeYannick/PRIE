/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Menu.findAll", query = "SELECT m FROM Menu m"),
    @NamedQuery(name = "Menu.findByIdMenu", query = "SELECT m FROM Menu m WHERE m.idMenu = :idMenu"),
    @NamedQuery(name = "Menu.findByLibelle", query = "SELECT m FROM Menu m WHERE m.libelle = :libelle"),
    @NamedQuery(name = "Menu.findByLien", query = "SELECT m FROM Menu m WHERE m.lien = :lien"),
    @NamedQuery(name = "Menu.findByIcone", query = "SELECT m FROM Menu m WHERE m.icone = :icone")})
public class Menu implements Serializable {

    @Size(max = 50)
    @Column(name = "COULEUR")
    private String couleur;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_MENU")
    private Integer idMenu;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @Size(max = 255)
    @Column(name = "LIEN")
    private String lien;
    @Size(max = 255)
    @Column(name = "ICONE")
    private String icone;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMenu")
    private Collection<SousMenu> sousMenuCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menu")
    private Collection<MenuProfil> menuProfilCollection;

    public Menu() {
    }

    public Menu(Integer idMenu) {
        this.idMenu = idMenu;
    }

    public Integer getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(Integer idMenu) {
        this.idMenu = idMenu;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    @XmlTransient
    public Collection<SousMenu> getSousMenuCollection() {
        return sousMenuCollection;
    }

    public void setSousMenuCollection(Collection<SousMenu> sousMenuCollection) {
        this.sousMenuCollection = sousMenuCollection;
    }

    @XmlTransient
    public Collection<MenuProfil> getMenuProfilCollection() {
        return menuProfilCollection;
    }

    public void setMenuProfilCollection(Collection<MenuProfil> menuProfilCollection) {
        this.menuProfilCollection = menuProfilCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMenu != null ? idMenu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.idMenu == null && other.idMenu != null) || (this.idMenu != null && !this.idMenu.equals(other.idMenu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Menu[ idMenu=" + idMenu + " ]";
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }
    
}
