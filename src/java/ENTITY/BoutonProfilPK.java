/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author HP
 */
@Embeddable
public class BoutonProfilPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PROFIL")
    private int idProfil;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_BOUTON")
    private int idBouton;

    public BoutonProfilPK() {
    }

    public BoutonProfilPK(int idProfil, int idBouton) {
        this.idProfil = idProfil;
        this.idBouton = idBouton;
    }

    public int getIdProfil() {
        return idProfil;
    }

    public void setIdProfil(int idProfil) {
        this.idProfil = idProfil;
    }

    public int getIdBouton() {
        return idBouton;
    }

    public void setIdBouton(int idBouton) {
        this.idBouton = idBouton;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idProfil;
        hash += (int) idBouton;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BoutonProfilPK)) {
            return false;
        }
        BoutonProfilPK other = (BoutonProfilPK) object;
        if (this.idProfil != other.idProfil) {
            return false;
        }
        if (this.idBouton != other.idBouton) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.BoutonProfilPK[ idProfil=" + idProfil + ", idBouton=" + idBouton + " ]";
    }
    
}
