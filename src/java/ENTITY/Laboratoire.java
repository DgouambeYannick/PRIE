/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "laboratoire")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Laboratoire.findLASTID", query = "SELECT MAX(l.idLabo) FROM Laboratoire l"),
    @NamedQuery(name = "Laboratoire.findAll", query = "SELECT l FROM Laboratoire l ORDER BY l.idLabo DESC"),
    @NamedQuery(name = "Laboratoire.findByStatut", query = "SELECT l FROM Laboratoire l WHERE l.statut = :statut"),
    @NamedQuery(name = "Laboratoire.findByIdLabo", query = "SELECT l FROM Laboratoire l WHERE l.idLabo = :idLabo"),
    @NamedQuery(name = "Laboratoire.findBySigle", query = "SELECT l FROM Laboratoire l WHERE l.sigle = :sigle"),
    @NamedQuery(name = "Laboratoire.findByLibelle", query = "SELECT l FROM Laboratoire l WHERE l.libelle = :libelle"),
    @NamedQuery(name = "Laboratoire.findByImage", query = "SELECT l FROM Laboratoire l WHERE l.image = :image"),
    @NamedQuery(name = "Laboratoire.findByDatecreate", query = "SELECT l FROM Laboratoire l WHERE l.datecreate = :datecreate")})
public class Laboratoire implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLabo")
    private Collection<Thematique> thematiqueCollection;

    @Size(max = 255)
    @Column(name = "STATUT")
    private String statut;

    @Size(max = 255)
    @Column(name = "CREATEUR")
    private String createur;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_LABO")
    private Integer idLabo;
    @Size(max = 255)
    @Column(name = "SIGLE")
    private String sigle;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "CONTENU")
    private String contenu;
    @Size(max = 255)
    @Column(name = "IMAGE")
    private String image;
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLabo")
    private Collection<Membre> membreCollection;

    public Laboratoire() {
    }

    public Laboratoire(Integer idLabo,String createur, String statut, String sigle, String libelle, String contenu, String image, Date datecreate) {
        this.idLabo = idLabo;
        this.statut=statut;
        this.sigle = sigle;
        this.libelle = libelle;
        this.contenu = contenu;
        this.image = image;
        this.createur = createur;
        this.datecreate = datecreate;
    }

    

    public Laboratoire(String createur, String sigle, String libelle, String contenu, String image, String statut, Date datecreate) {
        this.sigle = sigle;
        this.libelle = libelle;
        this.contenu = contenu;
        this.image = image;
        this.datecreate = datecreate;
        this.statut = statut;
        this.createur = createur;
    }

    

    public Integer getIdLabo() {
        return idLabo;
    }

    public void setIdLabo(Integer idLabo) {
        this.idLabo = idLabo;
    }

    public String getSigle() {
        return sigle;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    @XmlTransient
    public Collection<Membre> getMembreCollection() {
        return membreCollection;
    }

    public void setMembreCollection(Collection<Membre> membreCollection) {
        this.membreCollection = membreCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLabo != null ? idLabo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Laboratoire)) {
            return false;
        }
        Laboratoire other = (Laboratoire) object;
        if ((this.idLabo == null && other.idLabo != null) || (this.idLabo != null && !this.idLabo.equals(other.idLabo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Laboratoire[ idLabo=" + idLabo + " ]";
    }

    public String getCreateur() {
        return createur;
    }

    public void setCreateur(String createur) {
        this.createur = createur;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    @XmlTransient
    public Collection<Thematique> getThematiqueCollection() {
        return thematiqueCollection;
    }

    public void setThematiqueCollection(Collection<Thematique> thematiqueCollection) {
        this.thematiqueCollection = thematiqueCollection;
    }
    
}
