/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "image")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Image.findLASTID", query = "SELECT MAX(i.idImage) FROM Image i"),
    @NamedQuery(name = "Image.findAll", query = "SELECT i FROM Image i"),
    @NamedQuery(name = "Image.findByIdImage", query = "SELECT i FROM Image i WHERE i.idImage = :idImage"),
    @NamedQuery(name = "Image.findByLien", query = "SELECT i FROM Image i WHERE i.lien = :lien"),
    @NamedQuery(name = "Image.findByNamerepertoire", query = "SELECT i FROM Image i WHERE i.namerepertoire = :namerepertoire ORDER BY i.idImage DESC"),
    @NamedQuery(name = "Image.findBySliderPost", query = "SELECT i FROM Image i WHERE i.namerepertoire = :namerepertoire AND i.statut = :statut ORDER BY i.idImage DESC"),
    @NamedQuery(name = "Image.findByDescription", query = "SELECT i FROM Image i WHERE i.description = :description"),
    @NamedQuery(name = "Image.findByDatecreate", query = "SELECT i FROM Image i WHERE i.datecreate = :datecreate")})
public class Image implements Serializable {

    @Size(max = 15)
    @Column(name = "STATUT")
    private String statut;

    @Size(max = 255)
    @Column(name = "TAILLE")
    private String taille;

    @Size(max = 10)
    @Column(name = "EXTENSION")
    private String extension;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_IMAGE")
    private Integer idImage;
    @Size(max = 255)
    @Column(name = "LIEN")
    private String lien;
    @Size(max = 255)
    @Column(name = "NAMEREPERTOIRE")
    private String namerepertoire;
    @Size(max = 1000)
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;

    public Image() {
    }

    public Image(Integer idImage) {
        this.idImage = idImage;
    }

    public Image(String statut, String lien, String namerepertoire, String description, String extension, String taille, Date datecreate) {
        this.extension = extension;
        this.lien = lien;
        this.namerepertoire = namerepertoire;
        this.description = description;
        this.datecreate = datecreate;
        this.taille = taille;
        this.statut = statut;
    }
    public Image(int idImage,String statut, String lien, String namerepertoire, String description, String extension, String taille, Date datecreate) {
        this.idImage = idImage;
        this.extension = extension;
        this.lien = lien;
        this.namerepertoire = namerepertoire;
        this.description = description;
        this.datecreate = datecreate;
        this.taille = taille;
        this.statut = statut;
    }

    

    public Integer getIdImage() {
        return idImage;
    }

    public void setIdImage(Integer idImage) {
        this.idImage = idImage;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public String getNamerepertoire() {
        return namerepertoire;
    }

    public void setNamerepertoire(String namerepertoire) {
        this.namerepertoire = namerepertoire;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idImage != null ? idImage.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Image)) {
            return false;
        }
        Image other = (Image) object;
        if ((this.idImage == null && other.idImage != null) || (this.idImage != null && !this.idImage.equals(other.idImage))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Image[ idImage=" + idImage + " ]";
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
    
}
