/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "presentation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Presentation.findAll", query = "SELECT p FROM Presentation p "),
    @NamedQuery(name = "Presentation.findByIdPre", query = "SELECT p FROM Presentation p WHERE p.idPre = :idPre"),
    @NamedQuery(name = "Presentation.findByLibelle", query = "SELECT p FROM Presentation p WHERE p.libelle = :libelle")})
public class Presentation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PRE")
    private Integer idPre;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "CONTENU")
    private String contenu;

    public Presentation() {
    }

    public Presentation(Integer idPre, String libelle, String contenu) {
        this.idPre = idPre;
        this.libelle = libelle;
        this.contenu = contenu;
    }

    

    public Integer getIdPre() {
        return idPre;
    }

    public void setIdPre(Integer idPre) {
        this.idPre = idPre;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPre != null ? idPre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Presentation)) {
            return false;
        }
        Presentation other = (Presentation) object;
        if ((this.idPre == null && other.idPre != null) || (this.idPre != null && !this.idPre.equals(other.idPre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Presentation[ idPre=" + idPre + " ]";
    }
    
}
