/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "profil")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profil.findLASTID", query = "SELECT MAX(p.idProfil) FROM Profil p"),
    @NamedQuery(name = "Profil.findAll", query = "SELECT p FROM Profil p"),
    @NamedQuery(name = "Profil.findByIdProfil", query = "SELECT p FROM Profil p WHERE p.idProfil = :idProfil"),
    @NamedQuery(name = "Profil.findByLibelle", query = "SELECT p FROM Profil p WHERE p.libelle = :libelle"),
    @NamedQuery(name = "Profil.findByDatecreate", query = "SELECT p FROM Profil p WHERE p.datecreate = :datecreate")})
public class Profil implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PROFIL")
    private Integer idProfil;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @Basic(optional = false)
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profil")
    private Collection<BoutonProfil> boutonProfilCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profil")
    private Collection<MenuProfil> menuProfilCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProfil")
    private Collection<Compte> compteCollection;

    public Profil() {
    }

    public Profil(String libelle, Date datecreate) {
        this.libelle = libelle;
        this.datecreate = datecreate;
    }

    public Profil(Integer idProfil, String libelle,Date datecreate) {
        this.idProfil = idProfil;
        this.libelle = libelle;
        this.datecreate = datecreate;
    }

    public Integer getIdProfil() {
        return idProfil;
    }

    public void setIdProfil(Integer idProfil) {
        this.idProfil = idProfil;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    @XmlTransient
    public Collection<BoutonProfil> getBoutonProfilCollection() {
        return boutonProfilCollection;
    }

    public void setBoutonProfilCollection(Collection<BoutonProfil> boutonProfilCollection) {
        this.boutonProfilCollection = boutonProfilCollection;
    }

    @XmlTransient
    public Collection<MenuProfil> getMenuProfilCollection() {
        return menuProfilCollection;
    }

    public void setMenuProfilCollection(Collection<MenuProfil> menuProfilCollection) {
        this.menuProfilCollection = menuProfilCollection;
    }

    @XmlTransient
    public Collection<Compte> getCompteCollection() {
        return compteCollection;
    }

    public void setCompteCollection(Collection<Compte> compteCollection) {
        this.compteCollection = compteCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProfil != null ? idProfil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profil)) {
            return false;
        }
        Profil other = (Profil) object;
        if ((this.idProfil == null && other.idProfil != null) || (this.idProfil != null && !this.idProfil.equals(other.idProfil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Profil[ idProfil=" + idProfil + " ]";
    }
    
}
