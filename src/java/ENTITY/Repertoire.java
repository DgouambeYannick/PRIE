/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "repertoire")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Repertoire.findAll", query = "SELECT r FROM Repertoire r ORDER BY r.idRep DESC"),
    @NamedQuery(name = "Repertoire.findByIdRep", query = "SELECT r FROM Repertoire r WHERE r.idRep = :idRep"),
    @NamedQuery(name = "Repertoire.findByREPCAT", query = "SELECT r FROM Repertoire r WHERE r.idCat.idCat = :Cat ORDER BY r.idRep DESC"),
    @NamedQuery(name = "Repertoire.findByLibelle", query = "SELECT r FROM Repertoire r WHERE r.libelle = :libelle"),
    @NamedQuery(name = "Repertoire.findByTailledos", query = "SELECT r FROM Repertoire r WHERE r.tailledos = :tailledos"),
    @NamedQuery(name = "Repertoire.findByIcone", query = "SELECT r FROM Repertoire r WHERE r.icone = :icone"),
    @NamedQuery(name = "Repertoire.findByDatecreate", query = "SELECT r FROM Repertoire r WHERE r.datecreate = :datecreate"),
    @NamedQuery(name = "Repertoire.findByDatemodif", query = "SELECT r FROM Repertoire r WHERE r.datemodif = :datemodif")})
public class Repertoire implements Serializable {

    @Size(max = 255)
    @Column(name = "MODIFICATEUR")
    private String modificateur;

    @Size(max = 255)
    @Column(name = "CREATEUR")
    private String createur;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_REP")
    private Integer idRep;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @Size(max = 255)
    @Column(name = "TAILLEDOS")
    private String tailledos;
    @Size(max = 255)
    @Column(name = "ICONE")
    private String icone;
    @Basic(optional = false)
    
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @Column(name = "DATEMODIF")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datemodif;
    @JoinColumn(name = "ID_CAT", referencedColumnName = "ID_CAT")
    @ManyToOne(optional = false)
    private Categorie idCat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRep")
    private Collection<Document> documentCollection;

    public Repertoire() {
    }

    public Repertoire(Integer idRep) {
        this.idRep = idRep;
    }

    public Repertoire(Integer idRep, String createur, String libelle, String icone, Date datemodif) {
        this.idRep = idRep;
        this.libelle = libelle;
        this.icone = icone;
        this.datemodif = datemodif;
        this.createur = createur;
    }

    public Repertoire(String createur, String libelle, String icone, Date datemodif) {
        this.createur = createur;
        this.libelle = libelle;
        this.icone = icone;
        this.datemodif = datemodif;
    }

    

    

    public Integer getIdRep() {
        return idRep;
    }

    public void setIdRep(Integer idRep) {
        this.idRep = idRep;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getTailledos() {
        return tailledos;
    }

    public void setTailledos(String tailledos) {
        this.tailledos = tailledos;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    public Date getDatemodif() {
        return datemodif;
    }

    public void setDatemodif(Date datemodif) {
        this.datemodif = datemodif;
    }

    public Categorie getIdCat() {
        return idCat;
    }

    public void setIdCat(Categorie idCat) {
        this.idCat = idCat;
    }

    @XmlTransient
    public Collection<Document> getDocumentCollection() {
        return documentCollection;
    }

    public void setDocumentCollection(Collection<Document> documentCollection) {
        this.documentCollection = documentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRep != null ? idRep.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Repertoire)) {
            return false;
        }
        Repertoire other = (Repertoire) object;
        if ((this.idRep == null && other.idRep != null) || (this.idRep != null && !this.idRep.equals(other.idRep))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Repertoire[ idRep=" + idRep + " ]";
    }

    public String getCreateur() {
        return createur;
    }

    public void setCreateur(String createur) {
        this.createur = createur;
    }

    public String getModificateur() {
        return modificateur;
    }

    public void setModificateur(String modificateur) {
        this.modificateur = modificateur;
    }
    
}
