/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "sous_menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SousMenu.findAll", query = "SELECT s FROM SousMenu s"),
    @NamedQuery(name = "SousMenu.findByIdSousMenu", query = "SELECT s FROM SousMenu s WHERE s.idSousMenu = :idSousMenu"),
    @NamedQuery(name = "SousMenu.findByLibelle", query = "SELECT s FROM SousMenu s WHERE s.libelle = :libelle"),
    @NamedQuery(name = "SousMenu.findByLien", query = "SELECT s FROM SousMenu s WHERE s.lien = :lien"),
    @NamedQuery(name = "SousMenu.findByIcone", query = "SELECT s FROM SousMenu s WHERE s.icone = :icone")})
public class SousMenu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SOUS_MENU")
    private Integer idSousMenu;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @Size(max = 255)
    @Column(name = "LIEN")
    private String lien;
    @Size(max = 255)
    @Column(name = "ICONE")
    private String icone;
    @JoinColumn(name = "ID_MENU", referencedColumnName = "ID_MENU")
    @ManyToOne(optional = false)
    private Menu idMenu;

    public SousMenu() {
    }

    public SousMenu(Integer idSousMenu) {
        this.idSousMenu = idSousMenu;
    }

    public Integer getIdSousMenu() {
        return idSousMenu;
    }

    public void setIdSousMenu(Integer idSousMenu) {
        this.idSousMenu = idSousMenu;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    public Menu getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(Menu idMenu) {
        this.idMenu = idMenu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSousMenu != null ? idSousMenu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SousMenu)) {
            return false;
        }
        SousMenu other = (SousMenu) object;
        if ((this.idSousMenu == null && other.idSousMenu != null) || (this.idSousMenu != null && !this.idSousMenu.equals(other.idSousMenu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.SousMenu[ idSousMenu=" + idSousMenu + " ]";
    }
    
}
