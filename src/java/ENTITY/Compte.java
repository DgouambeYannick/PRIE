/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "compte")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compte.login", query = "SELECT c FROM Compte c WHERE c.login = :username AND c.statut = :statut"),
    @NamedQuery(name = "Compte.findAll", query = "SELECT c FROM Compte c ORDER BY c.idCompte DESC "),
    @NamedQuery(name = "Compte.findCompteAnyCreateur", query = "SELECT c FROM Compte c WHERE c.idCompte <> :user"),
    @NamedQuery(name = "Compte.findByIdCompte", query = "SELECT c FROM Compte c WHERE c.idCompte = :idCompte"),
    @NamedQuery(name = "Compte.findByCompteM", query = "SELECT c FROM Compte c WHERE c.idMembre.idMembre = :id"),
    @NamedQuery(name = "Compte.findByLogin", query = "SELECT c FROM Compte c WHERE c.login = :login"),
    @NamedQuery(name = "Compte.findByPwd", query = "SELECT c FROM Compte c WHERE c.pwd = :pwd"),
    @NamedQuery(name = "Compte.findByOnline", query = "SELECT c FROM Compte c WHERE c.online = :online AND c.idMembre.idLabo.idLabo = :labo"),
    @NamedQuery(name = "Compte.findByStatut", query = "SELECT c FROM Compte c WHERE c.statut = :statut"),
    @NamedQuery(name = "Compte.findByDatecreate", query = "SELECT c FROM Compte c WHERE c.datecreate = :datecreate"),
    @NamedQuery(name = "Compte.findByDatemodif", query = "SELECT c FROM Compte c WHERE c.datemodif = :datemodif"),
    @NamedQuery(name = "Compte.findByDatesup", query = "SELECT c FROM Compte c WHERE c.datesup = :datesup")})
public class Compte implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_COMPTE")
    private Integer idCompte;
    @Size(max = 255)
    @Column(name = "LOGIN")
    private String login;
    @Size(max = 255)
    @Column(name = "PWD")
    private String pwd;
   
    @Size(max = 3)
    @Column(name = "ONLINE")
    private String online;
    @Size(max = 255)
    @Column(name = "STATUT")
    private String statut;
    @Basic(optional = false)
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @Column(name = "DATEMODIF")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datemodif;
    @Column(name = "DATESUP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datesup;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCompte")
    private Collection<Publication> publicationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCompte")
    private Collection<Document> documentCollection;
    @JoinColumn(name = "ID_MEMBRE", referencedColumnName = "ID_MEMBRE")
    @OneToOne(optional = false)
    private Membre idMembre;
    @JoinColumn(name = "ID_PROFIL", referencedColumnName = "ID_PROFIL")
    @ManyToOne(optional = false)
    private Profil idProfil;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "compte")
    private Collection<CompteGroupe> compteGroupeCollection;
    
    public Compte() {
    }

    public Compte(Integer idCompte, String login, String pwd, String online, String statut,Date datemodif) {
        this.idCompte = idCompte;
        this.login = login;
        this.pwd = pwd;
        this.online = online;
        this.statut = statut;
        this.datemodif = datemodif;
    }

    public Compte(String login, String pwd, String online, String statut,Date dateajout, Date datemodif) {
        this.login = login;
        this.pwd = pwd;
        this.online = online;
        this.statut = statut;
        this.datemodif = datemodif;
        this.datecreate = dateajout;
    }

   

    public Integer getIdCompte() {
        return idCompte;
    }

    public void setIdCompte(Integer idCompte) {
        this.idCompte = idCompte;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    public Date getDatemodif() {
        return datemodif;
    }

    public void setDatemodif(Date datemodif) {
        this.datemodif = datemodif;
    }

    public Date getDatesup() {
        return datesup;
    }

    public void setDatesup(Date datesup) {
        this.datesup = datesup;
    }

    @XmlTransient
    public Collection<Publication> getPublicationCollection() {
        return publicationCollection;
    }

    public void setPublicationCollection(Collection<Publication> publicationCollection) {
        this.publicationCollection = publicationCollection;
    }

    @XmlTransient
    public Collection<Document> getDocumentCollection() {
        return documentCollection;
    }

    public void setDocumentCollection(Collection<Document> documentCollection) {
        this.documentCollection = documentCollection;
    }

    public Membre getIdMembre() {
        return idMembre;
    }

    public void setIdMembre(Membre idMembre) {
        this.idMembre = idMembre;
    }

    public Profil getIdProfil() {
        return idProfil;
    }

    public void setIdProfil(Profil idProfil) {
        this.idProfil = idProfil;
    }

    @XmlTransient
    public Collection<CompteGroupe> getCompteGroupeCollection() {
        return compteGroupeCollection;
    }

    public void setCompteGroupeCollection(Collection<CompteGroupe> compteGroupeCollection) {
        this.compteGroupeCollection = compteGroupeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCompte != null ? idCompte.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compte)) {
            return false;
        }
        Compte other = (Compte) object;
        if ((this.idCompte == null && other.idCompte != null) || (this.idCompte != null && !this.idCompte.equals(other.idCompte))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Compte[ idCompte=" + idCompte + " ]";
    }

    
}
