/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "publication")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Publication.findLASTID", query = "SELECT MAX(p.idPub) FROM Publication p"),
    @NamedQuery(name = "Publication.findAll", query = "SELECT p FROM Publication p ORDER BY p.idPub DESC"),
    @NamedQuery(name = "Publication.findByPUBCOMPTE", query = "SELECT p FROM Publication p WHERE p.idType.idType = :type AND p.idCompte.idCompte = :compte"),
    @NamedQuery(name = "Publication.findByTYPE", query = "SELECT p FROM Publication p WHERE p.idType.idType = :type"),
    @NamedQuery(name = "Publication.findByIdPub", query = "SELECT p FROM Publication p WHERE p.idPub = :idPub"),
    @NamedQuery(name = "Publication.findByALLannonce", query = "SELECT p FROM Publication p WHERE p.idType.idType = :idannonce ORDER BY p.idPub DESC"),
    @NamedQuery(name = "Publication.findByLibelle", query = "SELECT p FROM Publication p WHERE p.libelle = :libelle"),
    @NamedQuery(name = "Publication.findByLien", query = "SELECT p FROM Publication p WHERE p.lien = :lien"),
    @NamedQuery(name = "Publication.findByIcone", query = "SELECT p FROM Publication p WHERE p.icone = :icone"),
    @NamedQuery(name = "Publication.findByDatecreate", query = "SELECT p FROM Publication p WHERE p.datecreate = :datecreate"),
    @NamedQuery(name = "Publication.findByDatemodif", query = "SELECT p FROM Publication p WHERE p.datemodif = :datemodif")})
public class Publication implements Serializable {

    @Size(max = 255)
    @Column(name = "TAILLE")
    private String taille;

    @Size(max = 50)
    @Column(name = "STATUT")
    private String statut;
    @Size(max = 3)
    @Column(name = "TELECHARGER")
    private String telecharger;
    @Column(name = "ANNEE")
    private Integer annee;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PUB")
    private Integer idPub;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @Lob
    @Size(max = 16777215)
    @Column(name = "RESUME")
    private String resume;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "CONTENU")
    private String contenu;
    @Size(max = 255)
    @Column(name = "LIEN")
    private String lien;
    @Size(max = 255)
    @Column(name = "ICONE")
    private String icone;
    @Basic(optional = false)
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @Column(name = "DATEMODIF")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datemodif;
    @JoinColumn(name = "ID_TYPE", referencedColumnName = "ID_TYPE")
    @ManyToOne(optional = false)
    private TypePublication idType;
    @JoinColumn(name = "ID_COMPTE", referencedColumnName = "ID_COMPTE")
    @ManyToOne(optional = false)
    private Compte idCompte;

    public Publication() {
    }

    public Publication(Integer idPub) {
        this.idPub = idPub;
    }

    public Publication(String taille, int annee, String statut, String telecharger, String libelle, String resume, String contenu, String lien, String icone, Date datecreate, Date datemodif) {
        this.libelle = libelle;
        this.resume = resume;
        this.contenu = contenu;
        this.lien = lien;
        this.icone = icone;
        this.taille = taille;
        this.statut = statut;
        this.annee = annee;
        this.telecharger = telecharger;
        this.datecreate = datecreate;
        this.datemodif = datemodif;
    }

    public Publication(String taille, int annee, String statut, String telecharger, String libelle, String contenu, String lien, String icone, Date datecreate, Date datemodif) {
        this.libelle = libelle;
        this.contenu = contenu;
        this.lien = lien;
        this.icone = icone;
        this.statut = statut;
        this.annee = annee;
        this.telecharger = telecharger;
        this.datecreate = datecreate;
        this.datemodif = datemodif;
        this.taille = taille;
    }

    

    public Integer getIdPub() {
        return idPub;
    }

    public void setIdPub(Integer idPub) {
        this.idPub = idPub;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    public Date getDatemodif() {
        return datemodif;
    }

    public void setDatemodif(Date datemodif) {
        this.datemodif = datemodif;
    }

    public TypePublication getIdType() {
        return idType;
    }

    public void setIdType(TypePublication idType) {
        this.idType = idType;
    }

    public Compte getIdCompte() {
        return idCompte;
    }

    public void setIdCompte(Compte idCompte) {
        this.idCompte = idCompte;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPub != null ? idPub.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Publication)) {
            return false;
        }
        Publication other = (Publication) object;
        if ((this.idPub == null && other.idPub != null) || (this.idPub != null && !this.idPub.equals(other.idPub))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Publication[ idPub=" + idPub + " ]";
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getTelecharger() {
        return telecharger;
    }

    public void setTelecharger(String telecharger) {
        this.telecharger = telecharger;
    }

    public Integer getAnnee() {
        return annee;
    }

    public void setAnnee(Integer annee) {
        this.annee = annee;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }
    
}
