/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "type_publication")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypePublication.findAll", query = "SELECT t FROM TypePublication t ORDER BY t.idType DESC"),
    @NamedQuery(name = "TypePublication.findByIdType", query = "SELECT t FROM TypePublication t WHERE t.idType <> :idType AND t.idType <> :Type"),
    @NamedQuery(name = "TypePublication.findByLibelle", query = "SELECT t FROM TypePublication t WHERE t.libelle = :libelle")})
public class TypePublication implements Serializable {

    @Size(max = 10)
    @Column(name = "STATUT")
    private String statut;
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @Size(max = 255)
    @Column(name = "CREATEUR")
    private String createur;
    @Column(name = "DATEMODIF")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datemodif;
   

    @Size(max = 255)
    @Column(name = "ICONE")
    private String icone;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_TYPE")
    private Integer idType;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idType")
    private Collection<Publication> publicationCollection;

    public TypePublication() {
    }

    public TypePublication(Integer idType, String libelle, String icone, String createur, String statut, Date datemodif) {
        this.idType = idType;
        this.libelle = libelle;
        this.icone= icone;
         this.createur = createur;
        this.datemodif = datemodif;
        this.statut=statut;
    }

    public TypePublication(String libelle,String icone, String createur, String statut, Date datecreate) {
        this.libelle = libelle;
        this.icone= icone;
        this.createur = createur;
        this.datecreate = datecreate;
        this.datemodif = datemodif;
        this.statut=statut;
    }

    public Integer getIdType() {
        return idType;
    }

    public void setIdType(Integer idType) {
        this.idType = idType;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @XmlTransient
    public Collection<Publication> getPublicationCollection() {
        return publicationCollection;
    }

    public void setPublicationCollection(Collection<Publication> publicationCollection) {
        this.publicationCollection = publicationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idType != null ? idType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypePublication)) {
            return false;
        }
        TypePublication other = (TypePublication) object;
        if ((this.idType == null && other.idType != null) || (this.idType != null && !this.idType.equals(other.idType))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.TypePublication[ idType=" + idType + " ]";
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    public String getCreateur() {
        return createur;
    }

    public void setCreateur(String createur) {
        this.createur = createur;
    }

    public Date getDatemodif() {
        return datemodif;
    }

    public void setDatemodif(Date datemodif) {
        this.datemodif = datemodif;
    }

}
