/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "bouton")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bouton.findAll", query = "SELECT b FROM Bouton b"),
    @NamedQuery(name = "Bouton.findByIdBouton", query = "SELECT b FROM Bouton b WHERE b.idBouton = :idBouton"),
    @NamedQuery(name = "Bouton.findByLibelle", query = "SELECT b FROM Bouton b WHERE b.libelle = :libelle"),
    @NamedQuery(name = "Bouton.findByIcone", query = "SELECT b FROM Bouton b WHERE b.icone = :icone")})
public class Bouton implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_BOUTON")
    private Integer idBouton;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @Size(max = 50)
    @Column(name = "icone")
    private String icone;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bouton")
    private Collection<BoutonProfil> boutonProfilCollection;

    public Bouton() {
    }

    public Bouton(Integer idBouton) {
        this.idBouton = idBouton;
    }

    public Integer getIdBouton() {
        return idBouton;
    }

    public void setIdBouton(Integer idBouton) {
        this.idBouton = idBouton;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    @XmlTransient
    public Collection<BoutonProfil> getBoutonProfilCollection() {
        return boutonProfilCollection;
    }

    public void setBoutonProfilCollection(Collection<BoutonProfil> boutonProfilCollection) {
        this.boutonProfilCollection = boutonProfilCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBouton != null ? idBouton.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bouton)) {
            return false;
        }
        Bouton other = (Bouton) object;
        if ((this.idBouton == null && other.idBouton != null) || (this.idBouton != null && !this.idBouton.equals(other.idBouton))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Bouton[ idBouton=" + idBouton + " ]";
    }
    
}
