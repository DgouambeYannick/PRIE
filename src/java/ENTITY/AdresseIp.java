/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "adresse_ip")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdresseIp.findAll", query = "SELECT a FROM AdresseIp a"),
    @NamedQuery(name = "AdresseIp.findByIdAdresseIp", query = "SELECT a FROM AdresseIp a WHERE a.idAdresseIp = :idAdresseIp"),
    @NamedQuery(name = "AdresseIp.findByValeur", query = "SELECT a FROM AdresseIp a WHERE a.valeur = :valeur")})
public class AdresseIp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ADRESSE_IP")
    private Integer idAdresseIp;
    @Size(max = 255)
    @Column(name = "VALEUR")
    private String valeur;

    public AdresseIp() {
    }

    public AdresseIp(Integer idAdresseIp) {
        this.idAdresseIp = idAdresseIp;
    }

    public Integer getIdAdresseIp() {
        return idAdresseIp;
    }

    public void setIdAdresseIp(Integer idAdresseIp) {
        this.idAdresseIp = idAdresseIp;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAdresseIp != null ? idAdresseIp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdresseIp)) {
            return false;
        }
        AdresseIp other = (AdresseIp) object;
        if ((this.idAdresseIp == null && other.idAdresseIp != null) || (this.idAdresseIp != null && !this.idAdresseIp.equals(other.idAdresseIp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.AdresseIp[ idAdresseIp=" + idAdresseIp + " ]";
    }
    
}
