/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "newsletter")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Newsletter.findLASTID", query = "SELECT MAX(n.idNews) FROM Newsletter n"),
    @NamedQuery(name = "Newsletter.findFIRST", query = "SELECT MAX(n.idNews) FROM Newsletter n WHERE n.statut= :statut"),
    @NamedQuery(name = "Newsletter.findAll", query = "SELECT n FROM Newsletter n ORDER BY n.idNews DESC"),
    @NamedQuery(name = "Newsletter.findByIdNews", query = "SELECT n FROM Newsletter n WHERE n.idNews = :idNews"),
    @NamedQuery(name = "Newsletter.findByLibelle", query = "SELECT n FROM Newsletter n WHERE n.libelle = :libelle"),
    @NamedQuery(name = "Newsletter.findByLien", query = "SELECT n FROM Newsletter n WHERE n.lien = :lien"),
    @NamedQuery(name = "Newsletter.findByIcone", query = "SELECT n FROM Newsletter n WHERE n.icone = :icone"),
    @NamedQuery(name = "Newsletter.findByDatecreate", query = "SELECT n FROM Newsletter n WHERE n.datecreate = :datecreate")})
public class Newsletter implements Serializable {

    @Size(max = 15)
    @Column(name = "STATUT")
    private String statut;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_NEWS")
    private Integer idNews;
    @Size(max = 255)
    @Column(name = "LIBELLE")
    private String libelle;
    @Size(max = 255)
    @Column(name = "LIEN")
    private String lien;
    @Size(max = 255)
    @Column(name = "ICONE")
    private String icone;
    @Basic(optional = false)
    @Column(name = "DATECREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;

    public Newsletter() {
    }

    public Newsletter(Integer idNews) {
        this.idNews = idNews;
    }

    public Newsletter(String libelle, String lien, String icone, String statut, Date datecreate) {
        this.libelle = libelle;
        this.lien = lien;
        this.statut = statut;
        this.icone = icone;
        this.datecreate = datecreate;
    }

    

    public Integer getIdNews() {
        return idNews;
    }

    public void setIdNews(Integer idNews) {
        this.idNews = idNews;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNews != null ? idNews.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Newsletter)) {
            return false;
        }
        Newsletter other = (Newsletter) object;
        if ((this.idNews == null && other.idNews != null) || (this.idNews != null && !this.idNews.equals(other.idNews))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.Newsletter[ idNews=" + idNews + " ]";
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
    
}
