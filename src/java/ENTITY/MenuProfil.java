/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "menu_profil")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MenuProfil.findByMenuProfil", query = "SELECT m FROM MenuProfil m WHERE m.menuProfilPK.idMenu = :idMenu AND m.menuProfilPK.idProfil = :idProfil"),
    @NamedQuery(name = "MenuProfil.findAll", query = "SELECT m FROM MenuProfil m"),
    @NamedQuery(name = "MenuProfil.findByIdMenu", query = "SELECT m FROM MenuProfil m WHERE m.menuProfilPK.idMenu = :idMenu"),
    @NamedQuery(name = "MenuProfil.findByIdProfil", query = "SELECT m FROM MenuProfil m WHERE m.menuProfilPK.idProfil = :idProfil"),
    @NamedQuery(name = "MenuProfil.findByStatut", query = "SELECT m FROM MenuProfil m WHERE m.statut = :statut")})
public class MenuProfil implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MenuProfilPK menuProfilPK;
    
    @Column(name = "Statut")
    private Integer statut;
    @JoinColumn(name = "ID_PROFIL", referencedColumnName = "ID_PROFIL", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Profil profil;
    @JoinColumn(name = "ID_MENU", referencedColumnName = "ID_MENU", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Menu menu;

    public MenuProfil() {
    }

    public MenuProfil(MenuProfilPK menuProfilPK) {
        this.menuProfilPK = menuProfilPK;
    }
    public MenuProfil(MenuProfilPK menuProfilPK,int statut) {
        this.menuProfilPK = menuProfilPK;
        this.statut=statut;
    }

    public MenuProfil(int idMenu, int idProfil) {
        this.menuProfilPK = new MenuProfilPK(idMenu, idProfil);
    }

    public MenuProfilPK getMenuProfilPK() {
        return menuProfilPK;
    }

    public void setMenuProfilPK(MenuProfilPK menuProfilPK) {
        this.menuProfilPK = menuProfilPK;
    }

    public Integer getStatut() {
        return statut;
    }

    public void setStatut(Integer statut) {
        this.statut = statut;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menuProfilPK != null ? menuProfilPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MenuProfil)) {
            return false;
        }
        MenuProfil other = (MenuProfil) object;
        if ((this.menuProfilPK == null && other.menuProfilPK != null) || (this.menuProfilPK != null && !this.menuProfilPK.equals(other.menuProfilPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.MenuProfil[ menuProfilPK=" + menuProfilPK + " ]";
    }
    
}
