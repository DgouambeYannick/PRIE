/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "bouton_profil")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BoutonProfil.findByBoutonProfil", query = "SELECT b FROM BoutonProfil b WHERE b.boutonProfilPK.idBouton = :idBouton AND b.boutonProfilPK.idProfil = :idProfil"),
    @NamedQuery(name = "BoutonProfil.findAll", query = "SELECT b FROM BoutonProfil b"),
    @NamedQuery(name = "BoutonProfil.findByIdProfil", query = "SELECT b FROM BoutonProfil b WHERE b.boutonProfilPK.idProfil = :idProfil"),
    @NamedQuery(name = "BoutonProfil.findByIdBouton", query = "SELECT b FROM BoutonProfil b WHERE b.boutonProfilPK.idBouton = :idBouton"),
    @NamedQuery(name = "BoutonProfil.findByStatut", query = "SELECT b FROM BoutonProfil b WHERE b.statut = :statut")})
public class BoutonProfil implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BoutonProfilPK boutonProfilPK;
    @Column(name = "Statut")
    private Integer statut;
    @JoinColumn(name = "ID_BOUTON", referencedColumnName = "ID_BOUTON", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Bouton bouton;
    @JoinColumn(name = "ID_PROFIL", referencedColumnName = "ID_PROFIL", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Profil profil;

    public BoutonProfil() {
    }

    public BoutonProfil(BoutonProfilPK boutonProfilPK) {
        this.boutonProfilPK = boutonProfilPK;
    }

    public BoutonProfil(int idProfil, int idBouton) {
        this.boutonProfilPK = new BoutonProfilPK(idProfil, idBouton);
    }

    public BoutonProfilPK getBoutonProfilPK() {
        return boutonProfilPK;
    }

    public void setBoutonProfilPK(BoutonProfilPK boutonProfilPK) {
        this.boutonProfilPK = boutonProfilPK;
    }

    public Integer getStatut() {
        return statut;
    }

    public void setStatut(Integer statut) {
        this.statut = statut;
    }

    public Bouton getBouton() {
        return bouton;
    }

    public void setBouton(Bouton bouton) {
        this.bouton = bouton;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (boutonProfilPK != null ? boutonProfilPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BoutonProfil)) {
            return false;
        }
        BoutonProfil other = (BoutonProfil) object;
        if ((this.boutonProfilPK == null && other.boutonProfilPK != null) || (this.boutonProfilPK != null && !this.boutonProfilPK.equals(other.boutonProfilPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ENTITY.BoutonProfil[ boutonProfilPK=" + boutonProfilPK + " ]";
    }
    
}
