

<div class="main">
    <div class="container">
        <!-- BEGIN SERVICE BOX -->   

        <div class="row mix-block margin-bottom-40">

            <!-- TESTIMONIALS -->
            <div class="col-md-3 testimonials-v1">

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                        <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-user-md fa-1x" ></i>&nbsp;&nbsp;Nos Laboratoires </strong>
                        </h2>
                    </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                            <c:forEach var="listelabo" items="${menu}">
                                     <li><a style="text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                    
                            </c:forEach>
                        </ul>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                        <h2 class="panel-title" style="color:white">
                            <strong><i class="fa fa-bullhorn fa-1x" ></i>&nbsp;&nbsp;Nos Communications </strong>
                        </h2>
                    </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                            <li ><a href="annonce">Toutes nos Annonces</a></li>
                            <li class="active" ><a style="color:white" href="publication">Toutes Nos Publications</a></li>
                        </ul>
                    </div>

                </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                        <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-book fa-1x" ></i>&nbsp;&nbsp; A Propos </strong>
                        </h2>
                    </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                             <li><a href="about">Qui Sommes-Nous ?</a></li>
                        </ul>
                    </div>

                </div>								


            </div>

            <!-- END TESTIMONIALS -->

              <div class=" col-md-7 ">
               
               <div class="panel">
                   <div class="panel-heading">
                       <h2 class="tile-header">ALL <em class="font-blue-alt">${module.libelle}</em></h2>  
                   </div> 
                   <div class="panel-body">
                   <table id="datatable-responsive" class="table table-striped  responsive no-wrap" cellspacing="0"
                       width="100%">
                    <thead>
                    <tr>
                        <th></th>
                    </tr>
                    </thead>
                        
                    <tbody>
                         <c:forEach var="listeannonce" items="${allpub}">
                            <c:if test="${listeannonce.statut == 'publie'}">
                        <tr>
                            <td>
                                <div class="panel" style="background:#E7E7E7"> 
                                    <div class="panel-heading">
                                        <span style="float: right" class="icon-notification user-profile clearfix">
                                           <img width="50" src="admin/images/icone/${listeannonce.icone}" alt="Profile"/>
                                           <c:if test="${listeannonce.telecharger == 'oui'}">
                                            <a  href="iframe-pub?${listeannonce.idPub}" class="tooltip-button" data-placement="bottom" title="Telecharger doc taille=${listeannonce.taille}">
                                                 <i class="fa fa-download font-green font-size-20" ></i>
                                            </a>
                                           </c:if>
                                        </span>
                                      
                                        <strong style="color:black" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${listeannonce.libelle} </strong><br/>
                                     
                                    <em style="color: #CF051D">
                                        <i class="fa fa-calendar fa" ></i>
                                        <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listeannonce.datecreate}" />
                                    </em>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Par : ${listeannonce.idCompte.idMembre.civilite} ${listeannonce.idCompte.idMembre.nom} ${listeannonce.idCompte.idMembre.prenom}</strong>
                                        
                                    </div> 
                                    <div class="panel-body">  
                                        <!-- annonces-->
                                        <div class="col-md-12" style="background-color: white; color: black">
                                            ${listeannonce.resume} 
                                        </div>
                                    </div>   
                                </div>
                            </td>
                        </tr>
                        </c:if>
                        </c:forEach>
                    </tbody>
                  </table>
                   </div>  
                </div>
                
            </div>

            <!-- TESTIMONIALS -->
            <div class="col-md-2 testimonials-v1">
                 <div class="panel">
                      <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-bars fa-1x" ></i>&nbsp;&nbsp;Categories </strong>
                        </h2>
                      </div>
                        <div class="panel-body">
                            <ul class="nav sidebar-categories margin-bottom-40">
                              <li class="${activation2}"><a style="color:${color}; text-decoration: none" href="publication" title="Mon Curriculum Vitae">Curriculum Vitae</a></li> 
                                        
                              <c:forEach var="var" items="${allmodule}">
                                <c:choose>
                                    <c:when test="${var.idType == pageContext.request.getParameter('t')}">
                                        <c:if test="${var.statut == 'publie'}">
                                <li class="${activation}"><a style="color:white; text-decoration: none" href="publication?t=${var.idType}" title="${var.libelle}">${var.libelle}</a></li> 
                                        </c:if>
                                    </c:when>
                                     <c:otherwise>
                                         <c:if test="${var.statut == 'publie'}">
                                    <li><a href="publication?t=${var.idType}" title="${var.libelle}">${var.libelle}</a></li>      
                                         </c:if>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                               </ul>
                        </div>
                 </div>
  
            </div>
            <!-- END TESTIMONIALS -->
        </div>
     