
<div class="main">
    <div class="container">
        <div class="page-slider margin-bottom-40">
            <img class="img-responsive" src="${DIR_LABO}/${labo.image}" alt="">
        </div>  <!-- END SLIDER --> 
        <!-- BEGIN TABS AND TESTIMONIALS -->
        <div class="row mix-block margin-bottom-40">

                <!-- TESTIMONIALS -->
            <div class="col-md-3 testimonials-v1">
                 
                  <div class="panel">
                      <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-user-md fa-1x" ></i>&nbsp;&nbsp;Nos Laboratoires </strong>
                        </h2>
                      </div>
                        <div class="panel-body">
                            <ul class="nav sidebar-categories margin-bottom-40">
                              <c:forEach var="listelabo" items="${menu}">
                                <c:choose>
                                    <c:when test="${listelabo.idLabo == pageContext.request.getQueryString()}">
                                <li class="active"><a style="color:white; text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                     </c:when>
                                     <c:otherwise>
                                     <li><a style="text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                     </c:otherwise>
                                </c:choose>
                            </c:forEach>
                               </ul>
                        </div>
                        </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-bullhorn fa-1x" ></i>&nbsp;&nbsp;Nos Communications </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                          <li ><a href="annonce">Toutes nos Annonces</a></li>
                          <li ><a href="publication">Toutes Nos Publications</a></li>
                        </ul>
                    </div>

                </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-book fa-1x" ></i>&nbsp;&nbsp; A Propos </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                          <li><a href="about">Qui Sommes-Nous ?</a></li>
                        </ul>
                    </div>

                </div>								


            </div>

            <!-- TABS -->
            <div class="col-md-9" style="background-color: white">

                <div class="panel">
                    <div class="panel-heading">
                       <h2 class="tile-header"><span class="font-blue-alt">Liste des Membres du LABO - ${labo.sigle}</span></h2>  
                   </div> 
                    <div class="panel-body">
                        <table id="datatable-responsive"
                               class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                               width="100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>NOM&PRENOM</th>
                                    <th>TUTELLE</th>
                                    <th>FONCTION</th>
                                    <th>STATUT</th>
                                </tr>
                            </thead>

                            <tbody>
                                <c:forEach var="listeM" items="${membres}">
                                    <tr>
                                        <td>
                                            <span class="icon-notification user-profile clearfix">
                                                <img width="50" src="${DIR_AVATAR}/${listeM.image}" alt="Profile">
                                            </span>
                                        </td>
                                        <td><a title="Voir page de ${listeM.civilite} ${listeM.nom}" data-placement="bottom" class="tooltip-button" href="perso?${listeM.nomid}" style="text-decoration: none">${listeM.civilite} ${listeM.nom} ${listeM.prenom}</a></td>
                                        <td>${listeM.tutelle}</td>
                                        <td>${listeM.fonction}</td>
                                        <td style="color:#b72724;">${listeM.responsable}</td>
                                    </tr>
                                   </c:forEach>
                            </tbody>
                        </table>									
                    </div>
                </div>


            </div>
        </div>
            
