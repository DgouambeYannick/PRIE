<div class="main">
    <div class="container">
        <!-- BEGIN SLIDER -->
        <div class="page-slider margin-bottom-40">
            <img class="img-responsive" src="${DIR_LABO}/${membre.idLabo.image}" alt="">
        </div>  <!-- END SLIDER -->       
        <div class="row mix-block margin-bottom-40">
                <!-- TESTIMONIALS -->
            <div class="col-md-3 testimonials-v1">                   
                  <div class="panel">
                      <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-user-md fa-1x" ></i>&nbsp;&nbsp;Nos Laboratoires </strong>
                        </h2>
                      </div>
                        <div class="panel-body">
                            <ul class="nav sidebar-categories margin-bottom-40">
                              <c:forEach var="listelabo" items="${menu}">
                                <c:choose>
                                    <c:when test="${listelabo.idLabo == membre.idLabo.idLabo}">
                                <li class="active"><a style="color:white; text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                     </c:when>
                                     <c:otherwise>
                                     <li><a style="text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                     </c:otherwise>
                                </c:choose>
                            </c:forEach>
                               </ul>
                        </div>
                        </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong><i class="fa fa-bullhorn fa-1x" ></i>&nbsp;&nbsp;Nos Communications </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                          <li ><a href="annonce">Toutes nos Annonces</a></li>
                          <li ><a href="publication">Toutes Nos Publications</a></li>
                        </ul>
                    </div>

                </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong style="text-transform: uppercase"><i class="fa fa-book fa-1x" ></i>&nbsp;&nbsp; A Propos </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                         <li><a href="about">Qui Sommes-Nous ?</a></li>
                        </ul>
                    </div>

                </div>								
            </div>

            <!-- END TESTIMONIALS -->
             <div class=" col-md-7 ">
               <div class="panel"> 
                    <div class="panel-body">

                        <div class="col-lg-2">
                            <span class="icon-notification user-profile clearfix">      
                                <img width="100" src="${DIR_AVATAR}/${membre.image}" alt="">
                            </span> 
                        </div>
                        <div class="col-lg-10">
                            <h4>
                                <a style="text-decoration: none" href="perso?${membre.nomid}">${membre.civilite} ${membre.nom} ${membre.prenom}</a>
                            </h4>
                            <em class="font-blue-alt">${membre.fonction}</em><br/>
                            Tutelle :<em class="font-blue-alt"> ${membre.tutelle}</em><br/>
                            Email :<em class="font-blue-alt"> ${membre.email}</em><br/>
                            Tel :<em class="font-blue-alt"> ${membre.phone}</em><br/>
                            Adresse :<em class="font-blue-alt"> ${membre.adresse}</em><br/>
                            Mon URL :<em class="font-blue-alt font-bold"> ${URL}</em><br/>
                        </div>
                    </div>
                </div>
               <div class="panel">
                   <div class="panel-heading">
                       <h2 class="tile-header">ALL <em class="font-blue-alt">${module.libelle}</em></h2>  
                   </div> 
                   <div class="panel-body">
                   <table id="datatable-responsive" class="table table-striped  responsive no-wrap" cellspacing="0"
                       width="100%">
                    <thead>
                    <tr>
                        <th></th>
                    </tr>
                    </thead>
                        
                    <tbody>
                         <c:forEach var="listeannonce" items="${allpubtype}">
                            <c:if test="${listeannonce.statut == 'publie'}">
                        <tr>
                            <td>
                                <div class="panel" style="background:#E7E7E7"> 
                                    <div class="panel-heading">
                                        <span style="float: right" class="icon-notification user-profile clearfix">
                                           <img width="50" src="admin/images/icone/${listeannonce.icone}" alt="Profile"/>
                                           <c:if test="${listeannonce.telecharger == 'oui'}">
                                            <a href="iframe-pub?${listeannonce.idPub}" class="tooltip-button" data-placement="bottom" title="Telecharger ${listeannonce.taille}">
                                                 <i class="fa fa-download font-green font-size-20" ></i>
                                            </a>
                                           </c:if>
                                        </span>
                                   
                                        <strong style="color:black" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${listeannonce.libelle} </strong><br/>
                                     
                                    <em style="color: #CF051D">
                                        <i class="fa fa-calendar fa" ></i>
                                        <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listeannonce.datecreate}" />
                                    </em>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Par : ${listeannonce.idCompte.idMembre.civilite} ${listeannonce.idCompte.idMembre.nom} ${listeannonce.idCompte.idMembre.prenom}</strong>
                                        
                                    </div> 
                                    <div class="panel-body">  
                                        <!-- annonces-->
                                        <div class="col-md-12" style="background-color: white; color: black">
                                            ${listeannonce.contenu} 
                                        </div>
                                    </div>   
                                </div>
                            </td>
                        </tr>
                        </c:if>
                        </c:forEach>
                    </tbody>
                  </table>
                   </div>  
                </div>
                
            </div>
            <!-- END TABS -->
            <!-- TESTIMONIALS -->
            <div class="col-md-2 testimonials-v1">
                 <div class="panel">
                      <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-bars fa-1x" ></i>&nbsp;&nbsp;Categories </strong>
                        </h2>
                      </div>
                        <div class="panel-body">
                             <ul class="nav sidebar-categories margin-bottom-40">
                              <li class="${activation2}"><a style="color:${color}; text-decoration: none" href="perso?${membre.nomid}" title="Mon Curriculum Vitae">Curriculum Vitae</a></li> 
                                        
                              <c:forEach var="var" items="${allmodule}">
                                <c:choose>
                                    <c:when test="${var.idType == pageContext.request.getParameter('t')}">
                                        <c:if test="${var.statut == 'publie'}">
                                <li class="${activation}"><a style="color:white; text-decoration: none" href="perso?t=${var.idType}&AMP;i=${membre.idMembre}" title="${var.libelle}">${var.libelle}</a></li> 
                                        </c:if>
                                    </c:when>
                                     <c:otherwise>
                                         <c:if test="${var.statut == 'publie'}">
                                         <li><a href="perso?t=${var.idType}&AMP;i=${membre.idMembre}" title="${var.libelle}">${var.libelle}</a></li>      
                                         </c:if>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                               </ul>
                        </div>
                 </div>
  
            </div>
            <!-- END TESTIMONIALS -->
        </div>                
  
