
<div class="main">
    <div class="container">

        <!-- BEGIN TABS AND TESTIMONIALS -->
        <div class="row mix-block margin-bottom-40">

            <!-- TABS -->
            <div class=" col-md-12 ">

                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="tile-header">RESULTATS DE LA RECHERCHE : <em class="font-blue-alt">${element}</em></h2>  
                    </div> 
                    <div class="panel-body">
                        <table id="datatable-responsive" class="table table-striped  responsive no-wrap" cellspacing="0"
                               width="100%">
                            <thead>
                                <tr>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                <c:if test="${TestMemb == 0}">
                                    <c:forEach var="liste" items="${SearchMembre}">
                                        <tr>
                                            <td>
                                                <div class="panel" style="background:#E7E7E7">
                                                    <div class="panel-heading">
                                                        <span style="float: left" class="icon-notification user-profile clearfix">
                                                            <img width="50" src="${DIR_AVATAR}/${liste.image}" alt="Profile">
                                                        </span>
                                                        <strong style="color:black" >&nbsp;&nbsp;&nbsp;&nbsp;<a title="Voir page de ${liste.civilite} ${liste.nom}" data-placement="bottom" class="tooltip-button" href="perso?${liste.nomid}" style="text-decoration: none">${liste.civilite} ${liste.nom} ${liste.prenom}</a> </strong>
                                                        <em style="float: right">Dans Categorie : Membres</em>
                                                    </div>   
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${TestThem == 0}">
                                    <c:forEach var="liste" items="${SearchThematique}">
                                        <tr>
                                            <td>
                                                <div class="panel" style="background:#E7E7E7"> 
                                                    <div class="panel-heading">
                                                        <strong style="color:black" >&nbsp;&nbsp;&nbsp;&nbsp;<a data-placement="bottom" class="tooltip-button" href="themes?${liste.getIdLabo().getIdLabo()}" style="text-decoration: none">${liste.titre}</a> </strong>
                                                        Laboratoire : <em class="font-blue-alt">${liste.getIdLabo().getLibelle()}</em>
                                                        <em style="float: right">Dans Categorie : Thematiques</em>
                                                    </div>   
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${TestLabo == 0}">
                                    <c:forEach var="liste" items="${SearchLabo}">
                                        <tr>
                                            <td>
                                                <div class="panel" style="background:#E7E7E7"> 
                                                    <div class="panel-heading">
                                                        <strong style="color:black" >&nbsp;&nbsp;&nbsp;&nbsp;<a data-placement="bottom" class="tooltip-button" href="laboratoire?${liste.idLabo}" style="text-decoration: none">${liste.libelle}</a> (${liste.sigle}) </strong>
                                                        <em style="float: right">Dans Categorie : Laboratoires</em>
                                                    </div>   
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${TestPub == 0}">
                                    <c:forEach var="liste" items="${SearchPub}">
                                        <tr>
                                            <td>
                                                <div class="panel" style="background:#E7E7E7"> 
                                                    <div class="panel-heading">
                                                        <strong style="color:black" >&nbsp;&nbsp;&nbsp;&nbsp;<a data-placement="bottom" class="tooltip-button" href="publication?t=${liste.getIdType().getIdType()}" style="text-decoration: none">${liste.libelle}</a> </strong>
                                                        Type Publication : <em class="font-blue-alt">${liste.getIdType().getLibelle()}</em>
                                                        <em style="float: right">Dans Categorie : Publications</em>
                                                    </div>   
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:if>

                            </tbody>
                        </table>
                    </div>  
                </div>

            </div>

            <!-- Div du labo -->
            <hr/>

        </div>

