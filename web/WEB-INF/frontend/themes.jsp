
<div class="main">
    <div class="container">
        <!-- BEGIN SLIDER -->
        <div class="page-slider margin-bottom-40">
            <img class="img-responsive" src="${DIR_LABO}/${labo.image}" alt="">
        </div>  <!-- END SLIDER -->       
        <div class="row mix-block margin-bottom-40">
            <!-- TESTIMONIALS -->
            <div class="col-md-3 testimonials-v1">

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                        <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-user-md fa-1x" ></i>&nbsp;&nbsp;Nos Laboratoires </strong>
                        </h2>
                    </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                            <c:forEach var="listelabo" items="${menu}">
                                <c:choose>
                                    <c:when test="${listelabo.idLabo == pageContext.request.getQueryString()}">
                                        <li class="active"><a style="color:white; text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                        </c:when>
                                        <c:otherwise>
                                        <li><a style="text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                        </ul>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                        <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-bullhorn fa-1x" ></i>&nbsp;&nbsp;Nos Communications </strong>
                        </h2>
                    </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                            <li ><a href="annonce">Toutes nos Annonces</a></li>
                            <li ><a href="publication">Toutes Nos Publications</a></li>
                        </ul>
                    </div>

                </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                        <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-book fa-1x" ></i>&nbsp;&nbsp; A Propos </strong>
                        </h2>
                    </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                            <li><a href="about">Qui Sommes-Nous ?</a></li>
                        </ul>
                    </div>

                </div>								
            </div>

            <!-- END TESTIMONIALS -->
            <script type="text/javascript" src="admin/assets/widgets/accordion-ui/accordion.js"></script>
            <script type="text/javascript">/* jQuery UI Accordion */

                $(function () {
                    "use strict";
                    $(".accordion").accordion({
                        heightStyle: "content"
                    });
                });

                $(function () {
                    "use strict";
                    $("#accordion-hover")
                            .accordion({
                                event: 'mouseover',
                                heightStyle: 'auto'
                            });
                });</script>
            <!-- TABS -->
            <div class=" col-md-9" style="background-color: white">
                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="tile-header"><span class="font-blue-alt">Liste des Thématiques du ${labo.sigle}</span></h2>  
                    </div> 
                    <div class="panel-body">
                        <div class="example-box-wrapper">
                            <div class="accordion">
                                <c:forEach var="liste" items="${alltheme}">
                                    <c:if test="${liste.statut == 'publie'}">
                                        <h3>${liste.titre}</h3>

                                        <div>
                                            ${liste.contenu}
                                        </div> 
                                        </c:if>
                                    </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
