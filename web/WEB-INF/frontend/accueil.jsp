
<div class="main">
    <div class="container">
           <!-- BEGIN SLIDER -->
    <div class=" page-slider margin-bottom-40">
      <div class="fullwidthbanner-container revolution-slider">       
        <div class="fullwidthabnner">        
          <ul id="revolutionul">
            <!-- THE NEW SLIDE -->
            <c:forEach var="liste" items="${allslider}">
            <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" >
              <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
              <img src="front/img/about.jpg" alt="">
                   <!-- description du slide-->
                    <div class="caption lft slide_subtitle_white slide_item_left"
                         data-x="30"
                         data-y="10"
                         data-speed="400"
                         data-start="1500"
                         data-easing="easeOutExpo">
                         <img src="front/img/info.png" alt="Image 1">
                    </div>

                    <div class="caption lfl bg-black-opacity-alt slide_desc slide_item_left font-white"
                         style="padding-left: 10px; padding-right: 10px; font-weight: bold; font-family: 'Times New Roman'"
                        data-x="100"
                        data-y="180"
                        data-speed="400"
                        data-start="2000"
                        data-easing="easeOutExpo">
                      <p style=" text-transform: lowercase; color:white; font-family: 'Times New Roman'; text-decoration: none">${liste.description}</p>
                    </div> 
                    <!-- image de principal affich�e � droite-->
                    <div class="caption lfb"
                         data-x="640" 
                         data-y="0" 
                         data-speed="700" 
                         data-start="1000" 
                         data-easing="easeOutExpo">
                        <img src="${DIR_SLIDER}/${liste.lien}" alt="${liste.lien}">
                    </div>
              
            </li>
            </c:forEach>
                </ul>
                
                <div style="background-color: red" class=" bg-red tp-bannertimer tp-bottom"></div>
                
            </div>
        </div>
    </div>
    <!-- END SLIDER -->
        <!-- BEGIN TABS AND TESTIMONIALS -->
        <div class="row margin-bottom-40">
            <!-- TABS -->
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                        <h3 class="panel-title " style="color:white">
                            <strong ><i  class="fa fa-bullhorn fa-1x"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dernieres Annonces du P.R.I.E  </strong>
                        </h3>  
                    </div>
                    <div class="panel-body" style="background:white">
                        <c:forEach var="listeannonce" items="${allannonce}">
                            <c:if test="${listeannonce.statut == 'publie'}">
                        <div class="panel" style="background:#E7E7E7"> 
                            <div class="panel-heading">
                                <span style="float: right" class="icon-notification user-profile clearfix">
                                   <img width="30" src="admin/images/icone/${listeannonce.icone}" alt="Profile"/>
                                   <c:if test="${listeannonce.telecharger == 'oui'}">
                                    <a  href="iframe-annonce?${listeannonce.idPub}" class="tooltip-button" data-placement="bottom" title="Telecharger doc taille= ${listeannonce.taille}">
                                         <i class="fa fa-download font-green font-size-20" ></i>
                                    </a>
                                   </c:if>
                                </span>
                             
                                <strong style="color:black" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${listeannonce.libelle} </strong><br/>
                             
                            <em style="color: #CF051D">
                                <i class="fa fa-calendar fa" ></i>
                                <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listeannonce.datecreate}" />
                            </em>
                            </div> 
                            <div class="panel-body">  
                                <!-- annonces-->
                                <div class="col-md-12">
                                    ${listeannonce.resume} <br/>
                                    <a href="details?${listeannonce.idPub}" style="float: right; text-decoration: none; color: black" class="tooltip-button" data-placement="bottom" title="voir l'integralit� de cette annonce">
                                        <i style="color:#CF051D" class="fa fa-arrow-circle-right fa"></i>Lire la suite...
                                    </a>
                                </div>
                            </div>
                               
                                    
                            
                        </div>
                        </c:if>
                        </c:forEach>
                            <a href="annonce" style="float: right; text-decoration: none; color: black" class="tooltip-button" data-placement="bottom" title="Voir toutes les annonces">
                                <i style="color:#CF051D"   class="font-green fa fa-arrow-circle-right fa"></i>Voir PLus...
                            </a>
                        
                    </div>

                </div>

            </div>
            <!-- END TABS -->
            
            <!-- TESTIMONIALS -->
            <div class="col-sm-4 testimonials-v1">

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                        <h3 class="panel-title" style="color:white">
                            <strong><i class="fa fa-rocket fa-1x"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nos NewsLetters </strong>
                        </h3>  
                    </div>
                    <div class="panel-body">
                        <div id="myCarousel" class="carousel slide">
                            <!-- Carousel items -->
                            <div class="carousel-inner">
                                <div class="active item">

                                    <blockquote>
                                        
                                        <span class="icon-notification user-profile clearfix">
                                            <a href="iframe-news?${first.idNews}" class="tooltip-button" data-placement="bottom" title="consulter"> <img class="img-responsive" width="50" src="admin/images/icone/${first.icone}" alt="Profile"></a>
                                        </span>
                                        <em style="color: #CF051D">
                                            <i class="fa fa-calendar fa" ></i>
                                            <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${first.datecreate}" />
                                        </em>
                                        <p>${first.libelle}</p>
                                    </blockquote>

                                </div>
                          
                                <c:forEach var="liste" items="${allnewsletter}">
                                    <c:if test="${liste.statut=='publie'}">
                                <div class="item">

                                    <blockquote>
                                       
                                        <span class="icon-notification user-profile clearfix">
                                            <a href="iframe-news?${first.idNews}" class="tooltip-button" data-placement="bottom" title="consulter"> <img width="50" src="admin/images/icone/${liste.icone}" alt="Profile"></a>
                                        </span>
                                         <em style="color: #CF051D">
                                            <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${liste.datecreate}" />
                                        </em>
                                        <p>${liste.libelle}</p>
                                    </blockquote>
                                </div>
                                    </c:if>
                                </c:forEach>
                            </div>

                            <!-- Carousel nav -->
                            <a class="left-btn" href="#myCarousel" data-slide="prev"></a>
                            <a class="right-btn" href="#myCarousel" data-slide="next"></a>
                        </div>
                    </div>
                </div>

                <br/>

                <div class="panel">
                    <div class="panel-body">
                        ${facebook.contenu}
                       </div>
                </div>

            </div>
            <!-- END TESTIMONIALS -->
        </div>  
                                    
        <!-- BEGIN CLIENTS -->
        <div class="row margin-bottom-40 our-clients">
          <div class="col-md-3">
            <h2>Nos Partenaires</h2>
            <p>Les Ecoles partenaires du P�le Recherche Innovation et Entrepreneuriat de l'IUC</p>
          </div>
          <div class="col-md-9">
            <div class="owl-carousel owl-carousel6-brands">
              <div class="client-item">
                <a target="_blank" href="http://www.iuc-univ.net/">
                  <img src="front/img/iucc.png" class="img-responsive" alt="">
                  <img src="front/img/iucc.png" class="color-img img-responsive" alt="">
                </a>
              </div>
              <div class="client-item">
                <a href="#">
                  <img src="front/img/3iac.png" class="img-responsive" alt="">
                  <img src="front/img/3iac.png" class="color-img img-responsive" alt="">
                </a>
              </div>
              <div class="client-item">
                <a href="#">
                  <img src="front/img/icia.png" class="img-responsive" alt="">
                  <img src="front/img/icia.png" class="color-img img-responsive" alt="">
                </a>
              </div>
                <div class="client-item">
                <a href="#">
                  <img src="front/img/esstin.png" class="img-responsive" alt="">
                  <img src="front/img/esstin.png" class="color-img img-responsive" alt="">
                </a>
              </div>
              <div class="client-item">
                <a href="#">
                  <img src="front/img/istdi.png" class="img-responsive" alt="">
                  <img src="front/img/istdi.png" class="color-img img-responsive" alt="">
                </a>
              </div>
              
              <div class="client-item">
                <a href="#">
                  <img src="front/img/ismans.png" class="img-responsive" alt="">
                  <img src="front/img/ismans.png" class="color-img img-responsive" alt="">
                </a>
              </div>   
                <div class="client-item">
                <a href="#">
                  <img src="front/img/3il.png" class="img-responsive" alt="">
                  <img src="front/img/3il.png" class="color-img img-responsive" alt="">
                </a>
              </div> 
                <div class="client-item">
                <a href="#">
                  <img src="front/img/politech.png" class="img-responsive" alt="">
                  <img src="front/img/politech.png" class="color-img img-responsive" alt="">
                </a>
              </div> 
                <div class="client-item">
                <a href="#">
                  <img src="front/img/maine.png" class="img-responsive" alt="">
                  <img src="front/img/maine.png" class="color-img img-responsive" alt="">
                </a>
              </div> 
                <div class="client-item">
                <a href="#">
                  <img src="front/img/loraine.png" class="img-responsive" alt="">
                  <img src="front/img/loraine.png" class="color-img img-responsive" alt="">
                </a>
              </div>
            </div>
          </div>          
        </div>
        <!-- END CLIENTS -->
        
    </div>
</div>
    <div id="fb-root"></div>
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

    <!-- BEGIN FOOTER -->
    <div class="footer">
      <div class="container">
        <div class="row">
          <!-- BEGIN COPYRIGHT -->
         <div class="col-md-2  padding-top-10 font-green font-normal">
            2016 � ALL Rights Reserved. by P.R.I.E
            
          </div>
          <div class="col-md-5">
          <ul class="list-unstyled list-inline padding-top-10">
                <li><i class="fa fa-phone"></i><span>+237 691 678 278</span></li>
                <li><i class="fa fa-phone"></i><span>+237 650 027 931</span></li>
                <li><i class="fa fa-envelope-o"></i><span>prie@myiuc.com</span></li>
                <li><a style="text-decoration: none" href="admin/index"><i class="fa fa-lock"></i> Connexion</a></li>
          </ul>
          </div>
          <!-- END COPYRIGHT -->
          <!-- BEGIN PAYMENTS -->
           <div class="col-md-2 ">
            <ul class="social-footer list-unstyled list-inline pull-right">
                <li><a target="_blank" href="https://www.facebook.com/P%C3%B4le-Recherche-Innovation-et-Entrepreneuriat-PRIE-1608660849448534"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube"></i></a></li>
            </ul>  
          </div>
          <div class="col-md-3  padding-top-10 font-green font-normal">
            developers <a target="_blank" style="text-decoration: none" href="https://www.linkedin.com/in/yannick-dgouambe-971751103">Dgouambe Yannick</a> | 
                  <a style="text-decoration: none" class="font-normal"  href="#">Saah David</a>   
          </div>
          <!-- END PAYMENTS -->
        </div>
      </div>
    </div>
    <!-- END FOOTER -->

<script type="text/javascript" src="admin/assets/widgets/modal/modal.js"></script>
                
    <script src="front/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="front/assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="front/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="front/assets/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="front/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
    <script src="front/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->

    <!-- BEGIN RevolutionSlider -->

    <script src="front/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.plugins.min.js" type="text/javascript"></script>
    <script src="front/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
    <script src="front/assets/frontend/pages/scripts/revo-slider-init.js" type="text/javascript"></script>
    <!-- END RevolutionSlider -->
    
    <script type="text/javascript" src="admin/assets/widgets/input-switch/inputswitch-alt.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/slimscroll/slimscroll.js"></script>
    <script src="front/assets/frontend/layout/scripts/layout.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();
            Layout.initOWL();
            RevosliderInit.initRevoSlider();
            Layout.initTwitter();

            Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
            Layout.initNavScrolling();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
