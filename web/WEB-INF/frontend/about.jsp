
<div class="main">
    <div class="container">
        <!-- BEGIN TABS AND TESTIMONIALS -->
        <div class="row mix-block margin-bottom-40">

           <!-- TESTIMONIALS -->
            <div class="col-md-3 testimonials-v1">
                 
                  <div class="panel">
                      <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-user-md fa-1x" ></i>&nbsp;&nbsp;Nos Laboratoires </strong>
                        </h2>
                      </div>
                        <div class="panel-body">
                            <ul class="nav sidebar-categories margin-bottom-40">
                              <c:forEach var="listelabo" items="${menu}">
                                     <li><a style="text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                    
                            </c:forEach>
                               </ul>
                        </div>
                        </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-bullhorn fa-1x" ></i>&nbsp;&nbsp;Nos Communications </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                          <li ><a href="annonce">Toutes nos Annonces</a></li>
                          <li ><a href="publication">Toutes Nos Publications</a></li>
                        </ul>
                    </div>

                </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-book fa-1x" ></i>&nbsp;&nbsp; A Propos </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                          <li class="active"><a style="color:white" href="about">Qui Sommes-Nous ?</a></li>
                        </ul>
                    </div>

                </div>								


            </div>

            <!-- END TESTIMONIALS -->
            
            <!-- TABS -->
            <div class=" col-md-9 tab-style-1" style="background-color: white">
                <div class="panel">
                    <div class="panel-heading">
                        <h2 class="tile-header font-red">${about.libelle}</h2>
                    </div>

                    <div class="panel-body">

                        ${about.contenu}

                    </div>

                </div><hr/>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="tile-header">LES RESPONSABLES DE NOS LABORATOIRES</h3>
                    </div>

                    <div class="panel-body">
                         <div class="row front-team">
                            <ul class="list-unstyled">
                                <c:forEach var="liste" items="${responsable}">
                              <li class="col-md-3">
                                <div class="thumbnail">
                                  <span class="icon-notification user-profile clearfix">
                                    <img width="110" alt="" src="${DIR_AVATAR}/${liste.image}">
                                  </span>
                                  <h3>
                                    <strong>${liste.civilite} ${liste.nom}</strong> 
                                    <small class="font-blue-alt">${liste.fonction}</small>
                                  </h3>
                                  <p>Labo - ${liste.idLabo.sigle}</p>
                                  <center>
                                      <a href="perso?${liste.nomid}" class="btn btn-primary font-normal">Savoir Plus
                                          <i class="fa fa-thumbs-o-up"></i>
                                      </a>
                                  </center>
                                </div>
                              </li>
                              </c:forEach>
                            </ul>            
                          </div>
                    </div>

                </div><hr/>
                <div class="panel">
                    <div class="panel-body">
                        ${google.contenu}
                    </div>
                </div>

            </div>
        </div>




  