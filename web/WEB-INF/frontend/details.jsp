
<div class="main">
    <div class="container">

        <!-- BEGIN TABS AND TESTIMONIALS -->
        <div class="row mix-block margin-bottom-40">

             <!-- TESTIMONIALS -->
            <div class="col-md-3 testimonials-v1">
                 
                  <div class="panel">
                      <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-user-md fa-1x" ></i>&nbsp;&nbsp;Nos Laboratoires </strong>
                        </h2>
                      </div>
                        <div class="panel-body">
                            <ul class="nav sidebar-categories margin-bottom-40">
                              <c:forEach var="listelabo" items="${menu}">
                                     <li><a style="text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                    
                            </c:forEach>
                               </ul>
                        </div>
                        </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-bullhorn fa-1x" ></i>&nbsp;&nbsp;Nos Communications </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                          <li class="active"><a style="color:white" href="annonce">Toutes nos Annonces</a></li>
                          <li ><a href="publication">Toutes Nos Publications</a></li>
                        </ul>
                    </div>

                </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-book fa-1x" ></i>&nbsp;&nbsp; A Propos </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                          <li><a href="about">Qui Sommes-Nous ?</a></li>
                        </ul>
                    </div>
                </div>								
            </div>

            <!-- END TESTIMONIALS -->
            
            <!-- TABS -->
            <div class=" col-md-9 ">
               
               <div class="panel">
                   <div class="panel-heading">
                       <span style="float: right" class="icon-notification user-profile clearfix">
                           <img width="50" src="admin/images/icone/${annonce.icone}" alt="Profile"/>
                           <c:if test="${annonce.telecharger == 'oui'}">
                            <a href="iframe-annonce?${annonce.idPub}" class="tooltip-button" data-placement="bottom" title="Telecharger ${annonce.taille}">
                                 <i class="fa fa-download font-green font-size-20" ></i>
                            </a>
                           </c:if>
                        </span>
                       <h2 class="tile-header">${annonce.libelle}</h2>
                       <em style="color: #CF051D">
                            <i class="fa fa-calendar fa" ></i>
                            <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${annonce.datecreate}" />
                        </em> 
                   </div> 
                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FP%25C3%25B4le-Recherche-Innovation-et-Entrepreneuriat-PRIE-1608660849448534%2F&width=88&layout=button_count&action=like&size=large&show_faces=true&share=false&height=21&appId" width="88" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                   
                   <div class="panel-body"><hr/>
                        <p> ${annonce.contenu}</p><br/>
                       </div>  
                </div>
                
            </div>

            <!-- Div du labo -->
            <hr/>

        </div>
   

