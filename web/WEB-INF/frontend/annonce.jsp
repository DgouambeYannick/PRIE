
<div class="main">
    <div class="container">

        <!-- BEGIN TABS AND TESTIMONIALS -->
        <div class="row mix-block margin-bottom-40">

             <!-- TESTIMONIALS -->
            <div class="col-md-3 testimonials-v1">
                 
                  <div class="panel">
                      <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-user-md fa-1x" ></i>&nbsp;&nbsp;Nos Laboratoires </strong>
                        </h2>
                      </div>
                        <div class="panel-body">
                            <ul class="nav sidebar-categories margin-bottom-40">
                              <c:forEach var="listelabo" items="${menu}">
                                     <li><a style="text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                    
                            </c:forEach>
                               </ul>
                        </div>
                        </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-bullhorn fa-1x" ></i>&nbsp;&nbsp;Nos Communications </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                          <li class="active"><a style="color:white" href="annonce">Toutes nos Annonces</a></li>
                          <li ><a href="publication">Toutes Nos Publications</a></li>
                        </ul>
                    </div>

                </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-book fa-1x" ></i>&nbsp;&nbsp; A Propos </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                          <li><a href="about">Qui Sommes-Nous ?</a></li>
                        </ul>
                    </div>
                </div>								
            </div>

            <!-- END TESTIMONIALS -->
            
            <!-- TABS -->
            <div class=" col-md-9 ">
               
               <div class="panel">
                   <div class="panel-heading">
                       <h2 class="tile-header">TOUTES NOS ANNONCES</h2>  
                   </div> 
                   <div class="panel-body">
                   <table id="datatable-responsive" class="table table-striped  responsive no-wrap" cellspacing="0"
                       width="100%">
                    <thead>
                    <tr>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                        <c:forEach var="listeannonce" items="${allannonce}">
                            <c:if test="${listeannonce.statut == 'publie'}">
                        <tr>
                            <td>
                                <div class="panel" style="background:#E7E7E7"> 
                                    <div class="panel-heading">
                                        <span style="float: right" class="icon-notification user-profile clearfix">
                                           <img width="30" src="admin/images/icone/${listeannonce.icone}" alt="Profile"/>
                                           <c:if test="${listeannonce.telecharger == 'oui'}">
                                            <a href="iframe-annonce?${listeannonce.idPub}" class="tooltip-button" data-placement="bottom" title="Telecharger doc taille= ${listeannonce.taille}">
                                                 <i class="fa fa-download font-green font-size-20" ></i>
                                            </a>
                                           </c:if>
                                        </span>
                                      
                                        <strong style="color:black" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${listeannonce.libelle} </strong><br/>
                                      
                                    <em style="color: #CF051D">
                                        <i class="fa fa-calendar fa" ></i>
                                        <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listeannonce.datecreate}" />
                                    </em>
                                    </div> 
                                    <div class="panel-body">  
                                        <!-- annonces-->
                                        <div class="col-md-12">
                                            ${listeannonce.resume} <br/>
                                            <a href="details?${listeannonce.idPub}" style="float: right; text-decoration: none; color: black" class="tooltip-button" data-placement="bottom" title="voir l'integralité de cette annonce">
                                                <i style="color:#CF051D" class="fa fa-arrow-circle-right fa"></i>Lire la suite...
                                            </a>
                                        </div>
                                    </div>   
                                </div>
                            </td>
                        </tr>
                        </c:if>
                        </c:forEach>
                    </tbody>
                  </table>
                   </div>  
                </div>
                
            </div>

            <!-- Div du labo -->
            <hr/>

        </div>
