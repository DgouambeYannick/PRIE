<%-- 
    Document   : newjsp
    Created on : 24 mai 2016, 09:52:47
    Author     : HP
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <!--[if IE]>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title>Error</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
      <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="admin/images/favicon/prie144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114"
              href="admin/images/favicon/prie114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72"
              href="admin/images/favicon/prie72.png" />
        <link rel="apple-touch-icon-precomposed" href="admin/images/favicon/prie57.png">
        <link rel="shortcut icon" href="admin/images/favicon/prie.ico" />
        <link rel="icon" type="image/ico" href="admin/images/favicon/prie.ico"/>

        <link rel="stylesheet" type="text/css" href="admin/assets/helpers/animate.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/helpers/boilerplate.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/helpers/border-radius.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/helpers/grid.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/helpers/page-transitions.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/helpers/spacing.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/helpers/typography.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/helpers/utils.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/helpers/colors.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/material/ripple.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/badges.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/buttons.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/content-box.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/dashboard-box.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/forms.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/images.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/info-box.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/invoice.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/loading-indicators.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/menus.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/panel-box.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/response-messages.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/responsive-tables.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/ribbon.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/social-box.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/tables.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/tile-box.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/elements/timeline.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/icons/fontawesome/fontawesome.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/icons/linecons/linecons.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/icons/spinnericon/spinnericon.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/accordion-ui/accordion.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/calendar/calendar.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/carousel/carousel.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/charts/justgage/justgage.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/charts/morris/morris.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/charts/piegage/piegage.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/charts/xcharts/xcharts.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/chosen/chosen.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/colorpicker/colorpicker.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/datatable/datatable.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/datepicker/datepicker.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/datepicker-ui/datepicker.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/daterangepicker/daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/dialog/dialog.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/dropdown/dropdown.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/dropzone/dropzone.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/file-input/fileinput.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/input-switch/inputswitch.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/input-switch/inputswitch-alt.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/ionrangeslider/ionrangeslider.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/jcrop/jcrop.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/jgrowl-notifications/jgrowl.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/loading-bar/loadingbar.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/maps/vector-maps/vectormaps.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/markdown/markdown.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/modal/modal.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/multi-select/multiselect.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/multi-upload/fileupload.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/nestable/nestable.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/noty-notifications/noty.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/popover/popover.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/pretty-photo/prettyphoto.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/progressbar/progressbar.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/range-slider/rangeslider.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/slidebars/slidebars.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/slider-ui/slider.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/summernote-wysiwyg/summernote-wysiwyg.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/tabs-ui/tabs.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/timepicker/timepicker.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/tocify/tocify.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/tooltip/tooltip.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/touchspin/touchspin.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/uniform/uniform.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/wizard/wizard.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/widgets/xeditable/xeditable.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/snippets/chat.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/snippets/files-box.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/snippets/login-box.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/snippets/notification-box.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/snippets/progress-box.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/snippets/todo.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/snippets/user-profile.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/snippets/mobile-navigation.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/applications/mailbox.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/themes/admin/layout.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/themes/admin/color-schemes/default.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/themes/components/default.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/themes/components/border-radius.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/helpers/responsive-elements.css">
        <link rel="stylesheet" type="text/css" href="admin/assets/helpers/admin-responsive.css">
        <script type="text/javascript" src="admin/assets/js-core/jquery-core.js"></script>
        <script type="text/javascript" src="admin/assets/js-core/jquery-ui-core.js"></script>
        <script type="text/javascript" src="admin/assets/js-core/jquery-ui-widget.js"></script>
        <script type="text/javascript" src="admin/assets/js-core/jquery-ui-mouse.js"></script>
        <script type="text/javascript" src="admin/assets/js-core/jquery-ui-position.js"></script>
        <script type="text/javascript" src="admin/assets/js-core/transition.js"></script>
        <script type="text/javascript" src="admin/assets/js-core/modernizr.js"></script>
        <script type="text/javascript" src="admin/assets/js-core/jquery-cookie.js"></script>

        <!--<script type="text/javascript">$(window).load(function () {
            setTimeout(function () {
                $('#loading').fadeOut(400, "linear");
            }, 300);
        });</script>-->

    </head>

    <body>
<style type="text/css">html, body {
    height: 100%;
}

body {
    background: #fff;
    overflow: hidden;
}</style>
<script type="text/javascript" src="admin/assets/widgets/wow/wow.js"></script>
<script type="text/javascript">/* WOW animations */

wow = new WOW({
    animateClass: 'animated',
    offset: 100
});
wow.init();</script>
<img src="admin/images/blurred-bg-7.jpg" class="login-img wow fadeIn" alt="">

<div class="center-vertical">
    <div class="center-content">
        <center><img src="admin/images/icone/prie.png"></center>
        <div class="col-md-6 center-margin">
            <div class="server-message wow bounceInDown inverse"><h1>Error</h1>

                <h2>Page Introuvable</h2>

                <p>
Oupss!!!! La page que vous recherchez a été déplacé ou n'existe plus...<br/> Cliquez sur le bouton ci-dessous</p>

                    
                    <button  onclick="goback()" class="btn btn-lg btn-success">RETOURNE PAGE PRECEDENTE</button>
                
            </div>
        </div>
    </div>
</div>
<script>
    function goback(){
        window.history.back();
    }
</script>
<script type="text/javascript" src="admin/assets/widgets/dropdown/dropdown.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/tooltip/tooltip.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/popover/popover.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/progressbar/progressbar.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/button/button.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/collapse/collapse.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/superclick/superclick.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/input-switch/inputswitch-alt.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/slimscroll/slimscroll.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/slidebars/slidebars.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/slidebars/slidebars-demo.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/charts/piegage/piegage.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/charts/piegage/piegage-demo.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/screenfull/screenfull.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/content-box/contentbox.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/material/material.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/material/ripples.js"></script>
    <script type="text/javascript" src="admin/assets/widgets/overlay/overlay.js"></script>
    <script type="text/javascript" src="admin/assets/js-init/widgets-init.js"></script>
    <script type="text/javascript" src="admin/assets/themes/admin/layout.js"></script>
</body>
</html>