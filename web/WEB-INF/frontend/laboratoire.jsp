
<div class="main">
    <div class="container">
        <!-- BEGIN SLIDER -->
        <div class="page-slider margin-bottom-40">
            <img class="img-responsive" src="${DIR_LABO}/${labo.image}" alt="">
        </div>  <!-- END SLIDER -->       
        <div class="row mix-block margin-bottom-40">
                <!-- TESTIMONIALS -->
            <div class="col-md-3 testimonials-v1">
                 
                  <div class="panel">
                      <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-user-md fa-1x" ></i>&nbsp;&nbsp;Nos Laboratoires </strong>
                        </h2>
                      </div>
                        <div class="panel-body">
                            <ul class="nav sidebar-categories margin-bottom-40">
                              <c:forEach var="listelabo" items="${menu}">
                                <c:choose>
                                    <c:when test="${listelabo.idLabo == pageContext.request.getQueryString()}">
                                <li class="active"><a style="color:white; text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                     </c:when>
                                     <c:otherwise>
                                     <li><a style="text-decoration: none" href="laboratoire?${listelabo.idLabo}">Labo - ${listelabo.sigle}</a></li> 
                                     </c:otherwise>
                                </c:choose>
                            </c:forEach>
                               </ul>
                        </div>
                        </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-bullhorn fa-1x" ></i>&nbsp;&nbsp;Nos Communications </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                          <li ><a href="annonce">Toutes nos Annonces</a></li>
                          <li ><a href="publication">Toutes Nos Publications</a></li>
                        </ul>
                    </div>

                </div>

                <div class="panel">
                    <div class="panel-heading" style="background:#53ab3f;">
                          <h2 class="panel-title" style="color:white">
                            <strong ><i class="fa fa-book fa-1x" ></i>&nbsp;&nbsp; A Propos </strong>
                        </h2>
                      </div>
                    <div class="panel-body">
                        <ul class="nav sidebar-categories margin-bottom-40">
                          <li><a href="about">Qui Sommes-Nous ?</a></li>
                        </ul>
                    </div>

                </div>								
            </div>

            <!-- END TESTIMONIALS -->
            
            <!-- TABS -->
            <div class=" col-md-9" style="background-color: white">
                <div class="panel">
                    <div class="panel-heading">
                       <h2 class="tile-header"><span class="font-blue-alt">${labo.libelle}</span></h2>  
                   </div> 
                    <div class="panel-body">

                        <div class="col-lg-2">
                            <span class="icon-notification user-profile clearfix">      
                                <img width="100" src="${DIR_AVATAR}/${responsable.image}" alt="">
                            </span> 
                        </div>
                        <div class="col-lg-7">
                            <h4>
                                <a style="text-decoration: none" href="perso?${responsable.nomid}">${responsable.civilite} ${responsable.nom} ${responsable.prenom}</a>
                            </h4>
                            <em class="font-blue-alt">${responsable.fonction}</em><br/>
                            Tutelle :<em class="font-blue-alt"> ${responsable.tutelle}</em><br/>

                        </div>    
                        <div style="float: right; ">
                                <a href="membres?${labo.idLabo}" class="btn btn-primary" role="button">
                                    <em class="fa fa-users" ></em> Liste des Membres
                                </a>                              
                        </div>
                    </div>
                </div>
            
                <div class="panel">
                    <div class="panel-body">
                        ${labo.contenu}
                    </div>
                </div>


            </div>
        </div>
 