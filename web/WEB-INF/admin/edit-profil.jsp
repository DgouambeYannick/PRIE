
<script type="text/javascript" src="../admin/assets/widgets/parsley/parsley.js"></script>
<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
                <div id="page-title"><h2>GESTION DES MEMBRES</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

                    <script>
                        $(function (){
                            $(".notif").show("slow").delay(5000).hide("slow");
                        });
                    </script>
                    <c:if test="${val == 2}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Modification du profil de <span style="text-transform: uppercase; color:brown">${updateok}</span> Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                     <c:if test="${val == 1}">
                        <div class="alert alert-danger notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-down fa-3x"></i>
                          <span style="text-transform: uppercase; color:brown">${errorimage}</span>
                        </strong>
                    </div>
                    </c:if>
                <div class="panel">
                    <div class="panel-body"><h3 class="title-hero">MODIFICATION DE VOS INFORMATIONS</h3>

                        <div class="example-box-wrapper">
                            <form class="form-horizontal bordered-row" method="POST" action="edit-profil" enctype="multipart/form-data" id="demo-form" data-parsley-validate="">
                                <div class="row">

                                    <div class="col-md-5 bg-gray-alt">
                                        <div class="form-group"></div>
                                        <div class="form-group"><label class="col-sm-3 control-label font-black">Photo (Jpg, Png, Gif) :</label>
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="${sessionScope.DIR_AVATAR}/${membre.image}" alt="" /></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                <div style="display: inline-block ">
					                                    <span>
					                                    	<span class="fileupload-new">Choisir</span>
					                                    	<span class="fileupload-exists">Changer</span>
					                                    	<input  type="file" name="image"  />
					                                    </span>
                                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-4 control-label font-black">Civilite:</label>

                                            <div class="col-sm-8"><select required name="civilite" class="custom-select">
                                                <option>${membre.civilite}</option>
                                                <option>Mr.</option>
                                                <option>Mme.</option>
                                                <option>Mlle.</option>
                                                <option>Dr.</option>
                                                <option>Pr.</option>
                                            </select></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-4 control-label font-black">Nom : </label>

                                            <div class="col-sm-8">
                                                <input type="text" placeholder="Entrer Nom" value="${membre.nom}"
                                                                         required class="form-control" name="nom"/>
                                                <input type="hidden"  value="${membre.idMembre}" name="membre" />
                                                <input type="hidden"  value="${membre.responsable}" name="respo" />
                                                <input type="hidden"  value="${membre.nbvisite}" name="visite" />
                                                <input type="hidden"  value="${membre.nomid}" name="nomid" />
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-4 control-label font-black">Prenom : </label>

                                            <div class="col-sm-8"><input type="text" placeholder="Entrer Prenom" value="${membre.prenom}"
                                                                         required class="form-control" name="prenom"></div>
                                        </div>
                                        
                                        
                                    </div>

                                    <div class="col-md-1"></div>

                                    <div class="col-md-5 bg-gray-alt">
                                        <div class="form-group"></div>
                                                <div class="form-group"><label class="col-sm-4 control-label font-black">Fonction: </label>

                                                    <div class="col-sm-8"><input type="text" placeholder="Entrer Fonction"
                                                                                  class="form-control" value="${membre.fonction}" name="fonction"></div>
                                                </div>
                                                <div class="form-group"><label class="col-sm-4 control-label font-black">Tutelle : </label>

                                                    <div class="col-sm-8"><input type="text" placeholder="Entrer Tutelle"
                                                                                 value="${membre.tutelle}" class="form-control" name="tutelle"></div>
                                                </div>
                                                <div class="form-group"><label class="col-sm-4 control-label font-black">Email</label>

                                                <div class="col-sm-8"><input type="text" value="${membre.email}" data-parsley-type="email" name="email" placeholder="Entrer son Email"
                                                                             class="form-control"></div>
                                                 </div>
                                        
                                                 <div class="form-group"><label class="col-sm-4 control-label font-black">Adresse : </label>

                                                    <div class="col-sm-8"><input type="text" value="${membre.adresse}" placeholder="Entrer Adresse"
                                                                                  class="form-control" name="adresse"></div>
                                                </div>
                                                <div class="form-group"><label class="col-sm-4 control-label font-black">Telephone
                                                    : </label>

                                                    <div class="col-sm-8"><input value="${membre.phone}" type="text" name="telephone" placeholder="Entrer Telephone"
                                                                                 data-inputmask="&apos;mask&apos;:&apos;(237) 699-99-99-99&apos;"  class="input-mask form-control"></div>
                                                </div>
                                                 <div class="form-group"><label class="col-sm-4 control-label font-black">URL PERSO
                                                    : </label>

                                                    <div class="col-sm-8"><input value="${membre.urlperso}" type="text" name="urlperso" class=" form-control"></div>
                                                </div>
                                                <div class="form-group"><label class="col-sm-4 control-label font-black">Change LABO
                                                    : </label>
                                                    <div class="col-sm-8">
                                                        <select required name="laboratoire" class="custom-select">
                                                            <option value="${membre.idLabo.idLabo}">Labo - ${membre.idLabo.sigle}</option>
                                                            <c:forEach var="liste" items="${listelabo}">
                                                            <option value="${liste.idLabo}">Labo - ${liste.sigle}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                    </div>


                                </div>
                                <div class="bg-default content-box text-center pad20A mrg25T">
                                    <button type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MODIFIER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                    
                    
                    

