<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
<div id="page-title"><h2>GESTION LABORATOIRES</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>
                    <script>
                        $(function (){
                            $(".notif").show("slow").delay(5000).hide("slow");
                        });
                    </script>
                    <div class="alert alert-warning notif">
                        <strong style="margin-left:300px" >
                            ${statut}
                        </strong>
                    </div>
                <div class="panel">
                    <div class="panel-heading" style="background-color:#BCBCBC;">          
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a style="color:#b72724; text-decoration: none;" href="liste-laboratoire">
                           <i  class="glyph-icon icon-reply-all"></i><span class="font-size-20">retour</span>
                       </a>
                    </div>
                    <div class="panel-body"><h3 class="title-hero">Liste des membres du LABO : <span class="font-blue-alt">${labo.libelle}</span></h3>

                        <div class="example-box-wrapper">
                            <div class="row">

                                <div class="col-md-5 bg-gray-alt">
                                    <div class="box-sm-login">
                                            <div class="login-box clearfix">
                                                <div class="user-img">
                                                    <a href="#" title="" class="change-img">Changer photo</a>
                                                    <img src="../admin/images/avatar/${responsable.image}" alt="">
                                                </div>
                                                <div class="user-info">
                                                    <span>${responsable.nom} ${responsable.prenom}
                                                        <i>${responsable.fonction}</i>
                                                    </span>
                                                </div>
                                            </div>
                                            <h4>RESPONSABLE</h4>
                                        </div>
                                </div>
                            </div>
                                                    <br/>
                                                    <br/>
                            <div class="row">
                                <div class="col-md-12">
                                     <table id="datatable-responsive"
                                   class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                                   width="100%">
                                    <thead>
                                    <tr>
                                        <th>PHOTO</th>
                                        <th>NOM-PRENOM</th>
                                        <th>FONCTION</th>
                                        <th>STATUT</th>
                                        <th>LABORATOIRE</th>
                                        <th style="background-color:#3B5998; color:white">ACTIONS</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                        <c:forEach var="listeM" items="${membres}">
                                    <tr>
                                        <td>
                                            <span class="icon-notification user-profile clearfix">
                                                <img width="50" src="../admin/images/avatar/${listeM.image}" alt="Profile">
                                            </span>
                                        </td>
                                        <td>${listeM.civilite} ${listeM.nom} ${listeM.prenom}</td>
                                        <td>${listeM.fonction}</td>
                                        <td style="color:#b72724;">${listeM.responsable}</td>
                                        <td>${labo.libelle} (${labo.sigle})</td>
                                        <td>
                                            <a href="laboratoire-membre?s=${listeM.idMembre}&AMP;x=${labo.idLabo}" class="font-normal"><i style="color:#b72724;" class="glyph-icon icon-refresh"></i>&nbsp;Changer Statut</a><br/>
                                        </td>
                                    </tr>
                                   </c:forEach>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
