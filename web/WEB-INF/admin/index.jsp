<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Connexion</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <link rel="stylesheet" type="text/css" href="../plugins-login/index.css"/>
    <link rel="stylesheet" type="text/css" href="../plugins-login/bootstrap.css"/>
    <!-- bootstrap theme -->
    <link href="../plugins-login/bootstrap-theme.css" rel="stylesheet">
    <link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
    <!-- Custom styles -->
    <link href="../plugins-login/style.css" rel="stylesheet">
    <link href="../plugins-login/style-responsive.css" rel="stylesheet"/>

    <script src="../plugins-login/jquery.min.js"></script>
    <script src="../plugins-login/bootstrap.min.js"></script>
    <link href="../plugins-login/prie.css" rel="stylesheet">

    <script>
        function change() {
            //var out_in=document.authentification.decodage.value;
            var recup = document.getElementById("password").checked
            //var change_pass = 'text';

            if (recup == false) {
                document.getElementById("password").checked = true;
                document.getElementById("password").type = "text";
            }
            else {
                document.getElementById("password").checked = false;
                document.getElementById("password").type = "password";
            }
        }

    </script>
</head>
<body >
    <form method="post" class="login-form" name="authentification" action="index" id="authentification" >
        <div class="login-wrap ">
            <img class="logo-login img-circle" src="../plugins-login/prie.png"/>
            <br/>
            <div class="entete-login">
                <em>SE CONNECTER</em>
            </div>
            <br/>
            <br/>
            <br/>
            <div style=" color:black" class="input-group">
                <span class="input-group-addon"><i style=" color:black" class="fa fa-user fa"></i></span>
                <input type="text" name="username" required class="form-control" placeholder="Username" autofocus>
            </div>

            <div style=" color:black" class="input-group">
                <span class="input-group-addon"><i style=" color:black" class="fa fa-key fa"></i></span>
                <input type="password" name="password" id="password" required class="form-control" placeholder="Password">
                <span class="input-group-addon"><a href="#" name="decodage" onClick="change();" style=" color:black"
                                                   class="fa fa-eye fa"></a></span>
            </div>
            <label>
                <input type="checkbox" name="remember" value="Se souvenir de moi"><span style="color:black"> Se souvenir de moi</span>
            </label>
            <button name="connexion" class="btn btn-success btn-lg btn-block" type="submit"><i class="fa fa-user fa"></i>&nbsp; Connexion</button>
            <br/>
            <a href="../accueil" class="btn btn-lg btn-block " style="background-color: #ff2d55" ><i class="fa fa-home fa" style="color:white"></i>&nbsp; <span style="color:white">ACCUEIL</span></a>
            <label id="checkbox">
                <input type="checkbox" name="decodage" onClick="change();" value="remember-me">
            </label>
        </div>

    </form>
    <div  style="height: 10px"></div> 
    <div class="row" style="width: 500px; margin: auto" > 
        <center>
            <p class="font-blue-alt" style="background-color: #4cd964; color: white"> Si vous ne disposez pas de comptes P.R.I.E, veuillez envoyer un email à prie@myiuc.com</p>     
        </center>
    </div> 
    
     <script>
        $(function (){
            $(".notif").show("slow").delay(3000).hide("slow");
        });
    </script>
    <c:if test="${errorval == 0}">
        <div class="alert alert-danger notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-down fa-3x"></i>
            ${errorsms}
        </strong>
    </div>
    </c:if>
    
</body>
</html>
