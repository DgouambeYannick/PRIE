<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
                <div id="page-title"><h2>GESTION DES MODULES PUBLICATIONS</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

              <script>
                        $(function (){
                            $(".notif").show("slow").delay(5000).hide("slow");
                        });
                    </script>
                    <c:if test="${val == 2}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Modification du Module <span style="text-transform: uppercase; color:brown">${updatemodule}</span> Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 1}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Ajout du Module <span style="text-transform: uppercase; color:brown">${ajoutmodule}</span> Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 3}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                           <span style="text-transform: uppercase; color:brown">Changement du statut Effectu� avec succes !!!</span>
                        </strong>
                    </div>
                    </c:if>
                <div class="panel">
                    <div class="panel-heading" style="background-color:#BCBCBC;">          
                        <center>
                            <a href="#" data-backdrop="false" data-toggle="modal" data-target=".addmodule" style="border-radius: 0px;" class="btn btn-success btn-sm">
                                <i class="glyph-icon icon-plus"></i><strong>&nbsp;&nbsp;Ajouter un Module</strong>
                            </a>
                        </center>
                    </div>
                    <div class="panel-body">
                        <h3 class="title-hero">Liste des Modules</h3>

                        <div class="example-box-wrapper">
                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>LIBELLE</th>
                                    <th>STATUT</th>
                                    <th>DERNIERE MODIFICATION</th>
                                    <th style="background-color:#3B5998; color:white">ACTIONS</th>
                                </tr>
                                </thead>

                                <tbody>
                                <c:forEach var="listemodule" items="${allmodule}">
                                <tr>
                                    <td>${listemodule.libelle}</td>
                                    <td>${listemodule.statut}</td>
                                    <td>
                                        <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listemodule.datemodif}" /><br/>
                                        <em class="font-orange">Par: ${listemodule.createur} </em>
                                    </td>
                                    <td>
                                        <a href="#" data-backdrop="false" data-toggle="modal" data-target=".${listemodule.idType}" class="font-normal"><i style="color:#b72724;" class="glyph-icon icon-edit"></i>&nbsp;Renommer</a><br/>
                                        <a data-placement="bottom" title="Changer Statut" href="type-publication?${listemodule.idType}" class="font-normal tooltip-button"><i style="color:#b72724;" class="glyph-icon icon-refresh"></i>&nbsp;Changer Statut</a>
                                    
                                    </td>
                                </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                    
  <!-- renommer role -->
  <c:forEach var="listemodule" items="${allmodule}">
   <div class="modal fade ${listemodule.idType}" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Renommer le role : &nbsp;&nbsp <em class="font-blue-alt">${listemodule.libelle}</em></h4>
          </div>
          <form action="type-publication" method="post">
            <div class="modal-body">
                <input type="text" value="${listemodule.libelle}" name="module" class="form-control" placeholder="entrer le nouveau nom ici..."  /><br/>
                <input type="hidden" name="idtype" value="${listemodule.idType}" class="form-control"/> 
                                      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
                <button name="action" value="Modifier" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MODIFIER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div> 
              </form>
        </div>
      </div>
     </div>
  </c:forEach>
  
  <!-- ajouter profil-->
   <div class="modal fade addmodule" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Entrez le nom du nouveau Module</h4>
          </div>
          <form action="type-publication" method="post">
            <div class="modal-body">
                    <input type="text" name="module" class="form-control"  />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
                <button name="action" value="Ajouter" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>AJOUTER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div> 
          </form>
        </div>
      </div>
     </div>