<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
<div id="page-title"><h2>GESTION DES ROLES</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div> 
                    <script>
                        $(function (){
                            $(".notif").show("slow").delay(5000).hide("slow");
                        });
                    </script>
                    <c:if test="${val == 2}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Ajout des Menus Effectu�e avec success!!
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 1}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Retrait Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
            <div class="panel">
                    <div class="panel-heading" style="background-color:#BCBCBC;">
                        
                        <center>
                            <a style="color:#b72724; text-decoration: none;" href="gerer-role">
                                <i  class="glyph-icon icon-reply-all"></i><span class="font-size-20">retour</span>
                            </a>
                        </center>
                    </div>
                    <div class="panel-body">
                        <span class="title-hero">Liste des Menus de : <h2 class="font-normal">${profil.libelle}</h2></span>

                        <div class="example-box-wrapper">
                            <div class="row">

                            <div class="col-md-5 bg-gray-alt">
                            <table id="datatable-responsive"
                                   class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>LISTE DES MENUS</th>
                                    <th style="background-color:#3B5998; color:white">ACTIONS</th>
                                </tr>
                                </thead>

                                <tbody>
                                <c:forEach var="menus" items="${menus}">
                                <tr>
                                    <td><i class="${menus.icone}"></i> ${menus.libelle}</td>
                                    <td><a href="profil-btn-menu?x=${profil.idProfil}&AMP;m=${menus.idMenu}&AMP;t=delm"  class="font-normal"><i style="color:#b72724;" class="glyph-icon icon-trash"></i>&nbsp;retirer</a>
                                    </td>
                                </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            </div>
                                    <div class="col-md-1"></div>
                            <div class="col-md-5 bg-gray-alt">
                                <form action="profil-btn-menu" method="post">
                                    
                                        <div class="row">
                                         <div class="form-group">
                                            <div class="col-sm-12">
                                                <select multiple="multiple" class="multi-select" name="menu">
                                                    <c:forEach var="menu" items="${allmenu}">
                                                         <option value="${menu.idMenu}">${menu.libelle}</option>
                                                     </c:forEach>
                                                </select>
                                                <input type="hidden" name="profil" value="${profil.idProfil}" />
                                            </div>
                                         </div>
                                        </div>
                                    
                                    <div class="modal-footer">
                                        
                                        <button name="action" value="AjouterM" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>AJOUTER MENUS</span><i
                                                                    class="glyph-icon icon-linecons-paper-plane"></i></button>
                                    </div> 
                                  </form>
                            </div>
                        </div>
                    </div>
                </div>
 </div>
