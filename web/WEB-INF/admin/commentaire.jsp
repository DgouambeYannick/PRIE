<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : newjsp
    Created on : 24 mai 2016, 09:52:47
    Author     : HP
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <!--[if IE]>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title>Espace Membre</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
        <link rel="apple-touch-icon-precomposed" sizes="144x144"
              href="../admin/images/favicon/prie144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114"
              href="../admin/images/favicon/prie114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72"
              href="../admin/images/favicon/prie72.png" />
        <link rel="apple-touch-icon-precomposed" href="../admin/images/favicon/prie57.png">
        <link rel="shortcut icon" href="../admin/images/favicon/prie.ico" />
        <link rel="icon" type="image/ico" href="../admin/images/favicon/prie.ico"/>

        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/animate.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/boilerplate.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/border-radius.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/grid.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/page-transitions.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/spacing.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/typography.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/utils.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/colors.css">

        <link rel="stylesheet" type="text/css" href="../admin/assets/material/ripple.css">

        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/badges.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/buttons.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/content-box.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/dashboard-box.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/forms.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/images.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/loading-indicators.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/menus.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/panel-box.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/response-messages.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/responsive-tables.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/tables.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/tile-box.css">

        <link rel="stylesheet" type="text/css" href="../admin/assets/icons/fontawesome/fontawesome.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/icons/linecons/linecons.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/icons/spinnericon/spinnericon.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/charts/justgage/justgage.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/charts/piegage/piegage.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/chosen/chosen.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/datatable/datatable.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/dialog/dialog.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/dropdown/dropdown.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/dropzone/dropzone.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/file-input/fileinput.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/input-switch/inputswitch.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/input-switch/inputswitch-alt.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/jcrop/jcrop.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/modal/modal.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/multi-select/multiselect.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/nestable/nestable.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/popover/popover.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/pretty-photo/prettyphoto.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/progressbar/progressbar.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/slidebars/slidebars.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/tabs-ui/tabs.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/tocify/tocify.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/tooltip/tooltip.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/touchspin/touchspin.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/uniform/uniform.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/wizard/wizard.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/widgets/xeditable/xeditable.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/snippets/chat.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/snippets/files-box.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/snippets/login-box.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/snippets/notification-box.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/snippets/progress-box.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/snippets/todo.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/snippets/user-profile.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/snippets/mobile-navigation.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/themes/admin/layout.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/themes/admin/color-schemes/default.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/themes/components/default.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/themes/components/border-radius.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/responsive-elements.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/admin-responsive.css">

        <script type="text/javascript" src="../admin/assets/js-core/jquery-core.js"></script>
        <script type="text/javascript" src="../admin/assets/js-core/jquery-ui-core.js"></script>
        <script type="text/javascript" src="../admin/assets/js-core/jquery-ui-widget.js"></script>
        <script type="text/javascript" src="../admin/assets/js-core/jquery-ui-mouse.js"></script>
        <script type="text/javascript" src="../admin/assets/js-core/jquery-ui-position.js"></script>
        <script type="text/javascript" src="../admin/assets/js-core/transition.js"></script>
        <script type="text/javascript" src="../admin/assets/js-core/modernizr.js"></script>
        <script type="text/javascript" src="../admin/assets/js-core/jquery-cookie.js"></script>

        <!--<meta http-equiv="refresh" content="1; URL=commentaire?">-->
        <script type="text/javascript">
            function refresh_div()
            {

                var xhr_object = null;
                try
                {
                    xhr_object = new XMLHttpRequest();
                } catch (e)
                {
                    try
                    {
                        xhr_object = new ActiveXObject("Msxml2.XMLHTTP");
                    } catch (e)
                    {
                        try
                        {
                            xhr_object = new ActiveXObject("Miscosoft.XML");
                        } catch (e)
                        {
                            alert("votre navigateur ne supporte pas AJAX veuillez utiliser Chrome ou Metterz votre navigateur à jour");

                        }
                    }
                }

                var method = 'GET';
/****************commmentaire ***************************************/
                var filename = 'comments?g=${admin.idGroupe}';

                xhr_object.open(method, filename, true);

                xhr_object.onreadystatechange = function ()

                {

                    if (xhr_object.readyState === 4)

                    {

                        var tmp = xhr_object.responseText;

                        document.getElementById('refreshcomment').innerHTML = tmp;

                    }

                };

                xhr_object.send(null);

                setTimeout('refresh_div()', 1000);

            }


        </script>

    </head>

    <body onload="refresh_div()">
        <%--<c:set var="now" value="<%=new java.util.Date()%>" />--%>
        <div id="sb-site">

            <!--- ici la barre bleu gauche pour les membres dun laboratoire -->
            <div class="sb-slidebar bg-black sb-left sb-style-overlay">
                <div class="scrollable-content scrollable-slim-sidebar">
                    <div class="pad10A">

                        <div class="divider-header font-white font-normal">LES MEMBRES DU LABORATOIRE  
                            <span class="bs-label bg-orange tooltip-button"> ${sessionScope.useradmin.getIdMembre().getIdLabo().getSigle()}</span>
                        </div>

                        <ul class="chat-box">
                            <sql:query var="connect" dataSource="jdbc/bd_prie">
                                SELECT image, civilite, nom, prenom 
                                FROM compte, membre 
                                WHERE compte.id_membre = membre.id_membre AND compte.online='oui' AND membre.id_labo = '${sessionScope.useradmin.getIdMembre().getIdLabo().getIdLabo()}'
                            </sql:query>
                            <c:forEach  var="liste" items="${connect.rows}">
                                <li>
                                    <div class="status-badge">
                                        <img class="img-circle" width="40" src="${sessionScope.DIR_AVATAR}/${liste.image}" alt="">
                                        <div class="small-badge bg-green"></div>
                                    </div>
                                    <b>${liste.civilite} ${liste.nom} ${liste.prenom}</b>
                                    <p></p>
                                </li>
                            </c:forEach>
                        </ul>

                        <div class="divider-header">MEMBRES HORS LIGNES</div>
                        <ul class="chat-box">
                            <sql:query var="deconnecte" dataSource="jdbc/bd_prie">
                                SELECT image, civilite, nom, prenom 
                                FROM compte, membre 
                                WHERE compte.id_membre = membre.id_membre AND compte.online='non' AND membre.id_labo = '${sessionScope.useradmin.getIdMembre().getIdLabo().getIdLabo()}'
                            </sql:query>
                            <c:forEach items="${deconnecte.rows}" var="liste">
                                <li>
                                    <div class="status-badge">
                                        <img class="img-circle" width="40" src="${sessionScope.DIR_AVATAR}/${liste.image}" alt="">
                                        <div class="small-badge bg-red"></div>
                                    </div>
                                    <b>${liste.civilite} ${liste.nom} ${liste.prenom}</b>
                                    <p></p>
                                </li>
                            </c:forEach>

                        </ul>


                    </div>
                </div>
            </div>

            <!---------------------------------------------debut barre menu droit affichage des groupes------------------------------------------------->

            <div class="sb-slidebar bg-black sb-right sb-style-overlay">
                <div class="scrollable-content scrollable-slim-sidebar">
                    <div class="pad15A">
                        <p  class="popover-title">
                            Listing de Mes
                            <span class="bs-label bg-orange tooltip-button" title="Label example">GROUPES COLLABORATIFS</span>
                        </p>

                        <div id="sidebar-toggle-4" class="collapse in">
                            <ul class="notifications-box notifications-box-alt">


                            </ul>
                        </div>

                    </div>
                </div>
            </div>


            <div id="page-wrapper">
                <div id="mobile-navigation">
                    <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span>
                    </button>
                </div>
                <div id="page-sidebar">
                    <div  id="header-logo" class="logo-bg">
                        <a href="#" class="logo-content-big" title="P.R.I.E">P.R.I.E<i>UI</i> <span>Pole Recherche Innovation et Entrepreneuriat</span></a>
                        <a href="#" class="logo-content-small" title="P.R.I.E">P.R.I.E <i>UI</i> <span>Pole Recherche Innovation et Entrepreneuriat</span></a>
                        <a id="close-sidebar" href="#" title="Close sidebar"><i class="glyph-icon icon-outdent"></i></a>
                    </div>
                    <!---------------------------------------------debut barre menu gauche------------------------------------------------->
                    <div class="scroll-sidebar">
                        <ul id="sidebar-menu">
                            <li class="header"><span>ACCUEIL</span></li>
                            <li><a href="dashboard" title="Admin Dashboard"><i class="glyph-icon icon-linecons-tv icon-spin"></i> <span>Accueil Admin</span></a>
                            </li>

                            <li class="header"><span>AUTRES</span></li>
                            <li><a href="deconnexion" title="Deconnexion">
                                    <i class="glyph-icon icon-power-off icon-spin"></i><span>Deconnexion</span></a></li>
                            <li><a class=" sb-toggle-right"  href="#" title="Mes groupes">
                                    <i class="glyph-icon icon-comments-o icon-spin"></i><span>Mes groupes</span></a></li>
                            <li><a class=" sb-toggle-left"  href="#" title="Membres online">
                                    <i class="glyph-icon icon-group icon-spin"></i><span>Membres Online</span></a></li>    

                            <li class="header"><span>GESTION</span></li>
                            <!-------------------------------bouclage du menu--------------------------------------------------------------->
                            <c:forEach var="var" items="${MenusSidebar}">
                                <li>
                                    <a href="javascript:void(0);" title="${var.libelle}"><i class="${var.icone}"></i><span>${var.libelle}</span></a>
                                    <!---------------------------------------bouclage du sous menu------------------------------------------------------>
                                    <div class="sidebar-submenu">
                                        <ul>
                                            <c:forEach var="var2" items="${var.sousMenuCollection}">
                                                <li><a class="add-demo-transition" data-transition="pt-page-rotatePullRight-init" href="${var2.lien}" title="${var2.libelle}"><span>${var2.libelle}</span></a></li>

                                            </c:forEach>
                                        </ul>
                                    </div>
                                </li>
                            </c:forEach>


                            <!-------------------------------------------fin bouclage--------------------------------------------------->
                        </ul>
                    </div>
                </div>
                <!--------------------------------------fin barre menu gauche-------------------------------------------------------->


                <div id="page-content-wrapper">
                    <div id="page-content">

                        <!---------------------- debut barre haute-------------------------------------------->
                        <div id="page-header">
                            <!---------------------- debut barre haute droite icone profile user------------------------------------------->
                            <div id="header-nav-left">
                                <div class="user-account-btn dropdown">
                                    <a style="color:white; margin-right: 50px" href="#" title="mon compte"
                                       class="user-profile clearfix" data-toggle="dropdown">
                                        <img width="28" src="${sessionScope.DIR_AVATAR}/${sessionScope.useradmin.idMembre.image}" alt="Profile image">
                                        <span>${sessionScope.useradmin.idMembre.nom} ${sessionScope.useradmin.idMembre.prenom}</span> <i class="glyph-icon icon-angle-down"></i>
                                    </a>

                                    <div class="dropdown-menu float-right">
                                        <div class="box-sm-login">
                                            <div class="login-box clearfix">
                                                <div class="user-img">
                                                    <a href="#" title="" class="change-img">Changer photo</a>
                                                    <img src="${sessionScope.DIR_AVATAR}/${sessionScope.useradmin.idMembre.image}" alt="">
                                                </div>
                                                <div class="user-info"><span>${sessionScope.useradmin.idMembre.nom} ${sessionScope.useradmin.idMembre.prenom}<i>${sessionScope.useradmin.idProfil.libelle}</i></span>
                                                    <a class="add-demo-transition" data-transition="pt-page-rotatePullTop-init" href="editer-profile?${sessionScope.useradmin.idMembre.idMembre}" title="Editer profile">Editer profile</a>
                                                </div>
                                            </div>

                                            <div class="divider"></div>
                                            <div class="button-pane button-pane-alt pad5L pad5R text-center">
                                                <a href="deconnexion"
                                                   class="btn btn-flat display-block font-normal btn-danger"><i
                                                        class="glyph-icon icon-power-off"></i> Deconnexion</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!---------------------- debut barre haute gauche options------------------------------------------->
                            <div id="header-nav-right" >

                                <a href="#" data-placement="bottom" class="tooltip-button hdr-btn bg-primary-header"
                                   id="fullscreen-btn" title="Affichage Plein Ecran"><i class="glyph-icon icon-arrows-alt"></i></a>
                                <!--<a href="#" data-placement="bottom"
                                   class="tooltip-button hdr-btn sb-toggle-left bg-primary-header" id="chatbox-btn"
                                   title="Membres de mon Laboratoire"><i class="glyph-icon icon-group"></i></a>-->

                                <!---------------------- debut barre haute gauche icone invitations------------------------------------------->
                                <div class="dropdown" >
                                    <a data-placement="bottom" data-toggle="dropdown" class="tooltip-button bg-primary-header"
                                       href="#" title="Invitations">
                                        <sql:query var="res" dataSource="jdbc/bd_prie">
                                            SELECT COUNT(*) AS total FROM groupe, compte_groupe WHERE statut_invitation = 'En-cours' AND groupe.id_groupe=compte_groupe.id_groupe AND compte_groupe.id_compte='${sessionScope.useradmin.getIdCompte()}'
                                        </sql:query>
                                        <c:forEach var="liste" items="${res.rows}" >
                                            <span class="bs-badge float-right badge-danger">${liste.total}</span>

                                        </c:forEach>
                                        <i class="glyph-icon icon-bullhorn"></i>
                                    </a>

                                    <div class="dropdown-menu box-md float-left">
                                        <div class="popover-title display-block clearfix pad10A">LISTE DES INVITATIONS</div>
                                        <div class="scrollable-content scrollable-slim-box">
                                            <ul class="no-border notifications-box">
                                                <sql:query var="invit" dataSource="jdbc/bd_prie">
                                                    SELECT * FROM groupe, compte_groupe WHERE statut_invitation = 'En-cours' AND groupe.id_groupe=compte_groupe.id_groupe AND compte_groupe.id_compte='${sessionScope.useradmin.getIdCompte()}'
                                                </sql:query>
                                                <c:forEach var="liste" items="${invit.rows}" >       
                                                    <li>
                                                        <span class="icon-notification user-profile clearfix">
                                                            <img width="33" src="${sessionScope.DIR_GROUPE}/${liste.image}" alt="Profile groupe">
                                                        </span>
                                                        <span class="notification-text">Groupe ${liste.libelle}</span><br/>
                                                        <span class="notification-text font-blue">Crée par ${liste.createur}</span>

                                                        <div class="notification-time">
                                                            <a href="invitation?g=${liste.id_groupe}&AMP;c=${sessionScope.useradmin.getIdCompte()}" class="btn btn-xs btn-hover btn-blue-alt font-normal"><span>Accepter l'Invitation</span><i
                                                                    class="glyph-icon icon-linecons-thumbs-up"></i></a>
                                                        </div>
                                                    </li>
                                                </c:forEach>

                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="dropdown" >
                                    <a href="#" data-placement="bottom" data-toggle="dropdown" class="tooltip-button bg-primary-header"
                                       title="Mes groupes">
                                        <i class="glyph-icon icon-comments-o"></i>
                                    </a>
                                    <div class="dropdown-menu box-md float-left">
                                        <div class="popover-title display-block clearfix pad10A">MES GROUPES COLLABORATIFS</div>
                                        <div class="scrollable-content scrollable-slim-box">
                                            <ul class="no-border notifications-box">
                                                <sql:query var="result" dataSource="jdbc/bd_prie">
                                                    SELECT *  FROM groupe WHERE createur = '${sessionScope.useradmin.getLogin()}'
                                                </sql:query>

                                                <c:forEach var="liste" items="${result.rows}" >       
                                                    <li>
                                                        <span class="icon-notification user-profile clearfix">
                                                            <img class="img-circle" width="33" src="${sessionScope.DIR_GROUPE}/${liste.image}" alt="Profile groupe">
                                                        </span>
                                                        <span class="notification-text">Groupe ${liste.libelle}</span><br/>
                                                        <span class="notification-text font-green">Crée par ${liste.createur}</span>
                                                        <div class="notification-time">
                                                            <a href="commentaire?${liste.id_groupe}"  class=" notification-btn btn btn-xs btn-primary tooltip-button"
                                                               data-placement="left" title="Voir commentaires"><i
                                                                    class="glyph-icon icon-comments-o"></i></a>
                                                        </div>

                                                    </li> 
                                                </c:forEach> 
                                                <sql:query var="resultat" dataSource="jdbc/bd_prie">
                                                    SELECT * FROM groupe, compte_groupe WHERE statut_invitation = 'Accepte' AND groupe.id_groupe=compte_groupe.id_groupe AND compte_groupe.id_compte='${sessionScope.useradmin.getIdCompte()}'
                                                </sql:query>

                                                <c:forEach var="liste" items="${resultat.rows}" > 
                                                    <li>
                                                        <span class="icon-notification user-profile clearfix">
                                                            <img class="img-circle" width="33" src="${sessionScope.DIR_GROUPE}/${liste.image}" alt="Profile groupe">
                                                        </span>
                                                        <span class="notification-text">Groupe ${liste.libelle}</span><br/>
                                                        <span class="notification-text font-green">Crée par ${liste.createur}</span>

                                                        <div class="notification-time">
                                                            <a href="commentaire?${liste.id_groupe}"  class=" notification-btn btn btn-xs btn-primary tooltip-button"
                                                               data-placement="left" title="Voir commentaires"><i
                                                                    class="glyph-icon icon-comments-o"></i></a>
                                                        </div>
                                                    </li> 
                                                </c:forEach>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!---------------------- debut contenu-------------------------------------------->


                        <div class="content-box" >
                            <h3 class="content-box-header  text-left" style="background-color:#BCBCBC;">
                                <div class="chat-author">
                                    <img width="36" src="${sessionScope.DIR_GROUPE}/${admin.image}" alt=""> ${admin.libelle}
                                </div> 
                                <em style="font-size: 11px">
                                    Admin du groupe Collaboratif : <strong style="color: blue"> ${admin.createur}</strong>
                                </em>
                            </h3>

                            <div class="content-box-wrapper" >
                                <div class="scrollable-content scrollable-slim-sidebar scrollable-lg">
                                    <ul class="chat-box" id="refreshcomment">

                                    </ul>
                                </div>
                            </div>
                            <div class=" pad10A">
                                <form method="post" action="commentaire">
                                    <div class="input-group">
                                        <input required name="commentaire" type="text" placeholder="laisser un commentaire ..." class="form-control">
                                        <input name="groupe" value="${admin.idGroupe}" type="hidden" />
                                        <div class="input-group-btn">
                                            <button  name="action" value=Envoyer" type="submit" class="btn btn-blue-alt" tabindex="-1"><i
                                        class="glyph-icon icon-linecons-paper-plane"></i></button>
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="#">Pieces jointes</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    
                                </form>
                            </div>
                        </div>

                        <div id="page-footer">
                            <p ><em>Copyright &copy; 2016 - All Rights Reserved - P.R.I.E By 
                                    <a target="_blank" style="text-decoration: none" href="https://www.linkedin.com/in/yannick-dgouambe-971751103">Dgouambe Yannick</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <script type="text/javascript" src="../admin/assets/widgets/dropdown/dropdown.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/tooltip/tooltip.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/popover/popover.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/collapse/collapse.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/superclick/superclick.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/input-switch/inputswitch-alt.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/slimscroll/slimscroll.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/slidebars/slidebars.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/slidebars/slidebars-demo.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/screenfull/screenfull.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/content-box/contentbox.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/material/material.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/material/ripples.js"></script>
        <script type="text/javascript" src="../admin/assets/widgets/overlay/overlay.js"></script>
        <script type="text/javascript" src="../admin/assets/js-init/widgets-init.js"></script>
        <script type="text/javascript" src="../admin/assets/themes/admin/layout.js"></script>
    </div>
</body>
</html>