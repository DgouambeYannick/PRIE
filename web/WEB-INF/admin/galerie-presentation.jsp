<%-- 
    Document   : galerie-presentation
    Created on : 1 juin 2016, 01:36:38
    Author     : HP
--%>


<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">    
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/responsive-tables.css"/>
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/tables.css"/>
        <script src="../plugins-login/jquery.min.js"></script>
    <script src="../plugins-login/bootstrap.min.js"></script> 

        <link rel="stylesheet" type="text/css" href="../plugins-login/bootstrap.css"/>
         <link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <a style="border-radius: 0px; color:white;" href="upload-presentation" class="btn btn-primary">
            <i class="fa fa-upload fa"></i>&nbsp;UPLOAD UNE NOUVELLE IMAGE
        </a>
        <form  method="POST" action="galerie-presentation" >
		 <div class="panel panel-default" style="border: 0px solid blue; width: 100%">
                        <div class="panel-heading" style="background-color:#BCBCBC;">
                              <strong style="font-size: 100%">VOTRE GALLERIE D'IMAGES DE LA PAGE A-PROPOS</strong> 
                         
                        </div>
                     <div class="panel-body" style="border: 0px solid black; padding-left: 0px; padding-right: 0px;">
                         <div class="example-box-wrapper">
                            <table id="datatable-responsive"
                                   class="table table-striped  responsive no-wrap" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>

                                <tbody>
                                <c:forEach var="gallerie" items="${Gallerie}">
                                <tr>
                                    <td>
                                        <span class=" user-profile ">
                                            <a href="#" onclick="FileBrowserDialogue.urlimage('${sessionScope.DIR_PRESENTATION}/${gallerie.lien}')" class="user-img" title="Selectionner">
                                            <img  width="120" src="${sessionScope.DIR_PRESENTATION}/${gallerie.lien}" alt="${gallerie.description}">
                                            </a>
                                        </span>
                                    </td>
                                    <td>${gallerie.extension}</td>
                                    <td>${gallerie.taille}</td>
                                    <td>${gallerie.description}</td>
                                    <td><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${gallerie.datecreate}" /></td>
                                    <td>
                                        <a href="galerie-presentation?${gallerie.idImage}" style="display:inline; border-radius: 0px; color:white;"  class="btn btn-danger btn-sm"><i class="fa fa-trash-o fa"></i>&nbsp;Supprimer</a>
					<a href="#" onclick="FileBrowserDialogue.urlimage('${sessionScope.DIR_PRESENTATION}/${gallerie.lien}')" style="display:inline; border-radius: 0px; color:white;"  class="btn btn-success btn-sm" ><i class="fa fa-hand-o-right fa"></i>&nbsp;selectionner</a>
							  
                                    </td>
                                </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                     </div>
                 </div>
        </form>
    <script type="text/javascript" src="../admin/assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="../admin/assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="../admin/assets/widgets/datatable/datatable-responsive.js"></script>

<script type="text/javascript">/* Datatables responsive */

$(document).ready(function () {
    $('#datatable-responsive').DataTable({
        responsive: true
    });
});

$(document).ready(function () {
    $('.dataTables_filter input').attr("placeholder", "Recherche...");
});</script>
    <script type="text/javascript" src="../plugins-login/tinymce3/jscripts/tiny_mce/tiny_mce_popup.js"></script>
	<script type="text/javascript">
	//alert("okkk1111");
		var FileBrowserDialogue = {
			
			init : function(){
				//ici va le code d'initialisation
			},
			urlimage:function(URL){
				//alert("cccccccccccccc");
				//var url=document.my_form.my_field.value;
				var win = tinyMCEPopup.getWindowArg("window");
			
			//ici on insere une nouvelle information
				win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = URL;
				// creation du navigateur de l'image'
				if(typeof(win.ImageDialog)!= "undefined")
				{
					// mise à jour des dimensions de l'image
					if(win.ImageDialog.getImageData)
					{
						win.ImageDialog.getImageData();
					}
					// ... et aperçu si nécessaire
					if(win.ImageDialog.showPreviewImage)
					{
						win.ImageDialog.showPreviewImage(URL);
					}
				}
				// fermeture du popup window
				tinyMCEPopup.close();
			}
		}
		tinyMCEPopup.onInit.add(FileBrowserDialogue.init, FileBrowserDialogue);
	</script>
        </body>
</html>

