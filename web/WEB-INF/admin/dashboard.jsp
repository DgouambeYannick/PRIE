
                <div id="page-title"><h1>BIENVENUE SUR GEST-P.R.I.E</h1>
                     <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>
                 
                <div class="col-md-12">
                    <div class="row">
                       <c:forEach var="var" items="${MenusSidebar}">
                        <div class="col-md-3"  >
                            <a style="height: 165px" data-transition="pt-page-scaleUpCenter-init" data-placement="bottom" href="${var.lien}" title="Module ${var.libelle}" class="tooltip-button tile-box tile-box-shortcut add-demo-transition ${var.couleur}">
                                <div class="tile-header">${var.libelle}</div>
                                 <div class="tile-content-wrapper"><i class="${var.icone}"></i></div>
                            </a>
                        </div>  
                        </c:forEach>
                    </div>
                </div>
