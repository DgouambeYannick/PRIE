<div id="page-title"><h2>GESTION ARCHIVAGE</h2>
 <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>
 
 <div class="col-md-12">
    <div class="row">
        <c:forEach var="liste" items="${listecategorie}">
        <div class="col-md-2"  >
            <span class="icon-notification user-profile clearfix">
                <a data-transition="pt-page-scaleUpCenter-init" href="liste-dossier?${liste.idCat}" class="tooltip-button add-demo-transition" data-placement="bottom" 
                   title="Cr�er par: ${liste.createur} le <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${liste.datecreate}" />">
                    
                    <img width="100" src="../admin/images/icone/${liste.icone}" alt="Profile"/><br/>
                    <strong class="font-blue-alt">${liste.libelle}</strong><br/>
                    <span>Cr�er par: ${liste.createur} </span><br/>
                    <span>Date: <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${liste.datecreate}" /></span>
                </a>
            </span>
        </div>  
        </c:forEach>
    </div>
</div>