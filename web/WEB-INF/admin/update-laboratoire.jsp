<link rel="stylesheet" type="text/css" href="../plugins-login/tinymce.css"/>
 <script type="text/javascript" src="../plugins-login/tinymce3/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">�
		//po ur�ti nyM C E!!�
		// O2k7 skin (silver)
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "laboratoire",
		theme : "advanced",
		skin : "o2k7",
		skin_variant : "black",
		relative_urls:false,
		
		file_browser_callback:"fileBrowser",
		
		language:"en",
		plugins : "lists,pagebreak,style,layer,advhr,advimage,advlink,emotions,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "newdocument,cut,code,|,bold,italic,underline,strikethrough,|,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,help,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,bullist,numlist,pasteword",
		theme_advanced_buttons3 : "print,|,fullscreen,|,moveforward,movebackward,|,styleprops,|,cite,abbr,acronym,del,ins,|,hr,removeformat,visualaid,|,charmap,emotions,search",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
	/* fiel_name=nom du champ
	 * url= le lien
	 * type= le type
	 * win= la fenetre
	 */
	function fileBrowser(field_name, url, type, win)
	{
		tinyMCE.activeEditor.windowManager.open({
			file:"galerie-laboratoire",
			title:"GALLERIE LABORATOIRE",			/*le titre*/
			width:1050,					/*la largeur de la fenetre*/
			height:600,					/*la hauteur de la fenetre*/
			resizable:true,				/* redimentionnable ??*/
			inline:true,				/* faire fonctionner le fenetre en popups ??*/
			close_previous: false /* est ce kon ferme la fenetre precedente ??*/
		},
		{  /*ici je recupere la fenetre et l'input qui est li�*/
			window:win,
			input:field_name,
		});
		return false;  
	}
	
	</script> 
        
        <div id="page-title"><h2>GESTION LABORATOIRE</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>
                     <script>
            $(function (){
                $(".notif").show("slow").delay(5000).hide("slow");
            });
        </script>
        <c:if test="${val == 2}">
            <div class="alert alert-danger notif">
            <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
              <span style="text-transform: uppercase; color:brown">${errorimage}</span>
            </strong>
        </div>
        </c:if>
                <div class="panel">
                    <div class="panel-heading" style="background-color:#BCBCBC;">          
                        <center><a style="color:#b72724; text-decoration: none;" href="liste-laboratoire">
                           <i  class="glyph-icon icon-reply-all"></i><span class="font-size-20">retour</span>
                       </a>
                        </center>
                    </div>
                    <div class="panel-body"><h3 class="title-hero">MODIFICATION DU LABORATOIRE: <strong class="font-blue-alt">${labo.libelle} (${labo.sigle})</strong></h3>

                        <div class="example-box-wrapper">
                            <form class="form-horizontal bordered-row" method="POST" action="update-laboratoire" enctype="multipart/form-data" id="demo-form" data-parsley-validate="">
                                <div class="row">

                                    <div class="col-md-12 bg-gray-alt">
                                        <div class="form-group"></div>
                                        <div class="form-group"><label class="col-sm-3 control-label font-black">Image (Jpg, Png) (1925px-495px) :</label>
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail" style="width: 600px; height: 150px;"><img src="${sessionScope.DIR_LABO}/${labo.image}" alt="" /></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                <div style="display: inline-block ">
					                                    <span>
					                                    	<span class="fileupload-new">Choisir</span>
					                                    	<span class="fileupload-exists">Changer</span>
					                                    	<input  type="file" name="image"  />
					                                    </span>
                                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group"><label class="col-sm-3 control-label font-black">Sigle : </label>

                                            <div class="col-sm-6"><input value="${labo.sigle}" type="text" placeholder="Entrer le sigle"
                                                                         required class="form-control" name="sigle"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label font-black">Libelle : </label>

                                            <div class="col-sm-6">
                                                <input value="${labo.libelle}" type="text" placeholder="Entrer le libelle"
                                                                         required class="form-control" name="libelle">
                                                <input value="${labo.idLabo}" type="hidden" name="idLabo" />
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label font-black">Contenu : </label>

                                            <div class="col-sm-6">
                                                <textarea  id="laboratoire" rows="10" cols="80" placeholder="Entrer le libelle" name="contenu" class="form-control textarea-autosize tinyMCE" required>
                                                    ${labo.contenu}
                                                </textarea>
                                            </div>
                                        </div>
               
                                    </div>


                                </div>
                                <div class="bg-default content-box text-center pad20A mrg25T">
                                    <button type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MODIFIER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                    
           
                    

