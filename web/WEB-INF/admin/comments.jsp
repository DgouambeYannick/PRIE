<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



        <c:forEach var="liste" items="${allcomment}" > 
            <c:choose>
                <c:when test="${liste.code == sessionScope.useradmin.getIdCompte()}">
                    <div class="row">
                        <li class="col-sm-5"></li>
                    <li class="col-sm-6">
                        <div class="chat-author">
                            <img width="36" src="${sessionScope.DIR_AVATAR}/${liste.idMembre.image}" alt="">
                        </div>
                        <div class="popover right no-shadow font-normal" style="background-color: rgba(161, 238, 143, 0.57); ">
                            <div class="arrow"></div>
                            <div class="popover-content" >
                                <h3 ><a href="#" >${liste.createur}</a>

                                    <div class="float-right">
                                        <a href="#" class="btn glyph-icon icon-inbox font-gray tooltip-button"
                                           data-placement="bottom"
                                           title="piece jointe">

                                        </a>
                                    </div>
                                    
                                    <div class="float-left">
                                        <a href="comments?del=${liste.idCom}&AMP;grp=${liste.idGroupe.idGroupe}" class="btn glyph-icon icon-trash font-red tooltip-button"
                                           data-placement="bottom"
                                           title="Supprimer commentaire">

                                        </a>
                                    </div>
                                </h3>
                                ${liste.commentaire}
                                <div class="chat-time font-blue-alt"><i class="glyph-icon icon-clock-o"></i><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${liste.datecreate}" />
                                </div>
                            </div>
                        </div>
                    </li>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <li class="col-sm-5"></li>
                    <li class="float-left col-sm-6">
                        <div class="chat-author">
                            <img width="36" src="${sessionScope.DIR_AVATAR}/${liste.idMembre.image}" alt="">
                        </div>
                        <div class="popover right no-shadow font-normal" style="background-color:  rgba(255, 255, 255, 0.7);">
                            <div class="arrow"></div>
                            <div class="popover-content">
                                <h3><a href="#" >${liste.createur} </a>

                                    <div class="float-right">
                                        <a href="#" class="btn glyph-icon icon-inbox font-gray tooltip-button"
                                           data-placement="bottom"
                                           title="piece jointe">

                                        </a>
                                    </div>
                                </h3>
                                ${liste.commentaire}
                                <div class="chat-time font-red"><i class="glyph-icon icon-clock-o"></i><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${liste.datecreate}" />
                                </div>
                            </div>
                        </div>
                    </li>
                    
                    </div>
                                
                </c:otherwise>
            </c:choose>
        </c:forEach>

