<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="../plugins-login/tinymce.css"/>
<script type="text/javascript" src="../plugins-login/tinymce3/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">�
    //po ur�ti nyM C E!!�
    // O2k7 skin (silver)
    tinyMCE.init({
        // General options
        mode: "exact",
        elements: "news",
        theme: "advanced",
        skin: "o2k7",
        skin_variant: "black",
        relative_urls: false,
        file_browser_callback: "fileBrowser",
        language: "en",
        plugins: "lists,pagebreak,style,layer,advhr,advlink,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,inlinepopups,autosave",
        // Theme options
        theme_advanced_buttons1: "newdocument,cut,code,undo,redo,search|,bold,italic,underline,|,justifyfull,formatselect,fontselect,fontsizeselect,forecolor,backcolor,bullist,numlist,del,ins,link,unlink,styleprops",
        theme_advanced_buttons2: "insertdate,inserttime,preview,pasteword,print,fullscreen,|,hr,removeformat,visualaid",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,
        // Example content CSS (should be your site CSS)
        content_css: "css/content.css",
        // Drop lists for link/image/media/template dialogs
        template_external_list_url: "lists/template_list.js",
        external_link_list_url: "lists/link_list.js",
        external_image_list_url: "lists/image_list.js",
        media_external_list_url: "lists/media_list.js",
        // Replace values for the template plugin
        template_replace_values: {
            username: "Some User",
            staffid: "991234"
        }
    });


</script> 
<link rel="stylesheet" type="text/css" href="../plugins-login/publication.css"/>
<div id="page-title"><h2>GERER MES PUBLICATIONS</h2>
    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

<script>
    $(function () {
        $(".notif").show("slow").delay(5000).hide("slow");
    });
</script>
<c:if test="${val == 6}">
    <div class="alert alert-warning notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Erreur lors de la Modification !!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 0}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Modification Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 1}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Ajout Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 2}">
    <div class="alert alert-danger notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-down fa-3x"></i>
            <span style="text-transform: uppercase; color:brown">${errorfile}</span>
        </strong>
    </div>
</c:if>
<c:if test="${val == 3}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Suppression Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 4}">
    <div class="alert alert-danger notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-down fa-3x"></i>
            <span style="text-transform: uppercase; color:brown">${errortaille}</span>
        </strong>
    </div>
</c:if>
<c:if test="${val == 5}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Changement statut Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<div class="row">
    <div class="col-md-9">
        <div class="panel">
            <div class="panel-heading" style="background-color:#BCBCBC;">          
                <center>
                    <a href="#" data-backdrop="false" data-toggle="modal" data-target=".addmodule" style="border-radius: 0px;" class="btn btn-success btn-sm">
                        <i class="glyph-icon icon-plus"></i><strong>&nbsp;&nbsp;Ajouter ${module.libelle}</strong>
                    </a>
                </center>
            </div>

            <div class="panel-body">
                <h3 class="title-hero">
                    <span class="icon-notification user-profile clearfix">
                        <img width="50" src="../admin/images/icone/${module.icone}" alt="Profile">
                    </span>Mes ${module.libelle}</h3>
                <div class="example-box-wrapper">
                    <div class="row">
                        <table id="datatable-responsive"
                               class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                               width="100%">
                            <thead>
                                <tr>
                                    <th>FICHIER</th>
                                    <th>TAILLE</th>
                                    <th>LIBELLE</th>
                                    <th>STATUT</th>
                                    <th>TELECHARGEABLE</th>  
                                    <th>ANNEE</th>
                                    <th style="background-color:#3B5998; color:white">ACTIONS</th>
                                </tr>
                            </thead>

                            <tbody>
                                <c:forEach var="liste" items="${allpubtype}">
                                    <tr>
                                        <td>
                                            <span class="icon-notification user-profile clearfix">
                                                <a target="_blank" href="${sessionScope.DIR_PUBLICATION}/${liste.lien}" class="tooltip-button" data-placement="bottom" title="consulter"> <img width="50" src="../admin/images/icone/${liste.icone}" alt="Profile"></a>
                                            </span>
                                        </td>
                                        <td>${liste.taille}</td>
                                        <td>${liste.libelle}</td>
                                        <td>${liste.statut}</td>
                                        <td>${liste.telecharger}</td>
                                        <td>${liste.annee}</td> 
                                        <td>
                                            <a href="#" data-backdrop="false" data-toggle="modal" data-target=".${liste.idPub}" data-placement="bottom" title="Modifier" class="font-normal tooltip-button btn-sm btn-danger"><i  class="glyph-icon icon-edit"></i></a>&nbsp;&nbsp;&nbsp;
                                            <a href="#" data-backdrop="false" data-toggle="modal" data-target=".${liste.idPub+'9'}" data-placement="bottom" title="Supprimer" class="font-normal tooltip-button btn-sm btn-primary"><i  class="glyph-icon icon-trash"></i></a>&nbsp;&nbsp;&nbsp;
                                            <a data-placement="bottom" title="Changer statut" href="gerer-publication?x=${liste.idPub}&AMP;y=stat&AMP;t=${module.idType}" class="font-normal tooltip-button btn-sm btn-blue-alt"><i  class="glyph-icon icon-refresh"></i></a>&nbsp;&nbsp;&nbsp;
                                            <a data-placement="bottom" title="Changer statut de Telechargement" href="gerer-publication?x=${liste.idPub}&AMP;y=tel&AMP;t=${module.idType}" class="font-normal tooltip-button btn-sm btn-success"><i  class="glyph-icon icon-refresh"></i></a>

                                        </td>

                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="title-hero font-red"><i class="glyph-icon icon-bars"></i>  MODULES PUBLICATIONS</h3>
            </div>
            <div class="panel-body">
                <ul class="nav sidebar-categories margin-bottom-40">
                    <li class="${active2}">
                        <a class="add-demo-transition font-normal tooltip-button" data-placement="bottom" data-transition="pt-page-rotatePullRight-init" href="gerer-publication" title="Mon Curriculum Vitae">
                            <span class='font-black'>Mon Curriculum Vitae</span>
                        </a>
                    </li>
                </ul>
                <c:forEach var="var" items="${allmodule}">
                    <c:choose>
                        <c:when test="${var.idType == pageContext.request.getParameter('t')}">
                            <ul class="nav sidebar-categories margin-bottom-40">
                                <li class="${active}">
                                    <a class="add-demo-transition font-normal tooltip-button" data-placement="bottom" data-transition="pt-page-rotatePullRight-init" href="gerer-publication?t=${var.idType}" title="${var.libelle}">
                                        <span class='font-white'>Mes ${var.libelle}</span>
                                    </a>
                                </li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <ul class="nav sidebar-categories margin-bottom-40">
                                <li>
                                    <a class="add-demo-transition font-normal tooltip-button" data-placement="bottom" data-transition="pt-page-rotatePullRight-init" href="gerer-publication?t=${var.idType}" title="${var.libelle}">
                                        <span>Mes ${var.libelle}</span>
                                    </a>
                                </li>
                            </ul>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </div>
        </div>
    </div>
</div>


<div class="modal fade addmodule" tabindex="-1" role="dialog" 
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <h4 class="modal-title">AJOUTER <strong class="font-blue-alt">${module.libelle}</strong> </h4></div>
            <form method="post" action="gerer-publication" enctype="multipart/form-data" data-parsley-validate="">
                <div class="modal-body">
                    <div class="row" style="border:0px solid red">
                        <div class="col-lg-12" style="border:0px solid black">
                            <div class="panel panel-default" style="border:0px solid red">

                                <div class="panel-body" style="border:0px solid blue; padding: 0px;">
                                    <div class="table-responsive" style="border:0px solid green">

                                        <table class="table table-striped table-bordered" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td align="right">FICHIER (*pdf*word* - 5Mo Maximum)</td>  
                                                    <td align="left">

                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="../admin/images/icone/pdf.jpg" alt="" /></div>
                                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                            <div style="display: inline-block ">
                                                                <span>
                                                                    <span class="fileupload-new">Deposer</span>
                                                                    <span class="fileupload-exists">Changer</span>
                                                                    <input  type="file" required name="fichier"  />
                                                                </span>
                                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                                            </div>
                                                        </div>
                                                        <input name="idType" value="${module.idType}"  type="hidden"  class="form-control" >
                                                    </td>  
                                                </tr>
                                                <tr>
                                                    <td align="right">TITRE:</td>  
                                                    <td align="left">
                                                        <input name="libelle"  type="text" required class="form-control" >
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">RESUME</td>
                                                    <td align="left" >
                                                        <div class="form-group">
                                                            <textarea rows="10" cols="80" id="news" name="resume" class="form-control textarea-autosize">
                                              
                                                            </textarea>
                                                        </div>
                                                    </td>  
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Fermer
                    </button>
                    <button name="action" value="Ajouter"  type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>AJOUTER</span><i
                            class="glyph-icon icon-linecons-paper-plane"></i></button>
                </div>      
            </form>

        </div>
    </div>
</div>  

<c:forEach var="liste" items="${allpubtype}">
    <div class="modal fade ${liste.idPub}" tabindex="-1" role="dialog" 
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title">MODIFICATION DE <strong class="font-blue-alt">${liste.libelle}</strong> </h4></div>
                <form method="post" action="gerer-publication" enctype="multipart/form-data" data-parsley-validate="">
                    <div class="modal-body">
                        <div class="row" style="border:0px solid red">
                            <div class="col-lg-12" style="border:0px solid black">
                                <div class="panel panel-default" style="border:0px solid red">

                                    <div class="panel-body" style="border:0px solid blue; padding: 0px;">
                                        <div class="table-responsive" style="border:0px solid green">

                                            <table class="table table-striped table-bordered" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td align="right">FICHIER (*pdf* - 5Mo Maximum)</td>  
                                                        <td align="left">

                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="${sessionScope.DIR_PUBLICATION}/${liste.lien}" alt="" /></div>
                                                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                <div style="display: inline-block ">
                                                                    <span>
                                                                        <span class="fileupload-new">Deposer</span>
                                                                        <span class="fileupload-exists">Changer</span>
                                                                        <input  type="file" name="fichier"  />
                                                                    </span>
                                                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                                                </div>
                                                            </div>
                                                            <input name="idPub" value="${liste.idPub}"  type="hidden"  >
                                                            <input name="idType" value="${module.idType}"  type="hidden" >
                                                        </td>  
                                                    </tr>
                                                    <tr>
                                                        <td align="right">TITRE:</td>  
                                                        <td align="left">
                                                            <input name="libelle" value="${liste.libelle}" type="text" required class="form-control" >
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">RESUME</td>
                                                        <td align="left" >
                                                            <div class="form-group">
                                                                <textarea rows="10" cols="80" id="updatepub" name="resume" class="form-control textarea-autosize">
                                                                    ${liste.contenu}
                                                                </textarea>
                                                            </div>
                                                        </td>  
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Fermer
                        </button>
                        <button name="action" value="Modifier" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MISE A JOUR</span><i
                                class="glyph-icon icon-linecons-paper-plane"></i></button>
                    </div>      
                </form>

            </div>
        </div>
    </div>
</c:forEach>
<c:forEach  var="liste" items="${allpubtype}">
    <div class="modal fade ${liste.idPub+'9'}" tabindex="-1" role="dialog" 
         aria-labelledby="myLargeModalLabel" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Voulez-vous Vraiment supprimer le CV : ${liste.libelle}</h4>
                </div>
                <form action="gerer-publication" method="post" enctype="multipart/form-data">
                    <div class="modal-body">                        
                        <input name="idPub" value="${liste.idPub}"  type="hidden"  >
                        <input name="idType" value="${module.idType}"  type="hidden" >
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Annuler
                        </button>
                        <button name="action" value="Supprimer" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>SUPPRIMER</span><i
                                class="glyph-icon icon-linecons-paper-plane"></i></button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</c:forEach>


<script type="text/javascript">�
    //po ur�ti nyM C E!!�
    // O2k7 skin (silver)
    tinyMCE.init({
        // General options
        mode: "exact",
        elements: "updatepub",
        theme: "advanced",
        skin: "o2k7",
        skin_variant: "black",
        relative_urls: false,
        file_browser_callback: "fileBrowser",
        language: "en",
        plugins: "lists,pagebreak,style,layer,advhr,advlink,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,inlinepopups,autosave",
        // Theme options
        theme_advanced_buttons1: "newdocument,cut,code,undo,redo,search|,bold,italic,underline,|,justifyfull,formatselect,fontselect,fontsizeselect,forecolor,backcolor,bullist,numlist,del,ins,link,unlink,styleprops",
        theme_advanced_buttons2: "insertdate,inserttime,preview,pasteword,print,fullscreen,|,hr,removeformat,visualaid",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,
        // Example content CSS (should be your site CSS)
        content_css: "css/content.css",
        // Drop lists for link/image/media/template dialogs
        template_external_list_url: "lists/template_list.js",
        external_link_list_url: "lists/link_list.js",
        external_image_list_url: "lists/image_list.js",
        media_external_list_url: "lists/media_list.js",
        // Replace values for the template plugin
        template_replace_values: {
            username: "Some User",
            staffid: "991234"
        }
    });


</script>