<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="header-nav-left">
    <div class="user-account-btn dropdown">
        <sql:query var="memb" dataSource="jdbc/bd_prie">
            SELECT * FROM membre WHERE membre.id_membre='${sessionScope.useradmin.getIdMembre().getIdMembre()}'
        </sql:query> 
        <c:forEach var="liste" items="${memb.rows}" > 
            <a style="color:white; margin-right: 50px" href="#" title="mon compte"
               class="user-profile clearfix" data-toggle="dropdown">
                <img width="28" src="${sessionScope.DIR_AVATAR}/${liste.image}" alt="Profile image">
                <span>${liste.nom} ${liste.prenom}</span> <i class="glyph-icon icon-angle-down"></i>
            </a>

            <div class="dropdown-menu float-right">
                <div class="box-sm-login">
                    <div class="login-box clearfix">
                        <div class="user-img">
                            <a href="#" title="" class="change-img">Changer photo</a>
                            <img src="${sessionScope.DIR_AVATAR}/${liste.image}" alt="">
                        </div>
                        <div class="user-info"><span>${liste.nom} ${liste.prenom}<i>${sessionScope.useradmin.idProfil.libelle}</i></span>
                            <a class="add-demo-transition" data-transition="pt-page-rotatePullTop-init" href="editer-profile?${liste.id_membre}" title="Editer profile">Editer profile</a>
                        </div>
                    </div>

                    <div class="divider"></div>
                    <div class="button-pane button-pane-alt pad5L pad5R text-center">
                        <a href="deconnexion"
                           class="btn btn-flat display-block font-normal btn-danger"><i
                                class="glyph-icon icon-power-off"></i> Deconnexion</a>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>

<!---------------------- debut barre haute gauche options------------------------------------------->
<div id="header-nav-right" >
    <!---------------------- debut barre haute gauche icone invitations------------------------------------------->
    <a href="#" data-placement="bottom" class="tooltip-button hdr-btn bg-primary-header"
       id="fullscreen-btn" title="Affichage Plein Ecran"><i class="glyph-icon icon-arrows-alt"></i></a>
    <!--<a href="#" data-placement="bottom"
       class="tooltip-button hdr-btn sb-toggle-left bg-primary-header" id="chatbox-btn"
       title="Membres de mon Laboratoire"><i class="glyph-icon icon-group"></i></a>-->


    <div class="dropdown" >
        <a data-placement="bottom" data-toggle="dropdown" class="tooltip-button bg-primary-header"
           href="#" title="Invitations">
            <sql:query var="res" dataSource="jdbc/bd_prie">
                SELECT COUNT(*) AS total FROM groupe, compte_groupe WHERE statut_invitation = 'En-cours' AND groupe.id_groupe=compte_groupe.id_groupe AND compte_groupe.id_compte='${sessionScope.useradmin.getIdCompte()}'
            </sql:query>
            <c:forEach var="liste" items="${res.rows}" >
                    <span class="bs-badge float-right badge-danger">${liste.total}</span>
                
            </c:forEach>
            <i class="glyph-icon icon-bullhorn"></i>
        </a>

        <div class="dropdown-menu box-md float-left">
            <div class="popover-title display-block clearfix pad10A">LISTE DES INVITATIONS</div>
            <div class="scrollable-content scrollable-slim-box">
                <ul class="no-border notifications-box">
                    <sql:query var="invit" dataSource="jdbc/bd_prie">
                        SELECT * FROM groupe, compte_groupe WHERE statut_invitation = 'En-cours' AND groupe.id_groupe=compte_groupe.id_groupe AND compte_groupe.id_compte='${sessionScope.useradmin.getIdCompte()}'
                    </sql:query>
                    <c:forEach var="liste" items="${invit.rows}" >       
                        <li>
                            <span class="icon-notification user-profile clearfix">
                                <img width="33" src="${sessionScope.DIR_GROUPE}/${liste.image}" alt="Profile groupe">
                            </span>
                            <span class="notification-text">Groupe ${liste.libelle}</span><br/>
                            <span class="notification-text font-blue">Cr�e par ${liste.createur}</span>

                            <div class="notification-time">
                                <a href="invitation?g=${liste.id_groupe}&AMP;c=${sessionScope.useradmin.getIdCompte()}" class="btn btn-xs btn-hover btn-blue-alt font-normal"><span>Accepter l'Invitation</span><i
                                        class="glyph-icon icon-linecons-thumbs-up"></i></a>
                            </div>
                        </li>
                    </c:forEach>

                </ul>
            </div>
        </div>
    </div>

    <div class="dropdown" >
        <a href="#" data-placement="bottom" data-toggle="dropdown" class="tooltip-button bg-primary-header"
           title="Mes groupes">
            <i class="glyph-icon icon-comments-o"></i>
        </a>
        <div class="dropdown-menu box-md float-left">
            <div class="popover-title display-block clearfix pad10A">MES GROUPES COLLABORATIFS</div>
            <div class="scrollable-content scrollable-slim-box">
                <ul class="no-border notifications-box">
                    <sql:query var="result" dataSource="jdbc/bd_prie">
                        SELECT *  FROM groupe WHERE createur = '${sessionScope.useradmin.getLogin()}'
                    </sql:query>

                    <c:forEach var="liste" items="${result.rows}" >       
                        <li>
                            <span class="icon-notification user-profile clearfix">
                                <img class="img-circle" width="33" src="${sessionScope.DIR_GROUPE}/${liste.image}" alt="Profile groupe">
                            </span>
                            <span class="notification-text">Groupe ${liste.libelle}</span><br/>
                            <span class="notification-text font-green">Cr�e par ${liste.createur}</span>
                            <div class="notification-time">
                                <a href="commentaire?${liste.id_groupe}"  class=" notification-btn btn btn-xs btn-primary tooltip-button"
                                   data-placement="left" title="Voir commentaires"><i
                                        class="glyph-icon icon-comments-o"></i></a>
                            </div>

                        </li> 
                    </c:forEach> 
                    <sql:query var="resultat" dataSource="jdbc/bd_prie">
                        SELECT * FROM groupe, compte_groupe WHERE statut_invitation = 'Accepte' AND groupe.id_groupe=compte_groupe.id_groupe AND compte_groupe.id_compte='${sessionScope.useradmin.getIdCompte()}'
                    </sql:query>

                    <c:forEach var="liste" items="${resultat.rows}" > 
                        <li>
                            <span class="icon-notification user-profile clearfix">
                                <img class="img-circle" width="33" src="${sessionScope.DIR_GROUPE}/${liste.image}" alt="Profile groupe">
                            </span>
                            <span class="notification-text">Groupe ${liste.libelle}</span><br/>
                            <span class="notification-text font-green">Cr�e par ${liste.createur}</span>

                            <div class="notification-time">
                                <a href="commentaire?${liste.id_groupe}"  class=" notification-btn btn btn-xs btn-primary tooltip-button"
                                   data-placement="left" title="Voir commentaires"><i
                                        class="glyph-icon icon-comments-o"></i></a>
                            </div>
                        </li> 
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
</div>

