<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>            
<div id="page-title"><h2>GESTION GROUPES</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div> 
                    <script>
                        $(function (){
                            $(".notif").show("slow").delay(5000).hide("slow");
                        });
                    </script>
                    <c:if test="${val == 1}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Invitations envoy�s avec succ�s !!
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 2}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Retrait du membre Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
            <div class="panel">
                    <div class="panel-heading" style="background-color:#BCBCBC;">
                        
                        <center>
                            <a style="color:#b72724; text-decoration: none;" href="creer-groupe">
                                <i  class="glyph-icon icon-reply-all"></i><span class="font-size-20">retour</span>
                            </a>
                        </center>
                    </div>
                    <div class="panel-body">
                        <h3 class="title-hero">
                            <span class="icon-notification user-profile clearfix">
                               <img width="50" src="${sessionScope.DIR_GROUPE}/${groupe.image}" alt="Profile">
                             </span>
                               Membres de votre Groupe: <span class="font-blue-alt">${groupe.libelle}</span>
                        </h3>

                        <div class="example-box-wrapper">
                            <div class="row">

                            <div class="col-md-6 bg-gray-alt">
                            <table id="datatable-responsive"
                                   class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>PHOTO</th>
                                    <th>MEMBRE</th>
                                    <th>INVITATION</th>
                                    <th style="background-color:#3B5998; color:white">ACTIONS</th>
                                </tr>
                                </thead>

                                <tbody>
                                <c:forEach var="GroupeCompte" items="${AllCompteGroupe}" varStatus="iter">
                                <tr>
                                    <td>
                                         <span class="icon-notification user-profile clearfix">
                                           <img width="50" src="${sessionScope.DIR_AVATAR}/${comptes[iter.index].idMembre.image}" alt="Profile">
                                        </span>
                                    </td>
                                    <td>${comptes[iter.index].idMembre.civilite} ${comptes[iter.index].idMembre.nom} ${comptes[iter.index].idMembre.prenom}</td>
                                    <td>${GroupeCompte.statutInvitation}</td>
                                    <td><a href="groupe-membres?g=${groupe.idGroupe}&AMP;c=${comptes[iter.index].idCompte}&AMP;t=del"  class="tooltip-button font-normal btn-sm btn-primary" data-placement="bottom" title="Retirer ce Membre"><i class="glyph-icon icon-trash"></i></a>&nbsp;retirer
                                    </td>
                                </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            </div>
                                    <div class="col-md-1"></div>
                            <div class="col-md-5 bg-gray-alt">
                                <form action="groupe-membres" method="post">
                                    
                                        <div class="row">
                                         <div class="form-group">
                                            <div class="col-sm-12">
                                                <select multiple="multiple" class="multi-select" name="compte">
                                                    <c:forEach var="membres" items="${allcompte}" >
                                                         <option value="${membres.idCompte}">${membres.idMembre.civilite} ${membres.idMembre.nom} ${membres.idMembre.prenom}</option>
                                                     </c:forEach>
                                                </select>
                                                <input type="hidden" name="groupe" value="${groupe.idGroupe}" />
                                            </div>
                                         </div>
                                        </div>
                                    
                                    <div class="modal-footer">
                                        
                                        <button name="action" value="Ajouter" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>Envoyer L'invitation</span><i
                                                                    class="glyph-icon icon-linecons-paper-plane"></i></button>
                                    </div> 
                                  </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

