<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
<div id="page-title"><h2>GESTION GROUPES</h2>
    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5>
</div>

<script>
    $(function () {
        $(".notif").show("slow").delay(5000).hide("slow");
    });
</script>
<c:if test="${val == 2}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Modification du groupe Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 1}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Ajout du Groupe Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 3}">
    <div class="alert alert-danger notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-down fa-3x"></i>
            <span style="text-transform: uppercase; color:brown">${errorimage}</span>
        </strong>
    </div>
</c:if>
<c:if test="${val == 4}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Suppression du Groupe Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 5}">
    <div class="alert alert-warning notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Suppression du Groupe Echou�!!! Veuillez d'abord retirer tous les Membres ...
        </strong>
    </div>
</c:if>
<div class="panel">
    <div class="panel-heading" style="background-color:#BCBCBC;">          
        <center>
            <a href="#" data-backdrop="false" data-toggle="modal" data-target=".addgroupe" style="border-radius: 0px;" class="btn btn-success btn-sm">
                <i class="glyph-icon icon-plus"></i><strong>&nbsp;&nbsp;Ajouter un groupe</strong>
            </a>
        </center>
    </div>
    <div class="panel-body">
        <h3 class="title-hero">Liste de vos groupes</h3>

        <div class="example-box-wrapper">
            <table id="datatable-responsive"
                   class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                   width="100%">
                <thead>
                    <tr>
                        <th>IMAGE</th>
                        <th>TITRE DU GROUPE</th>
                        <th>DERNIERE MODIFICATION</th>
                        <th style="background-color:#3B5998; color:white">ACTIONS</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="liste" items="${allgroupe}">
                        <tr>
                            <td>
                                <span class="icon-notification user-profile clearfix">
                                    <a target="_blank" href="${sessionScope.DIR_GROUPE}/${liste.image}" class="tooltip-button" data-placement="bottom" title="consulter"> <img width="70" src="${sessionScope.DIR_GROUPE}/${liste.image}" alt="Profile"></a>
                                </span>
                            </td>
                            <td>${liste.libelle}</td>
                            <td><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${liste.datemodif}" /></td>
                            <td>
                                <a href="#" data-backdrop="false" data-toggle="modal" data-target=".${liste.idGroupe+'9'}"  class="tooltip-button font-normal btn-sm btn-primary" data-placement="bottom" title="Supprimer"><i  class="glyph-icon icon-trash"></i></a>&nbsp;Supprimer<br/>
                                <a href="#" data-backdrop="false" data-toggle="modal" data-target=".${liste.idGroupe}" class="tooltip-button font-normal btn-sm btn-warning" data-placement="bottom" title="Modifier"><i  class="glyph-icon icon-edit"></i></a>&nbsp;Modifier<br/>
                                <a href="groupe-membres?g=${liste.idGroupe}"  class="tooltip-button font-normal btn-sm btn-blue-alt" data-placement="bottom" title="All Membres"><i  class="glyph-icon icon-users"></i></a>&nbsp;All Membres

                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" src="../admin/assets/widgets/parsley/parsley.js"></script>

<form method="post" action="creer-groupe" enctype="multipart/form-data" data-parsley-validate="">
               
<div class="modal fade addgroupe" tabindex="-1" role="dialog" 
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <h4 class="modal-title">AJOUTER UN GROUPE</h4></div>
             <div class="modal-body">
                    <div class="row" style="border:0px solid red">
                        <div class="col-lg-12" style="border:0px solid black">
                            <div class="panel panel-default" style="border:0px solid red">

                                <div class="panel-body" style="border:0px solid blue; padding: 0px;">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group"><label class="col-sm-3 control-label font-black">Image du groupe (Par defaut) (Jpg, Png) :</label>
                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;"><img src="../admin/images/icone/prie.png" alt="" /></div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;"></div>
                                                    <div style="display: inline-block ">
                                                        <span>
                                                            <span class="fileupload-new">Choisir</span>
                                                            <span class="fileupload-exists">Changer</span>
                                                            <input  type="file" name="image"  />
                                                        </span>
                                                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-3 control-label font-black">Titre du groupe : </label>

                                                <div class="col-sm-6">
                                                    <input required  type="text" name="titre" class="form-control"  />
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Fermer
                    </button>
                    <a href="#" data-backdrop="false" data-toggle="modal" data-target=".menu" class="btn btn-lg btn-gray-alt font-normal">
                        <span>SUIVANT</span>
                    </a>
                </div>  
        </div>
    </div>
</div>

  <div class="modal fade menu" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" style="width: 1200px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Selectionner les Membres de ce Groupe</h4>
          </div>
         
            <div class="modal-body">
                    <select multiple="multiple" class="multi-select" name="listecompte">
                        <c:forEach var="membres" items="${allcompte}" >
                             <option value="${membres.idCompte}">${membres.idMembre.civilite} ${membres.idMembre.nom} ${membres.idMembre.prenom}</option>
                         </c:forEach>
                    </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    PRECEDENT
                </button>
                <button name="action" value="Ajouter" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>AJOUTER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div> 
         
        </div>
      </div>
     </div>
  </form>

<c:forEach var="liste" items="${allgroupe}">
    <div class="modal fade ${liste.idGroupe}" tabindex="-1" role="dialog" 
         aria-labelledby="myLargeModalLabel" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Renommer le Groupe : &nbsp;&nbsp <em class="font-blue-alt">${liste.libelle}</em></h4>
                </div>
                <form action="creer-groupe" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group"><label class="col-sm-3 control-label font-black">Image du groupe (Jpg, Png) :</label>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;"><img src="${sessionScope.DIR_GROUPE}/${liste.image}" alt="" /></div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;"></div>
                                <div style="display: inline-block ">
                                    <span>
                                        <span class="fileupload-new">Choisir</span>
                                        <span class="fileupload-exists">Changer</span>
                                        <input  type="file" name="image"  />
                                    </span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                </div>
                            </div>
                        </div>
                        <input type="text" value="${liste.libelle}" required name="titre" class="form-control" placeholder="entrer le nouveau nom ici..."  /><br/>                          
                        <input type="hidden" name="groupe" value="${liste.idGroupe}" class="form-control"/> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Fermer
                        </button>
                        <button name="action" value="Modifier" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MODIFIER</span><i
                                class="glyph-icon icon-linecons-paper-plane"></i></button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</c:forEach>

<c:forEach var="groupe" items="${allgroupe}">
    <div class="modal fade ${groupe.idGroupe+'9'}" tabindex="-1" role="dialog" 
         aria-labelledby="myLargeModalLabel" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Voulez-vous Vraiment supprimer le Groupe : &nbsp;&nbsp <em class="font-blue-alt">${groupe.libelle}</em> ?</h4>
                </div>
                <form action="creer-groupe" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <p>Veuillez vous assurer d'avoir retirer tous les Membres...</p>
                        <input type="hidden" name="groupe" value="${groupe.idGroupe}" class="form-control"/> 
                        <input type="hidden" value="${groupe.libelle}" name="titre" />
                        <input type="hidden"  name="image" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Annuler
                        </button>
                        <button name="action" value="Supprimer" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>SUPPRIMER</span><i
                                class="glyph-icon icon-linecons-paper-plane"></i></button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</c:forEach>

