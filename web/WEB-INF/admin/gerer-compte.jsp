<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
<div id="page-title"><h2>GESTION DES COMPTES</h2>
    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

<script>
    $(function () {
        $(".notif").show("slow").delay(5000).hide("slow");
    });
</script>
<c:if test="${val == 1}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Modification du compte de <span style="text-transform: uppercase; color:brown">${updatecompte}</span> Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 2}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Ajout du compte de <span style="text-transform: uppercase; color:brown">${ajoutcompte}</span> Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 3}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Statut chang� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 4}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" <i class="fa fa-thumbs-up fa-3x"></i>
            Suppression du compte Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 5}">
    <div class="alert alert-warning notif">
       <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Modification du compte de <span style="text-transform: uppercase; color:brown">${updatecompte}</span> Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<div class="panel">
    <div class="panel-heading" style="background-color:#BCBCBC;">          
        <center><a href="gerer-compte-membre" style="border-radius: 0px;" class="btn btn-success btn-sm">
                <i class="glyph-icon icon-plus"></i><strong>&nbsp;&nbsp;Cr�er un Compte</strong>
            </a>
        </center>
    </div>
    <div class="panel-body">
        <h3 class="title-hero">Liste des Comptes</h3>

        <div class="example-box-wrapper">
            <table id="datatable-responsive"
                   class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                   width="100%">
                <thead>
                    <tr>
                        <th>PHOTO</th>
                        <th>NOM-PRENOM</th>
                        <th>ROLE</th>
                        <th>LOGIN</th>
                        <th>PASSWORD</th>
                        <th>STATUT</th>
                        <th>DERNIERE MODIFICATION</th>
                        <th style="background-color:#3B5998; color:white">ACTIONS</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="listecompte" items="${allcompte}">
                        <tr>
                            <td>
                                <span class="icon-notification user-profile clearfix">
                                    <img width="50" src="${sessionScope.DIR_AVATAR}/${listecompte.photo}" alt="Profile">
                                </span>
                            </td>
                            <td>${listecompte.civilite} ${listecompte.nom} ${listecompte.prenom}</td>
                            <td>${listecompte.role}</td>
                            <td><i class="glyph-icon icon-user"></i>&nbsp;${listecompte.login}</td>
                            <td><i class="glyph-icon icon-key"></i>******</td>
                            <td style="color:#b72724;">${listecompte.statut}</td>
                            <td><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listecompte.datemodif}" /></td>
                            <td>
                                <a data-placement="bottom" href="#" data-backdrop="false" data-toggle="modal" data-target=".${listecompte.idCompte}" title="Modifier Compte" class="font-normal tooltip-button btn-sm btn-warning"><i class="glyph-icon icon-edit"></i></a>&nbsp;Modifier<br/>
                                <a data-placement="bottom" href="gerer-compte?${listecompte.idCompte}" title="Changer Statut Compte"  class="font-normal tooltip-button btn-sm btn-blue-alt"><i  class="glyph-icon icon-refresh"></i></a>&nbsp;Changer Statut<br/>
                                <a href="#" data-backdrop="false" data-toggle="modal" data-target=".${listecompte.idCompte+'9'}"  class="tooltip-button font-normal btn-sm btn-primary" data-placement="bottom" title="Supprimer"><i  class="glyph-icon icon-trash"></i></a>&nbsp;Supprimer<br/>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript" src="../admin/assets/widgets/parsley/parsley.js"></script>

<!-- modification du compte  -->
<c:forEach var="listecompte" items="${allcompte}">
    <div class="modal fade ${listecompte.idCompte}" tabindex="-1" role="dialog" 
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title">MODIFICATION DU COMPTE DE 
                        <em class="font-blue-alt"> ${listecompte.civilite} ${listecompte.nom} ${listecompte.prenom}</em></h4></div>
                <form method="post" action="gerer-compte" data-parsley-validate="">
                    <div class="modal-body">
                        <div class="row" style="border:0px solid red">
                            <div class="col-lg-12" style="border:0px solid black">
                                <div class="panel panel-default" style="border:0px solid red">

                                    <div class="panel-body" style="border:0px solid blue; padding: 0px;">
                                        <div class="table-responsive" style="border:0px solid green">

                                            <table class="table table-striped table-bordered" width="100%">
                                                <tbody>

                                                    <tr>
                                                        <td align="right">Login</td>  
                                                        <td align="left"><input name="login" value="${listecompte.login}" type="text" required class="form-control" ></td>    
                                                        <td align="right">Profil</td>
                                                        <td align="left">
                                                            <select required name="profil" class="custom-select">
                                                               <option value="${listecompte.idrole}">${listecompte.role}</option>     
                                                                <c:forEach var="liste" items="${allprofil}">
                                                                    <option value="${liste.idProfil}">${liste.libelle}</option>
                                                                </c:forEach>
                                                            </select>
                                                        </td>  
                                                    </tr>
                                                    <tr>
                                                        <td align="right">Mot de passe</td>  
                                                        <td align="left"><input name="password" value="${listecompte.password}" type="text"  required class="form-control"></td>   
                                                    </tr>
                                                    <tr>
                                                        <td align="right"></td>  
                                                        <td align="left">
                                                            <input type="hidden" name="compte" value="${listecompte.idCompte}" class="form-control"/>
                                                            <input type="hidden" name="statut" value="${listecompte.statut}" class="form-control">
                                                        </td>   
                                                        <td align="right"></td>  
                                                        <td align="left"><input type="hidden" name="membre" value="${listecompte.idmemb}" class="form-control"></td>  
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Fermer
                        </button>
                        <button name="action" value="Modifier" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MODIFIER</span><i
                                class="glyph-icon icon-linecons-paper-plane"></i></button>
                    </div>      
                </form>

            </div>
        </div>
    </div>
</c:forEach>

<c:forEach var="listecompte" items="${allcompte}">
    <div class="modal fade ${listecompte.idCompte+'9'}" tabindex="-1" role="dialog" 
         aria-labelledby="myLargeModalLabel" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Voulez-vous Vraiment supprimer le Compte de : &nbsp;&nbsp <em class="font-blue-alt">${listecompte.civilite} ${listecompte.nom} ${listecompte.prenom}</em> ?</h4>
                </div>
                <form action="gerer-compte" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="compte" value="${listecompte.idCompte}" class="form-control"/> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Annuler
                        </button>
                        <button name="action" value="Supprimer" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>SUPPRIMER</span><i
                                class="glyph-icon icon-linecons-paper-plane"></i></button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</c:forEach>



