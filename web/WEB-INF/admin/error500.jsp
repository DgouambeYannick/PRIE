<%-- 
    Document   : newjsp
    Created on : 24 mai 2016, 09:52:47
    Author     : HP
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <!--[if IE]>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title>500</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
        <link rel="apple-touch-icon-precomposed" sizes="144x144"
              href="../admin/assets/images/icons/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114"
              href="../admin/assets/images/icons/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72"
              href="../admin/assets/images/icons/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../admin/assets/images/icons/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="../admin/assets/images/icons/favicon.png">

         <link rel="stylesheet" type="text/css" href="../admin/assets/elements/buttons.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/page-transitions.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/utils.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/helpers/colors.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/response-messages.css">
        <link rel="stylesheet" type="text/css" href="../admin/assets/elements/responsive-tables.css">

    </head>

    <body>
<style type="text/css">html, body {
    height: 100%;
}

body {
    background: #fff;
    overflow: hidden;
}</style>
<script type="text/javascript" src="../admin/assets/widgets/wow/wow.js"></script>
<script type="text/javascript">/* WOW animations */

wow = new WOW({
    animateClass: 'animated',
    offset: 100
});
wow.init();</script>
<img src="../admin/images/blurred-bg-7.jpg" class="login-img wow fadeIn" alt="">

<div class="center-vertical">
    <div class="center-content">
        <div class="col-md-6 center-margin">
            <div class="server-message wow bounceInDown inverse"><h1>Error 500</h1>

                <h2>The server encountered a syntax error and could not complete your request.</h2>

                <p>Cliquer sur le bouton ci-dessous ou Contacter l'Administrateur <span style="color:greenyellow">(fansi.yannick@gmail.com)</span></p>
                <button  onclick="goback()" class="btn btn-lg btn-success">RETOURNE PAGE PRECEDENTE</button>
            </div>
        </div>
    </div>
</div>
<script>
    function goback(){
        window.history.back();
    }
</script>
</body>

</html>