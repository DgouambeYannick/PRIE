<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>  
<div id="page-title"><h2>GESTION ARCHIVAGE</h2>
    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

<script>
    $(function () {
        $(".notif").show("slow").delay(5000).hide("slow");
        $(".action").show("slow");
    });
</script>
<c:if test="${val == 1}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Ajout Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 2}">
    <div class="alert alert-danger notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-down fa-3x"></i>
            Erreur l'extension <span style="text-transform: uppercase; color:brown">${errorfile}</span> n'est pas valide!!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 4}">
    <div class="alert alert-danger notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-o-down fa-3x"></i>
            <span style="text-transform: uppercase; color:brown">${errortaille}</span>
        </strong>
    </div>
</c:if>
<c:if test="${val == 3}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Modification du document <span style="text-transform: uppercase; color:brown">${updatedos}</span> Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 7}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Mise A jour du document <span style="text-transform: uppercase; color:brown">${updatedos}</span> Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 5}">
    <div class="alert alert-warning action"> 
        <center>
            <span style="font-size:20px" class='font-black' ><i class="fa fa-info-circle fa-2x"></i>
                Oups!!!! Impossible d'Ajouter car Le Document <strong class='font-red'> ${libelle}</strong > Existe Deja!!!
            </span>
        </center>
    </div>

</c:if>
<c:if test="${val == 6}">
    <div class="alert alert-warning action"> 
        <center>
            <span style="font-size:20px" class='font-black' ><i class="fa fa-info-circle fa-2x"></i>
                Oups!!!! Le document  <strong class='font-red'> ${libelle}</strong > Ne correspond pas � celui qui doit etre mit A jour dans le syst�me !!!
            </span>
        </center>
    </div>

</c:if>

<div class="panel">
    <div class="panel-heading" style="background-color:#BCBCBC;">          
        <center>
            <a style="color:#b72724; text-decoration: none;" href="liste-dossier?${categorie}">
                <i  class="glyph-icon icon-reply-all"></i><span class="font-size-20">retour</span>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#" data-backdrop="false" data-toggle="modal" data-target=".adddoc" style="border-radius: 0px;" class="btn btn-success btn-sm">
                <i class="glyph-icon icon-plus"></i><strong>&nbsp;&nbsp;Ajouter Document</strong>
            </a>
        </center>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6"> 
                <h3 class="title-hero">
                    <span class="icon-notification user-profile clearfix">
                        <img width="70" src="../admin/images/icone/document.png" alt="Profile">
                    </span>
                    Liste des Documents: <strong class="font-blue-alt">${repertoire.libelle}</strong>
                </h3>
            </div> 
            <div class="col-sm-6">
                <div class="example-box-wrapper">
                    <div class="alert alert-notice">
                        <a href="#" title="Close" class="glyph-icon alert-close-btn icon-remove"></a>
                        <div class="bg-blue alert-icon"><i class="glyph-icon icon-info"></i></div>
                        <div class="alert-content">
                            <h4 class="alert-title">NOTICE INFO</h4>
                            <p>NE PAS RENOMMER UN DOCUMENT APRES TELECHARGEMENT AFIN DE GARANTIR UNE BONNE TRACABILITE ET LE BON FONCTIONNEMENT DU SYSTEME...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="example-box-wrapper">
            <div class="row">
                <table id="datatable-responsive"
                       class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                       width="100%">
                    <thead>
                        <tr>
                            <th>FICHIER</th>
                            <th>LIBELLE</th>
                            <th>TAILLE</th>
                            <th>EXTENSION</th>
                            <th>TYPE DOCUMENT</th>
                            <th>VERSION</th>
                            <th>DATE MISE A JOUR</th>
                        </tr>
                    </thead>

                    <tbody>
                        <c:forEach var="liste" items="${AllDOC}" varStatus="iter">
                            <tr>
                                <td>
                                    <span class="icon-notification user-profile clearfix">
                                        <img width="50" src="../admin/images/icone/${liste.icone}" alt="Profile">
                                        <a target="_blank" href="${sessionScope.DIR_ARCHIVE}/${liste.lien}" class="tooltip-button" data-placement="bottom" title="Telecharger">
                                            <i class="glyph-icon icon-download font-green font-size-20" ></i>&nbsp;&nbsp;
                                        </a>
                                        <a data-backdrop="false" data-toggle="modal" data-target=".${liste.idDoc}"  href="#" class="tooltip-button" data-placement="bottom" title="Renommer">
                                            <i class="glyph-icon icon-pencil font-red font-size-20" ></i>
                                        </a>
                                    </span>

                                </td>
                                <td>${liste.libelle}</td>
                                <td>${liste.tailledoc}</td>
                                <td>${liste.extension}</td>
                                <td>${liste.type}</td>
                                <td class='font-red'>V. ${liste.version}</td>
                                <td><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${liste.datemodif}" /><br/>
                                    <em class='font-red'>par: ${liste.modificateur}</em>
                                </td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>  
            </div>
        </div>
    </div>
</div>

<!-- renommer role -->
<c:forEach var="liste" items="${AllDOC}">
    <div class="modal fade ${liste.idDoc}" tabindex="-1" role="dialog" 
         aria-labelledby="myLargeModalLabel" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Mise A jour du Document : &nbsp;&nbsp <em class="font-blue-alt">${liste.libelle}</em></h4>
                </div>
                <form action="liste-document" enctype="multipart/form-data" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label font-black">(*pdf*word*excel*powerpoint* - 5Mo Maximum ):</label>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="../admin/images/icone/${liste.icone}" alt="" /></div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div style="display: inline-block ">
                                    <span>
                                        <span class="fileupload-new">Deposer</span>
                                        <span class="fileupload-exists">Changer</span>
                                        <input  type="file" name="fichier"  />
                                    </span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                </div>
                            </div>
                        </div>
                        <input type="text" value="${liste.libelle}" name="libelle" class="form-control" placeholder="entrer le nouveau nom ici..."  /><br/>
                        <input type="hidden" name="iddoc" value="${liste.idDoc}" class="form-control"/> 
                        <input type="hidden" name="idrep" value="${repertoire.idRep}" class="form-control"/> 

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Fermer
                        </button>
                        <button name="action" value="Renommer" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MODIFIER</span><i
                                class="glyph-icon icon-linecons-paper-plane"></i></button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</c:forEach>

<!-- ajouter profil-->
<div class="modal fade adddoc" tabindex="-1" role="dialog" 
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Entrer le nom du Document</h4>
            </div>
            <form action="liste-document" enctype="multipart/form-data" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label font-black">(*pdf*word*excel*powerpoint* - 5Mo Maximum ):</label>
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="../admin/images/icone/pdf.jpg" alt="" /></div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div style="display: inline-block ">
                                <span>
                                    <span class="fileupload-new">Deposer</span>
                                    <span class="fileupload-exists">Changer</span>
                                    <input  type="file" name="fichier"  />
                                </span>
                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                            </div>
                        </div>
                    </div>
                    <input type="text" placeholder="Entrer le libelle du document..." class="form-control" name="libelle">
                    <input type="hidden" name="idrep" value="${repertoire.idRep}" class="form-control" /> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Fermer
                    </button>
                    <button name="action" value="Ajouter" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>AJOUTER</span><i
                            class="glyph-icon icon-linecons-paper-plane"></i></button>
                </div> 
            </form>
        </div>
    </div>
</div>

