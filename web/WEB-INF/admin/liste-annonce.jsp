<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>               
<div id="page-title"><h2>GESTION ANNONCES</h2>
    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

<script>
    $(function () {
        $(".notif").show("slow").delay(5000).hide("slow");
    });
</script>
<c:if test="${val == 1}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Suppression Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 2}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Changement du Statut Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 3}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Statut de telechargement chang� avec succes!!!
        </strong>
    </div>
</c:if>
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">Liste des Annonces</h3>

        <div class="example-box-wrapper">
            <table id="datatable-responsive"
                   class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                   width="100%">
                <thead>
                    <tr>
                        <th>FICHIER</th>
                        <th>TAILLE</th>
                        <th>TITRE</th>
                        <th>STATUT</th>
                        <th>TELECHARGEABLE</th>
                        <th>ANNEE</th>
                        <th>DERNIERE MODIFICATION</th>
                        <th style="background-color:#3B5998; color:white">ACTIONS</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="listeannonce" items="${allannonce}">
                        <tr>
                            <td>
                                <span class="icon-notification user-profile clearfix">
                                    <a target="_blank" href="${sessionScope.DIR_ANNONCE}/${listeannonce.lien}" class="tooltip-button" data-placement="bottom" title="consulter" ><img width="50" src="../admin/images/icone/${listeannonce.icone}" alt="Profile"/></a>
                                </span>
                            </td>
                            <td>${listeannonce.taille}</td>
                            <td>${listeannonce.libelle}</td>
                            <td>${listeannonce.statut}</td> 
                            <td>${listeannonce.telecharger}</td>
                            <td>${listeannonce.annee}</td>
                            <td>
                                <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listeannonce.datemodif}" /><br/>
                                <em class="font-orange">Par: ${listeannonce.idCompte.idMembre.civilite} ${listeannonce.idCompte.idMembre.nom} ${listeannonce.idCompte.idMembre.prenom} </em>
                            </td>
                            <td>
                                <a data-placement="bottom" href="update-annonce?x=${listeannonce.idPub}" class="font-normal tooltip-button btn-sm btn-warning " title="Editer Laboratoire"><i  class="glyph-icon icon-edit"></i></a>&nbsp;Editer<br/>        
                                <a href="#" data-backdrop="false" data-toggle="modal" data-target=".${listeannonce.idPub}"   class="tooltip-button font-normal btn-sm btn-primary" data-placement="bottom" title="Supprimer"><i  class="glyph-icon icon-trash"></i></a>&nbsp;Supprimer<br/>
                                <a data-placement="bottom" title="Changer statut" href="liste-annonce?x=${listeannonce.idPub}&AMP;y=stat" class="font-normal tooltip-button btn-sm btn-blue-alt"><i  class="glyph-icon icon-refresh"></i></a>&nbsp;Changer statut<br/>
                                <a data-placement="bottom" title="Changer statut de Telechargement" href="liste-annonce?x=${listeannonce.idPub}&AMP;y=tel" class="font-normal tooltip-button btn-sm btn-success"><i  class="glyph-icon icon-refresh"></i></a>&nbsp;Telechargeable ?
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<c:forEach var="liste" items="${allannonce}">
    <div class="modal fade ${liste.idPub}" tabindex="-1" role="dialog" 
         aria-labelledby="myLargeModalLabel" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Voulez-vous Vraiment supprimer l'annonce : &nbsp;&nbsp <em class="font-blue-alt">${liste.libelle}</em> ?</h4>
                </div>
                <form action="liste-annonce" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="annonce" value="${liste.idPub}" class="form-control"/> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Annuler
                        </button>
                        <button name="action" value="Supprimer" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>SUPPRIMER</span><i
                                class="glyph-icon icon-linecons-paper-plane"></i></button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</c:forEach>
