<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
<div id="page-title"><h2>GESTION ARCHIVAGE</h2>
 <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>
 <script>
        $(function (){
            $(".notif").show("slow").delay(5000).hide("slow");
        });
    </script>
  <c:if test="${val == 2}">
        <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-o-up fa-3x"></i>
            Modification du <span style="text-transform: uppercase; color:brown">${updatedos}</span> Effectu� avec succes!!!
        </strong>
    </div>
    </c:if>
    <c:if test="${val == 1}">
        <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-o-up fa-3x"></i>
            Ajout du dossier <span style="text-transform: uppercase; color:brown">${ajoutdos}</span> Effectu� avec succes!!!
        </strong>
    </div>
    </c:if>
    <c:if test="${val == 3}">
        <div class="alert alert-danger notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-o-down fa-3x"></i>
            D�sole ce dossier existe dej� !!!
        </strong>
    </div>
    </c:if>
 <div class="col-md-12">
     <div class="panel">
        <div class="panel-heading" style="background-color:#BCBCBC;">            
            <center>
                <a style="color:#b72724; text-decoration: none;" href="liste-archive">
                           <i  class="glyph-icon icon-reply-all"></i><span class="font-size-20">retour</span>
                       </a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" data-backdrop="false" data-toggle="modal" data-target=".addrepertoire" style="border-radius: 0px;" class="btn btn-success btn-sm">
                    <i class="glyph-icon icon-plus"></i><strong>&nbsp;&nbsp;Cr�er un Dossier</strong>
                </a>
            </center>
        </div>
        <div class="panel-body">
            <h3 class="title-hero">
                <span class="icon-notification user-profile clearfix">
                   <img width="50" src="../admin/images/icone/${categorie.icone}" alt="Profile">
                 </span>Liste des Dossiers : ${categorie.libelle}
            </h3>
        <div class="row">
            <c:forEach var="liste" items="${ALLDOS}">
            <div class="col-md-1"  >
                <span class="icon-notification user-profile clearfix">
                    <a data-transition="pt-page-scaleUpCenter-init" href="liste-document?${liste.idRep}" class="tooltip-button add-demo-transition" data-placement="bottom" title="Voir ses documents">
                        <img width="50" src="../admin/images/icone/${liste.icone}" alt="Profile"/>                        
                    </a>
                    <a data-backdrop="false" data-toggle="modal" data-target=".${liste.idRep}"  href="#" class="tooltip-button" data-placement="bottom" title="Renommer">
                         <i class="glyph-icon icon-pencil font-red font-size-20" ></i>
                    </a>
                         <br/>
                    <strong class="font-blue-alt">${liste.libelle}</strong><br/>
                    <span>Cr�er par: ${liste.createur} </span>
                </span>
            </div>  
            </c:forEach>
        </div>
        </div>
     </div>
</div>

    <c:forEach var="liste" items="${ALLDOS}">
   <div class="modal fade ${liste.idRep}" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Renommer le Dossier : &nbsp;&nbsp <em class="font-blue-alt">${liste.libelle}</em></h4>
          </div>
          <form action="liste-dossier" method="post">
            <div class="modal-body">
                <input type="text" value="${liste.libelle}" name="libelle" class="form-control" placeholder="entrer le nouveau nom ici..."  /><br/>
                <input type="hidden" name="idrep" value="${liste.idRep}" class="form-control"/> 
                 <input type="hidden" name="idcat" value="${categorie.idCat}" class="form-control"/>                      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
                <button name="action" value="Modifier" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MODIFIER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div> 
              </form>
        </div>
      </div>
     </div>
  </c:forEach>            

  <!-- ajouter profil-->
   <div class="modal fade addrepertoire" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Entrer le nom du Dossier</h4>
          </div>
          <form action="liste-dossier" method="post">
            <div class="modal-body">
                    <input type="text" name="libelle" class="form-control"  />
                    <input type="hidden" name="idcat" value="${categorie.idCat}" class="form-control" /> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
                <button name="action" value="Ajouter" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>AJOUTER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div> 
          </form>
        </div>
      </div>
     </div>