<script type="text/javascript" src="../admin/assets/widgets/parsley/parsley.js"></script>
<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
<div id="page-title"><h2>GESTION DES MEMBRES</h2>
    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

<script>
    $(function () {
        $(".notif").show("slow").delay(7000).hide("slow");
    });
</script>
<c:if test="${val == 1}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Ajout du membre <span style="text-transform: uppercase; color:brown">${ajoutok}</span> Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 2}">
    <div class="alert alert-warning notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-down fa-3x"></i>
            <span style="text-transform: uppercase; color:brown">${errorimage}</span>
        </strong>
    </div>
</c:if>
<c:if test="${val == 4}">
    <div class="alert alert-danger notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-down fa-3x"></i>
            <span style="text-transform: uppercase; color:brown">${errortaille}</span>
        </strong>
    </div>
</c:if>
<script type="text/javascript" src="../admin/assets/widgets/tabs/tabs.js"></script>
<script type="text/javascript" src="../admin/assets/widgets/tabs/tabs-responsive.js"></script>
<script type="text/javascript">/* Responsive tabs */
    $(function () {
        "use strict";
        $('.nav-responsive').tabdrop();
    });</script>
<div class="panel">
    <div class="panel-body">

        <div class="example-box-wrapper">
            <ul class="nav-responsive nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab">IMPORTATION</a></li>
                <li><a href="#tab2" data-toggle="tab">ENREGISTREMENT</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <form class="form-horizontal bordered-row" method="POST" action="ajouter-membre" enctype="multipart/form-data" id="demo-form" data-parsley-validate="">
                        <div class="row">

                            <div class="col-md-5 bg-gray-alt">
                                <div class="form-group">
                                    <p>Importer un fichier Excel <span style="color:red">(.XLS)</span> <br/>Entete Recommand�e <i class="glyph-icon icon-arrow-right"></i> <em style="color:red">Civilite | Nom | Prenom | Fonction | Tutelle | Email | Adresse | Telephone </em> </p>
                                        
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label font-black">Fichier Excel(.XLS)</label>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="../admin/images/icone/pdf.jpg" alt="" /></div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div style="display: inline-block ">
                                            <span>
                                                <span class="fileupload-new">Deposer</span>
                                                <span class="fileupload-exists">Changer</span>
                                                <input  type="file" name="file"  />
                                            </span>
                                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-1"></div>

                            <div class="col-md-5 bg-gray-alt">
                                <div class="form-group"><label class="col-sm-4 control-label font-black">Choisir Laboratoire :</label>

                                    <div class="col-sm-8">
                                        <select required name="laboratoire" class="custom-select">
                                            <option></option>
                                            <c:forEach var="liste" items="${listelabo}">
                                                <option value="${liste.idLabo}">Labo - ${liste.sigle}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                            </div>


                        </div>
                        <div class="bg-default content-box text-center pad20A mrg25T">
                            <button name="action" value="Importer" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>VALIDER IMPORTATION</span><i
                                    class="glyph-icon icon-linecons-paper-plane"></i></button>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="tab2">
                    <form class="form-horizontal bordered-row" method="POST" action="ajouter-membre" enctype="multipart/form-data" id="demo-form" data-parsley-validate="">
                        <div class="row">

                            <div class="col-md-5 bg-gray-alt">
                                <div class="form-group"></div>
                                <div class="form-group"><label class="col-sm-3 control-label font-black">Photo (Jpg, Png, Gif) :</label>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="../admin/images/avatar/user.png" alt="" /></div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div style="display: inline-block ">
                                            <span>
                                                <span class="fileupload-new">Choisir</span>
                                                <span class="fileupload-exists">Changer</span>
                                                <input  type="file" name="image"  />
                                            </span>
                                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group"><label class="col-sm-4 control-label font-black">Civilite:</label>

                                    <div class="col-sm-8"><select required name="civilite" class="custom-select">
                                            <option>Mr.</option>
                                            <option>Mme.</option>
                                            <option>Mlle.</option>
                                            <option>Dr.</option>
                                            <option>Pr.</option>
                                        </select></div>
                                </div>
                                <div class="form-group"><label class="col-sm-4 control-label font-black">Nom : </label>

                                    <div class="col-sm-8"><input type="text" placeholder="Entrer Nom"
                                                                 required class="form-control" name="nom"></div>
                                </div>
                                <div class="form-group"><label class="col-sm-4 control-label font-black">Prenom : </label>

                                    <div class="col-sm-8"><input type="text" placeholder="Entrer Prenom"
                                                                 required class="form-control" name="prenom"></div>
                                </div>


                            </div>

                            <div class="col-md-1"></div>

                            <div class="col-md-5 bg-gray-alt">
                                <div class="form-group"></div>
                                <div class="form-group"><label class="col-sm-4 control-label font-black">Fonction
                                        : </label>

                                    <div class="col-sm-8"><input type="text" placeholder="Entrer Fonction"
                                                                 class="form-control" name="fonction"></div>
                                </div>
                                <div class="form-group"><label class="col-sm-4 control-label font-black">Tutelle : </label>

                                    <div class="col-sm-8"><input type="text" placeholder="Entrer Tutelle"
                                                                 class="form-control" name="tutelle"></div>
                                </div>
                                <div class="form-group"><label class="col-sm-4 control-label font-black">Email</label>

                                    <div class="col-sm-8"><input type="text" data-parsley-type="email" name="email" placeholder="Entrer son Email"
                                                                 class="form-control"></div>
                                </div>

                                <div class="form-group"><label class="col-sm-4 control-label font-black">Adresse : </label>

                                    <div class="col-sm-8"><input type="text" placeholder="Entrer Adresse"
                                                                 class="form-control" name="adresse"></div>
                                </div>
                                <div class="form-group"><label class="col-sm-4 control-label font-black">Telephone
                                        : </label>

                                    <div class="col-sm-8"><input type="text" name="telephone" placeholder="Entrer Telephone"
                                                                 data-inputmask="&apos;mask&apos;:&apos;(237) 699-99-99-99&apos;"  class="input-mask form-control"></div>
                                </div>
                                <div class="form-group"><label class="col-sm-4 control-label font-black">Laboratoire :</label>

                                    <div class="col-sm-8">
                                        <select required name="laboratoire" class="custom-select">
                                            <option></option>
                                            <c:forEach var="liste" items="${listelabo}">
                                                <option value="${liste.idLabo}">Labo - ${liste.sigle}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                            </div>


                        </div>
                        <div class="bg-default content-box text-center pad20A mrg25T">
                            <button name="action" value="Ajouter" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>VALIDER</span><i
                                    class="glyph-icon icon-linecons-paper-plane"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




