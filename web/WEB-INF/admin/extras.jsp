<link rel="stylesheet" type="text/css" href="../plugins-login/tinymce.css"/>
<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
 <script type="text/javascript" src="../plugins-login/tinymce3/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">�
		//po ur�ti nyM C E!!�
		// O2k7 skin (silver)
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "editor",
		theme : "advanced",
		skin : "o2k7",
		skin_variant : "black",
		relative_urls:false,
		
		file_browser_callback:"fileBrowser",
		
		language:"en",
		plugins : "lists,pagebreak,style,layer,advhr,advimage,advlink,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "newdocument,cut,code,|,bold,italic,underline,strikethrough,|,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,help,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,bullist,numlist,pasteword",
		theme_advanced_buttons3 : "print,|,fullscreen,|,moveforward,movebackward,|,styleprops,|,cite,abbr,acronym,del,ins,|,hr,removeformat,visualaid,|,charmap,search",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
	/* fiel_name=nom du champ
	 * url= le lien
	 * type= le type
	 * win= la fenetre
	 */
	function fileBrowser(field_name, url, type, win)
	{
		tinyMCE.activeEditor.windowManager.open({
			file:"galerie-presentation",
			title:"GALLERIE PAGE A-PROPOS",			/*le titre*/
			width:1100,					/*la largeur de la fenetre*/
			height:800,					/*la hauteur de la fenetre*/
			resizable:true,				/* redimentionnable ??*/
			inline:true,				/* faire fonctionner le fenetre en popups ??*/
			close_previous: false /* est ce kon ferme la fenetre precedente ??*/
		},
		{  /*ici je recupere la fenetre et l'input qui est li�*/
			window:win,
			input:field_name,
		});
		return false;  
	}
	
	</script> 
        
        
                <div id="page-title"><h2>GESTION DES EXTRAS</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

                    <script>
                        $(function (){
                            $(".notif").show("slow").delay(5000).hide("slow");
                        });
                    </script>
                    <c:if test="${val == 1}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Mise � jour de la page A-PROPOS Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 2}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Mise � jour du plugin Facebook Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 3}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Mise � jour su plugin Google Map Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
             <script type="text/javascript" src="../admin/assets/widgets/accordion-ui/accordion.js"></script>
                <script type="text/javascript">/* jQuery UI Accordion */

                $(function () {
                    "use strict";
                    $(".accordion").accordion({
                        heightStyle: "content"
                    });
                });

                $(function () {
                    "use strict";
                    $("#accordion-hover")
                            .accordion({
                                event: 'mouseover',
                                heightStyle: 'auto'
                            });
                });</script>
                <div class="panel">
                    <div class="panel-body"><h3 class="title-hero">EXTRAS</h3>

                        <div class="example-box-wrapper bg-gray-alt">
                            <form class="form-horizontal bordered-row " method="POST" action="extras" enctype="multipart/form-data" id="demo-form" data-parsley-validate="">
                              <div class="accordion">
				<h4>Page A-propos</h4>
                                <div class="row">

                                    <div class="col-md-12 ">
                                        <div class="form-group"></div>
                                    
                                        <div class="form-group"><label class="col-sm-3 control-label font-black">TITRE : </label>

                                            <div class="col-sm-6"><input type="text" placeholder="Entrer le titre"
                                                                         required class="form-control" value="${Apropos.libelle}" name="libelle"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label font-black">Contenu : </label>

                                            <div class="col-sm-6">
                                                <textarea  id="editor" rows="10"   name="contenu" class="form-control textarea-autosize tinyMCE" required>
                                                    ${Apropos.contenu}
                                                </textarea>
                                            </div>
                                        </div>
               
                                    </div>

                                    <div class="bg-default text-center pad20A ">
                                         <button name="action" value="apropos" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MISE A JOUR</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
                                    </div>
                                </div>
                                                
                                <h4>Plugin Facebook</h4>
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group"></div>
                                        <div class="form-group">
                                            <textarea  id="editor" rows="10"   name="contenu2" class="form-control textarea-autosize" >
                                                ${facebook.contenu}
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="bg-default text-center pad20A ">
                                         <button name="action" value="facebook" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MISE A JOUR</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
                                    </div>
                                </div>
                                
                                <h4>Plugin Google Maps</h4>
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group"></div>
                                        <div class="form-group">
                                            <textarea  id="editor" rows="10"   name="contenu3" class="form-control textarea-autosize" >
                                                ${google.contenu}
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="bg-default text-center pad20A ">
                                         <button name="action" value="googlemap" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MISE A JOUR</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>   
           
                    

