             <link href="../plugins-login/font-awesome.css" rel="stylesheet"/>  
                <div id="page-title"><h2>GESTION LABORATOIRES</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

                    <script>
                        $(function (){
                            $(".notif").show("slow").delay(5000).hide("slow");
                        });
                    </script>
                    <c:if test="${val == 1}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Modification du Labo Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                    
                <div class="panel">
                    <div class="panel-body">
                        <h3 class="title-hero">Liste des Laboratoires</h3>

                        <div class="example-box-wrapper">
                            <table id="datatable-responsive"
                                   class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>IMAGE</th>
                                    <th>STATUT</th>
                                    <th>SIGLE</th>
                                    <th>LIBELLE</th>
                                    <th>DATE MODIFICATION</th>
                                    <th style="background-color:#3B5998; color:white">ACTIONS</th>
                                </tr>
                                </thead>

                                <tbody>
                                <c:forEach var="listelabo" items="${alllabo}">
                                <tr>
                                    <td>
                                        <span class="icon-notification user-profile clearfix">
                                            <img width="50" src="${sessionScope.DIR_LABO}/${listelabo.image}" alt="Profile">
                                        </span>
                                    </td>
                                    <td style="color:#b72724;">${listelabo.statut}</td>
                                    <td>${listelabo.sigle}</td>
                                    <td>${listelabo.libelle}</td>
                                    <td>
                                        <fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listelabo.datecreate}" /><br/>
                                        <em class="font-orange">Par: ${listelabo.createur}</em>
                                    </td>
                                    <td>
                                        <a data-placement="bottom" href="update-laboratoire?x=${listelabo.idLabo}" class="font-normal tooltip-button btn-sm btn-warning " title="Editer Laboratoire"><i  class="glyph-icon icon-edit"></i></a>&nbsp;Editer<br/>
                                       <a data-placement="bottom" href="laboratoire-membre?x=${listelabo.idLabo}" class="font-normal tooltip-button btn-sm btn-success" title="Liste des Membres du Labo"><i  class="glyph-icon icon-group"></i></a>&nbsp;Membres<br/>
                                       <a data-placement="bottom" href="gerer-thematique?l=${listelabo.idLabo}" class="font-normal tooltip-button btn-sm btn-black" title="Liste des Thematiques du Labo"><i  class="glyph-icon icon-bars"></i></a>&nbsp;Thematiques<br/>
                                       <a data-placement="bottom" href="liste-laboratoire?x=${listelabo.idLabo}"  class="tooltip-button font-normal btn-sm btn-blue-alt"  title="Changer Statut"><i class="glyph-icon icon-refresh"></i></a>&nbsp;Changer Statut
                                     
                                    </td>
                                </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                    
 