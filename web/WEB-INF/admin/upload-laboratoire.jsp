<%-- 
    Document   : upload-laboratoire
    Created on : 30 mai 2016, 18:39:38
    Author     : HP
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <script src="../plugins-login/jquery.min.js"></script>
         <script src="../plugins-login/bootstrap.min.js"></script> 
         <link rel="stylesheet" href="../plugins-login/bootstrap-fileupload.min.css" />
        <script src="../plugins-login/bootstrap-fileupload.js"></script>
        <link rel="stylesheet" type="text/css" href="../plugins-login/bootstrap.css"/>
    </head>
    <body>
        <script>
            $(function (){
                $(".notif").show("slow").delay(5000).hide("slow");
            });
        </script>
            <c:if test="${val == 2}">
                <div class="alert alert-danger notif">
                <strong style="margin-left:300px" ><i class="fa fa-frown-o fa-3x"></i>
                    <span style="text-transform: uppercase; color:brown">${errorimage}</span>
                </strong>
                </div>
            </c:if>
         <c:if test="${val == 4}">
                <div class="alert alert-danger notif">
                <strong style="margin-left:300px" ><i class="fa fa-frown-o fa-3x"></i>
                    <span style="text-transform: uppercase; color:brown">${errortaille}</span>
                </strong>
                </div>
            </c:if>
        <form  method="POST" action="upload-laboratoire" enctype="multipart/form-data" style="width:50%; margin: auto">
	
               <div class="form-group" style=" display:block; ">
                      <label><strong style="font-size: large">Description</strong></label>
                      <input type="text" class="form-control" name="description" style="width:50%" required />
                        
               </div> 
		<div class="form-group" style=" display:block; ">
				 <label class="control-label col-lg-4">Uniquement les images au format (JPG, PNG, GIF)</label>
                        <div class="col-lg-8">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="../admin/images/avatar/user.png" alt="" /></div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                    <span class="btn btn-file btn-primary">
                                        <span class="fileupload-new">Choisir l'image</span>
                                        <span class="fileupload-exists">Changer</span>
                                        <input required type="file" name="image"/></span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                </div>
                            </div>
                            <br/>
				             <br />
				             <br />
                        <input type="submit" value="ENREGISTRER" class="btn btn-success" />
                        </div>
               </div>		
	</form>
    </body>
</html>
