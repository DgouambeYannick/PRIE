
                <div id="page-title"><h2>GESTION DES MEMBRES</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

              
                <div class="panel">
                    <div class="panel-body"><h3 class="title-hero">Liste des membres</h3>

                        <div class="example-box-wrapper">
                            <table id="datatable-responsive"
                                   class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>PHOTO</th>
                                    <th>NOM-PRENOM</th>
                                    <th>ADRESSE</th>
                                    <th>TELEPHONE</th>
                                    <th>FONCTION</th>
                                    <th>RESPONSABLE</th>
                                    <th>LABORATOIRE</th>
                                    <th>DERNIERE MODIFICATION</th>
                                </tr>
                                </thead>

                                <tbody>
                                    <c:forEach var="listeM" items="${allmembre}">
                                <tr>
                                    <td>
                                        <span class="icon-notification user-profile clearfix">
                                            <a data-placement="bottom" title="Details" class="tooltip-button" href="edit-profil?${listeM.idMembre}"><img width="50" src="${sessionScope.DIR_AVATAR}/${listeM.image}" alt="Profile"/></a>
                                        </span>
                                    </td>
                                    <td><a data-placement="bottom" title="Details" class="tooltip-button" href="edit-profil?${listeM.idMembre}">${listeM.civilite} ${listeM.nom} ${listeM.prenom}</a></td>
                                    <td>${listeM.adresse}</td>
                                    <td>${listeM.phone}</td>
                                    <td>${listeM.fonction}</td>
                                    <td style="color:#b72724;">${listeM.responsable}</td>
                                    <td>${listeM.idLabo.libelle} (${listeM.idLabo.sigle})</td>
                                    <td><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listeM.datecreate}" /></td>
                                    
                                </tr>
                               </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
