<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
                <div id="page-title"><h2>GESTION ARCHIVAGE</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

              <script>
                        $(function (){
                            $(".notif").show("slow").delay(5000).hide("slow");
                        });
                    </script>
                    <c:if test="${val == 2}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-o-up fa-3x"></i>
                            Modification de la cat�gorie <span style="text-transform: uppercase; color:brown">${updatemodule}</span> Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 1}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-o-up fa-3x"></i>
                            Ajout de la cat�gorie <span style="text-transform: uppercase; color:brown">${ajoutmodule}</span> Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                   
                <div class="panel">
                    <div class="panel-heading" style="background-color:#BCBCBC;">          
                        <center>
                            <a href="#" data-backdrop="false" data-toggle="modal" data-target=".addmodule" style="border-radius: 0px;" class="btn btn-success btn-sm">
                                <i class="glyph-icon icon-plus"></i><strong>&nbsp;&nbsp;Ajouter une cat�gorie d'archives</strong>
                            </a>
                        </center>
                    </div>
                    <div class="panel-body">
                        <h3 class="title-hero">Liste des Cat�gories d'Archives</h3>

                        <div class="example-box-wrapper">
                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>LIBELLE</th>
                                    <th>CREE PAR</th>
                                    <th>DERNIERE MODIFICATION</th>
                                    <th style="background-color:#3B5998; color:white">ACTIONS</th>
                                </tr>
                                </thead>

                                <tbody>
                                <c:forEach var="listecat" items="${listecategorie}">
                                <tr>
                                    <td>${listecat.libelle}</td>
                                    <td><i class="glyph-icon icon-user"></i> ${listecat.createur}</td>
                                    <td><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listecat.datecreate}" /></td>
                                    <td>
                                        <a data-placement="bottom" title="Renommer"  class="tooltip-button font-normal btn-sm btn-primary" href="#" data-backdrop="false" data-toggle="modal" data-target=".${listecat.idCat}" ><i  class="glyph-icon icon-edit"></i></a>&nbsp;Renommer
                                    </td>
                                </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
  <!-- renommer role -->
  <c:forEach var="listecat" items="${listecategorie}">
   <div class="modal fade ${listecat.idCat}" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Renommer la Categorie : &nbsp;&nbsp <em class="font-blue-alt">${listecat.libelle}</em></h4>
          </div>
          <form action="ajouter-categorie" method="post">
            <div class="modal-body">
                <input type="text" value="${listecat.libelle}" name="libelle" class="form-control" placeholder="entrer le nouveau libelle ici..."  /><br/>
                <input type="hidden" name="idCat" value="${listecat.idCat}" class="form-control"/>
                <input type="hidden" name="login" value="${listecat.createur}" class="form-control"/>
                                      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
                <button name="action" value="Modifier" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MODIFIER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div> 
              </form>
        </div>
      </div>
     </div>
  </c:forEach>
  
  <!-- ajouter profil-->
   <div class="modal fade addmodule" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Entrez une nouvelle Categorie D'archivage</h4>
          </div>
          <form action="ajouter-categorie" method="post">
            <div class="modal-body">
                    <input type="text" name="libelle" class="form-control"  />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
                <button name="action" value="Ajouter" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>AJOUTER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div> 
          </form>
        </div>
      </div>
     </div>