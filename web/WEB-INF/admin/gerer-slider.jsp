<link rel="stylesheet" type="text/css" href="../plugins-login/tinymce.css"/>
<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
 <script type="text/javascript" src="../plugins-login/tinymce3/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">�
			//po ur�ti nyM C E!!�
		// O2k7 skin (silver)
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "slider",
		theme : "advanced",
		skin : "o2k7",
		skin_variant : "black",
		relative_urls:false,
		
		file_browser_callback:"fileBrowser",
		
		language:"en",
		plugins : "lists,pagebreak,style,layer,advhr,advlink,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "newdocument,cut,code,undo,redo,search|,bold,italic,underline,|,justifyfull,formatselect,fontselect,fontsizeselect,forecolor,backcolor,del,ins,link,unlink,styleprops",
		theme_advanced_buttons2 : "insertdate,inserttime,preview,pasteword,print,fullscreen,|,hr,removeformat,visualaid",
		
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});

	
	</script> 
                <div id="page-title"><h2>GESTION SLIDER</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

              <script>
                        $(function (){
                            $(".notif").show("slow").delay(5000).hide("slow");
                        });
                    </script>
                    <c:if test="${val == 1}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Ajout Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                     <c:if test="${val == 2}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Suppression Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 3}">
                        <div class="alert alert-danger notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-down fa-3x"></i>
                          <span style="text-transform: uppercase; color:brown">${errorfile}</span>
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 4}">
                        <div class="alert alert-danger notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-down fa-3x"></i>
                          <span style="text-transform: uppercase; color:brown">${errortaille}</span>
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 5}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                          <span style="text-transform: uppercase; color:brown">Modification Effectu�e avec success</span>
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 6}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                          <span style="text-transform: uppercase; color:brown">Statut chang� avec success</span>
                        </strong>
                    </div>
                    </c:if>
                <div class="panel">
                    <div class="panel-heading" style="background-color:#BCBCBC;">          
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="#" data-backdrop="false" data-toggle="modal" data-target=".addslider" style="border-radius: 0px;" class="btn btn-success btn-sm">
                            <i class="glyph-icon icon-plus"></i><strong>&nbsp;&nbsp;Ajouter une Image</strong>
                        </a>
                    </div>
                    <div class="panel-body">
                        <h3 class="title-hero">Liste des Images du slider</h3>

                        <div class="example-box-wrapper">
                            <table id="datatable-responsive"
                                   class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>IMAGE</th>
                                    <th>EXTENSION</th>
                                    <th>TAILLE</th>
                                    <th>DESCRIPTION</th>
                                    <th>STATUT</th>
                                    <th>DERNIERE MODIFICATION</th>
                                    <th style="background-color:#3B5998; color:white">ACTIONS</th>
                                </tr>
                                </thead>

                                <tbody>
                                    <c:forEach var="liste" items="${allslider}">
                                <tr>
                                    <td>
                                        <span class="icon-notification user-profile clearfix">
                                            <a target="_blank" href="${sessionScope.DIR_SLIDER}/${liste.lien}" class="tooltip-button" data-placement="bottom" title="consulter"> <img width="120" src="${sessionScope.DIR_SLIDER}/${liste.lien}" alt="Profile"></a>
                                        </span>
                                    </td>
                                    <td>${liste.extension}</td>
                                    <td>${liste.taille}</td>
                                    <td>${liste.description}</td>
                                    <td style="color:#b72724;">${liste.statut}</td>
                                    <td><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${liste.datecreate}" /></td>
                                    <td>
                                        <a href="#" data-backdrop="false" data-toggle="modal" data-target=".${liste.idImage+'9'}"  class="tooltip-button font-normal btn-sm btn-primary" data-placement="bottom" title="Supprimer"><i  class="glyph-icon icon-trash"></i></a>&nbsp;Supprimer<br/>
                                        <a href="#" data-backdrop="false" data-toggle="modal" data-target=".${liste.idImage}" class="tooltip-button font-normal btn-sm btn-warning" data-placement="bottom" title="Modifier"><i  class="glyph-icon icon-edit"></i></a>&nbsp;Modifier<br/>
                                        <a href="gerer-slider?${liste.idImage}"  class="tooltip-button font-normal btn-sm btn-blue-alt" data-placement="bottom" title="Changer Statut"><i  class="glyph-icon icon-refresh"></i></a>&nbsp;Changer Statut
                                     
                                    </td>
                                </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        
<script type="text/javascript" src="../admin/assets/widgets/parsley/parsley.js"></script>


<div class="modal fade addslider" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <h4 class="modal-title">AJOUTER UNE IMAGE</h4></div>
           <form method="post" action="gerer-slider" enctype="multipart/form-data" data-parsley-validate="">
                <div class="modal-body">
                <div class="row" style="border:0px solid red">
                        <div class="col-lg-12" style="border:0px solid black">
                <div class="panel panel-default" style="border:0px solid red">

                    <div class="panel-body" style="border:0px solid blue; padding: 0px;">
                        
                        <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group"><label class="col-sm-3 control-label font-black">Image (Jpg, Png) :</label>
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail" style="width: 600px; height: 150px;"><img src="../admin/images/slider/slider.png" alt="" /></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                <div style="display: inline-block ">
					                                    <span>
					                                    	<span class="fileupload-new">Choisir</span>
					                                    	<span class="fileupload-exists">Changer</span>
					                                    	<input required type="file" name="image"  />
					                                    </span>
                                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label font-black">DESCRIPTION : </label>

                                            <div class="col-sm-6">
                                                <textarea rows="10" cols="80" id="slider" name="description" class="form-control textarea-autosize">
                                              
                                               </textarea>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                        
                    </div>
                </div>
            </div>
           </div>
           </div>
           <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
                <button name="action" value="Ajouter"  type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>AJOUTER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div>      
           </form>
            
        </div>
    </div>
</div>

<c:forEach var="liste" items="${allslider}">
<div class="modal fade ${liste.idImage}" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <h4 class="modal-title">MODIFICATION DE L'IMAGE</h4></div>
           <form method="post" action="gerer-slider" enctype="multipart/form-data" data-parsley-validate="">
                <div class="modal-body">
                <div class="row" style="border:0px solid red">
                        <div class="col-lg-12" style="border:0px solid black">
                <div class="panel panel-default" style="border:0px solid red">

                    <div class="panel-body" style="border:0px solid blue; padding: 0px;">
                        
                        <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group"><label class="col-sm-3 control-label font-black">Image (Jpg, Png) :</label>
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="${sessionScope.DIR_SLIDER}/${liste.lien}" alt="" /></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                <div style="display: inline-block ">
					                                    <span>
					                                    	<span class="fileupload-new">Choisir</span>
					                                    	<span class="fileupload-exists">Changer</span>
                                                                                <input type="file" name="image"  />
					                                    </span>
                                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Retirer</a>
                                                </div>
                                            </div>
                                        </div>
                                                <input type="hidden" value="${liste.idImage}" name="slider"/>
                                        <div class="form-group"><label class="col-sm-3 control-label font-black">DESCRIPTION : </label>

                                           <div class="col-sm-6">
                                               
                                                <textarea rows="10" cols="80" id="slider2" name="description" class="form-control textarea-autosize">
                                                ${liste.description}
                                               </textarea>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                        
                    </div>
                </div>
            </div>
           </div>
           </div>
           <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
                <button name="action" value='Modifier'  type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>Modifier</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div>      
           </form>
            
        </div>
    </div>
</div>
</c:forEach>

<c:forEach var="liste" items="${allslider}">
    <div class="modal fade ${liste.idImage+'9'}" tabindex="-1" role="dialog" 
         aria-labelledby="myLargeModalLabel" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Voulez-vous Vraiment supprimer cet image</h4>
                    <div class="form-group">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="${sessionScope.DIR_SLIDER}/${liste.lien}" alt="" /></div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            
                        </div>
                    </div>
                </div>
                <form action="gerer-slider" method="post">
                    <div class="modal-body">
                        
                        <input type="hidden" name="slider" value="${liste.idImage}" class="form-control"/> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Annuler
                        </button>
                        <button name="action" value="Supprimer" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>SUPPRIMER</span><i
                                class="glyph-icon icon-linecons-paper-plane"></i></button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</c:forEach>

<script type="text/javascript">�
			//po ur�ti nyM C E!!�
		// O2k7 skin (silver)
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "slider2",
		theme : "advanced",
		skin : "o2k7",
		skin_variant : "black",
		relative_urls:false,
		
		file_browser_callback:"fileBrowser",
		
		language:"en",
		plugins : "lists,pagebreak,style,layer,advhr,advlink,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "newdocument,cut,code,undo,redo,search|,bold,italic,underline,|,justifyfull,formatselect,fontselect,fontsizeselect,forecolor,backcolor,del,ins,link,unlink,styleprops",
		theme_advanced_buttons2 : "insertdate,inserttime,preview,pasteword,print,fullscreen,|,hr,removeformat,visualaid",
		
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});

	
	</script> 


