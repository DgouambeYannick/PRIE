
                <div id="page-title"><h2>GESTION DES COMPTES</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>
    
                <div class="panel">
                    <div class="panel-heading" style="background-color:#BCBCBC;">          
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <a style="color:#b72724; text-decoration: none;" href="gerer-compte">
                           <i  class="glyph-icon icon-reply-all"></i><span class="font-size-20">retour</span>
                       </a>
                    </div>
                    <div class="panel-body"><h3 class="title-hero">LISTE DES MEMBRES SANS COMPTE</h3>

                        <div class="example-box-wrapper">
                            <table id="datatable-responsive"
                                   class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>PHOTO</th>
                                    <th>NOM-PRENOM</th>
                                    <th>FONCTION</th>
                                    <th>RESPONSABLE</th>
                                    <th>LABORATOIRE</th>
                                    <th>DATE AJOUT</th>
                                    <th style="background-color:#3B5998; color:white">ACTIONS</th>
                                </tr>
                                </thead>

                                <tbody>
                                    <c:forEach var="listeM" items="${allmembre}">
                                <tr>
                                    <td>
                                        <span class="icon-notification user-profile clearfix">
                                            <img width="50" src="${sessionScope.DIR_AVATAR}/${listeM.image}" alt="Profile">
                                        </span>
                                    </td>
                                    <td>${listeM.civilite} ${listeM.nom} ${listeM.prenom}</td>
                                    <td>${listeM.fonction}</td>
                                    <td>${listeM.responsable}</td>
                                    <td>${listeM.idLabo.libelle} (${listeM.idLabo.sigle})</td>
                                    <td><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listeM.datecreate}" />
                                        </td>
                                    <td>
                                        <a data-placement="bottom" title="Attribuer un compte" href="#" data-backdrop="false" data-toggle="modal" data-target=".${listeM.idMembre}" class="font-normal tooltip-button"><i style="color:#b72724;" class="glyph-icon icon-key"></i>&nbsp;Droits</a>
                                    </td>
                                </tr>
                               </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


<!-- CREATION DU COMPTE  -->
<c:forEach var="listeM" items="${allmembre}">
<div class="modal fade ${listeM.idMembre}" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <h4 class="modal-title">CREATION DU COMPTE DE
                    <em class="font-blue-alt">${listeM.civilite} ${listeM.nom} ${listeM.prenom}</em></h4></div>
           <form method="post" action="gerer-compte" data-parsley-validate="">
                <div class="modal-body">
                <div class="row" style="border:0px solid red">
                        <div class="col-lg-12" style="border:0px solid black">
                <div class="panel panel-default" style="border:0px solid red">

                    <div class="panel-body" style="border:0px solid blue; padding: 0px;">
                        <div class="table-responsive" style="border:0px solid green">

                             <table class="table table-striped table-bordered" width="100%">
                                <tbody>

                                    <tr>
                                      <td align="right">Login</td>  
                                      <td align="left"><input name="login" type="text" required class="form-control" ></td>   
                                      <td align="right">Profil</td>  
                                      <td align="left">
                                            <select required name="profil" class="custom-select">
                                               
                                            <c:forEach var="liste" items="${allprofil}">
                                                <option value="${liste.idProfil}">${liste.libelle}</option>
                                            </c:forEach>
                                            </select>
                                      </td>  
                                    </tr>
                                     <tr>
                                      <td align="right">Mot de passe</td>  
                                      <td align="left"><input type="password" id="ps1" name="password" required class="form-control"></td>   
                                      <td align="right"></td>  
                                      <td align="left"></td>  
                                    </tr>
                                    <tr>
                                      <td align="right">Confirmer le Mot de passe</td>  
                                      <td align="left"><input type="password" data-parsley-equalto="#ps1" required class="form-control"></td>   
                                      <td align="right"></td>  
                                      <td align="left"></td>  
                                    </tr>
                                    <tr>
                                      <td align="right"></td>  
                                      <td align="left"><input type="hidden" name="membre" value="${listeM.idMembre}" class="form-control"></td>   
                                      <td align="right"></td>  
                                      <td align="left"></td>  
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
           </div>
           </div>
           <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
                <button name="action" value="Ajouter" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>AJOUTER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div>      
           </form>
            
        </div>
    </div>
</div>
</c:forEach>