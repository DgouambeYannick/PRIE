<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
                <div id="page-title"><h2>GESTION DES ROLES</h2>
                    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

              <script>
                        $(function (){
                            $(".notif").show("slow").delay(5000).hide("slow");
                        });
                    </script>
                    <c:if test="${val == 2}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Modification du role en <span style="text-transform: uppercase; color:brown">${updateprofil}</span> Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                    <c:if test="${val == 1}">
                        <div class="alert alert-success notif">
                        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
                            Ajout du role <span style="text-transform: uppercase; color:brown">${ajoutprofil}</span> Effectu� avec succes!!!
                        </strong>
                    </div>
                    </c:if>
                <div class="panel">
                    <div class="panel-heading" style="background-color:#BCBCBC;">          
                        <center>
                            <a href="#" data-backdrop="false" data-toggle="modal" data-target=".addrole" style="border-radius: 0px;" class="btn btn-success btn-sm">
                                <i class="glyph-icon icon-plus"></i><strong>&nbsp;&nbsp;Cr�er un Role</strong>
                            </a>
                        </center>
                    </div>
                    <div class="panel-body">
                        <h3 class="title-hero">Liste des Role</h3>

                        <div class="example-box-wrapper">
                        <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>ROLE</th>
                                    <th>DERNIERE MODIFICATION</th>
                                    <th style="background-color:#3B5998; color:white">ACTIONS</th>
                                </tr>
                                </thead>

                                <tbody>
                                <c:forEach var="listeprofil" items="${allprofil}">
                                <tr>
                                    <td>${listeprofil.libelle}</td>
                                    <td><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${listeprofil.datecreate}" /></td>
                                    <td>
                                        <a data-placement="bottom" title="Renommer Role" href="#" data-backdrop="false" data-toggle="modal" data-target=".${listeprofil.idProfil}" class="font-normal tooltip-button btn-sm btn-blue-alt"><i class="glyph-icon icon-refresh"></i></a>&nbsp;Renommer<br/>
                                        <a data-placement="bottom" title="Editer Role" href="profil-btn-menu?r=${listeprofil.idProfil}"  class="font-normal tooltip-button btn-sm btn-warning"><i  class="glyph-icon icon-edit"></i></a>&nbsp;Editer
                                    </td>
                                </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                    
  <!-- renommer role -->
  <c:forEach var="listeprofil" items="${allprofil}">
   <div class="modal fade ${listeprofil.idProfil}" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Renommer le role : &nbsp;&nbsp <em class="font-blue-alt">${listeprofil.libelle}</em></h4>
          </div>
          <form action="gerer-role" method="post">
            <div class="modal-body">
                <input type="text" value="${listeprofil.libelle}" name="profil" class="form-control" placeholder="entrer le nouveau nom ici..."  /><br/>
                <input type="hidden" name="idprofil" value="${listeprofil.idProfil}" class="form-control"/> 
                                      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
                <button name="action" value="Modifier" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>MODIFIER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div> 
              </form>
        </div>
      </div>
     </div>
  </c:forEach>
  
  <!-- ajouter profil-->
 <form action="gerer-role" method="post">
   <div class="modal fade addrole" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Entrez le nom du nouveau profil</h4>
          </div>
         
            <div class="modal-body">
                    <input type="text" name="profil" class="form-control"  />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Fermer
                </button>
                <a href="#" data-backdrop="false" data-toggle="modal" data-target=".role" class="btn btn-lg btn-gray-alt font-normal">
                    <span>SUIVANT</span>
                </a>
            </div> 
        </div>
      </div>
     </div>
  
     <div class="modal fade role" tabindex="-1" role="dialog" 
                aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Selecionner les Menus de ce Profil</h4>
          </div>
         
            <div class="modal-body">
                    <select multiple="multiple" class="multi-select" name="menu">
                        <c:forEach var="allmenu" items="${allmenu}" >
                             <option value="${allmenu.idMenu}">${allmenu.libelle}</option>
                         </c:forEach>
                    </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    PRECEDENT
                </button>
                <button name="action" value="Ajouter" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>AJOUTER</span><i
                                            class="glyph-icon icon-linecons-paper-plane"></i></button>
            </div> 
         
        </div>
      </div>
     </div>
   </form>