<link rel="stylesheet" type="text/css" href="../plugins-login/tinymce.css"/>
<link href="../plugins-login/font-awesome.css" rel="stylesheet"/>
<script type="text/javascript" src="../plugins-login/tinymce3/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">�
		//po ur�ti nyM C E!!�
		// O2k7 skin (silver)
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "theme",
		theme : "advanced",
		skin : "o2k7",
		skin_variant : "black",
		relative_urls:false,
		
		file_browser_callback:"fileBrowser",
		
		language:"en",
		plugins : "lists,pagebreak,style,layer,advhr,advimage,advlink,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "newdocument,cut,code,|,bold,italic,underline,strikethrough,|,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,help,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,bullist,numlist,pasteword",
		theme_advanced_buttons3 : "print,|,fullscreen,|,moveforward,movebackward,|,styleprops,|,cite,abbr,acronym,del,ins,|,hr,removeformat,visualaid,|,charmap,search",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
	/* fiel_name=nom du champ
	 * url= le lien
	 * type= le type
	 * win= la fenetre
	 */
	function fileBrowser(field_name, url, type, win)
	{
		tinyMCE.activeEditor.windowManager.open({
			file:"galerie-laboratoire",
			title:"GALLERIE LABORATOIRE",			/*le titre*/
			width:1100,					/*la largeur de la fenetre*/
			height:800,					/*la hauteur de la fenetre*/
			resizable:true,				/* redimentionnable ??*/
			inline:true,				/* faire fonctionner le fenetre en popups ??*/
			close_previous: false /* est ce kon ferme la fenetre precedente ??*/
		},
		{  /*ici je recupere la fenetre et l'input qui est li�*/
			window:win,
			input:field_name,
		});
		return false;  
	}
	
	</script> 
<div id="page-title"><h2>GESTION LABORATOIRE</h2>
    <h5 class="font-blue-alt">Pole Recherche Innovation et Entrepreneuriat</h5></div>

<script>
    $(function () {
        $(".notif").show("slow").delay(5000).hide("slow");
    });
</script>
<c:if test="${val == 1}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Ajout du theme Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 3}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
            Suppression Effectu� avec succes!!!
        </strong>
    </div>
</c:if>
<c:if test="${val == 2}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
           Modification du theme Effectu�e avec success
        </strong>
    </div>
</c:if>
<c:if test="${val == 4}">
    <div class="alert alert-success notif">
        <strong style="margin-left:300px" ><i class="fa fa-thumbs-up fa-3x"></i>
           Changement du statut Effectu�e avec success
        </strong>
    </div>
</c:if>
<div class="panel">
    <div class="panel-heading" style="background-color:#BCBCBC;">          
        <CENTER>
            <a style="color:#b72724; text-decoration: none;" href="liste-laboratoire">
                <i  class="glyph-icon icon-reply-all"></i><span class="font-size-20">retour</span>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#" data-backdrop="false" data-toggle="modal" data-target=".addtheme" style="border-radius: 0px;" class="btn btn-success btn-sm">
                <i class="glyph-icon icon-plus"></i><strong>&nbsp;&nbsp;Ajouter Theme</strong>
            </a>
        </center>
    </div>
    <div class="panel-body">
        <h3 class="title-hero">Liste des Thematiques du <span class="font-blue-alt">${laboratoire.libelle}</span> </h3>

        <div class="example-box-wrapper">
            <table id="datatable-responsive"
                   class="table table-striped table-bordered responsive no-wrap" cellspacing="0"
                   width="100%">
                <thead>
                    <tr>
                        <th>TITRE</th>
                        <th>STATUT</th>
                        <th>DERNIERE MODIFICATION</th>
                        <th style="background-color:#3B5998; color:white">ACTIONS</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="liste" items="${alltheme}">
                        <tr>
                            <td>${liste.titre}</td>
                            <td style="color:#b72724;">${liste.statut}</td>
                            <td><fmt:formatDate type="both" dateStyle="long" timeStyle="long" value="${liste.datecreate}" /></td>
                            <td>
                                <a href="#" data-backdrop="false" data-toggle="modal" data-target=".${liste.idTheme+'9'}"  class="tooltip-button font-normal btn-sm btn-primary" data-placement="bottom" title="Supprimer"><i  class="glyph-icon icon-trash"></i></a>&nbsp;Supprimer<br/>
                                <a href="#" data-backdrop="false" data-toggle="modal" data-target=".${liste.idTheme}" class="tooltip-button font-normal btn-sm btn-warning" data-placement="bottom" title="Editer"><i  class="glyph-icon icon-edit"></i></a>&nbsp;Editer<br/>
                                <a href="gerer-thematique?s=${liste.idTheme}"  class="tooltip-button font-normal btn-sm btn-blue-alt" data-placement="bottom" title="Changer Statut"><i  class="glyph-icon icon-refresh"></i></a>&nbsp;Changer Statut

                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript" src="../admin/assets/widgets/parsley/parsley.js"></script>


<div class="modal fade addtheme" tabindex="-1" role="dialog" 
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <h4 class="modal-title">AJOUTER UN NOUVEAU THEME</h4></div>
            <form method="post" action="gerer-thematique" enctype="multipart/form-data" data-parsley-validate="">
                <div class="modal-body">
                    <div class="row" style="border:0px solid red">
                        <div class="col-lg-12" style="border:0px solid black">
                            <div class="panel panel-default" style="border:0px solid red">

                                <div class="panel-body" style="border:0px solid blue; padding: 0px;">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label font-black">TITRE : </label>

                                                <div class="col-sm-6">
                                                    <input type="hidden" value="${laboratoire.idLabo}" name="labo"/>
                                                    <input type="text" name="titre" class="form-control" />
                                                </div>
                                            </div><br/><br/><br/>
                                            <div class="form-group"><label class="col-sm-3 control-label font-black">CONTENU : </label>

                                                <div class="col-sm-6">
                                                    <textarea rows="10" cols="80" id="theme" name="contenu" class="form-control textarea-autosize">
                                              
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Fermer
                    </button>
                    <button name="action" value="Ajouter"  type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>AJOUTER</span><i
                            class="glyph-icon icon-linecons-paper-plane"></i></button>
                </div>      
            </form>

        </div>
    </div>
</div>

<c:forEach var="liste" items="${alltheme}">
    <div class="modal fade ${liste.idTheme}" tabindex="-1" role="dialog" 
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title">MODIFICATION DU THEME </h4></div>
                <form method="post" action="gerer-thematique" enctype="multipart/form-data" data-parsley-validate="">
                    <div class="modal-body">
                        <div class="row" style="border:0px solid red">
                            <div class="col-lg-12" style="border:0px solid black">
                                <div class="panel panel-default" style="border:0px solid red">

                                    <div class="panel-body" style="border:0px solid blue; padding: 0px;">

                                        <div class="row">
                                            <input type="hidden" value="${liste.idTheme}" name="theme"/>
                                            <input type="hidden" value="${laboratoire.idLabo}" name="labo"/>
                                            <div class="col-md-12">
                                                <div class="form-group"><label class="col-sm-3 control-label font-black">TITRE : </label>

                                                    <div class="col-sm-6">
                                                        <input type="text" value="${liste.titre}" name="titre" class="form-control" />
                                                    </div>
                                                </div><br/><br/><br/>
                                                <div class="form-group"><label class="col-sm-3 control-label font-black">CONTENU : </label>

                                                    <div class="col-sm-6">
                                                        <textarea rows="10" cols="80" id="theme2" name="contenu" class="form-control textarea-autosize">
                                                            ${liste.contenu}
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Fermer
                        </button>
                        <button name="action" value='Modifier'  type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>Modifier</span><i
                                class="glyph-icon icon-linecons-paper-plane"></i></button>
                    </div>      
                </form>

            </div>
        </div>
    </div>
</c:forEach>

<c:forEach var="liste" items="${alltheme}">
    <div class="modal fade ${liste.idTheme+'9'}" tabindex="-1" role="dialog" 
         aria-labelledby="myLargeModalLabel" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Voulez-vous Vraiment supprimer le Theme : ${liste.titre}</h4>
                </div>
                <form action="gerer-thematique" method="post">
                    <div class="modal-body">

                        <input type="hidden" name="theme" value="${liste.idTheme}" class="form-control"/> 
                        <input type="hidden" value="${laboratoire.idLabo}" name="labo"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Annuler
                        </button>
                        <button name="action" value="Supprimer" type="submit" class="btn btn-lg btn-hover btn-blue-alt font-normal"><span>SUPPRIMER</span><i
                                class="glyph-icon icon-linecons-paper-plane"></i></button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</c:forEach>

<script type="text/javascript">�
		//po ur�ti nyM C E!!�
		// O2k7 skin (silver)
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "theme2",
		theme : "advanced",
		skin : "o2k7",
		skin_variant : "black",
		relative_urls:false,
		
		file_browser_callback:"fileBrowser",
		
		language:"en",
		plugins : "lists,pagebreak,style,layer,advhr,advimage,advlink,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "newdocument,cut,code,|,bold,italic,underline,strikethrough,|,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,help,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,bullist,numlist,pasteword",
		theme_advanced_buttons3 : "print,|,fullscreen,|,moveforward,movebackward,|,styleprops,|,cite,abbr,acronym,del,ins,|,hr,removeformat,visualaid,|,charmap,search",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
	/* fiel_name=nom du champ
	 * url= le lien
	 * type= le type
	 * win= la fenetre
	 */
	function fileBrowser(field_name, url, type, win)
	{
		tinyMCE.activeEditor.windowManager.open({
			file:"galerie-laboratoire",
			title:"GALLERIE LABORATOIRE",			/*le titre*/
			width:1100,					/*la largeur de la fenetre*/
			height:800,					/*la hauteur de la fenetre*/
			resizable:true,				/* redimentionnable ??*/
			inline:true,				/* faire fonctionner le fenetre en popups ??*/
			close_previous: false /* est ce kon ferme la fenetre precedente ??*/
		},
		{  /*ici je recupere la fenetre et l'input qui est li�*/
			window:win,
			input:field_name,
		});
		return false;  
	}
	
	</script> 



